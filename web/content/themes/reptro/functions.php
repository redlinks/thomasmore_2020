<?php
/**
 * reptro functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package reptro
 */

if ( ! function_exists( 'reptro_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function reptro_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on reptro, use a find and replace
	 * to change 'reptro' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'reptro', get_template_directory() . '/languages' );


	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', reptro_main_fonts_url() ) );



	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Adding Images size.
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_image_size/
	 */

	add_image_size( 'reptro-blog-thumb', cs_get_option( 'feature_image_width', 1200 ), cs_get_option( 'feature_image_height', 560 ), true );
	add_image_size( 'reptro-blog-small', 480, 519, true );
	add_image_size( 'reptro-client-img', 400, 400, true );
	add_image_size( 'reptro-team-thumb', 140, 140, true );
	add_image_size( 'reptro-course-thumb', 570, 461, true );
	



	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' 	=> 	esc_html__( 'Primary', 'reptro' ),
		'footer'	=>	esc_html__( 'Footer', 'reptro' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );


	/*
	 * Enable support for Custom Logo.
	 * See https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 58,
		'width'       => 160,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );


	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Add WooCommerce Support
	add_theme_support( 'woocommerce' );

	// Gutenberg Support
	add_theme_support( 'align-wide' );
	add_theme_support( 'editor-styles' );
	add_theme_support( 'wp-block-styles' );

	// Gutenberg editor color

	$xt_primary_color       		= cs_get_option( 'xt_primary_color' );
	$xt_primary_color_dark       	= cs_get_option( 'xt_primary_color_dark' );
	$xt_primary_color_light       	= cs_get_option( 'xt_primary_color_light' );

	add_theme_support( 'editor-color-palette', array(
        array(
            'name' 	=> esc_html__( 'Theme Primary Color', 'reptro' ),
            'slug' 	=> 'theme-primary-color',
            'color' => $xt_primary_color,
        ),
        array(
            'name' 	=> esc_html__( 'Theme Primary Color Dark', 'reptro' ),
            'slug' 	=> 'theme-primary-color-dark',
            'color' => $xt_primary_color_dark,
        ),
        array(
            'name' 	=> esc_html__( 'Theme Primary Color Light', 'reptro' ),
            'slug' 	=> 'theme-primary-color-light',
            'color' => $xt_primary_color_light,
        ),
        array(
            'name' 	=> esc_html__( 'White', 'reptro' ),
            'slug' 	=> 'theme-white',
            'color' => '#ffffff',
        ),
    ));

    // Gutenberg editor font size

    add_theme_support( 'editor-font-sizes', array(
	    array(
	        'name' 		=> esc_html__( 'Small', 'reptro' ),
	        'shortName' => esc_html__( 'S', 'reptro' ),
	        'size' 		=> 12,
	        'slug' 		=> 'small'
	    ),
	    array(
	        'name' 		=> esc_html__( 'Regular', 'reptro' ),
	        'shortName' => esc_html__( 'M', 'reptro' ),
	        'size' 		=> 16,
	        'slug' 		=> 'regular'
	    ),
	    array(
	        'name' 		=> esc_html__( 'Large', 'reptro' ),
	        'shortName' => esc_html__( 'L', 'reptro' ),
	        'size' 		=> 36,
	        'slug' 		=> 'large'
	    ),
	    array(
	        'name' 		=> esc_html__( 'Larger', 'reptro' ),
	        'shortName' => esc_html__( 'XL', 'reptro' ),
	        'size' 		=> 50,
	        'slug' 		=> 'larger'
	    )
	));
}
endif;
add_action( 'after_setup_theme', 'reptro_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function reptro_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'reptro_content_width', 640 );
}
add_action( 'after_setup_theme', 'reptro_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function reptro_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Blog Sidebar', 'reptro' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here. It will be shown to the blog pages.', 'reptro' ),
		'before_widget' => '<section id="%1$s" class="widget sidebar %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Course Sidebar', 'reptro' ),
		'id'            => 'course',
		'description'   => esc_html__( 'Add widgets here. It will be shown to the course page.', 'reptro' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Single Course Sidebar', 'reptro' ),
		'id'            => 'single_course',
		'description'   => esc_html__( 'Add widgets here. It will be shown to the single course page.', 'reptro' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Events Sidebar', 'reptro' ),
		'id'            => 'events_sidebar',
		'description'   => esc_html__( 'Add widgets here. It will be shown to the event pages.', 'reptro' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets', 'reptro' ),
		'id'            => 'footer-widgets',
		'description'   => esc_html__( 'Add widgets here. It will be shown to the footer area.', 'reptro' ),
		'before_widget' => '<div id="%1$s" class="col-lg-'. esc_attr( reptro_cs_get_option( 'footer_widget_column', 3 ) ) .' col-md-6 widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title footer-widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Off Canvas Sidebar', 'reptro' ),
		'id'            => 'off-canvas-sidebar',
		'description'   => esc_html__( 'Add widgets here. It will be shown to off-canvas sidebar.', 'reptro' ),
		'before_widget' => '<section id="%1$s" class="widget sidebar %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'reptro_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function reptro_scripts() {
	wp_enqueue_style( 'reptro-style', get_stylesheet_uri() );

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '3.3.7' );

	wp_enqueue_style( 'reptro-bootstrap-grid', get_template_directory_uri() . '/assets/css/bootstrap-grid.min.css', array(), '4.0' );	

	if( is_rtl() ){
		wp_enqueue_style( 'bootstrap-rtl', get_template_directory_uri() . '/assets/css/bootstrap-rtl.css', array(), '3.3.7' );
	}

	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.css', array(), '4.7.0' );

	wp_enqueue_style( 'simple-line-icons', get_template_directory_uri() . '/assets/fonts/education-icons/simple-line-icons/css/simple-line-icons.css', array(), '1.0' );

	wp_register_style( 'fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css', array(), '3.0.47' );

	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '2.2.1' );

	if ( defined( 'LEARNDASH_LMS_PLUGIN_DIR' ) ) {
		wp_enqueue_style( 'reptro-learndash-style', get_template_directory_uri() . '/assets/css/learndash-style.css', array(), '1.0' );
	}

	wp_enqueue_style( 'reptro-main-style', get_template_directory_uri() . '/assets/css/main.css', array(), '1.0' );

	wp_enqueue_style( 'reptro-responsive-style', get_template_directory_uri() . '/assets/css/responsive.css', array(), '1.0' );


	$need_color_customizer = cs_get_option( 'need_color_customizer' );

	if( $need_color_customizer != true ){
		wp_enqueue_style( 'reptro-color-style', get_template_directory_uri() . '/assets/css/color.css', array(), '1.0' );
	}

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '3.3.7', true );
	
	wp_enqueue_script( 'slicknav', get_template_directory_uri() . '/assets/js/jquery.slicknav.min.js', array('jquery'), '1.0.4', true );

	wp_register_script( 'bars', get_template_directory_uri() . '/assets/js/bars.js', array('jquery'), '', true );

	wp_register_script( 'fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js', array('jquery'), '3.0.47', true );

	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '2.2.1', true );
	
	$xt_off_canvas_sidebar = cs_get_option( 'xt_off_canvas_sidebar' );

	if($xt_off_canvas_sidebar == true){
		wp_enqueue_script( 'js-offcanvas', get_template_directory_uri() . '/assets/js/js-offcanvas.pkgd.min.js', array('jquery'), '1.0', true );
	}

	wp_register_script( 'isotope', get_template_directory_uri() . '/assets/js/isotope.pkgd.js', array('jquery'), '3.0.1', true );

	wp_enqueue_script( 'reptro-init-script', get_template_directory_uri() . '/assets/js/init.js', array('jquery'), '1.0', true );

	wp_enqueue_script( 'reptro-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'reptro_scripts', 11 );


/**
 * Block editor styles
 */

function reptro_block_editor_styles() {
	wp_enqueue_style( 'reptro-color-style', get_template_directory_uri() . '/assets/css/color.css', array(), '1.0' );
    wp_enqueue_style( 'charity-main-fonts', reptro_main_fonts_url(), array(), null );
    wp_enqueue_style( 'reptro-block-editor-style', get_template_directory_uri() . '/assets/css/editor-blocks.css', array(), '20181230' );
}
add_action( 'enqueue_block_editor_assets', 'reptro_block_editor_styles' );


/**
 * Admin Scripts
 */

function reptro_admin_scripts() {
	wp_enqueue_style( 'admin-style', get_template_directory_uri() . '/assets/css/admin-style.css', array(), '1.0' );
}
add_action( 'admin_enqueue_scripts', 'reptro_admin_scripts' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Bootstrap Nav Walker Class
 */
require get_template_directory() . '/inc/wp-bootstrap-navwalker.php';

/**
 * TGM Plugin Installer
 */
if ( ! class_exists( 'TGM_Plugin_Activation' ) ) {
	require get_template_directory() . '/admin/class-tgm-plugin-activation.php';
}


/**
 * XooTheme Core Functions
 */
require get_template_directory() . '/inc/xt-core-functions.php';


/**
 * Theme Custom Functions
 */
require get_template_directory() . '/inc/theme-functions.php';


/**
 * Learnpress Functions
 */

require get_template_directory() . '/inc/learnpress-functions.php';

/**
 * LearnDash Functions
 */

if ( defined( 'LEARNDASH_LMS_PLUGIN_DIR' ) ) {
	require get_template_directory() . '/inc/learndash-functions.php';
}


/**
 * WooCommerce Scripts
 */

if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce-functions.php';
}