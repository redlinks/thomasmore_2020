<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reptro
 */

$single_course_layout = cs_get_option('single_course_layout');

if ( ! is_active_sidebar( 'single_course' ) ) {
	return;
}
?>

<?php if( $single_course_layout != 'full_width' ):?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( apply_filters( 'reptro_widget_area_class', 'col-lg-4' ) ); ?>">
		<?php dynamic_sidebar( 'single_course' ); ?>
	</aside><!-- #secondary -->
<?php endif;?>