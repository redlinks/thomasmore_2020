<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reptro
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
	if( is_page() ){
		$xt_disable_header_footer = reptro_get_post_meta( 'business_x_page_side_options', 'xt_disable_header_footer', false, true );
	}else{
		$xt_disable_header_footer = false;
	}
?>

<div id="page" class="site">

	<?php if( $xt_disable_header_footer == false ): ?>

		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'reptro' ); ?></a>

		<?php do_action( 'reptro_before_header' ); ?>

		<?php do_action( 'reptro_theme_main_header' ); ?>

		<?php do_action( 'reptro_after_header' ); ?>

	<?php endif; ?>

	<div id="content" class="site-content">
		<div class="site-content-inner <?php echo esc_attr( apply_filters( 'reptro_container_class', 'container' ) ); ?>">
			<?php do_action( 'reptro_before_content' ); ?>