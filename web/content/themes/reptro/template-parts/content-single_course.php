<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package reptro
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('xt-single-course-item'); ?>>
	<?php
		if( is_single() ){
			the_content();
		}else{
			the_excerpt();
		}
	?>
</article><!-- #post-## -->