<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package reptro
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
			the_content();

			if( function_exists('xt_bootstrap_link_pages') ){
				xt_bootstrap_link_pages( array(
					'before' => '<nav class="xt_theme_paignation xt-theme-page-links">' . esc_html__( 'Pages:', 'reptro' ) . '<ul class="pager">',
					'after'  => '</ul></nav>',
				) );
			}else{
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'reptro' ),
					'after'  => '</div>',
				) );
			}
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer<?php echo esc_attr( do_action('reptro_page_footer_wrapper_class') );?>">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'reptro' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
