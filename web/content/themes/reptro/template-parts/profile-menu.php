<?php
/**
 * Template for displaying user profile menu.
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();
$profile = LP_Profile::instance();
$user 	 = $profile->get_user();
?>

<ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php echo wp_kses_post( $user->get_profile_picture() ); ?></a>
		<ul class="dropdown-menu reptro-learnpress-profile-tabs">
			<?php
			foreach ( $profile->get_tabs()->tabs() as $tab_key => $tab_data ) {

				if ( $tab_data->is_hidden() || ! $tab_data->user_can_view() ) {
					continue;
				}

				$slug        = $profile->get_slug( $tab_data, $tab_key );
				$link        = $profile->get_tab_link( $tab_key, true );
				$tab_classes = array( esc_attr( $tab_key ) );

				if ( $profile->is_current_tab( $tab_key ) ) {
					$tab_classes[] = 'active';
				}

				if ( ( $sections = $tab_data->sections() ) && sizeof( $sections ) > 1 ) {
					$tab_classes[] = 'dropdown';
				}

				?>

		        <li class="<?php echo join( ' ', $tab_classes ) ?>">
		            <!--tabs-->
		            <a href="<?php echo esc_url( $link ); ?>" data-slug="<?php echo esc_attr( $link ); ?>">
						<?php echo apply_filters( 'learn_press_profile_' . $tab_key . '_tab_title', esc_html( $tab_data['title'] ), $tab_key ); ?>
		            </a>
		            <!--section-->

					<?php if ( ( $sections = $tab_data->sections() ) && sizeof( $sections ) > 1 ) { ?>

		                <ul class="dropdown-menu">
							<?php foreach ( $sections as $section_key => $section_data ) {

								$classes = array( esc_attr( $section_key ) );
								if ( $profile->is_current_section( $section_key, $section_key ) ) {
									$classes[] = 'active';
								}

								$section_slug = $profile->get_slug( $section_data, $section_key );
								$section_link = $profile->get_tab_link( $tab_key, $section_slug );
								?>

		                        <li class="<?php echo join( ' ', $classes ); ?>">
		                            <a href="<?php echo esc_url( $section_link ); ?>"><?php echo esc_html( $section_data['title'] ); ?></a>
		                        </li>

							<?php } ?>

		                </ul>

					<?php } ?>

		        </li>
			<?php } ?>
			<?php if( is_user_logged_in() ): ?>
				<li><a href="<?php echo esc_url( wp_logout_url() ) ?>"><?php esc_html_e( 'Logout', 'reptro' ) ?></a></li>
			<?php endif; ?>
		</ul>

	</li>

</ul>