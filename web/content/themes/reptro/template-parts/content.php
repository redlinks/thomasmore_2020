<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package reptro
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( array( 'xt-blog' ) ); ?>>

    <div class="each-blog xt-main-blog">
        <div class="blog-inner">
        	<?php if( has_post_thumbnail() ) : ?>
        		<?php if ( is_single() ) : ?>
	                <div class="blog-img">
	                	<?php the_post_thumbnail( 'reptro-blog-thumb' ); ?>
	                	<?php reptro_post_date(); ?>
	                </div>
	            <?php else : ?>
	            	<div class="blog-img">
	            		<a href="<?php the_permalink(); ?>">
		                	<?php the_post_thumbnail( 'reptro-blog-thumb' ); ?>
		                </a>
		                <?php reptro_post_date(); ?>
		                <?php if(is_sticky()): ?>
		                	<span class="xt-blog-sticky-post"><i class="sli-star"></i></span>
	                	<?php endif; ?> 
	                </div>
	            <?php endif; ?> 
       		<?php endif; ?>
            <div class="inner-content shadow">
            	<header class="entry-header">
            		<?php if ( 'post' == get_post_type() && has_category() ) : ?>
            			<div class="xt-post-cat"><?php the_category( ' ' ); ?></div>
            		<?php endif; ?>

					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

					endif;?>

					<?php if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php reptro_post_meta(); ?>
						</div>
					<?php endif; ?>

					<?php if(is_sticky() && !has_post_thumbnail()): ?>
	                	<span class="xt-blog-sticky-post"><i class="sli-star"></i></span>
                	<?php endif; ?> 

				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php
						if( is_single() ){
							the_content( sprintf(
								/* translators: %s: Name of current post. */
								wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'reptro' ), array( 'span' => array( 'class' => array() ) ) ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							) );
						}else{
							the_excerpt(); 

						}

						if( function_exists('xt_bootstrap_link_pages') ){
							xt_bootstrap_link_pages( array(
								'before' => '<nav class="xt_theme_paignation xt-theme-page-links">' . esc_html__( 'Pages:', 'reptro' ) . '<ul class="pager">',
								'after'  => '</ul></nav>',
							) );
						}else{
							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'reptro' ),
								'after'  => '</div>',
							) );
						}
					?>
				</div><!-- .entry-content -->

            </div>
        </div>
    </div>

</article><!-- #post-## -->








