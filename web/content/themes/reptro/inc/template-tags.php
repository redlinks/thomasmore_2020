<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package reptro
 */

if ( ! function_exists( 'reptro_post_meta' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function reptro_post_meta() {


	$byline = sprintf(
		esc_html_x( 'By %s', 'post author', 'reptro' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="byline"><i class="sli-user"></i> ' . $byline . '</span>'; // WPCS: XSS OK.


	/* translators: used between list items, there is a space after the comma */
	$tags_list = get_the_tag_list( '', esc_html__( ', ', 'reptro' ) );
	if ( $tags_list ) {
		printf( '<span class="tags-links"><i class="sli-tag"></i> %1$s</span>', $tags_list ); // WPCS: XSS OK.
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link"><i class="sli-bubble"></i> ';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'reptro' ), array( 'span' => array( 'class' => array() ), 'i' => array( 'class' => array(), 'aria-hidden' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

}
endif;

if ( ! function_exists( 'reptro_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function reptro_entry_footer() {

	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'reptro' ) );
		if ( $categories_list && reptro_categorized_blog() ) {
			printf( '<span class="cat-links"><i class="fa fa-briefcase" aria-hidden="true"></i> %1$s</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'reptro' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links"><i class="fa fa-tags" aria-hidden="true"></i> %1$s</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( '<i class="fa fa-comments" aria-hidden="true"></i> Leave a Comment<span class="screen-reader-text"> on %s</span>', 'reptro' ), array( 'span' => array( 'class' => array() ), 'i' => array( 'class' => array(), 'aria-hidden' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'reptro' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link"><i class="fa fa-pencil-square" aria-hidden="true"></i> ',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function reptro_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'reptro_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'reptro_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so reptro_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so reptro_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in reptro_categorized_blog.
 */
function reptro_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'reptro_categories' );
}
add_action( 'edit_category', 'reptro_category_transient_flusher' );
add_action( 'save_post',     'reptro_category_transient_flusher' );


/**
 * Post Date
 */

function reptro_post_date(){
	if( get_post_type() == 'post' ){
		printf('<div class="xt-post-date"><span class="xt-post-day-month">%s</span><span class="xt-post-author"><a href="%s"><i class="sli-user"></i>%s</a></span></div>', esc_html( get_the_date( get_option( 'date_format' ) ) ), esc_url( get_author_posts_url(get_the_author_meta( 'ID' )) ), esc_html( get_the_author_meta( 'display_name' ) ) );
	}
}


