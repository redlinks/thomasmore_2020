<?php

/**
 * reptro
 * Author : XooThemes
 */

defined( 'CS_OPTION' )     or  define( 'CS_OPTION',     '_cs_options' );
defined( 'CS_CUSTOMIZE' )  or  define( 'CS_CUSTOMIZE',  '_cs_customize_options' );

/**
 *
 * Get option
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'cs_get_option' ) ) {
  function cs_get_option( $option_name = '', $default = '' ) {

    $options = apply_filters( 'cs_get_option', get_option( CS_OPTION ), $option_name, $default );

    if( ! empty( $option_name ) && ! empty( $options[$option_name] ) ) {
      return $options[$option_name];
    } else {
      return ( ! empty( $default ) ) ? $default : null;
    }

  }
}

/**
 *
 * Get customize option
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( ! function_exists( 'cs_get_customize_option' ) ) {
  function cs_get_customize_option( $option_name = '', $default = '' ) {

    $options = apply_filters( 'cs_get_customize_option', get_option( CS_CUSTOMIZE ), $option_name, $default );

    if( ! empty( $option_name ) && ! empty( $options[$option_name] ) ) {
      return $options[$option_name];
    } else {
      return ( ! empty( $default ) ) ? $default : null;
    }

  }
}


/**
 * Adding Custom Styles
 */

if(!function_exists('reptro_add_inline_styles')){
	function reptro_add_inline_styles() {

	    $footer_top_space 			= cs_get_option( 'footer_top_space', 75 );
	    $footer_bottom_space 		= cs_get_option( 'footer_bottom_space', 45 );
	    $footer_background_color 	= cs_get_option( 'footer_background_color', '#26282c' );
	    $footer_title_color 		= cs_get_option( 'footer_title_color', '#ffffff' );
	    $footer_content_color 		= cs_get_option( 'footer_content_color', '#999999' );
	    $footer_link_color 			= cs_get_option( 'footer_link_color', '#ffffff' );

		$bottom_bar_background_color 	= cs_get_option( 'bottom_bar_background_color', '#202026' );		
		$bottom_bar_text_color 			= cs_get_option( 'bottom_bar_text_color', '#999' );		
		$bottom_bar_link_color 			= cs_get_option( 'bottom_bar_link_color', '#fff' );


		$xt_page_header_bg = cs_get_option( 'xt_page_header_bg' );
		if( $xt_page_header_bg ){
	    	$xt_page_header_bg = wp_get_attachment_image_src( $xt_page_header_bg, 'full' );
	    	$xt_page_header_bg = $xt_page_header_bg[0];
	    }
	    if( is_page() && has_post_thumbnail() ){
			$xt_page_header_bg = get_the_post_thumbnail_url( get_the_id(), 'full' );
	    }

		$xt_page_header_bg_color 	= cs_get_option( 'xt_page_header_bg_color', 'rgba(38, 40, 44, 0.8)' );
	    $xt_page_header_p_top 		= cs_get_option( 'xt_page_header_p_top', 45 );
	    $xt_page_header_p_bottom 	= cs_get_option( 'xt_page_header_p_bottom', 45 );

	    $body_color 		= reptro_cs_get_option( 'xt_body_color', '#555' );
	    $title_color 		= reptro_cs_get_option( 'xt_title_color', '#303d48' );
	    $body_bg 			= reptro_cs_get_option( 'xt_body_bg', '#ffffff' );
	    $body_bg_boxed 		= reptro_cs_get_option( 'xt_body_bg_boxed', '#efefef' );
	    $body_font_size 	= reptro_cs_get_option( 'body_font_size', 16 );
	    $body_line_height 	= reptro_cs_get_option( 'body_line_height', 26 );

	    /**
	     * Customizer Style
	     */

	    $header_bg 					= reptro_cs_get_customize_option( 'header_bg' );
	    $header_color 				= reptro_cs_get_customize_option( 'header_color' );
	    $nav_pre_header_bg 			= reptro_cs_get_customize_option( 'nav_pre_header_bg' );
	    $logo_max_width 	    	= reptro_cs_get_customize_option( 'logo_max_width', 120 );
	    $logo_max_height 	    	= reptro_cs_get_customize_option( 'logo_max_height' );

	    $menu_left_right_space 	    = reptro_cs_get_customize_option( 'menu_left_right_space', 25 );
	    $menu_top_space 	    	= reptro_cs_get_customize_option( 'menu_top_space', 30 );
	    $menu_bottom_space 	    	= reptro_cs_get_customize_option( 'menu_bottom_space', 30 );

	    $dropdown_menu_width 	    = reptro_cs_get_customize_option( 'dropdown_menu_width', 290 );

	    $main_slider_overlay 		= reptro_cs_get_customize_option( 'main_slider_overlay', 'rgba(0,0,0,0.4)' );
	    $main_slider_color 			= reptro_cs_get_customize_option( 'main_slider_color', '#fff' );
	    $main_slider_min_height 	= reptro_cs_get_customize_option( 'main_slider_min_height', 600 );
	    $main_slider_padding_top 	= reptro_cs_get_customize_option( 'main_slider_padding_top', 80 );
	    $main_slider_padding_bottom = reptro_cs_get_customize_option( 'main_slider_padding_bottom', 300 );

	    $custom_css = '';

	    if( $nav_pre_header_bg ){
	    	$custom_css .= "
	    		.navbar.color-bg.xt-header-one,
	    		.xt-head-2 .xt-header-top {
		    		background-color: {$nav_pre_header_bg};
		    	}
	    	";
	    }

	    if( $header_bg ){
	    	$custom_css .= "
	    		.xt-header.site-header {
		    		background-color: {$header_bg};
		    	}
	    	";
	    }

	    if( $header_color ){
	    	$custom_css .= "
	    		.xt-header-top,
		    	.xt-header-top a {
		    		color: {$header_color};
		    	}
		    	.header-contact ul li,
		    	.header-contact ul li a {
		    		border-color: {$header_color};
		    	}
	    	";
	    }


	    if( !is_rtl() ){
	    	$custom_css .= "
	    		.navbar-nav li a {
					padding: {$menu_top_space}px 0 {$menu_bottom_space}px {$menu_left_right_space}px;
				}
				.navbar-nav > li > a:before {
					left: {$menu_left_right_space}px;
				}
		    ";
	    }else{
	    	$custom_css .= "
	    		.navbar-nav li a {
					padding: {$menu_top_space}px {$menu_left_right_space}px {$menu_bottom_space}px 0;
				}
				.navbar-nav > li > a:before {
					right: {$menu_left_right_space}px;
				}
		    ";
	    }

	    $custom_css .= "
			.dropdown-menu {
			  min-width: {$dropdown_menu_width}px;
			}
	    	.xt-business-home.item:before {
	    		background-color: {$main_slider_overlay};
	    	}
	    	.xt-business-home.item, .xt-business-home.item .btn {
	    		color: {$main_slider_color};
	    	}
	    	.xt-business-home.cover-bg.fix-bg.item {
	    		height: {$main_slider_min_height}px;
	    	}
	    	.xt-business-home .bx-overlay {
	    		padding-top: {$main_slider_padding_top}px;
			    padding-bottom: {$main_slider_padding_bottom}px;
	    	}
	    	.footer-widgets {
				padding-top: {$footer_top_space}px;
			    padding-bottom: {$footer_bottom_space}px;
			    background: {$footer_background_color};
			    color: {$footer_content_color};
			}
			.footer-widgets .widget-title {
				color: {$footer_title_color};
			}
			.footer-widgets .widget ul li a,
			.footer-widgets a,
			.widget-contact-info ul li span.heading {
				color: {$footer_link_color};
			}
	    	.site-info {
			    background-color: {$bottom_bar_background_color};
			    color: {$bottom_bar_text_color};
			}
			.xt-footer .footer-nav ul li a {
				color: {$bottom_bar_link_color};
			}
			.xt-page-title-area:before {
				background: {$xt_page_header_bg_color};
			}
			.xt-page-title-area {
				padding-top: {$xt_page_header_p_top}px;
				padding-bottom: {$xt_page_header_p_bottom}px;
			}
			body {
				color: {$body_color};
				background: {$body_bg};
				line-height: {$body_line_height}px;
				font-size: {$body_font_size}px;
			}
			h1, h2, h3, h4, h5, h6,
			body .elementor-widget-heading .elementor-heading-title {
				color: {$title_color};
			}
			body.xt-site-layout-boxed {
				background: {$body_bg_boxed};
			}
		    ";

		    if( $xt_page_header_bg ){
			$custom_css .= "
				.xt-page-title-area {
					background-image: url( $xt_page_header_bg );
				}
			";
		}

		if( $logo_max_width  ){
			$custom_css .= "
				.site-header .logo-wrapper img {
				  max-width: {$logo_max_width}px;
				}
			";
		}

		if( $logo_max_height  ){
			$custom_css .= "
				.site-header .logo-wrapper img {
				  max-height: {$logo_max_height}px;
				}
			";
		}

		$need_color_customizer 		= cs_get_option( 'need_color_customizer' );
		$xt_primary_color 			= cs_get_option( 'xt_primary_color' );
		$xt_primary_color_dark 		= cs_get_option( 'xt_primary_color_dark' );
		$xt_primary_color_light 	= cs_get_option( 'xt_primary_color_light' );

		if( $need_color_customizer == true ){
			/* default */
			$custom_css .= ".inner-content .entry-content .expand_collapse a:hover,.inner-content .entry-content #learndash_profile .inner-content .entry-content .expand_collapse a:hover,.inner-content .entry-content .expand_collapse a:focus,.inner-content .entry-content #learndash_profile .inner-content .entry-content .expand_collapse a:focus,.learndash .completed:after,#learndash_profile .completed:after,.entry-content .tml-links li a:hover,.entry-content .tml-links li a:focus,.c-offcanvas .js-offcanvas-close i,a,a:focus,a:hover,.header-slider .owl-nav>div:hover i,ul.reptro_fancy_lists li .fa,.color,.has-theme-primary-color-color,.mobile-menu-area .slicknav_nav .current-menu-item>a,.mobile-menu-area .slicknav_nav .current-menu-item>.slicknav_row a,.section-title h2 span,.header-contact ul li .fa,.header-contact ul li a:hover,.xt-service .xt-service-inner i,.xt-featured-list .featured-list-inner ul li .fa,.xt-our-service .each-inner .xt-service-inner-2 i,.xt-our-service .each-inner .xt-service-inner-2 .figure-caption a,.xt-skills .technical-skills .b_tooltip span,.inner-content .entry-content a,.entry-title a:hover,.xt-contact-inner i,.xt-footer .footer-nav ul li a:hover,.xt-footer .footer-nav ul li a.active,.xt-blog-category ul li a:hover,.xt-blog-category ul li a span,.xt-popular-post ul li .side-post-content a:hover,.xt-blog-tag ul li a:hover,.xt-blog .blog-full-post .tag-social .tag span,.xt-blog .blog-full-post .tag-social .social span,.xt-blog .blog-full-post .tag-social .social ul li a .fa:hover,.xt-comments .comment .comment-content a,.xt-footer-3 .footer-3-address ul li h6,.xt-footer-3 .footer-3-address ul li a:hover,.widget ul li a:hover,.widget a:hover,.xt-media-body .reply-link a,.entry-footer i,ul.xt-footer-menu li.current_page_item a,.xt-footer .footer-nav ul li.current_page_item a,.footer-widgets .widget ul li a:hover,.footer-widgets a:hover,.slicknav_btn,.xt_row_primary_bg_color_white .btn-border:hover,.xt_row_primary_bg_color_white .kc_button.btn-border:hover,.woocommerce-MyAccount-navigation ul li a,.woocommerce-MyAccount-navigation ul li.is-active a,.xt-course-info li i,.course-item-nav .prev a:hover,.course-item-nav .next a:hover,.course-item-nav .prev a:focus,.course-item-nav .next a:focus,.xt-header-one .reptro-header-btn .btn,.xt-service-two-item i,.reptro-event-details-side i,.xt-blog .entry-meta span i,.reptro-ctl-area .btn-border:hover,.reptro-ctl-area .btn-border:focus,.theme-main-slide-buttons .btn-border:hover,.theme-main-slide-buttons .btn-border:focus,.xt_row_primary_bg_color_white .reptro-ctl-area .btn-border:hover,.xt-service-two-item a.xt-service-two-btn,.xt_row_primary_bg_color_white .repto-button-fill.elementor-widget-button a.elementor-button,.xt_row_primary_bg_color_white .repto-button-fill.elementor-widget-button .elementor-button,.xt_row_primary_bg_color_white .repto-button-border.elementor-widget-button a.elementor-button:hover .elementor-button-text,.xt_row_primary_bg_color_white .repto-button-border.elementor-widget-button .elementor-button:hover .elementor-button-text,.xt_row_primary_bg_color_white .repto-button-border.elementor-widget-button a.elementor-button:focus .elementor-button-text,.xt_row_primary_bg_color_white .repto-button-border.elementor-widget-button .elementor-button:focus .elementor-button-text,.kc_accordion_wrapper.xt_accordion_theme_primary .kc_accordion_header>span.ui-accordion-header-icon,.reptro-call-to-action-btn,.reptro-call-to-action-btn:hover,.reptro-call-to-action-btn:focus,.widget .reptro-call-to-action-btn,.widget .reptro-call-to-action-btn:hover,.widget .reptro-call-to-action-btn:focus,.footer-widgets .reptro-call-to-action-btn,.footer-widgets .reptro-call-to-action-btn:hover,.footer-widgets .reptro-call-to-action-btn:focus,#tribe-events-content .tribe-events-tooltip h4,#tribe_events_filters_wrapper .tribe_events_slider_val,.single-tribe_events a.tribe-events-gcal,.single-tribe_events a.tribe-events-ical{ color: {$xt_primary_color} }";

			$custom_css .= ".widget_lp-widget-course-info .lp-widget .lp-course-info-fields .lp-course-info .lp-label,.navbar-nav>li>.dropdown-menu li a:before,.navbar-nav>li>a:before,.xt-service .xt-service-inner:hover,.btn-shuttr-hvr:before,.btn.btn-fill,.btn-border:hover,.btn.btn-border:hover::after,.btn.btn-border:focus::after,.btn-base,.color-bg,.main-color-bg,.xt-featured-member .item ul li a .fa,.xt-skills .technical-skills .bar_group__bar.thick,.xt-skills .technical-skills .bar_group .elastic,.xt-footer .footer-social ul li a,.xt-header-one.strict,.mean-container .mean-nav ul li a.mean-expand,.mean-container .mean-nav ul li a.mean-expand:hover,.xt-footer-3 .xt-footer-info-3 ul li a .fa,input[type=submit],.comment-respond .form-submit input[type=submit],.search-form input[type=submit],.reptro_paignation ul.pager li>span,.xt_theme_paignation ul.pager li>span,.pager li.active a,.kc_button.xt-theme-primary-btn:hover,.pager li a:hover,.reptro_paignation ul.pager li>a:hover,.xt-head-2 .xt-header-top,.xt-theme-primary-content-area,.single-team .member-desc span:after,.xt-header-type-boxed_background .slicknav_menu,.slicknav_menu .slicknav_icon-bar,.woocommerce span.onsale,.xt-contact-call-to-action,.woocommerce .widget_price_filter .ui-slider .ui-slider-range,.xt_row_primary_bg_color_white,.xt-back-to-top,.xt-object,body.woocommerce #respond input#submit:hover,body.woocommerce a.button:hover,body.woocommerce button.button:hover,body.woocommerce input.button:hover,body.woocommerce #respond input#submit:focus,body.woocommerce a.button:focus,body.woocommerce button.button:focus,body.woocommerce input.button:focus,.woocommerce nav.woocommerce-pagination ul li a:focus,.woocommerce nav.woocommerce-pagination ul li a:hover,.woocommerce nav.woocommerce-pagination ul li span.current,.woocommerce div.product div.images .woocommerce-product-gallery__trigger,.woocommerce #respond input#submit.alt:hover,.woocommerce a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce input.button.alt:hover,.woocommerce .cart .button:hover,.woocommerce .cart input.button:hover,.woocommerce-cart table.cart input:disabled[disabled]:hover,.woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover,.woocommerce #payment #place_order:hover,.woocommerce-page #payment #place_order:hover,.entry-content .woocommerce #respond input#submit:hover,.entry-content .woocommerce a.button:hover,.entry-content .woocommerce button.button:hover,.entry-content .woocommerce input.button:hover,.entry-content .woocommerce #respond input#submit:focus,.entry-content .woocommerce a.button:focus,.entry-content .woocommerce button.button:focus,.entry-content .woocommerce input.button:focus,.reptro-course-loop-sale,#learn-press-user-profile #learn-press-profile-nav .tabs>li.active>a,#learn-press-user-profile #learn-press-profile-nav .tabs>li.active>ul li.active>a,#learn-press-user-profile #learn-press-profile-nav .tabs>li a:hover,#learn-press-user-profile #learn-press-profile-nav .tabs>li:hover:not(.active)>a,#learn-press-user-profile button,.lp-course-buttons button,.learn-press-form-login button[type=submit],.learn-press-form-register button[type=submit],.lp-button.lp-button-guest-checkout,#course-item-content-header .form-button.lp-button-back button,form[name=search-course] .search-course-button,.lp-button.button,.lp-quiz-buttons button,.lp-course-buttons a.button,.lp-course-buttons #learn-press-pmpro-notice.purchase-course a,.pmpro_btn,.pmpro_btn:link,.pmpro_content_message a,.pmpro_content_message a:link,body.confirm #popup_container #popup_panel button:hover,body.confirm #popup_container #popup_panel input[type=button]:hover,body.confirm #popup_container #popup_panel input[type=submit]:hover,body.confirm #popup_container #popup_panel button:focus,body.confirm #popup_container #popup_panel input[type=button]:focus,body.confirm #popup_container #popup_panel input[type=submit]:focus,body .learn-press-pagination .page-numbers>li span,body .learn-press-pagination .page-numbers>li a:hover,.woocommerce nav.woocommerce-pagination ul li a:hover,.woocommerce nav.woocommerce-pagination ul li span,.xt-blog-post-slider.owl-theme .owl-dots .owl-dot.active span,.xt-blog-post-slider.owl-theme .owl-dots .owl-dot:hover span,.xt-blog-post-slider.owl-theme .owl-nav>div,.reptro-course-slider.owl-theme .owl-dots .owl-dot.active span,.reptro-course-slider.owl-theme .owl-dots .owl-dot:hover span,.reptro-course-slider.owl-theme .owl-nav>div,.reptro-course-category-slider.owl-theme .owl-dots .owl-dot.active span,.reptro-course-category-slider.owl-theme .owl-dots .owl-dot:hover span,.reptro-course-category-slider.owl-theme .owl-nav>div,body ul.learn-press-nav-tabs .course-nav.active:after,body ul.learn-press-nav-tabs .course-nav:hover:after,body .course-curriculum ul.curriculum-sections .section-content .course-item:before,body .learn-press-message:before,.xt-navbar.xt-head-3 .xt-header-top,#tribe-events .tribe-events-button,#tribe-events .tribe-events-button:hover,#tribe_events_filters_wrapper input[type=submit],.tribe-events-button,.tribe-events-button.tribe-active:hover,.tribe-events-button.tribe-inactive,.tribe-events-button:hover,.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-],.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a,#tribe-bar-form .tribe-bar-submit input[type=submit],.portfolio-nav ul li.current span,.portfolio-nav ul li span:hover,.portfolio-nav ul li span:focus,body .reptro-category-items li.active > span,body .reptro-category-items span:hover,body .reptro-category-items span:focus,.reptro-hero-area-play-btn,.reptro-cta-widget,.widget_nav_menu li:before,.widget_nav_menu li:after,.widget_archive li:before,.widget_archive li:after,.widget_categories li:before,.widget_categories li:after,.widget_pages li:before,.widget_pages li:after,.widget_meta li:before,.widget_meta li:after,.widget_recent_comments li:before,.widget_recent_comments li:after,.widget_recent_entries li:before,.widget_recent_entries li:after,.widget_rss li:before,.widget_rss li:after,.product-categories li:before,.product-categories li:after,.widget_recent_comments #recentcomments li:after,.widget_recent_entries li a:after,.widget_rss>ul>li:after,.woocommerce .woocommerce-widget-layered-nav-list li.woocommerce-widget-layered-nav-list__item:before,.woocommerce .woocommerce-widget-layered-nav-list li.woocommerce-widget-layered-nav-list__item:after,.xt-post-item .xt-post-item-categories,.xt-blog .xt-post-date>span.xt-post-day-month,.xt-blog .xt-post-cat a,.xt_column_primary_bg_color .elementor-widget-wrap,.reptro-ctl-area.reptro-ctl-default,.xt_row_primary_bg_color_white .reptro-ctl-area .btn-border:hover,.xt_row_primary_bg_color_white .reptro-ctl-area .btn-border:focus,.repto-button-border.elementor-widget-button a.elementor-button:hover,.repto-button-border.elementor-widget-button .elementor-button:hover,.repto-button-fill.elementor-widget-button a.elementor-button,.repto-button-fill.elementor-widget-button .elementor-button,.reptro-section-title.section-title-type-border:after,.section-title-small.section-title-small-border-yes:after,.xt-blog .entry-meta:after,.xt-blog .blog-inner .inner-content .post-info:after,.repto-button-border.elementor-widget-button a.elementor-button::after,.repto-button-border.elementor-widget-button .elementor-button::after,.xt-video.xt-video-popup-type-button_only .xt-video-popup-btn,.header-slider .owl-theme .owl-dots .owl-dot.active span,.header-slider .owl-theme .owl-dots .owl-dot:hover span,table.lp-list-table thead tr th,.widget-area .social-icons ul li a,.has-theme-primary-color-background-color,.btn-join,#btn-join,.learndash_checkout_buttons input.btn-join[type=button],#learndash_next_prev_link a,#learndash_back_to_lesson a,.wpProQuiz_button,button,input[type=button],input[type=reset],input[type=submit],dd.course_progress div.course_progress_blue{ background-color: {$xt_primary_color} }";

			$custom_css .=".btn-border.reptro_form:hover,.btn-border.reptro_form,.reptro_form.form-control:focus,.btn-border,.btn-fill,.form-control:focus,.title-hr,.reptro-service-item .title-hr,.repto-button-border.elementor-widget-button a.elementor-button,.repto-button-border.elementor-widget-button .elementor-button,.xt-blog-form .btn,.mean-container .mean-nav ul li,.widget select:focus,.widget_search input[type=search]:active,.widget_search input[type=search]:hover,.widget_search input[type=search]:focus,.comment-form input[type=text]:focus,.comment-form input[type=url]:focus,.comment-form input[type=email]:focus,.comment-form textarea:focus,input[type=password]:focus,input[type=submit],.search-form input[type=search]:focus,.xt_theme_paignation ul.pager li>span,.reptro_paignation ul.pager li>span,.reptro_paignation ul.pager li>a:hover,.pager li.active a,.pager li a:hover,.btn,.single-team .member-thumb:before,.footer-widgets input:focus,.footer-widgets textarea:focus,.widget-area input:focus,.widget-area textarea:focus,body.woocommerce #respond input#submit,body.woocommerce a.button,body.woocommerce button.button,body.woocommerce input.button,.woocommerce .cart .button,.woocommerce .cart input.button,.woocommerce-cart table.cart input:disabled[disabled],.woocommerce-cart .wc-proceed-to-checkout a.checkout-button,.woocommerce #payment #place_order,.woocommerce-page #payment #place_order,.woocommerce form .form-row input.input-text:focus,.woocommerce form .form-row textarea:focus,.entry-content .woocommerce #respond input#submit,.entry-content .woocommerce a.button,.entry-content .woocommerce button.button,.entry-content .woocommerce input.button,.navbar .navbar-nav .dropdown-menu,body .lp-list-table thead tr th,input[type=text]:focus,input[type=email]:focus,input[type=url]:focus,input[type=password]:focus,input[type=search]:focus,input[type=number]:focus,input[type=tel]:focus,input[type=range]:focus,input[type=date]:focus,input[type=month]:focus,input[type=week]:focus,input[type=time]:focus,input[type=datetime]:focus,input[type=datetime-local]:focus,input[type=color]:focus,textarea:focus,select:focus,.widget.widget_tag_cloud a:hover,.widget.widget_product_tag_cloud a:hover,body .learn-press-pagination .page-numbers>li span,body .learn-press-pagination .page-numbers>li a:hover,.woocommerce nav.woocommerce-pagination ul li a:hover,.woocommerce nav.woocommerce-pagination ul li span,.header-contact-profile-menu img:hover,.portfolio-nav ul li.current span,.portfolio-nav ul li span:hover,.portfolio-nav ul li span:focus,.wp-block-quote:not(.is-large):not(.is-style-large),.wp-block-quote.is-style-large,button:focus,input[type=button]:focus,input[type=reset]:focus,input[type=submit]:focus,button:active,input[type=button]:active,input[type=reset]:active,input[type=submit]:active{ border-color: {$xt_primary_color} }";

			$custom_css .= ".xt-video .xt-video-overlay>a .video-popup-play-icon,.xt_row_primary_bg_color_white .xt-video.xt-video-popup-type-button_only .xt-video-popup-btn .video-popup-play-icon{ border-left-color: {$xt_primary_color} }";

			$custom_css .= ".xt-blog-sticky-post{ border-right-color: {$xt_primary_color} }";

			$custom_css .= ".xt-skills .technical-skills .b_tooltip--tri, .reptro-course-category-dropdown { border-top-color: {$xt_primary_color} }";

			$custom_css .= "body.woocommerce div.product .woocommerce-tabs ul.tabs li.active,body .reptro-category-items li.active,body .lp-tab-sections .section-tab.active span,.reptro-course-item-inner{ border-bottom-color: {$xt_primary_color} }";


			/* Dark */

			$custom_css .= ".has-theme-primary-color-dark-color{ color: {$xt_primary_color_dark} }";

			$custom_css .= ".woocommerce .widget_price_filter .ui-slider .ui-slider-handle,.btn.btn-fill::before,.btn.btn-fill::after,input[type=submit]:hover,.comment-respond .form-submit input[type=submit]:hover,.search-form input[type=submit]:hover,#learn-press-user-profile button:hover,#learn-press-user-profile button:focus,.lp-course-buttons button:hover,.lp-course-buttons button:focus,form[name=search-course] .search-course-button:hover,form[name=search-course] .search-course-button:focus,#course-item-content-header .form-button.lp-button-back button:hover,#course-item-content-header .form-button.lp-button-back button:focus,.lp-button.button:hover,.lp-button.button:focus,.lp-course-buttons a.button:hover,.lp-course-buttons a.button:focus,.lp-course-buttons #learn-press-pmpro-notice.purchase-course a:hover,.lp-course-buttons #learn-press-pmpro-notice.purchase-course a:focus,.lp-quiz-buttons button:hover,.lp-quiz-buttons button:focus,.xt-blog-post-slider.owl-theme .owl-nav>div:hover,.reptro-course-slider.owl-theme .owl-nav>div:hover,.reptro-course-category-slider.owl-theme .owl-nav>div:hover,.pmpro_btn:hover,.pmpro_content_message a:hover,.xt-back-to-top:hover,.xt-back-to-top:focus,.xt-featured-member .item ul li a .fa:hover,.xt-featured-member .item ul li a .fa:focus,#tribe-events .tribe-events-button:hover,#tribe-events .tribe-events-button:focus,#tribe_events_filters_wrapper input[type=submit]:hover,.tribe-events-button:hover,.tribe-events-button.tribe-active:hover,.tribe-events-button.tribe-inactive,.tribe-events-button:hover,.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]:hover,.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]>a:hover,#tribe-bar-form .tribe-bar-submit input[type=submit]:hover,#tribe-bar-form .tribe-bar-submit input[type=submit]:focus,.reptro-events-items .reptro_loop_nine figure:hover .reptro-event-schedule,.reptro-events-items .reptro_loop_nine figure:focus .reptro-event-schedule,.reptro-hero-area-play-btn:hover,.reptro-hero-area-play-btn:focus,.xt-footer .footer-social ul li a:hover,.xt-footer .footer-social ul li a:focus,.xt-blog .xt-grid-read-more-icon:hover,.xt-blog .xt-grid-read-more-icon:focus,.repto-button-fill.elementor-widget-button a.elementor-button:hover,.repto-button-fill.elementor-widget-button .elementor-button:hover,.xt_row_primary_bg_color_white .repto-button-fill.elementor-widget-button a.elementor-button:hover,.xt_row_primary_bg_color_white .repto-button-fill.elementor-widget-button .elementor-button:hover, .has-theme-primary-color-dark-background-color{ background: {$xt_primary_color_dark} }";

			$custom_css .= ".xt-footer .footer-social ul li { border-color: {$xt_primary_color_dark} }";

			/* Light */

			$custom_css .= ".has-theme-primary-color-light-color{ color: {$xt_primary_color_light} }";

			$custom_css .= ".xt-project-gallery .grid-item figure:hover figcaption,.xt-project-gallery .grid-item figure:focus figcaption,.xt-featured-member #quote-carousel .carousel-indicators .active span, .has-theme-primary-color-light-background-color{ background-color: {$xt_primary_color_light} }";

		}

		$custom_css = apply_filters( 'xt_theme_custom_css', $custom_css );    

		wp_enqueue_style( 'reptro-main-style', get_template_directory_uri() . '/assets/css/main.css', array(), '1.0' );

	    wp_add_inline_style( 'reptro-main-style', $custom_css );

	}
}
add_action( 'wp_enqueue_scripts', 'reptro_add_inline_styles', 30 );


/**
 * Demo Installer
 * 
 * Using the plugin : https://wordpress.org/plugins/one-click-demo-import/
 *
 * Documentation : http://proteusthemes.github.io/one-click-demo-import/
 */

if(!function_exists('reptro_import_theme_demo_files')){
	function reptro_import_theme_demo_files() {
	  return array(
	    array(
	      'import_file_name'             => esc_html__( 'Reptro Demo Import', 'reptro' ),
	      'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/demo-data/demo-content.xml',
	      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/demo-data/widgets.json',
	      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'lib/demo-data/customizer.dat',
	      'import_notice'                => esc_html__( 'After you import this demo, you will have to setup the menu and static front page.', 'reptro' ),
	    )
	  );
	}
}

add_filter( 'pt-ocdi/import_files', 'reptro_import_theme_demo_files' );


/**
 * Setup Preloader & Back to top
 */

add_action( 'wp_footer', 'reptro_setup_preloader_and_back_to_top' );

if(!function_exists( 'reptro_setup_preloader_and_back_to_top' )){
	function reptro_setup_preloader_and_back_to_top() {
		$xt_show_site_pre_loader 	= cs_get_option( 'xt_show_site_pre_loader' );
		$xt_show_back_to_top 		= cs_get_option( 'xt_show_back_to_top' );

		?>
			<?php if( $xt_show_back_to_top == true ): ?>
			    <a href="#" class="xt-back-to-top">
			    	<i class="fa fa-arrow-up"></i>
			    </a>
			<?php endif; ?>

			<?php if( $xt_show_site_pre_loader == true && is_front_page() ): ?>
			    <div class="xt-loader-wrapper">
					<div class="xt-loader">
						<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/images/icons/puff.svg' ) ?>" alt="<?php echo esc_html__( 'Site Loader', 'reptro' ) ?>">
					</div>
					<div class="xt-loader-section xt-loader-section-left"></div>
					<div class="xt-loader-section xt-loader-section-right"></div>
				</div>
			<?php endif; ?>
		<?php
	}
}


/**
 * TML login registration page template
 */

add_filter( 'template_include', 'reptro_set_login_pages_template' );

if( !function_exists('reptro_set_login_pages_template') ){
	function reptro_set_login_pages_template( $original_template ) {
		if ( function_exists('tml_get_action') ) {
			if( $action = tml_get_action() ){
				return get_template_directory() . '/page-templates/login-register-page.php';
			}else{
				return $original_template;
			}
		} else {
			return $original_template;
		}
	}
}


/**
 * Placeholder Image
 */

if(!function_exists('reptro_get_placeholder_image')){
	function reptro_get_placeholder_image(){
        $output = sprintf( '<img src="%s">', esc_url( get_template_directory_uri() . '/assets/images/placeholder-image.jpg' ));
        return $output;
	}
}

/**
 * off canvas sidebar trigger icon on menu
 */

add_filter( 'wp_nav_menu_items', 'reptro_off_canvas_sidebar_trigger_icon', 10, 2 );

if(!function_exists('reptro_off_canvas_sidebar_trigger_icon')){
	function reptro_off_canvas_sidebar_trigger_icon($items, $args){
		$xt_off_canvas_sidebar = cs_get_option( 'xt_off_canvas_sidebar' );

        if ($args->theme_location == 'primary' && $args->menu_id == 'navbar-nav'  && $xt_off_canvas_sidebar == true ) {
	        $items .= '<li class="reptro-off-canvas-trigger"><a href="#reptro-off-canvas-sidebar" id="reptro-off-canvas-trigger-button"><i class="fa fa-bars"></i></a></li>';
	    }
	    return $items;
	}
}

/**
 * off canvas sidebar widgets
 */

add_action( 'wp_footer', 'reptro_off_canvas_sidebar' );

if(!function_exists( 'reptro_off_canvas_sidebar' )){
	function reptro_off_canvas_sidebar() {
		$xt_off_canvas_sidebar = cs_get_option( 'xt_off_canvas_sidebar' );

		if( is_page() ){
			$xt_disable_header_footer = reptro_get_post_meta( 'business_x_page_side_options', 'xt_disable_header_footer', false, true );
		}else{
			$xt_disable_header_footer = false;
		}
		
		?>	
			<?php if( $xt_disable_header_footer == false ): ?>
				<?php if( $xt_off_canvas_sidebar == true && has_nav_menu( 'primary' ) ): ?>
				    <aside id="reptro-off-canvas-sidebar" class="widget-area" data-direction="<?php echo esc_attr( is_rtl() ? 'left' : 'right' ); ?>">
				    	<button class="js-offcanvas-close"><i class="fa fa-times"></i></button>
				    	<?php dynamic_sidebar( 'off-canvas-sidebar' ); ?>
				    </aside>
				<?php endif; ?>
			<?php endif; ?>
		<?php
	}
}