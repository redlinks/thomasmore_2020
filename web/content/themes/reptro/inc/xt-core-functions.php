<?php

/**
 * XooThemes Core Functions for WordPress themes
 * Author : XooThemes
 */


/**
 * Register Google fonts.
 *
 * @return string Google fonts URL for the theme.
 */


if ( ! function_exists( 'reptro_main_fonts_url' ) ) {
	function reptro_main_fonts_url() {
	    $fonts_url = '';
	    $fonts     = array();
	    $subsets   = '';
	    if ( 'off' !== esc_html_x( 'on', 'Poppins font: on or off', 'reptro' ) ) {
	        $fonts[] = 'Poppins:400,600,700,900';
	    }
	    if ( $fonts ) {
	        $fonts_url = add_query_arg( array(
	            'family' => urlencode( implode( '|', $fonts ) ),
	            'subset' => urlencode( $subsets ),
	        ), 'https://fonts.googleapis.com/css' );
	    }
	    return $fonts_url;
	}
}
/**
 * Enqueue Google Fonts styles
 */
add_action( 'wp_enqueue_scripts', 'reptro_google_fonts_adding' );

if( !function_exists('reptro_google_fonts_adding') ){
	function reptro_google_fonts_adding() {
	    wp_enqueue_style( 'charity-main-fonts', reptro_main_fonts_url(), array(), null );
	}
}


/**
 * Get CS Customize value with default value
 * 
 * $id : (string) (Required) customize feild id
 * 
 * $default_value : (string) (Optional) customize feild default falue
 */

if( !function_exists('reptro_cs_get_customize_option') ){
	function reptro_cs_get_customize_option ( $id, $default_value = null ){

		$value = cs_get_customize_option( $id );

		if( $value == null &&  $default_value != null ){
			$value = $default_value;
		}

        return $value;
	}
}


/**
 * Get CS Option value with default value
 * 
 * $id : (string) (Required) option feild id
 * 
 * $default_value : (string) (Optional) option feild default falue
 */

if( !function_exists('reptro_cs_get_option') ){
	function reptro_cs_get_option ( $id, $default_value = null ){

		$value = cs_get_option( $id );

		if( $value == null &&  $default_value != null ){
			$value = $default_value;
		}

        return $value;
	}
}

/**
 * Get CS Meta value
 * 
 * $meta_section : (string) (Required) metabox section key
 * 
 * $meta_field : (string) (Required) metabox field key
 * 
 * $default_value : (string) (Optional) metabox default falue
 * 
 * $single : (bool) (Optional) Whether to return a single value. Default value: true

 * $id : int (Optional) Loop post id. Default value: null
 */

if( !function_exists('reptro_get_post_meta') ){
	function reptro_get_post_meta ( $meta_section, $meta_field, $default_value = null, $single = true, $id = null ){

		if( !is_search() && !is_404() ){
			$id = get_the_id();
			if( $id ){
				$values = get_post_meta( $id, $meta_section, true );
			}else{
				global $wp_query;
				$id = $wp_query->post->ID;
				$values = get_post_meta( $id, $meta_section, true );
				wp_reset_postdata();
			}

			$value = $default_value;

			if( isset($values) && is_array($values) ){
				if ( array_key_exists( $meta_field, $values ) ) {
		            $value = $values[$meta_field];
		        }
			}
		}else{
			$value = $default_value;
		}	

        return $value;
	}
}


/**
 * is Blog
 */

if( !function_exists('reptro_is_blog') ){
	function reptro_is_blog () {
		global  $post;
		$posttype = get_post_type($post );
		return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
	}
}


/**
 * is Search has result
 */

if( !function_exists('reptro_is_search_has_results') ){
	function reptro_is_search_has_results() {
	    return 0 != $GLOBALS['wp_query']->found_posts;
	}
}


/**
 * Page Layout setup
 */

add_filter( 'reptro_container_class', 'reptro_page_layout_setup' );

if( !function_exists('reptro_page_layout_setup') ){
	function reptro_page_layout_setup( $class ){

		if ( is_page() ){
			$page_layout = reptro_get_post_meta( 'business_x_page_side_options', 'page_layout', 'grid', true );

            if( $page_layout && $page_layout == 'grid' ){
            	$class = 'container';
            }else {
            	$class = 'fullwidth_page';
            }
		}

		if( is_singular( 'elementor_library' ) ){
			$class = 'fullwidth_page';
		}

		if ( function_exists('tml_get_action') ) {
			if( $action = tml_get_action() ){
				$class = 'container';
			}
		}

		return $class;
	}
}


/**
 * Content area class
 */

add_filter( 'reptro_content_area_class', 'reptro_contet_area_class' );
if(!function_exists('reptro_contet_area_class')){
	function reptro_contet_area_class ( $class ) {

		if( reptro_is_blog() ){

			$blog_layout = reptro_cs_get_option( 'blog_layout', 'right' );

			if( $blog_layout == 'right' ){
				$class = 'col-lg-8';
			}elseif ( $blog_layout == 'left' ) {
				$class = 'col-lg-8 order-2';
			}elseif( $blog_layout = 'full_width' ){
				$class = 'col-lg-12';
			}

		}

		if( is_page() ){

			$page_sidebar_position = reptro_get_post_meta( 'business_x_page_side_options', 'page_sidebar_position', 'no_sidebar', true );

			if( $page_sidebar_position == 'right' ){
				$class = 'col-lg-9';
			}elseif ( $page_sidebar_position == 'left' ) {
				$class = 'col-lg-9 order-2';
			}elseif( $page_sidebar_position = 'no_sidebar' ){
				$class = 'page_no_sidebar';
			}

		}

		if( is_post_type_archive( 'tribe_events' ) ){
			$class = 'col-lg-12';
		}

		return $class;
	}
}


/**
 * Widget area class
 */

add_filter( 'reptro_widget_area_class', 'reptro_widget_area_class' );
if(!function_exists('reptro_widget_area_class')){
	function reptro_widget_area_class ( $class ) {
		
		if( reptro_is_blog() ){

			$blog_layout = reptro_cs_get_option( 'blog_layout', 'right' );

			if( $blog_layout == 'right' ){
				$class = 'col-lg-4';
			}elseif ( $blog_layout == 'left' ) {
				$class = 'col-lg-4 order-1';
			}elseif( $blog_layout = 'full_width' ){
				$class = '';
			}

		}


		if( is_page() ){
			$page_sidebar_position = reptro_get_post_meta( 'business_x_page_side_options', 'page_sidebar_position', 'no_sidebar', true );

			if( $page_sidebar_position == 'right' ){
				$class = 'col-lg-3';
			}elseif ( $page_sidebar_position == 'left' ) {
				$class = 'col-lg-3 order-1';
			}elseif( $page_sidebar_position = 'no_sidebar' ){
				$class = '';
			}
		}
		
		return $class;

	}
}


/**
 * Before Content
 */

add_action( 'reptro_before_content', 'reptro_before_main_content', 20 );

if( !function_exists('reptro_before_main_content') ){
	function reptro_before_main_content(){
		$x_class = '';

		if( reptro_is_blog() == true || is_post_type_archive() == true || is_singular() == true ){
			$x_class = ' row';
		}

		if( is_singular('page') ){
			$page_layout = reptro_get_post_meta( 'business_x_page_side_options', 'page_layout', 'grid', true );

			if( $page_layout && $page_layout == 'grid' ){
				$x_class = ' shadow';
			}else{
				$x_class = '';
			}
		}

		if( is_404() ){
			$x_class = ' row justify-content-center';
		}

		if ( function_exists('tml_get_action') ) {
			if( tml_get_action() ){
				$x_class = ' row justify-content-center';
			}
		}

		?>
			<div class="theme-main-content-inner<?php echo esc_attr( apply_filters( 'reptro_main_content_inner', $x_class ) ); ?>">
		<?php
	}
}


/**
 * After Content
 */

add_action( 'reptro_after_content', 'reptro_after_main_content' );

if( !function_exists('reptro_after_main_content') ){
	function reptro_after_main_content(){
		?>
			</div>
		<?php
	}
}


/**
 * Page main content inner class
 */

add_filter( 'reptro_main_content_inner', 'reptro_page_main_content_inner_class' );

if( !function_exists('reptro_page_main_content_inner_class') ){
	function reptro_page_main_content_inner_class( $class ){
		if ( is_page() ){
			$page_sidebar_position = reptro_get_post_meta( 'business_x_page_side_options', 'page_sidebar_position', 'no_sidebar', true );

			if( $page_sidebar_position != 'no_sidebar' ){
				$class = ' row';
			}
		}

		if( is_singular( 'elementor_library' ) ){
			$class = '';
		}

		return $class;
	}
}


/**
 *  page footer wrapper class
 *  action located at content-page.php
 */

add_action( 'reptro_page_footer_wrapper_class', 'reptro_page_footer_wrapper_class_setup' );

if(!function_exists('reptro_page_footer_wrapper_class_setup')){
	function reptro_page_footer_wrapper_class_setup(){

		$class = '';
		
		if ( is_page() ){
			$page_layout = reptro_get_post_meta( 'business_x_page_side_options', 'page_layout', 'grid', true );

			if( $page_layout && $page_layout == 'grid' ){
				$class = '';
			}else {
				$class = ' container';
			}
		}

		echo esc_attr( $class );
	}
}


/**
 * Footer Bottom Bar
 */

add_action( 'reptro_footer_bottom_bar', 'reptro_footer_bottom_bar' );

if(!function_exists('reptro_footer_bottom_bar')){
	function reptro_footer_bottom_bar(){
		$menu = wp_nav_menu( array(
		    'menu'              => 'footer',
		    'theme_location'    => 'footer',
		    'depth'             => 1,
		    'container'         => '',
		    'menu_class'        => 'xt-footer-menu',
		    'fallback_cb'		=> false,
		    'echo'				=> false,
		) );

		$need_footer_bottom_bar 			= cs_get_option('need_footer_bottom_bar');
		$bottom_bar_content 				= cs_get_option('bottom_bar_content');
		$copyright_text 					= cs_get_option('copyright_text');
		$footer_text 						= cs_get_option('footer_text');
		$footer_bottom_bar_social_icons 	= cs_get_option( 'footer_bottom_bar_social_icons', '' );

		if( $need_footer_bottom_bar == true ):
		?>
			<div class="site-info<?php echo esc_attr( $bottom_bar_content == 'copyright' ? ' text-center' : '' ) ?>">
				<div class="<?php echo esc_attr( apply_filters( 'reptro_footer_site_info_container_class', 'container' ) ); ?>">
					<?php
						switch ($bottom_bar_content ) {
							case 'menu_social':
								?>
									<div class="row">
										<?php if ( $menu):?>
											<div class="col-lg-7 col-md-8">
												<div class="footer-nav">
													<?php echo wp_kses_post( $menu ); ?>
												</div>
											</div>
										<?php endif;?>
										<?php if( is_array($footer_bottom_bar_social_icons) && !empty($footer_bottom_bar_social_icons) ): ?>
											<div class="col-lg-5 col-md-4">
												<div class="footer-social text-right">
							                        <ul>
							                           <?php 
							                           		foreach ($footer_bottom_bar_social_icons as $icon) {
							                           			if( $icon['label'] ){
							                           				printf( '<li><a href="%s" target="_blank"><i class="%s"></i><span>%s</span></a></li>', esc_url( $icon['url'] ), esc_attr( $icon['icon'] ), esc_html( $icon['label'] ) );
							                           			}else{
							                           				printf( '<li><a href="%s" target="_blank"><i class="%s"></i></a></li>', esc_url( $icon['url'] ), esc_attr( $icon['icon'] ) );
							                           			}
															}
							                           ?>
							                        </ul>
												</div>
											</div>
										<?php endif;?>
									</div>
								<?php
							break;

							case 'copyright_menu':
								?>
									<div class="row">
										<div class="col-lg-5 col-md-4">
											<?php echo wpautop( wp_kses_post( $copyright_text ) ) ?>
										</div>
										<?php if ( $menu):?>
											<div class="col-lg-7 col-md-8 text-right">
												<div class="footer-nav">
													<?php echo wp_kses_post( $menu ); ?>
												</div>
											</div>
										<?php endif;?>
									</div>
								<?php
							break;

							case 'copyright_social':
								?>
									<div class="row">
										<div class="col-lg-7 col-md-6">
											<?php echo wpautop( wp_kses_post( $copyright_text ) ) ?>
										</div>
										<?php if( is_array($footer_bottom_bar_social_icons) && !empty($footer_bottom_bar_social_icons) ): ?>
										<div class="col-lg-5 col-md-6">
											<div class="footer-social text-right">
						                        <ul>
						                           <?php 
						                           		foreach ($footer_bottom_bar_social_icons as $icon) {
						                           			if( $icon['label'] ){
						                           				printf( '<li><a href="%s" target="_blank"><i class="%s"></i><span>%s</span></a></li>', esc_url( $icon['url'] ), esc_attr( $icon['icon'] ), esc_html( $icon['label'] ) );
						                           			}else{
						                           				printf( '<li><a href="%s" target="_blank"><i class="%s"></i></a></li>', esc_url( $icon['url'] ), esc_attr( $icon['icon'] ) );
						                           			}
														}
						                           ?>
						                        </ul>
											</div>
										</div>
										<?php endif; ?>
									</div>
								<?php
							break;

							default:
								echo wpautop( wp_kses_post( $copyright_text ) );
							break;
						}
					?>
				</div>
			</div>
		<?php
		endif;
	}
}


/**
 * Theme main header
 */

add_action( 'reptro_theme_main_header', 'reptro_theme_main_header_setup' );

if( !function_exists('reptro_theme_main_header_setup') ){
	function reptro_theme_main_header_setup(){

		$xt_menu_hover_border = cs_get_option( 'xt_menu_hover_border' );
		$classes = array();

		if( $xt_menu_hover_border == true ){
			$classes[] = 'xt-menu-hover-border';
		}

		$classes[] = 'xt-head-2';

		$classes = array_unique($classes);
		$classes = implode (" ", $classes);

		$xt_show_pre_header 			= cs_get_option( 'xt_show_pre_header' );
		$xt_pre_header_phone 			= cs_get_option( 'xt_pre_header_phone' );
		$xt_pre_header_email 			= cs_get_option( 'xt_pre_header_email' );
		$xt_business_hour 				= cs_get_option( 'xt_business_hour', '' );
		$xt_social_icons 				= cs_get_option( 'xt_social_icons', '' );
		$xt_show_header_btn 			= cs_get_option( 'xt_show_header_btn' );
		$xt_show_header_btn_phone 		= cs_get_option( 'xt_show_header_btn_phone' );
		$xt_header_btn_type 			= cs_get_option( 'xt_header_btn_type' );
		$pre_header_right_content 		= cs_get_option( 'pre_header_right_content' );
		$xt_pre_header_menu 			= cs_get_option( 'xt_pre_header_menu' );
		$xt_pre_header_column 			= cs_get_option( 'xt_pre_header_column', '66-33' );

		switch ($xt_pre_header_column) {
			case '50-50':
				$xt_pre_header_column_left  = '6';
				$xt_pre_header_column_right = '6';
				break;

			case '66-33':
				$xt_pre_header_column_left  = '8';
				$xt_pre_header_column_right = '4';
				break;

			case '33-66':
				$xt_pre_header_column_left  = '4';
				$xt_pre_header_column_right = '8';
				break;		
			
			default:
				$xt_pre_header_column_left  = '8';
				$xt_pre_header_column_right = '4';
				break;
		}
		?>
		
		<header id="masthead" class="site-header-type-default xt-header site-header navbar navbar-default xt-navbar<?php echo esc_attr( $classes ? ' '.$classes : '' ); ?>">	
			<?php if( $xt_show_pre_header == true ): ?>
	            <div class="xt-header-top">
	                <div class="<?php echo esc_attr( apply_filters( 'reptro_site_header_container_class', 'container' ) ); ?>">
	                    <div class="row">
	                        <div class="col-lg-<?php echo esc_attr($xt_pre_header_column_left); ?> col-md-<?php echo esc_attr($xt_pre_header_column_left); ?>">
	                            <div class="xt-pre-header-left-content">
	                            	<?php do_action( 'reptro_pre_header_left_content_before' ); ?>
	                            	<?php if($xt_business_hour): ?>
	                               		<span class="xt-business-hour"><i class="sli-clock"></i><?php echo esc_html( $xt_business_hour ); ?> </span>
	                            	<?php endif; ?>
	                            	<?php if($xt_pre_header_email): ?>
	                                	<span class="xt-business-email"><i class="sli-paper-plane"></i><a href="mailto:<?php echo esc_html( $xt_pre_header_email ) ?>"><?php echo esc_html( $xt_pre_header_email ); ?></a></span>
	                                <?php endif; ?>
	                                <?php if($xt_pre_header_phone): ?>
	                                	<span class="xt-business-phone"><i class="sli-phone"></i><a href="tel:<?php echo esc_attr( str_replace(array( ' ', '(', ')' ), '', $xt_pre_header_phone) ) ?>"><?php echo esc_html( $xt_pre_header_phone ); ?></a></span>
	                                <?php endif; ?>
	                                <?php do_action( 'reptro_pre_header_left_content_after' ); ?>
	                            </div>
	                        </div>

	                        <?php if( $pre_header_right_content == 'login_register' ): ?>
	                        	<div class="col-lg-<?php echo esc_attr($xt_pre_header_column_right); ?> col-md-<?php echo esc_attr($xt_pre_header_column_right); ?> text-right">
	                        		<?php if( !is_user_logged_in() ): ?>
	                        			<ul class="reptro-login-register">
	                        				<li><a href="<?php echo esc_url( wp_login_url() ); ?>"><i class="sli-user"></i><?php esc_html_e( 'Login', 'reptro' ) ?></a></li>/<li><a href="<?php echo esc_url( wp_registration_url() ); ?>"><?php esc_html_e( 'Register', 'reptro' ) ?></a></li>
	                        			</ul>
	                        		<?php else: ?>
	                        			<ul class="reptro-profile">
	                        				<?php if( function_exists('learn_press_user_profile_link') ): ?>
	                        					<li><a href="<?php echo esc_url( learn_press_user_profile_link() ); ?>"><i class="sli-user"></i><?php esc_html_e( 'Profile', 'reptro' ) ?></a></li>/
	                        				<?php endif; ?>
	                        				<li><a href="<?php echo esc_url( wp_logout_url() ); ?>"><?php esc_html_e( 'Logout', 'reptro' ) ?></a></li>
	                        			</ul>
	                        		<?php endif; ?>
	                        	</div>
	                        <?php elseif( $pre_header_right_content == 'nav_menu' ): ?>	
	                        	<div class="col-lg-<?php echo esc_attr($xt_pre_header_column_right); ?> col-md-<?php echo esc_attr($xt_pre_header_column_right); ?> text-right">
	                        		<?php
	                        			$args = array(
	                        				'menu'			=> $xt_pre_header_menu,
	                        				'menu_class'	=> 'reptro-pre-header-menu',
	                        				'container'		=> 'ul',
	                        				'depth'			=> 1,
	                        			);
	                        			wp_nav_menu( $args );
	                        		?>
	                        	</div>
	                        <?php else: ?>
		                        <?php if( is_array($xt_social_icons) && !empty($xt_social_icons) ): ?>
			                        <div class="col-lg-<?php echo esc_attr($xt_pre_header_column_right); ?> col-md-<?php echo esc_attr($xt_pre_header_column_right); ?>">
			                            <div class="header-social text-right">
					                        <ul>
					                           <?php 
					                           		foreach ($xt_social_icons as $icon) {
														printf( '<li><a href="%s" target="_blank"><i class="%s"></i></a></li>', esc_url( $icon['url'] ), esc_attr( $icon['icon'] ) );
													}
					                           ?>
					                        </ul>
										</div>
			                        </div>
		                    	<?php endif; ?>
	                    	<?php endif; ?>
	                    </div>
	                </div>
	            </div>
            <?php endif; ?>

            <div class="xt-main-nav">
                <nav class="navbar nav-scroll xt-home-2 show-header-btn-<?php echo esc_attr( $xt_show_header_btn ) ?> header-btn-type-<?php echo esc_attr( $xt_header_btn_type ) ?>">
                    <div class="<?php echo esc_attr( apply_filters( 'reptro_site_header_container_class', 'container' ) ); ?>">
                        <div class="xt-main-navbar-inner">
                            <div class="xt-navbar-menu-and-logo">
                            	<div class="xt-navbar-menu-and-logo-inner">
	                                <?php do_action( 'reptro_logo_setup' ); ?>

	                                <div class="xt-navbar-main-menu">
	                                	<?php do_action( 'reptro_before_main_menu' ); ?>
										<?php 
											wp_nav_menu( array(
										        'theme_location'    => 'primary',
										        'depth'             => 4,
										        'container'         => '',
										        'menu_class'        => 'nav-menu nav navbar-nav navbar-right',
										        'menu_id'           => 'navbar-nav',
										        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
										        'walker'            => new wp_bootstrap_navwalker())
										    );
										?>
										<?php do_action( 'reptro_after_main_menu' ); ?>
									</div>

								</div>
                            </div>

                            <?php if( $xt_show_header_btn == true ) : ?>
	                            <div class="xt-navbar-side text-right">
	                                <?php do_action( 'reptro_header_btn' ); ?>
	                            </div> 
                            <?php endif; ?>  
                        </div>
                    </div>
                </nav><!-- /.navbar -->
            </div>

			<!-- Mobile Menu-->
            <div class="reptro-mobile-menu menu-spacing nav-scroll xt-header-type-without_background" data-direction="<?php echo( is_rtl() ? 'true' : '' ); ?>"> 
            	<div class="mobile-menu-area">
            		<?php do_action( 'reptro_logo_setup' ); ?>

            		<div class="mobile-menu clearfix">
						<?php
							wp_nav_menu( array( 
								'theme_location'    => 'primary', 
								'container'         => 'nav',
								'container_id'      => 'mobile-menu-active',
								'container_class'   => 'mobile-menu-init',
								'menu_class'        => 'main',
								'depth'             => 4,
								'menu_id'			=> 'reptro_mobile_menu'
							) );
						?>
					</div>
				</div>
				<?php if( $xt_show_header_btn_phone == true ) : ?>
                    <div class="reptro-mobile-header-button text-center">
                        <?php do_action( 'reptro_header_btn' ); ?>
                    </div> 
                <?php endif; ?> 
			</div>

		</header><!-- #masthead -->

		<?php
	}
}


/**
 * Menu Custom items for mobile menu
 */

add_filter( 'wp_nav_menu_items', 'reptro_add_loginout_links_mobile_menu', 10, 2 );

if( !function_exists('reptro_add_loginout_links_mobile_menu') ){
	function reptro_add_loginout_links_mobile_menu( $items, $args ) {

		$xt_show_mobile_menu_login 	= cs_get_option( 'xt_show_mobile_menu_login' );

		if( $xt_show_mobile_menu_login == true ){
			if( $args->menu_id == 'reptro_mobile_menu' ){
				if (is_user_logged_in() && $args->theme_location == 'primary') {
					if( function_exists('learn_press_user_profile_link') ){
						$items .= '<li><a href="'. esc_url( learn_press_user_profile_link() ) .'">'. esc_html__( 'Profile', 'reptro' ) .'</a></li>';
					}

			        $items .= '<li><a href="'. esc_url( wp_logout_url() ) .'">'. esc_html__( 'Log Out', 'reptro' ) .'</a></li>';
			    }
			    elseif (!is_user_logged_in() && $args->theme_location == 'primary') {
			        $items .= '<li><a href="'. esc_url( site_url('wp-login.php') ) .'">'. esc_html__( 'Log In', 'reptro' ) .'</a></li>';
			    }
			}
		}

	    return $items;
	}
}



/**
 * Returns a custom logo, linked to home.
 */

if( !function_exists('reptro_get_custom_logo') ){
	function reptro_get_custom_logo( $logo_id = 0 ) {
		$html = '';
		$switched_blog = false;

		$custom_logo_id = get_theme_mod( $logo_id );

		// We have a logo. Logo is go.
		if ( $custom_logo_id ) {
			$custom_logo_attr = array(
				'class'    => 'custom-logo',
				'itemprop' => 'logo',
			);

			/*
			 * If the logo alt attribute is empty, get the site title and explicitly
			 * pass it to the attributes used by wp_get_attachment_image().
			 */
			$image_alt = get_post_meta( $custom_logo_id, '_wp_attachment_image_alt', true );
			if ( empty( $image_alt ) ) {
				$custom_logo_attr['alt'] = get_bloginfo( 'name', 'display' );
			}

			/*
			 * If the alt attribute is not empty, there's no need to explicitly pass
			 * it because wp_get_attachment_image() already adds the alt attribute.
			 */
			$html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" itemprop="url">%2$s</a>',
				esc_url( home_url( '/' ) ),
				wp_get_attachment_image( $custom_logo_id, 'full', false, $custom_logo_attr )
			);
		}

		// If no logo is set but we're in the Customizer, leave a placeholder (needed for the live preview).
		elseif ( is_customize_preview() ) {
			$html = sprintf( '<a href="%1$s" class="custom-logo-link" style="display:none;"><img class="custom-logo"/></a>',
				esc_url( home_url( '/' ) )
			);
		}

		if ( $switched_blog ) {
			restore_current_blog();
		}

		/**
		 * Filters the custom logo output.
		 *
		 * @since 4.5.0
		 * @since 4.6.0 Added the `$blog_id` parameter.
		 *
		 * @param string $html    Custom logo HTML output.
		 * @param int    $blog_id ID of the blog to get the custom logo for.
		 */
		return apply_filters( 'reptro_get_custom_logo', $html, $logo_id );
	}
}

/**
 * Displays a custom logo, linked to home.
 */

if( !function_exists('reptro_the_custom_logo') ){
	function reptro_the_custom_logo( $logo_id = 0  ) {
		echo reptro_get_custom_logo($logo_id);
	}
}

/**
 * Logo Setup
 */

add_action( 'reptro_logo_setup','reptro_logo_setup_function' );

if( !function_exists('reptro_logo_setup_function') ){
    function reptro_logo_setup_function(){
    	?>
        <div class="logo-wrapper" itemscope itemtype="http://schema.org/Brand">
            <?php 
				if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
				    the_custom_logo();
				}else{
				    printf( '<h1 class="site-title"><a href="%s" rel="home">%s</a></h1>', esc_url( home_url( '/' ) ), esc_html( get_bloginfo( 'name' ) ) );
				}
            ?> 
        </div>
        <?php
    }
}


/**
 * header button
 */

add_action( 'reptro_header_btn', 'reptro_add_header_btn' );

if( !function_exists('reptro_add_header_btn') ){
    function reptro_add_header_btn(){
    	$xt_show_header_btn 		= cs_get_option( 'xt_show_header_btn' );
    	$xt_header_btn_type 		= cs_get_option( 'xt_header_btn_type' );
    	$xt_show_header_btn_text 	= cs_get_option( 'xt_show_header_btn_text' );
    	$xt_show_header_btn_url 	= cs_get_option( 'xt_show_header_btn_url' );
    	$xt_header_btn_window 		= cs_get_option( 'xt_header_btn_window' );
    	if($xt_show_header_btn && $xt_show_header_btn == 'true' ):
	    	?>
		        <div class="<?php echo esc_attr( $xt_header_btn_type == 'static_btn' ? 'reptro-header-btn' : 'reptro-header-profile-img' ) ?>">
		        	<?php if( $xt_header_btn_type == 'static_btn' ): ?>
		            	<a class="btn btn-lg btn-fill" target="<?php echo esc_attr( $xt_header_btn_window == true ? '_blank' : '_self' ); ?>" href="<?php echo esc_url($xt_show_header_btn_url); ?>"><?php echo esc_html( $xt_show_header_btn_text ) ?></a>
		       		<?php elseif( $xt_header_btn_type == 'login_or_profile' ): ?>
		       			<?php if( is_user_logged_in() ): ?>
		       				<?php if( function_exists('learn_press_user_profile_link') ): ?>
		       					<div class="header-contact-profile-menu">
                            		<div class="reptro-profile-menu clearfix">
                            			<?php get_template_part( 'template-parts/profile', 'menu' ); ?>
                            		</div>
	                            </div>
		       				<?php endif; ?>
		       			<?php else: ?>
		       				<a class="btn btn-lg btn-fill" href="<?php echo esc_url( wp_login_url() ); ?>"><?php echo esc_html__( 'Login', 'reptro' ) ?></a>
		       			<?php endif; ?>
		       		<?php endif; ?>
		        </div>
	        <?php
    	endif;
    }
}


/**
 * Page Title
 */

add_action( 'reptro_after_header', 'reptro_page_title' );

if( !function_exists('reptro_page_title') ){
	function reptro_page_title(){

		$title = get_the_title();

		if( is_search() ){

			if( reptro_is_search_has_results() ){
				$title = esc_html__( 'Search result', 'reptro' );
			}else{
				$title = esc_html__( 'Nothing Found', 'reptro' );
			}
			
		}elseif( is_singular( 'lp_course' ) ){
			$terms = get_the_terms( get_the_id(), 'course_category' );
			if( $terms && !empty($terms) ){
				$term = array_shift($terms);
				$title = $term->name;
			}else{
				$title = esc_html__( 'Course', 'reptro' );
			}
		}elseif( reptro_lp_is_archive() ){
			$title = esc_html( get_the_title( learn_press_get_page_id( 'courses' ) ) );
		}elseif( is_singular( 'tribe_events' ) ){
			$title = esc_html__( 'Events', 'reptro' );
		}elseif( is_post_type_archive( 'tribe_events' ) ){
			$title = esc_html__( 'Events', 'reptro' );
		}elseif( is_archive() ){
			$title = get_the_archive_title();
		}

		if( is_page() ){
			$need_page_title = reptro_get_post_meta( 'business_x_page_side_options', 'need_page_title', true, true );
		}else{
			$need_page_title = false;
		}

		if( is_home() || is_singular( 'post' ) ){
			$title = apply_filters( 'xt_blog_page_title', esc_html__( 'Blog', 'reptro' ) );
		}

		if( is_404() ){
			$title = apply_filters( 'xt_not_found_page_title', esc_html__( '404', 'reptro' ) );
		}

		// Disable page header for home page
		if( is_front_page() ){
			$need_page_title = false;
		}


		$need_page_title 	= apply_filters( 'xt_theme_need_page_title', $need_page_title );
		$title 				= apply_filters( 'xt_theme_page_title', $title );
		$xt_show_breadcrumb = apply_filters( 'xt_theme_show_breadcrumb', cs_get_option( 'xt_show_breadcrumb' ) );
		

		if( is_page() && $need_page_title != false || reptro_lp_is_archive() || is_single() || is_archive() || is_home() || is_search() || is_404() ){
			?>
				<div class="xt-page-title-area">
					<div class="xt-page-title">
						<div class="container">
							<h1 class="entry-title"><?php echo wp_kses_post( $title ); ?></h1>
						</div>
					</div>
					<?php if( $xt_show_breadcrumb == true ): ?>
						<div class="xt-breadcrumb-wrapper">
							<div class="container">
								<?php do_action( 'reptro_breadcrumb' );?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php
		}

	}
}



/**
 * Setup breadcrumb
 */

add_action( 'reptro_breadcrumb', 'reptro_breadcrumb_setup', 10 );

if( !function_exists('reptro_breadcrumb_setup') ){
	function reptro_breadcrumb_setup( $args = array() ){

		$args = wp_parse_args( $args, apply_filters( 'reptro_breadcrumb_defaults', array() ) );

		$options = array(
			'show_htfpt' => true,
			'separator'	 => '',
		);

		if( reptro_lp_is_archive() || is_singular( 'lp_course' ) || is_tax('course_category') ){
			learn_press_breadcrumb();
		}elseif( is_singular( 'sfwd-courses' ) || is_post_type_archive( 'sfwd-courses' ) || is_singular( 'sfwd-lessons' ) || is_post_type_archive( 'sfwd-lessons' ) || is_singular( 'sfwd-topic' ) || is_post_type_archive( 'sfwd-topic' ) || is_singular( 'sfwd-quiz' ) || is_post_type_archive( 'sfwd-quiz' ) ){
			
			if( function_exists('uo_breadcrumbs') ){
				uo_breadcrumbs();
			}else{
				$breadcrumb = new DS_WP_Breadcrumb( $args, $options );
			}
		}elseif( class_exists('DS_WP_Breadcrumb') && !is_front_page() ){

			$breadcrumb = new DS_WP_Breadcrumb( $args, $options );
		}

	}
}



/**
 * Breadcrumb $args
 */

add_filter( 'reptro_breadcrumb_defaults', 'reptro_breadcrumb_args' );

if( !function_exists('reptro_breadcrumb_args') ){
	function reptro_breadcrumb_args( $args = array() ){

		$args = array(
			'before' 		=> '<nav class="xt-breadcrumb"><ul>',
			'after' 		=> '</ul></nav>',
			'standard' 		=> '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">%s</li>',
			'current' 		=> '<li class="current">%s</li>',
			'link' 			=> '<a href="%s" itemprop="url"><span itemprop="title">%s</span></a>'
		);

		return $args;
	}
}


/**
 * Excerpt more
 */

if(!function_exists('reptro_excerpt_more')){
	function reptro_excerpt_more( $more ) {
	    return '&#8230;';
	}
}
add_filter( 'excerpt_more', 'reptro_excerpt_more' );

/**
 * Excerpt with custom length
 */

if(!function_exists('reptro_get_the_excerpt')){
	function reptro_get_the_excerpt( $length = 100 ) {
	    $excerpt = get_the_excerpt();
		$excerpt = substr( $excerpt , 0, $length); 
		return $excerpt;
	}
}



/**
 * Link Pages Bootstrap
 * @author toscha
 * @link http://wordpress.stackexchange.com/questions/14406/how-to-style-current-page-number-wp-link-pages
 * @param  array $args
 * @return void
 * Modification of wp_link_pages() with an extra element to highlight the current page.
 */

if( !function_exists('xt_bootstrap_link_pages') ):
	function xt_bootstrap_link_pages( $args = array () ) {
	    $defaults = array(
			'before' => '<nav class="reptro_paignation"><ul class="pager">',
			'after' => '</ul></nav>',
			'before_link' => '<li>',
			'after_link' => '</li>',
			'current_before' => '<li class="active">',
			'current_after' => '</li>',
	        'link_before' => '',
	        'link_after'  => '',
	        'pagelink'    => '%',
	        'echo'        => 1
	    );
	    $r = wp_parse_args( $args, $defaults );
	    $r = apply_filters( 'wp_link_pages_args', $r );
	    extract( $r, EXTR_SKIP );
	    global $page, $numpages, $multipage, $more, $pagenow;
	    if ( ! $multipage )
	    {
	        return;
	    }
	    $output = $before;
	    for ( $i = 1; $i < ( $numpages + 1 ); $i++ )
	    {
	        $j       = str_replace( '%', $i, $pagelink );
	        $output .= ' ';
	        if ( $i != $page || ( ! $more && 1 == $page ) )
	        {
	            $output .= "{$before_link}" . _wp_link_page( $i ) . "{$link_before}{$j}{$link_after}</a>{$after_link}";
	        }
	        else
	        {
	            $output .= "{$current_before}{$link_before}<span>{$j}</span>{$link_after}{$current_after}";
	        }
	    }
	    print wp_kses_post( $output . $after );
	}
endif;


/**
 * WordPress Bootstrap pagination
 */

if( !function_exists('xt_wp_numeric_pagination') ):
    function xt_wp_numeric_pagination( $args = array() ) {
        
        $defaults = array(
            'range'           => 4,
            'custom_query'    => FALSE,
            'previous_string' => '<i class="fa fa-angle-left"></i>',
            'next_string'     => '<i class="fa fa-angle-right"></i>',
            'before_output'   => '<nav class="reptro_paignation"><ul class="pager">',
            'after_output'    => '</ul></nav>'
        );
        
        $args = wp_parse_args( 
            $args, 
            apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
        );
        
        $args['range'] = (int) $args['range'] - 1;
        if ( !$args['custom_query'] )
            $args['custom_query'] = $GLOBALS['wp_query'];
        $count = (int) $args['custom_query']->max_num_pages;
        $page  = intval( get_query_var( 'paged' ) );
        $ceil  = ceil( $args['range'] / 2 );
        
        if ( $count <= 1 )
            return FALSE;
        
        if ( !$page )
            $page = 1;
        
        if ( $count > $args['range'] ) {
            if ( $page <= $args['range'] ) {
                $min = 1;
                $max = $args['range'] + 1;
            } elseif ( $page >= ($count - $ceil) ) {
                $min = $count - $args['range'];
                $max = $count;
            } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
                $min = $page - $ceil;
                $max = $page + $ceil;
            }
        } else {
            $min = 1;
            $max = $count;
        }
        
        $echo = '';
        $previous = intval($page) - 1;
        $previous = esc_attr( get_pagenum_link($previous) );
        
        if ( $previous && (1 != $page) )
        	$echo .= sprintf ( '<li><a href="%s" title="%s">%s</a></li>', esc_url( $previous ), esc_html__( 'previous', 'reptro' ), $args['previous_string'] );
        
        if ( !empty($min) && !empty($max) ) {
            for( $i = $min; $i <= $max; $i++ ) {
                if ($page == $i) {
                    $echo .= sprintf ( '<li class="active"><span class="active">%s</span></li>', esc_html( str_pad( (int)$i, 2, '0', STR_PAD_LEFT ) ) );
                } else {
                    $echo .= sprintf( '<li><a href="%s">%002d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
                }
            }
        }
        
        $next = intval($page) + 1;
        $next = esc_attr( get_pagenum_link($next) );
        if ($next && ($count != $page) )
        	$echo .= sprintf ( '<li><a href="%s" title="%s">%s</a></li>', esc_url( $next ), esc_html__( 'next', 'reptro' ), $args['next_string'] );
        
        if ( isset($echo) )
            echo  wp_kses_post( $args['before_output'] . $echo . $args['after_output'] );
    }
endif;



/**
 * Pagination RTL support
 */

add_filter( 'wp_bootstrap_pagination_defaults', 'reptro_pagination_rtl_support' );

if(!function_exists('reptro_pagination_rtl_support')){
	function reptro_pagination_rtl_support($args){
	  	if( is_rtl() ){
		   $args['next_string']   = '<i class="fa fa-angle-left"></i>';
		   $args['previous_string']  = '<i class="fa fa-angle-right"></i>';
		}
		return $args;
	}
}

/**
 *  Author bio
 */

if ( ! function_exists( 'reptro_get_author_bio' ) ) :
function reptro_get_author_bio() {
	$description = get_the_author_meta( 'description' );

	if( $description != '' ):
	    ?>
		    <div class="xt-author-area">
			    <div class="xt-author-bio shadow">
			    	<div class="row">
				        <div class="xt-author-avatar col-sm-3">
				            <?php echo get_avatar( get_the_author_meta( 'user_email' ), 320 ); ?>
				        </div>
				        <div class="xt-author-comment col-sm-9">
				            <h3 class="xt-author-name"><?php echo esc_html( get_the_author() ); ?></h3>
				            <?php echo wpautop( esc_html( get_the_author_meta( 'description' ) ) ); ?>
				        </div>
			        </div>
			    </div>
			</div>
	    <?php
	endif;
}
endif;


/**
 * Comment list walker
 */

/**
 * A custom WordPress comment walker class to implement the Bootstrap 3 Media object in wordpress comment list.
 *
 * @package     WP Bootstrap Comment Walker
 * @version     1.0.0
 * @author      Edi Amin <to.ediamin@gmail.com>
 * @license     http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link        https://github.com/ediamin/wp-bootstrap-comment-walker
 */

if( !class_exists('reptro_Bootstrap_Comment_Walker') ){
	class reptro_Bootstrap_Comment_Walker extends Walker_Comment {
		/**
		 * Output a comment in the HTML5 format.
		 *
		 * @access protected
		 * @since 1.0.0
		 *
		 * @see wp_list_comments()
		 *
		 * @param object $comment Comment to display.
		 * @param int    $depth   Depth of comment.
		 * @param array  $args    An array of arguments.
		 */
		protected function html5_comment( $comment, $depth, $args ) {
			$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
			?>		
			<<?php echo esc_attr( $tag ); ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( $this->has_children ? 'parent xt-media' : 'xt-media' ); ?>>

				<div class="xt-comment-body">
					<div class="xt-comment-heading">
						<?php if ( 0 != $args['avatar_size'] ): ?>
							<div class="xt-comment-avatar">
								<a href="<?php echo esc_url( get_comment_author_url() ); ?>" class="xt-media-object">
									<?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
								</a>
							</div>
						<?php endif; ?>
						<div class="xt-comment-author-name">
							<h4><?php echo get_comment_author_link(); ?></h4>
							<span class="comment-metadata">
								<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
									<time datetime="<?php esc_attr( comment_time( 'c' ) ); ?>">
										<?php printf( esc_html_x( '%1$s at %2$s', '1: date, 2: time', 'reptro' ), get_comment_date(), get_comment_time() ); ?>
									</time>
								</a>
							</span>
						</div>
					</div>

					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation label label-info"><?php esc_html_e( 'Your comment is awaiting moderation.', 'reptro' ); ?></p>
					<?php endif; ?>				

					<div class="comment-content">
						<?php comment_text(); ?>
					</div><!-- .comment-content -->
					
					<ul class="list-inline">
						<?php edit_comment_link( esc_html__( 'Edit', 'reptro' ), '<li class="edit-link">', '</li>' ); ?>

						<?php
							comment_reply_link( array_merge( $args, array(
								'add_below' => 'div-comment',
								'depth'     => $depth,
								'max_depth' => $args['max_depth'],
								'before'    => '<li class="reply-link">',
								'after'     => '</li>'
							) ) );	
						?>

					</ul>

				</div>		
			<?php
		}	
	}
}


/**
 *  Body Classes
 */

add_filter('body_class', 'reptro_get_post_meta_page_body_classes');

if(!function_exists('reptro_get_post_meta_page_body_classes')){
	function reptro_get_post_meta_page_body_classes($classes) {

	    if( is_page() ){
	    	$page_layout 			= reptro_get_post_meta( 'business_x_page_side_options', 'page_layout', 'grid', true );
	    	$need_page_title 		= reptro_get_post_meta( 'business_x_page_side_options', 'need_page_title', true, true );
	    	$page_sidebar_enable 	= reptro_get_post_meta( 'business_x_page_side_options', 'page_sidebar_enable', false, true );
	    	$page_sidebar_position 	= reptro_get_post_meta( 'business_x_page_side_options', 'page_sidebar_position', 'no_sidebar', true );

			if( $page_layout && $page_layout == 'grid' ){
				$classes[] = 'xt-page-layout-grid';
			}elseif( $page_layout && $page_layout == 'full_screen' ) {
				$classes[] = 'xt-page-layout-full-width';
			}

			if( $need_page_title && $need_page_title = true ){
				$classes[] = 'xt-has-page-title';
			}else{
				$classes[] = 'xt-no-page-title';
			}

			if( $page_sidebar_enable && $page_sidebar_enable = true ){
				$classes[] = 'xt-has-page-sidebar';
			}else{
				$classes[] = 'xt-no-page-sidebar';
			}

			if( $page_sidebar_position && $page_sidebar_position == 'left' ){
				$classes[] = 'xt-page-left-sidebar';
			}elseif( $page_sidebar_position && $page_sidebar_position == 'right' ) {
				$classes[] = 'xt-page-right-sidebar';
			}
	    }


	    $xt_site_layout 	= reptro_cs_get_option( 'xt_site_layout', 'boxed' );
	    $xt_site_layout_url = '';

	    if( isset($_GET['site_layout']) ) {
	    	$xt_site_layout_url = $_GET['site_layout'];
	    }


	    if( $xt_site_layout_url ){
	    	if( $xt_site_layout_url && $xt_site_layout_url == 'boxed' ){
		    	$classes[] = 'xt-site-layout-boxed';
		    }elseif( $xt_site_layout_url && $xt_site_layout_url == 'full_width' ){
		    	$classes[] = 'xt-site-layout-full_width';
		    }
	    }else{
	    	if( $xt_site_layout && $xt_site_layout == 'boxed' ){
		    	$classes[] = 'xt-site-layout-boxed';
		    }elseif( $xt_site_layout && $xt_site_layout == 'full_width' ){
		    	$classes[] = 'xt-site-layout-full_width';
		    }
	    }

	    $xt_show_site_pre_loader 	= cs_get_option( 'xt_show_site_pre_loader' );

	    if( $xt_show_site_pre_loader == true ){
	    	$classes[] = 'xt-site-loading';
	    }

	    if ( function_exists('tml_get_action') ) {
			if( $action = tml_get_action() ){
				$classes[] = 'xt-page-layout-full-width';
			}
		}

	    return $classes;
	}
}


/**
 * Required Plugins
 */

add_action( 'tgmpa_register', 'reptro_register_required_plugins' );

if(!function_exists('reptro_register_required_plugins')){
	function reptro_register_required_plugins() {

		$plugins = array(
			array(
				'name'      => esc_html__( 'Reptro CPT and Shortcode', 'reptro' ),
				'slug'      => 'xt-reptro-cpt-n-shortcode',
				'source'    => get_template_directory() . '/lib/plugins/xt-reptro-cpt-n-shortcode.zip',
				'required'  => true,
				'version'   => '1.1.9',
			),
			array(
				'name'      => esc_html__( 'Reptro Elementor Icons', 'reptro' ),
				'slug'      => 'reptro-elementor-icons',
				'source'    => get_template_directory() . '/lib/plugins/reptro-elementor-icons.zip',
				'required'  => true,
				'version'   => '1.0.1',
			),
			array(
				'name'      => esc_html__( 'Elementor Page Builder', 'reptro' ),
				'slug'      => 'elementor',
				'required'  => true,
			),
			array(
				'name'      => esc_html__( 'LearnPress - WordPress LMS Plugin', 'reptro' ),
				'slug'      => 'learnpress',
				'required'  => true,
			),
			array(
				'name'      => esc_html__( 'LearnPress - Course Review', 'reptro' ),
				'slug'      => 'learnpress-course-review',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'LearnPress Courses Wishlist', 'reptro' ),
				'slug'      => 'learnpress-wishlist',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'LearnPress Prerequisite Courses', 'reptro' ),
				'slug'      => 'learnpress-prerequisites-courses',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'Theme My Login', 'reptro' ),
				'slug'      => 'theme-my-login',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'Contact Form 7', 'reptro' ),
				'slug'      => 'contact-form-7',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'WooCommerce', 'reptro' ),
				'slug'      => 'woocommerce',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'The Events Calendar', 'reptro' ),
				'slug'      => 'the-events-calendar',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'One Click Demo Import', 'reptro' ),
				'slug'      => 'one-click-demo-import',
				'required'  => false,
			),
			array(
				'name'      => esc_html__( 'Regenerate Thumbnails', 'reptro' ),
				'slug'      => 'regenerate-thumbnails',
				'required'  => false,
			),
		);

		if ( defined( 'LEARNDASH_LMS_PLUGIN_DIR' ) ) {
			$plugins[] = array(
				'name'      => esc_html__( 'Uncanny LearnDash Toolkit', 'reptro' ),
				'slug'      => 'uncanny-learndash-toolkit',
				'required'  => false,
			);
			
			$plugins[] = array(
				'name'      => esc_html__( 'LearnDash Course Grid', 'reptro' ),
				'slug'      => 'learndash-course-grid',
				'source'    => get_template_directory() . '/lib/plugins/xt-reptro-cpt-n-shortcode.zip',
				'required'  => false,
				'version'   => '1.5.1',
			);
		}

		$config = array(
			'id'           => 'reptro',
			'default_path' => '',
			'menu'         => 'tgmpa-install-plugins',
			'parent_slug'  => 'themes.php',
			'capability'   => 'edit_theme_options',
			'has_notices'  => true,
			'dismissable'  => true,
			'dismiss_msg'  => '',
			'is_automatic' => false,
			'message'      => '',
		);
		tgmpa( $plugins, $config );
	}
}

/**
 * Remove One Click demo importer branding
 */

add_filter( 'pt-ocdi/disable_pt_branding', '__return_false' );


/**
 * Single SideBar Setup
 */

add_action( 'reptro_single_get_sidebar', 'reptro_single_get_sidebar' );

if( !function_exists('reptro_single_get_sidebar') ){
	function reptro_single_get_sidebar(){
		if( is_singular( 'sfwd-quiz' ) ){
			get_sidebar('single_course');
		}elseif( is_singular( 'sfwd-lessons' ) ){
			get_sidebar('single_course');
		}elseif( is_singular( 'sfwd-topic' ) ){
			get_sidebar('single_course');
		}else{
			get_sidebar();
		}
	}
}