<?php

/**
 * LP Conditional tags
 */

if( !function_exists('reptro_lp_is_archive') ){
	function reptro_lp_is_archive() {
		if ( class_exists( 'LearnPress' ) ) {
			return learn_press_is_courses() || learn_press_is_course_tag() || learn_press_is_course_category() || learn_press_is_search() || learn_press_is_course_tax();
		}
	}
}

/**
 * Single Course Image Size
 */

add_filter( 'single_course_image_size', 'reptro_single_course_image_size' );
if( !function_exists('reptro_single_course_image_size') ){
	function reptro_single_course_image_size( $size ){
		return 'reptro-blog-thumb';
	}
}

/**
 * Single Course Page
 */


/**
 * Single Course Page [ Landing ]
 */

remove_action( 'learn-press/content-landing-summary', 'learn_press_course_meta_start_wrapper', 5 );
remove_action( 'learn-press/content-landing-summary', 'learn_press_course_students', 10 );
remove_action( 'learn-press/content-landing-summary', 'learn_press_course_meta_end_wrapper', 15 );
remove_action( 'learn-press/content-landing-summary', 'learn_press_course_tabs', 20 );
remove_action( 'learn-press/content-landing-summary', 'learn_press_course_price', 25 );
remove_action( 'learn-press/content-landing-summary', 'learn_press_course_buttons', 30 );


add_action( 'learn-press/content-landing-summary', 'reptro_before_course_header', 5 );
add_action( 'learn-press/content-landing-summary', 'learn_press_course_title', 10 );
add_action( 'learn-press/content-landing-summary', 'learn_press_course_meta_start_wrapper', 15 );
add_action( 'learn-press/content-landing-summary', 'reptro_course_instructor', 20 );
add_action( 'learn-press/content-landing-summary', 'reptro_course_category', 25 );
add_action( 'learn-press/content-landing-summary', 'reptro_course_ratings', 30 );
add_action( 'learn-press/content-landing-summary', 'learn_press_course_progress', 35 );
add_action( 'learn-press/content-landing-summary', 'learn_press_course_progress', 40 );
add_action( 'learn-press/content-landing-summary', 'learn_press_course_meta_end_wrapper', 40 );
add_action( 'learn-press/content-landing-summary', 'reptro_course_payment_landing', 45 );
add_action( 'learn-press/content-landing-summary', 'reptro_after_course_header', 50 );
add_action( 'learn-press/content-landing-summary', 'learn_press_course_thumbnail', 55 );
add_action( 'learn-press/content-landing-summary', 'learn_press_course_tabs', 60 );
add_action( 'learn-press/content-landing-summary', 'reptro_relative_courses', 60 );


/**
 * Single Course Page [ Learning ]
 */


remove_action( 'learn-press/content-learning-summary', 'learn_press_course_meta_start_wrapper', 10 );
remove_action( 'learn-press/content-learning-summary', 'learn_press_course_students', 15 );
remove_action( 'learn-press/content-learning-summary', 'learn_press_course_meta_end_wrapper', 20 );
remove_action( 'learn-press/content-learning-summary', 'learn_press_course_progress', 25 );
remove_action( 'learn-press/content-learning-summary', 'learn_press_course_remaining_time', 30 );
remove_action( 'learn-press/content-learning-summary', 'learn_press_course_tabs', 35 );
remove_action( 'learn-press/content-learning-summary', 'learn_press_course_buttons', 40 );


add_action( 'learn-press/content-learning-summary', 'reptro_before_course_header', 5 );
add_action( 'learn-press/content-learning-summary', 'learn_press_course_title', 10 );
add_action( 'learn-press/content-learning-summary', 'learn_press_course_meta_start_wrapper', 15 );
add_action( 'learn-press/content-learning-summary', 'reptro_course_instructor', 20 );
// add_action( 'learn-press/content-learning-summary', 'reptro_course_category', 25 );
add_action( 'learn-press/content-learning-summary', 'reptro_course_ratings', 30 );
add_action( 'learn-press/content-learning-summary', 'learn_press_course_progress', 35 );
add_action( 'learn-press/content-learning-summary', 'learn_press_course_meta_end_wrapper', 40 );
add_action( 'learn-press/content-learning-summary', 'reptro_course_payment_learning', 45 );
add_action( 'learn-press/content-learning-summary', 'reptro_after_course_header', 50 );
add_action( 'learn-press/content-learning-summary', 'learn_press_course_thumbnail', 60 );
add_action( 'learn-press/content-learning-summary', 'learn_press_course_tabs', 65 );
add_action( 'learn-press/content-learning-summary', 'reptro_relative_courses', 70 );




/**
 * archive courses page
 */

$show_course_instructor = cs_get_option('show_course_instructor');
$show_course_students   = cs_get_option('show_course_students');

remove_action( 'learn-press/after-courses-loop-item', 'learn_press_course_loop_item_buttons', 35 );
remove_action( 'learn-press/courses-loop-item-title', 'learn_press_courses_loop_item_thumbnail', 10 );
remove_action( 'learn-press/after-courses-loop-item', 'learn_press_courses_loop_item_price', 20 );

add_action( 'learn-press/before-courses-loop-item', 'reptro_learn_press_courses_loop_item_thumbnail', 5 );
add_action( 'learn-press/before-courses-loop-item', 'reptro_learn_press_before_courses_loop_item', 5 );

if( $show_course_instructor == true ){
	add_action( 'learn-press/after-courses-loop-item', 'learn_press_courses_loop_item_instructor', 25 );
}else{
	remove_action( 'learn-press/after-courses-loop-item', 'learn_press_courses_loop_item_instructor', 25 );
}

if( $show_course_students == true ){
	add_action( 'learn-press/after-courses-loop-item', 'learn_press_course_students', 25 );
}else{
	remove_action( 'learn-press/after-courses-loop-item', 'learn_press_course_students', 25 );
}

add_action( 'learn-press/after-courses-loop-item', 'reptro_learn_press_course_excerpt', 30, 1 );

add_action( 'learn-press/after-courses-loop-item', 'reptro_learn_press_after_courses_loop_item', 100 );
//add_action( 'learn-press/after-courses-loop-item', 'reptro_learn_press_course_loop_item_button', 35 );




/**
 * Content area class
 */

add_filter( 'reptro_content_area_class', 'reptro_lp_content_area_class' );

if(!function_exists('reptro_lp_content_area_class')){
	function reptro_lp_content_area_class ( $class ) {

		if( is_post_type_archive('lp_course') || is_tax('course_category') ){

			$course_layout = cs_get_option('course_layout');

			if( $course_layout == 'right' ){
				$class = 'col-lg-9';
			}elseif ( $course_layout == 'left' ) {
				$class = 'col-lg-9 order-1';
			}elseif( $course_layout = 'full_width' ){
				$class = 'col-lg-12';
			}
		}

		if( is_singular( 'lp_course' ) ){
			
			$single_course_layout = cs_get_option('single_course_layout');

			if( $single_course_layout == 'right' ){
				$class = 'col-lg-9';
			}elseif ( $single_course_layout == 'left' ) {
				$class = 'col-lg-9 order-1';
			}elseif( $single_course_layout = 'full_width' ){
				$class = 'col-lg-12';
			}
		}

		return $class;
	}
}

/**
 * Widget area class
 */

add_filter( 'reptro_widget_area_class', 'reptro_lp_widget_area_class' );
if(!function_exists('reptro_lp_widget_area_class')){
	function reptro_lp_widget_area_class ( $class ) {

		if( is_post_type_archive('lp_course') || is_tax('course_category') ){

			$course_layout = cs_get_option('course_layout');

			if( $course_layout == 'right' ){
				$class = 'col-lg-3';
			}elseif ( $course_layout == 'left' ) {
				$class = 'col-lg-3 order-2';
			}elseif( $course_layout = 'full_width' ){
				$class = '';
			}
		}

		if( is_singular( 'lp_course' ) ){
			
			$single_course_layout = cs_get_option('single_course_layout');

			if( $single_course_layout == 'right' ){
				$class = 'col-lg-3';
			}elseif ( $single_course_layout == 'left' ) {
				$class = 'col-lg-3 order-2';
			}elseif( $single_course_layout = 'full_width' ){
				$class = '';
			}
		}
		
		return $class;

	}
}



/**
 * Before Course Header
 */

if(!function_exists('reptro_before_course_header')){
	function reptro_before_course_header(){
		?>
			<div class="xt-single-course-header clearfix">
		<?php
	}
}

/**
 * Course instructor
 */

if(!function_exists('reptro_course_instructor')){
	function reptro_course_instructor(){
		?>
			<div class="course-author" itemscope="" itemtype="http://schema.org/Person">
				<?php printf( get_avatar( get_the_author_meta( 'ID' ), 40 ) ); ?>	
				<div class="author-contain">
					<label itemprop="jobTitle"><?php esc_html_e( 'Teacher', 'reptro' ); ?></label>
					<div class="value" itemprop="name">
						<a href="<?php echo esc_url( reptro_author_profile_url( get_the_author_meta('ID') ) ); ?>"><?php esc_html( the_author() ); ?></a>
					</div>
				</div>
			</div>
		<?php
	}
}

/**
 * Course category
 */

if(!function_exists('reptro_course_category')){
	function reptro_course_category(){
		if( $first_cat = get_post_first_category( get_the_id(), 'course_category' ) ):
			?>
				<div class="course-categories">
					<label><?php esc_html_e( 'Categories', 'reptro' ); ?></label>
					<div class="value">
						<span class="cat-links">
							<?php echo wp_kses_post( $first_cat );?>
						</span>
					</div>
				</div>
			<?php
		endif;
	}
}

/**
 * Course Payment landing
 */

if(!function_exists('reptro_course_payment_landing')){
	function reptro_course_payment_landing(){
		?>
		<div class="course-payment">
			<?php learn_press_course_price(); ?>
			<?php learn_press_course_buttons(); ?>
		</div>
		<?php
	}
}

/**
 * Course Payment learning
 */

if(!function_exists('reptro_course_payment_learning')){
	function reptro_course_payment_learning(){
		?>
		<div class="course-payment">
			<?php learn_press_course_buttons(); ?>
		</div>
		<?php
	}
}


/**
 * After Course Header
 */

if(!function_exists('reptro_after_course_header')){
	function reptro_after_course_header(){
		?>
			</div>
		<?php
	}
}


/**
 * course author profile link
 */

if( !function_exists('reptro_author_profile_url') ){
	function reptro_author_profile_url( $author_id ){
		$output = '';
		$output .= home_url( '/' );

		$learn_press_profile_page_id = get_option( 'learn_press_profile_page_id' );
		$profile_page = get_post($learn_press_profile_page_id); 
		$profile_page_slug = $profile_page->post_name;
		if($profile_page_slug ){
			$output .= $profile_page_slug.'/';
		}

		$author_url = get_the_author_meta( 'user_login', $author_id);
		if($author_url){
			$output .= $author_url.'/';
		}

		return $output;
	}
}


/**
 * Get post first category / tag
 */

if( !function_exists('get_post_first_category') ){
	function get_post_first_category ( $post_id, $taxonomy = null ){
		if( !$post_id ){
			$post_id = get_the_id();
		}

		if( $post_id &&  $taxonomy ){
			$terms = get_the_terms( $post_id, $taxonomy );
			$term_name = $term_link = '';
			if( $terms && !empty($terms) ){
				$term = array_shift($terms);
				$term_name = $term->name;
				$term_link = get_term_link($term->term_id);
			}
		}

		if( $term_name && $term_link ){
			return sprintf( '<a href="%s">%s</a>', esc_url( $term_link ), esc_html( $term_name ) );
		}else{
			return false;
		}
	}
}


/**
 * Course loop inner start
 */

if(!function_exists('reptro_learn_press_before_courses_loop_item')){
	function reptro_learn_press_before_courses_loop_item(){
		echo '<div class="reptro-course-item-inner">';
	}
}


/**
 * Course excerpt
 */

if(!function_exists('reptro_learn_press_course_excerpt')){
	function reptro_learn_press_course_excerpt( $type ){
		
		if( $type == 'list' ){
			printf( '<div class="reptro-course-item-short-description">%s</div>', wpautop( get_the_excerpt() ) );
		}

	}
}


/**
 * Course loop inner end
 */

if(!function_exists('reptro_learn_press_after_courses_loop_item')){
	function reptro_learn_press_after_courses_loop_item(){
		echo '</div>';
	}
}



/**
 * Course details button
 */


if(!function_exists('reptro_learn_press_course_loop_item_button')){
	function reptro_learn_press_course_loop_item_button() {
		?>
			<div class="reptro-course-details-btn">
				<?php printf( '<a class="btn btn-fill btn-lg" href="%s">%s</a>', esc_url( get_the_permalink() ), esc_html__( 'Details', 'reptro' ) ); ?>
			</div>
		<?php
	}
}


/**
 * EP Display rating stars
 */

if(!function_exists('reptro_print_rating')){
	function reptro_print_rating( $rate ) {

		?>
		<div class="xt-review-stars-wrapper">
			<ul class="xt-review-stars">
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
			</ul>
			<ul class="xt-review-stars xt-fill" style="<?php echo esc_attr( 'width: ' . ( $rate * 20 ) . '%' ) ?>">
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
				<li><span class="fa fa-star"></span></li>
			</ul>
		</div>
		<?php
	}
}


/**
 * Display course ratings
 */

if(!function_exists('reptro_course_ratings')){
	function reptro_course_ratings() {

		if ( !class_exists('LP_Addon_Course_Review_Preload') ) {
			return;
		}
		$course_id   = get_the_ID();
		$course_rate = learn_press_get_course_rate( $course_id );
		$ratings     = learn_press_get_course_rate_total( $course_id );
		?>
		<div class="xt-course-review">
			<?php if( is_singular( 'lp_course' ) ): ?>
				<label><?php esc_html_e( 'Review', 'reptro' ); ?></label>
			<?php endif; ?>

			<?php reptro_print_rating( $course_rate ); ?>
			<span><?php $ratings ? printf( _n( '(%1$s review)', '(%1$s reviews)', $ratings, 'reptro' ), number_format_i18n( $ratings ) ) : esc_html_e( '(0 review)', 'reptro' ); ?></span>
		</div>
		<?php
	}
}


/**
 * Output the thumbnail of the course within loop
 */

if ( ! function_exists( 'reptro_learn_press_courses_loop_item_thumbnail' ) ) {

	function reptro_learn_press_courses_loop_item_thumbnail() {
		$show_course_details_button = cs_get_option( 'show_course_details_button' );

		$course = LP_Global::course();

		if ( $course->get_origin_price() != $course->get_price() ) {
			printf('<span class="reptro-course-loop-sale">%s</span>', apply_filters( 'reptro_course_sale_text', esc_html__( 'Sale', 'reptro' ) ));
		}

		echo '<div class="reptro-course-loop-thumbnail-area">';
			if( $show_course_details_button == true ){
				reptro_learn_press_course_loop_item_button();
			}
			learn_press_get_template( 'loop/course/thumbnail.php' );
			do_action( 'reptro_learn_press_courses_loop_item_price_rating' );
		echo '</div>';
	}
}


add_action( 'reptro_learn_press_courses_loop_item_price_rating', 'reptro_add_learn_press_courses_loop_item_price_rating' );

function reptro_add_learn_press_courses_loop_item_price_rating(){

	$show_course_rating = cs_get_option( 'show_course_rating' );
	$show_course_price  = cs_get_option( 'show_course_price' );

	echo '<div class="reptro-course-loop-price-and-rating">';
		if( $show_course_price == true ){
			learn_press_get_template( 'loop/course/price.php' );
		}
		if ( class_exists('LP_Addon_Course_Review_Preload') && $show_course_rating == true ) {
			reptro_course_ratings();
		}
	echo '</div>';
}

/**
 * Single Course Information
 */


if(!function_exists('reptro_course_info')){
	function reptro_course_info(){
		global $post;
		$course 		= learn_press_get_course( $post->ID );
		$max_students	= $course->get_max_students();
		$duration 		= get_post_meta( $post->ID, '_lp_duration', true );
		$skill_level 	= reptro_get_post_meta( '_course_side_options', 'skill-level', 'all', true );
		$language 		= reptro_get_post_meta( '_course_side_options', 'language', 'English', true );
		$retake_count 	= get_post_meta( $post->ID, '_lp_retake_count', true );
		if( !$retake_count ){
			$retake_count = esc_html__( 'N/A', 'reptro' );
		}
		?>
			<?php do_action( 'reptro_before_course_info' ); ?>

			<h3><?php echo apply_filters( 'reptro_course_info_title', esc_html__( 'Course Features', 'reptro' ) ); ?></h3>

			<ul>
				<li><i class="fa fa-users" aria-hidden="true"></i><span class="course-info-label"><?php esc_html_e('Students', 'reptro' );?></span><span class="course-info-value"><?php echo esc_html( learn_press_course_students() ); ?></span></li>

				<?php if( $max_students ): ?>
				<li><i class="fa fa-check" aria-hidden="true"></i><span class="course-info-label"><?php esc_html_e('Max Students', 'reptro' );?></span><span class="course-info-value"><?php echo esc_html( $max_students ); ?></span></li>
				<?php endif; ?>

				<li><i class="fa fa-clock-o" aria-hidden="true"></i><span class="course-info-label"><?php esc_html_e('Duration', 'reptro' );?></span><span class="course-info-value"><?php echo esc_html( $duration );?></span></li>
				<li><i class="fa fa-level-up" aria-hidden="true"></i><span class="course-info-label"><?php esc_html_e('Skill level', 'reptro' );?></span><span class="course-info-value"><?php echo esc_html( $skill_level ); ?></span></li>
				<li><i class="fa fa-language" aria-hidden="true"></i><span class="course-info-label"><?php esc_html_e('Language', 'reptro' );?></span><span class="course-info-value"><?php echo esc_html( $language ); ?></span></li>
				<li><i class="fa fa-undo" aria-hidden="true"></i><span class="course-info-label"><?php esc_html_e('Re-take course', 'reptro' );?></span><span class="course-info-value"><?php echo esc_html( $retake_count ); ?></span></li>
				<?php do_action( 'reptro_course_info_item' )?>	
			</ul>

			<?php do_action( 'reptro_after_course_info' )?>
		<?php
	}
}


/**
 * LP breadcrumb delimiter
 */

add_filter( 'learn_press_breadcrumb_defaults', 'reptro_learn_press_breadcrumb_delimiter' );

if( !function_exists('reptro_learn_press_breadcrumb_delimiter') ){
	function reptro_learn_press_breadcrumb_delimiter( $args ){

		if( is_rtl() ){
			$args['delimiter'] = '<i class="fa fa-caret-left" aria-hidden="true"></i>';
		}else{
			$args['delimiter'] = '<i class="fa fa-caret-right" aria-hidden="true"></i>';
		}

		return $args;
	}
}

/**
 * Related Courses
 */

if(!function_exists('reptro_relative_courses')){
	function reptro_relative_courses(){
		global $post;
		$course_category 	= $course_tag = array();
		$course_categories 	= $course_tags = '';
	    $categories 		= wp_get_post_terms( $post->ID, 'course_category' );
	    $tags 				= wp_get_post_terms( $post->ID, 'course_tag' );

	    if( $categories && !empty($categories) ){
	    	foreach ( $categories as $category ) {
	    		$course_category[] = $category->term_id;
	    	}
	    	$course_categories = implode(', ', $course_category);
	    }

	    if( $tags && !empty($tags) ){
	    	foreach ( $tags as $tag ) {
	    		$course_tag[] = $tag->term_id;
	    	}
	    	$course_tags = implode(', ', $course_tag);
	    }

		echo do_shortcode( '[schooling_course autoplay="yes" shortcode_title="'.esc_html__( 'Related Courses', 'reptro' ).'" type="slider" items="3" desktopsmall="2" not_in="'. $post->ID .'" navigation="yes" pagination="no" all_courses_btn="no"]' );

	}
}


/**
 * course archive page course grid columns
 */

add_filter( 'reptro_course_archive_grid_column', 'reptro_course_archive_grid_column_setup' );

if(!function_exists('reptro_course_archive_grid_column_setup')){
	function reptro_course_archive_grid_column_setup( $classes ){

			$course_columns = cs_get_option('course_columns');
			$classes 		= array();

			switch ($course_columns) {
				case '2':
				$classes[] = 'col-lg-6';
					break;

				case '3':
				$classes[] = 'col-lg-4';
					break;	

				case '4':
				$classes[] = 'col-lg-3';
					break;

				case '6':
				$classes[] = 'col-lg-2';
					break;				
				
				default:
				$classes[] = 'col-lg-3';
					break;
			}
		

		return $classes;
	}
}


/**
 * Course Category image / icon column
 */

add_filter( 'manage_edit-course_category_columns', 'reptro_colurse_cat_columns_head', 10, 3 );
add_filter( 'manage_course_category_custom_column', 'reptro_colurse_cat_columns_content_taxonomy', 10, 3 );

/* Column Head */

if( !function_exists('reptro_colurse_cat_columns_head') ){
	function reptro_colurse_cat_columns_head( $defaults ) {
	    $defaults['reptro_cat_icons']  	= esc_html__( 'Icon', 'reptro' );
	    $defaults['reptro_cat_img']  	= esc_html__( 'Image', 'reptro' );
	    return $defaults;
	}
}


/* Column Content */

if( !function_exists('reptro_colurse_cat_columns_content_taxonomy') ){
	function reptro_colurse_cat_columns_content_taxonomy($c, $column_name, $term_id) {
	    if ($column_name == 'reptro_cat_icons') {
	    	$output 	= '';
	    	$term_data 	= get_term_meta( $term_id, '_xt_course_category_options', true );


	        if( is_array($term_data) && !empty($term_data) && array_key_exists('_xt_course_cat_icon', $term_data) ){
	        	if( $term_data['_xt_course_cat_icon'] ){
	        		$output = wp_get_attachment_image( $term_data['_xt_course_cat_icon'], array( 40, 40 ) );
	        	}
	        }

	        return $output;
	    }

	    if ($column_name == 'reptro_cat_img') {
	    	$output 	= '';
	    	$term_data 	= get_term_meta( $term_id, '_xt_course_category_options', true );


	        if( is_array($term_data) && !empty($term_data) && array_key_exists('_xt_course_cat_img', $term_data) ){
	        	if( $term_data['_xt_course_cat_img'] ){
	        		$output = wp_get_attachment_image( $term_data['_xt_course_cat_img'], array( 40, 40 ) );
	        	}
	        }

	        return $output;
	    }
	}
}