<?php

/**
 * Content area class
 */

add_filter( 'reptro_content_area_class', 'reptro_ld_content_area_class' );

if(!function_exists('reptro_ld_content_area_class')){
	function reptro_ld_content_area_class ( $class ) {

		if( is_post_type_archive('sfwd-courses') || is_tax('ld_course_category') ){

			$course_layout = cs_get_option('course_layout');

			if( $course_layout == 'right' ){
				$class = 'col-lg-9';
			}elseif ( $course_layout == 'left' ) {
				$class = 'col-lg-9 order-2';
			}elseif( $course_layout = 'full_width' ){
				$class = 'col-lg-12';
			}
		}

		if( is_singular( 'sfwd-courses' ) || is_singular( 'sfwd-quiz' ) || is_singular( 'sfwd-lessons' ) || is_singular( 'sfwd-topic' ) ){
			
			$single_course_layout = cs_get_option('single_course_layout');

			if( $single_course_layout == 'right' ){
				$class = 'col-lg-9';
			}elseif ( $single_course_layout == 'left' ) {
				$class = 'col-lg-9 order-2';
			}elseif( $single_course_layout = 'full_width' ){
				$class = 'col-lg-12';
			}
		}

		return $class;
	}
}

/**
 * Widget area class
 */

add_filter( 'reptro_widget_area_class', 'reptro_ld_widget_area_class' );

if(!function_exists('reptro_ld_widget_area_class')){
	function reptro_ld_widget_area_class ( $class ) {

		if( is_post_type_archive('sfwd-courses') || is_tax('ld_course_category') ){

			$course_layout = cs_get_option('course_layout');

			if( $course_layout == 'right' ){
				$class = 'col-lg-3';
			}elseif ( $course_layout == 'left' ) {
				$class = 'col-lg-3 order-1';
			}elseif( $course_layout = 'full_width' ){
				$class = '';
			}
		}

		if( is_singular( 'sfwd-courses' ) || is_singular( 'sfwd-quiz' ) || is_singular( 'sfwd-lessons' ) || is_singular( 'sfwd-topic' ) ){
			
			$single_course_layout = cs_get_option('single_course_layout');

			if( $single_course_layout == 'right' ){
				$class = 'col-lg-3';
			}elseif ( $single_course_layout == 'left' ) {
				$class = 'col-lg-3 order-1';
			}elseif( $single_course_layout = 'full_width' ){
				$class = '';
			}
		}
		
		return $class;

	}
}