<?php

/**
 * reptro WooCommerce Functions
 * Author: XooThemes
 * Since : 1.0
 */

/**
 * Adding WooCommerce 3.0 gallery and Zoom Support
 */

if(!function_exists('reptro_woocommerce_theme_setup')){
	function reptro_woocommerce_theme_setup() {
		$xt_need_woo_zoom 				= cs_get_option('xt_need_woo_zoom');
		$xt_need_woo_lightbox 			= cs_get_option('xt_need_woo_lightbox');
		$xt_need_woo_lightbox_slider 	= cs_get_option('xt_need_woo_lightbox_slider');

		if( $xt_need_woo_zoom == true ){
			add_theme_support( 'wc-product-gallery-zoom' );
		}
		if( $xt_need_woo_lightbox == true ){
			add_theme_support( 'wc-product-gallery-lightbox' );
		}
		if( $xt_need_woo_lightbox_slider == true ){
			add_theme_support( 'wc-product-gallery-slider' );
		}
	}
}

add_action( 'after_setup_theme', 'reptro_woocommerce_theme_setup' );

/**
 * Enqueue WooCommerce scripts and styles.
 */

if(!function_exists('reptro_woocommerce_scripts')){
	function reptro_woocommerce_scripts() {

		wp_enqueue_style( 'reptro-woocommerce-main', get_template_directory_uri() . '/assets/css/woocommerce-main.css', array(), '1.0' );
	}
}
add_action( 'wp_enqueue_scripts', 'reptro_woocommerce_scripts' );



/**
 * Check WooCommerce
 */

if ( ! function_exists( 'reptro_is_woocommerce_activated' ) ) {
	function reptro_is_woocommerce_activated() {
		if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
	}
}


/**
 * Content area class
 */

add_filter( 'reptro_content_area_class', 'reptro_wocommerce_contet_area_class' );
if(!function_exists('reptro_wocommerce_contet_area_class')){
	function reptro_wocommerce_contet_area_class ( $class ) {

		if( is_singular( 'product' ) ){
			$xt_product_layout 	= cs_get_option('xt_product_layout', 'right');

			if( $xt_product_layout == 'right' ){
				$class = 'col-lg-9';
			}elseif ( $xt_product_layout == 'left' ) {
				$class = 'col-lg-9 order-2';
			}elseif( $xt_product_layout = 'full_width' ){
				$class = 'col-lg-12';
			}
		}

		if( is_shop() || is_woocommerce() ){

			$xt_shop_layout = cs_get_option( 'xt_shop_layout', 'right' );

			if( $xt_shop_layout == 'right' ){
				$class = 'col-lg-9';
			}elseif ( $xt_shop_layout == 'left' ) {
				$class = 'col-lg-9 order-2';
			}elseif( $xt_shop_layout = 'full_width' ){
				$class = 'col-lg-12';
			}

		}

		return $class;
	}
}


/**
 * Widget area class
 */

add_filter( 'reptro_widget_area_class', 'reptro_woocomerce_widget_area_class' );
if(!function_exists('reptro_woocomerce_widget_area_class')){
	function reptro_woocomerce_widget_area_class ( $class ) {

		if( is_singular( 'product' ) ){

			$xt_product_layout 	= cs_get_option('xt_product_layout');

			if( $xt_product_layout == 'right' ){
				$class = 'col-lg-3';
			}elseif ( $xt_product_layout == 'left' ) {
				$class = 'col-lg-3 order-1';
			}elseif( $xt_product_layout = 'full_width' ){
				$class = '';
			}

		}

		if( is_shop() || is_woocommerce() ){

			$xt_shop_layout = cs_get_option( 'xt_shop_layout', 'right' );

			if( $xt_shop_layout == 'right' ){
				$class = 'col-lg-3';
			}elseif ( $xt_shop_layout == 'left' ) {
				$class = 'col-lg-3 order-1';
			}elseif( $xt_shop_layout = 'full_width' ){
				$class = '';
			}

		}

		return $class;
	}
}


/**
 * Page main content inner class
 */

add_filter( 'reptro_main_content_inner', 'reptro_wc_page_main_content_inner_class' );

if( !function_exists('reptro_wc_page_main_content_inner_class') ){
	function reptro_wc_page_main_content_inner_class( $class ){
		if ( is_woocommerce() ){
			$class = ' row';
		}

		return $class;
	}
}


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

if(!function_exists('reptro_woocommerce_widgets_init')){
	function reptro_woocommerce_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'WooCommerce Shop', 'reptro' ),
			'id'            => 'woocommerce_shop',
			'description'   => esc_html__( 'Add widgets here. It will be shown to the WooCommerce shop and few other WooCommerce pages.', 'reptro' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
		register_sidebar( array(
			'name'          => esc_html__( 'WooCommerce Product', 'reptro' ),
			'id'            => 'woocommerce_product',
			'description'   => esc_html__( 'Add widgets here. It will be shown to the WooCommerce product page.', 'reptro' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
}

add_action( 'widgets_init', 'reptro_woocommerce_widgets_init' );



/**
 * Woo Page Title
 */


add_filter( 'xt_theme_page_title', 'reptro_woocommerce_page_title' );

if(!function_exists('reptro_woocommerce_page_title')){
	function reptro_woocommerce_page_title( $title ){

		$wc_page_title = woocommerce_page_title( false );

		if( is_shop() || is_singular( 'product' ) ){
			$title = woocommerce_page_title( false );

			if( $wc_page_title ){
				$title = $wc_page_title;
			}else {
				$title = esc_html__( 'Shop', 'reptro' );
			}
		}

		return $title;
	}
}


/**
 * Remove default Woo Page Title
 */

add_filter( 'woocommerce_show_page_title' , 'reptro_woo_hide_page_title' );

if(!function_exists('reptro_woo_hide_page_title')){
	function reptro_woo_hide_page_title() {
		return false;
	}
}


/**
 * Shop product columns
 */

add_filter('loop_shop_columns', 'reptro_woocommerce_product_loop_columns');

if (!function_exists('reptro_woocommerce_product_loop_columns')) {
	function reptro_woocommerce_product_loop_columns() {
		$xt_shop_loop_column = cs_get_option( 'xt_shop_loop_column', 3 );
		return $xt_shop_loop_column;
	}
}

/**
 * Adding column number to body class
 */

add_filter('body_class', 'reptro_woocommerce_body_class');

if(!function_exists('reptro_woocommerce_body_class')){
	function reptro_woocommerce_body_class($classes) {
		$xt_shop_loop_column = cs_get_option( 'xt_shop_loop_column', 3 );

	    if ( is_woocommerce()) {
	        $classes[] = 'xt-product-columns-'.$xt_shop_loop_column;
	    }
	    return $classes;
	}
}

/**
 * Shop filter wrapper
 */

add_action( 'woocommerce_before_shop_loop', 'reptro_woocommerce_shop_filter_wrapper_start', 10 );
add_action( 'woocommerce_before_shop_loop', 'reptro_woocommerce_shop_filter_wrapper_end', 40 );

if(!function_exists('reptro_woocommerce_shop_filter_wrapper_start')){
	function reptro_woocommerce_shop_filter_wrapper_start(){
		echo '<div class="xt-woocommerce-shop-filter-wrapper clearfix">';
	}
}

if(!function_exists('reptro_woocommerce_shop_filter_wrapper_end')){
	function reptro_woocommerce_shop_filter_wrapper_end(){
		echo '</div>';
	}
}

/**
 * Shop loop item wrapper
 */

add_action( 'woocommerce_before_shop_loop_item_title', 'reptro_woocommerce_shop_loop_wrapper_start', 20 );

if(!function_exists('reptro_woocommerce_shop_loop_wrapper_start')){
	function reptro_woocommerce_shop_loop_wrapper_start(){
		echo '<div class="xt-product-wrapper-inner">';
	}
}

add_action( 'woocommerce_after_shop_loop_item', 'reptro_woocommerce_shop_loop_wrapper_end', 20 );

if(!function_exists('reptro_woocommerce_shop_loop_wrapper_end')){
	function reptro_woocommerce_shop_loop_wrapper_end(){
		echo '</div>';
	}
}

/**
 * Shop loop price and review wrapper
 */

add_action( 'woocommerce_after_shop_loop_item', 'reptro_woocommerce_shop_price_wrapper_start', 5 );

if(!function_exists('reptro_woocommerce_shop_price_wrapper_start')){
	function reptro_woocommerce_shop_price_wrapper_start(){
		echo '<div class="xt-price-rating-wrapper">';
	}
}

add_action( 'woocommerce_after_shop_loop_item', 'reptro_woocommerce_shop_price_wrapper_end', 6 );

if(!function_exists('reptro_woocommerce_shop_price_wrapper_end')){
	function reptro_woocommerce_shop_price_wrapper_end(){
		echo '</div>';
	}
}


/**
 * Move the loop price
 */

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_price', 5 );

/**
 * Move the loop review
 */

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_rating', 5 );

/**
 * Move the loop link close
 */

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 15 );


add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 25 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 10 );

/**
 * Related products
 */

add_filter( 'woocommerce_output_related_products_args', 'reptro_woocommerce_related_products_args' );

if(!function_exists('reptro_woocommerce_related_products_args')){
	function reptro_woocommerce_related_products_args( $args ) {
		$args['posts_per_page'] = cs_get_option( 'xt_related_per_page', 3 );
		$args['columns'] = cs_get_option( 'xt_related_loop_column', 3 );

		return $args;
	}
}


/**
 * Upsell products
 */

add_filter( 'woocommerce_upsell_display_args', 'reptro_woocommerce_upsell_products_args', 20 );

if(!function_exists('reptro_woocommerce_upsell_products_args')){
	function reptro_woocommerce_upsell_products_args( $args ) {

		$args['columns'] = cs_get_option( 'xt_related_loop_column', 3 );

		return $args;
	}
}


/**
 * Single product wrapper
 */

add_action( 'woocommerce_before_single_product_summary', 'reptro_woocommerce_single_product_wrapper_start', 5 );

if(!function_exists('reptro_woocommerce_single_product_wrapper_start')){
	function reptro_woocommerce_single_product_wrapper_start(){
		echo '<div class="xt-single-product-wrapper-inner clearfix">';
	}
}


add_action( 'woocommerce_after_single_product_summary', 'reptro_woocommerce_single_product_wrapper_end', 5 );

if(!function_exists('reptro_woocommerce_single_product_wrapper_end')){
	function reptro_woocommerce_single_product_wrapper_end(){
		echo '</div>';
	}
}


/**
 * Number of products in shop page
 */

add_filter( 'loop_shop_per_page', 'reptro_woocommerce_shop_number_of_products', 20 );

if(!function_exists('reptro_woocommerce_shop_number_of_products')){
	function reptro_woocommerce_shop_number_of_products( $number ){
		return cs_get_option('xt_shop_number_of_products');
	}
}