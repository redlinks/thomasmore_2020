<?php
/**
 * Template for displaying thumbnail of course within the loop.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/loop/course/thumbnail.php.
 *
 * @author  ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

$course 					= LP_Global::course();
$course_archive_image_size 	= reptro_cs_get_option( 'course_archive_image_size', 'reptro-course-thumb' );
?>

<div class="course-thumbnail">

	<?php echo wp_kses_post( $course->get_image( $course_archive_image_size ) ); ?>

</div>
