<?php
/**
 * The template for displaying single course content
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header( ); ?>

	<div id="primary" class="content-area <?php echo esc_attr( apply_filters( 'reptro_content_area_class', 'col-lg-8' ) ); ?>">
		<main id="main" class="site-main">
			<?php while ( have_posts() ) : the_post();?>

				<?php get_template_part( 'template-parts/content', 'single_course' ); ?>

			<?php endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar('single_course');
get_footer();
