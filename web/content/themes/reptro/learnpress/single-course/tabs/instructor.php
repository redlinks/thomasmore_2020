<?php
/**
 * Template for displaying instructor of single course.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/single-course/instructor.php.
 *
 * @author   ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

$course 		= LP_Global::course();
$description 	= $course->get_author()->get_description();
?>

<div class="course-author">

    <h3><?php esc_html_e( 'Instructor', 'reptro' ); ?></h3>

	<?php do_action( 'learn-press/before-single-course-instructor' ); ?>

	<div class="row">
		<div class="xt-author-avatar col-sm-2 col-xs-2">
			<?php echo wp_kses_post( $course->get_instructor()->get_profile_picture( '', 110 ) ); ?>
		</div>
		<div class="xt-author-content col-sm-10 col-xs-10">
			<?php echo wp_kses_post( $course->get_instructor_html() ); ?>
		    <?php if( $description ): ?>
                <div class="author-bio" itemprop="description">
					<?php echo wpautop( esc_html( $description ) ); ?>
                </div>
            <?php endif; ?>
		</div>
	</div>

	<?php do_action( 'learn-press/after-single-course-instructor' ); ?>

</div>