<?php
/**
 * Template for displaying overview tab of single course.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/single-course/tabs/overview.php.
 *
 * @author  ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();
?>

<?php 
	global $course; 
	$show_course_info = cs_get_option( 'show_course_info' );
?>

<div class="row">
	<div class="xt-course-content <?php echo esc_attr( $show_course_info == true ? 'col-lg-8' : '' ); ?>">
		<div class="course-description" id="learn-press-course-description">

			<?php
			/**
			 * @deprecated
			 */
			do_action( 'learn_press_begin_single_course_description' );

			/**
			 * @since 3.0.0
			 */
			do_action( 'learn-press/before-single-course-description' );

			echo wp_kses_post( $course->get_content() );

			/**
			 * @since 3.0.0
			 */
			do_action( 'learn-press/after-single-course-description' );

			/**
			 * @deprecated
			 */
			do_action( 'learn_press_end_single_course_description' );
			?>

		</div>
	</div>
	<?php if( $show_course_info == true ): ?>
		<div class="xt-course-info col-lg-4">
			<?php reptro_course_info() ?>
		</div>
	<?php endif; ?>
</div>