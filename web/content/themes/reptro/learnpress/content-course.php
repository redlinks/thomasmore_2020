<?php
/**
 * Template for displaying course content within the loop.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/content-course.php
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

if( !isset($type) ){
    $type = '';
}

$user = LP_Global::user();


$loop_classes = array();

if( !isset($column) ){
    $column = apply_filters( 'reptro_course_archive_grid_column', array('col-lg-4') );
}

if( !empty($column) ){
    $loop_classes = array_merge( $loop_classes, $column );
}

// isotope category classes
$categories_as_class = reptro_isotope_categories_as_class( 'course_category' );

if( !empty($categories_as_class) && isset($category_filtering) && $category_filtering == 'yes' && $type == 'grid' ){
    $loop_classes  = array_merge( $loop_classes, $categories_as_class );
}

$item_tag = 'li';
if( isset($type) && $type == 'slider' ){
    $item_tag = 'div';
}

?>

<<?php echo esc_html( $item_tag ); ?> id="post-<?php the_ID(); ?>" <?php post_class( apply_filters( 'reptro_course_loop_classes', $loop_classes ) ); ?>>

	<?php
    // @deprecated
    do_action( 'learn_press_before_courses_loop_item' );

    // @since 3.0.0
    do_action( 'learn-press/before-courses-loop-item' );
    ?>

    <a href="<?php the_permalink(); ?>" class="course-permalink">

		<?php
        // @deprecated
        do_action( 'learn_press_courses_loop_item_title' );

        // @since 3.0.0
        do_action( 'learn-press/courses-loop-item-title' );
        ?>

    </a>

	<?php

    // @since 3.0.0
	do_action( 'learn-press/after-courses-loop-item', $type );

	// @deprecated
    do_action( 'learn_press_after_courses_loop_item' );

    ?>

</<?php echo esc_html( $item_tag ); ?>>