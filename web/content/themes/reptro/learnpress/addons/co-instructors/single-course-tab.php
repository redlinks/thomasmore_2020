<?php
/**
 * Template for displaying instructor tab in single course page.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/addons/co-instructor/single-course-tab.php.
 *
 * @author ThimPress
 * @package LearnPress/Co-Instructor/Templates
 * @version 3.0.0
 */

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( $instructors ) {

    printf( '<h3 class="xt-co-instructor-title">%s</h3>', esc_html__( 'Co Instructors', 'reptro' ) );

	foreach ( $instructors as $instructor ) {
		$lp_info      = get_the_author_meta( 'lp_info', $instructor );
		$link         = learn_press_user_profile_link( $instructor );
        $description  = get_the_author_meta( 'description', $instructor );
		?>
        <div class="list-course-instructors">
            <div class="author-wrapper row">
                <div class="author-avatar col-sm-2 col-xs-2">
					<?php echo get_avatar( $instructor, 110 ); ?>
                </div> 
                <div class="xt-author-content col-sm-10 col-xs-10">
                    <div class="author-bio">
                        <div class="author-top">
                            <a itemprop="url" class="name" href="<?php echo esc_url( $link ); ?>">
                                <span itemprop="name"><?php echo get_the_author_meta( 'display_name', $instructor ); ?></span>
                            </a>
    						<?php if ( isset( $lp_info['major'] ) && $lp_info['major'] ) : ?>
                                <p class="job" itemprop="jobTitle"><?php echo esc_html( $lp_info['major'] ); ?></p>
    						<?php endif; ?>
                        </div>

                    </div>
                    <?php if( $description ): ?>
                        <div class="author-description" itemprop="description">
        					<?php echo wpautop( esc_html( $description ) ); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
		<?php
	}
}