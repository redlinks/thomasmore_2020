<?php
/**
 * Template for displaying button to toggle course wishlist on/off.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/addons/wishlist/button.php.
 *
 * @author ThimPress
 * @package LearnPress/Wishlist/Templates
 * @version 3.0.0
 */

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

printf(
	'<button class="btn btn-fill btn-sm learn-press-course-wishlist learn-press-course-wishlist-button-%2$d wishlist-button %s" data-id="%s" data-nonce="%s" title="%s" data-text="%s">%s</button>',
	join( " ", $classes ),
	$course_id,
	wp_create_nonce( 'course-toggle-wishlist' ),
	$title,
	esc_html__( 'Processing...', 'reptro' ),
	$state == 'on' ? esc_html__( 'Remove from Wishlist', 'reptro' ) : esc_html__( 'Add to Wishlist', 'reptro' )
);