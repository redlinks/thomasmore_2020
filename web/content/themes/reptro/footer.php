<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reptro
 */
?>

<?php 
	if( is_page() ){
		$xt_disable_header_footer = reptro_get_post_meta( 'business_x_page_side_options', 'xt_disable_header_footer', false, true );
	}else{
		$xt_disable_header_footer = false;
	}
?>
			<?php do_action( 'reptro_after_content' ) ?>
		</div> <!-- site-content-inner -->
	</div><!-- #content -->

	<?php if( $xt_disable_header_footer == false ): ?>
		<footer id="colophon" class="xt-footer site-footer">
			<?php if ( is_active_sidebar( 'footer-widgets' ) ): ?>
				<div class="footer-widgets">
					<div class="<?php echo esc_attr( apply_filters( 'reptro_footer_widget_container_class', 'container' ) ); ?>">
						<div id="footer-wrapper">
							<div class="row">
								<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-widgets') ) : ?>
								<?php endif; ?>
							</div> <!-- end .row -->
						</div> <!-- end #footer-wrapper -->
					</div> <!-- end .container -->
				</div> <!-- end .footer-widgets -->
			<?php endif; ?>

			<?php do_action( 'reptro_footer_bottom_bar' ); ?>
		</footer><!-- #colophon -->
	<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
