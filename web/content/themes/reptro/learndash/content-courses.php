<?php
/**
 * Template part for displaying LearnDash Course Loop Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package reptro
 */

?>

<?php 
	
	$loop_classes = array('course');

	$column = apply_filters( 'reptro_course_archive_grid_column', array('col-lg-4') );

	if( !empty($column) ){
	    $loop_classes = array_merge( $loop_classes, $column );
	}

	// isotope category classes
	$categories_as_class = reptro_isotope_categories_as_class( 'ld_course_category' );

	if( !empty($categories_as_class) && isset($category_filtering) && $category_filtering == 'yes' && $type == 'grid' ){
	    $loop_classes  = array_merge( $loop_classes, $categories_as_class );
	}


	// LD Course Details 

	$course_id 	= learndash_get_course_id( get_the_id() );
	$meta 		= get_post_meta( $course_id, '_sfwd-courses', true );
	$price 		= $meta['sfwd-courses_course_price'];
	$price_type = $meta['sfwd-courses_course_price_type'];

?>

<li id="ld-course-<?php the_ID(); ?>" <?php post_class( apply_filters( 'reptro_course_loop_classes', $loop_classes ) ); ?>>
	<div class="reptro-course-loop-thumbnail-area">
		<div class="reptro-course-details-btn">
			<?php printf( '<a class="btn btn-fill btn-lg" href="%s">%s</a>', esc_url( learndash_get_step_permalink( get_the_ID(), $course_id ) ), esc_html__( 'Details', 'reptro' ) ); ?>
		</div>

		<?php if( has_post_thumbnail() ): ?>
		<div class="course-thumbnail"><?php the_post_thumbnail( 'reptro-course-thumb' ) ?></div>
		<?php endif; ?>

		<div class="reptro-course-loop-price-and-rating">
			<?php if( $price_type != 'free' && isset($price) ): ?>
				<div class="course-price"> <span class="price"><?php echo esc_html( $price ) ?></span></div>
			<?php endif; ?>
		</div>
	</div>

	<div class="reptro-course-item-inner">
		<a href="<?php echo esc_url( learndash_get_step_permalink( get_the_ID(), $course_id ) ) ?>" class="course-permalink"><h3 class="course-title"><?php the_title() ?></h3> </a>
		<div class="course-info"> 
			<?php echo reptro_get_the_excerpt( apply_filters( 'reptro_ld_course_excerpt_length', 50 ) ); ?>
		</div>
	</div>
</li>