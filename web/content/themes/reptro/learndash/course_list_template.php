<?php
/**
 * @package nmbs
 */
$col = empty($shortcode_atts["col"])? 3:intval($shortcode_atts["col"]);
$smcol = $col/1.5;
$col = empty($col)? 1:($col >= 12)? 12:$col;
$smcol = empty($smcol)? 1:($smcol >= 12)? 12:$smcol;
$col = intVal(12/$col);
$smcol = intVal(12/$smcol);

global $post; $post_id = $post->ID;

$course_id = $post_id;
$user_id   = get_current_user_id();

$enable_video = get_post_meta( $post->ID, '_learndash_course_grid_enable_video_preview', true );
$embed_code   = get_post_meta( $post->ID, '_learndash_course_grid_video_embed_code', true );
$button_text  = get_post_meta( $post->ID, '_learndash_course_grid_custom_button_text', true );

// Retrive oembed HTML if URL provided
if ( preg_match( '/^http/', $embed_code ) ) {
	$embed_code = wp_oembed_get( $embed_code, array( 'height' => 600, 'width' => 400 ) );
}

if ( isset( $shortcode_atts['course_id'] ) ) {
	$button_link = learndash_get_step_permalink( get_the_ID(), $shortcode_atts['course_id'] );
} else {
	$button_link = get_permalink();
}

$button_link = apply_filters( 'learndash_course_grid_custom_button_link', $button_link, $post_id );

$button_text = isset( $button_text ) && ! empty( $button_text ) ? $button_text : esc_html__( 'See more...', 'reptro' );
$button_text = apply_filters( 'learndash_course_grid_custom_button_text', $button_text, $post_id );

$options = get_option( 'sfwd_cpt_options' );
$currency_setting = class_exists( 'LearnDash_Settings_Section' ) ? LearnDash_Settings_Section::get_section_setting( 'LearnDash_Settings_Section_PayPal', 'paypal_currency' ) : null;
$currency = '';

if ( isset( $currency_setting ) || ! empty( $currency_setting ) ) {
	$currency = $currency_setting;
} elseif ( isset( $options['modules'] ) && isset( $options['modules']['sfwd-courses_options'] ) && isset( $options['modules']['sfwd-courses_options']['sfwd-courses_paypal_currency'] ) ) {
	$currency = $options['modules']['sfwd-courses_options']['sfwd-courses_paypal_currency'];
}

if ( class_exists( 'NumberFormatter' ) ) {
	
	$locale = get_locale();
	$number_format = new NumberFormatter( $locale . '@currency=' . $currency, NumberFormatter::CURRENCY );
	$currency = $number_format->getSymbol( NumberFormatter::CURRENCY_SYMBOL );
}

/**
 * Currency symbol filter hook
 * 
 * @param string $currency Currency symbol
 * @param int    $course_id
 */
$currency = apply_filters( 'learndash_course_grid_currency', $currency, $course_id );

$course_options = get_post_meta($post_id, "_sfwd-courses", true);
$price = $course_options && isset($course_options['sfwd-courses_course_price']) ? $course_options['sfwd-courses_course_price'] : esc_html__( 'Free', 'reptro' );
$price_type = $course_options && isset( $course_options['sfwd-courses_course_price_type'] ) ? $course_options['sfwd-courses_course_price_type'] : '';

if( array_key_exists('sfwd-courses_course_short_description', $course_options) ){
	$short_description = $course_options['sfwd-courses_course_short_description'];
}else{
	$short_description = '';
}

/**
 * Filter: individual grid class
 * 
 * @param int 	$course_id Course ID
 * @param array $course_options Course options
 * @var string
 */
$grid_class = apply_filters( 'learndash_course_grid_class', '', $course_id, $course_options );

$has_access   = sfwd_lms_has_access( $course_id, $user_id );
$is_completed = learndash_course_completed( $user_id, $course_id );

$price_text = '';

if ( is_numeric( $price ) && ! empty( $price ) ) {
	$price_format = apply_filters( 'learndash_course_grid_price_text_format', '{currency}{price}' );

	$price_text = str_replace(array( '{currency}', '{price}' ), array( $currency, $price ), $price_format );
} elseif ( is_string( $price ) && ! empty( $price ) ) {
	$price_text = $price;
} elseif ( empty( $price ) ) {
	$price_text = esc_html__( 'Free', 'reptro' );
}

$class       = 'ld_course_grid_price';
$ribbon_text = get_post_meta( $post->ID, '_learndash_course_grid_custom_ribbon_text', true );
$ribbon_text = isset( $ribbon_text ) && ! empty( $ribbon_text ) ? $ribbon_text : '';

if ( $has_access && ! $is_completed && $price_type != 'open' && empty( $ribbon_text ) ) {
	$class .= ' ribbon-enrolled';
	$ribbon_text = esc_html__( 'Enrolled', 'reptro' );
} elseif ( $has_access && $is_completed && $price_type != 'open' && empty( $ribbon_text ) ) {
	$class .= '';
	$ribbon_text = esc_html__( 'Completed', 'reptro' );
} elseif ( $price_type == 'open' && empty( $ribbon_text ) ) {
	if ( is_user_logged_in() && ! $is_completed ) {
		$class .= ' ribbon-enrolled';
		$ribbon_text = esc_html__( 'Enrolled', 'reptro' );
	} elseif ( is_user_logged_in() && $is_completed ) {
		$class .= '';
		$ribbon_text = esc_html__( 'Completed', 'reptro' );
	} else {
		$class .= ' ribbon-enrolled';
		$ribbon_text = '';
	}
} elseif ( $price_type == 'closed' && empty( $price ) ) {
	$class .= ' ribbon-enrolled';

	if ( is_numeric( $price ) ) {
		$ribbon_text = $price_text;
	} else {
		$ribbon_text = '';
	}
} else {
	if ( empty( $ribbon_text ) ) {
		$class .= ! empty( $course_options['sfwd-courses_course_price'] ) ? ' price_' . $currency : ' free';
		$ribbon_text = $price_text;
	} else {
		$class .= ' custom';
	}
}

/**
 * Filter: individual course ribbon text
 *
 * @param string $ribbon_text Returned ribbon text
 * @param int    $course_id   Course ID
 * @param string $price_type  Course price type
 */
$ribbon_text = apply_filters( 'learndash_course_grid_ribbon_text', $ribbon_text, $course_id, $price_type );

if ( '' == $ribbon_text ) {
	$class = '';
}

/**
 * Filter: individual course ribbon class names
 *
 * @param string $class     	 Returned class names
 * @param int    $course_id 	 Course ID
 * @param array  $course_options Course's options
 * @var string
 */
$class = apply_filters( 'learndash_course_grid_ribbon_class', $class, $course_id, $course_options );

?>
<div class="ld_course_grid col-sm-<?php echo esc_attr( $smcol );?> col-lg-<?php echo esc_attr( $col ); ?> <?php echo esc_attr( $grid_class ); ?>">
	<article id="post-<?php the_ID(); ?>" <?php post_class( array('course', (has_post_thumbnail() ? 'xt-ld-grid-has-thumbnail' : 'xt-ld-grid-no-thumbnail')) ); ?>>
		<div class="reptro-course-loop-thumbnail-area">
			<?php if ( $shortcode_atts['show_thumbnail'] == 'true' ) : ?>

				<?php if ( $post->post_type == 'sfwd-courses' ) : ?>
				<div class="reptro-course-details-btn"><a class="btn btn-fill btn-lg" role="button" href="<?php echo esc_url( $button_link ); ?>"><?php echo esc_attr( $button_text ); ?></a></div>
				<?php endif; ?>

				<?php if ( $post->post_type == 'sfwd-courses' ) : ?>
					<div class="reptro-course-loop-price-and-rating">
						<div class="course-price reptro-ld-<?php echo esc_attr( $class ); ?>"><span class="price"><?php echo esc_attr( $ribbon_text ); ?></span></div>	
					</div>
				<?php endif; ?>

				<div class="course-thumbnail">
					<?php if ( 1 == $enable_video && ! empty( $embed_code ) ) : ?>
					<div class="ld_course_grid_video_embed">
					<?php echo wp_kses_post( $embed_code ); ?>
					</div>
					<?php elseif( has_post_thumbnail() ) :?>
					<a href="<?php the_permalink(); ?>" rel="bookmark">
						<?php the_post_thumbnail('course-thumb'); ?>
					</a>
					<?php else :?>
					<a href="<?php echo esc_url( $button_link ); ?>" rel="bookmark">
						<img src="<?php echo plugins_url( 'no_image.jpg', LEARNDASH_COURSE_GRID_FILE); ?>"/>
					</a>
					<?php endif;?>
				</div>

			<?php endif; ?>
		</div>

		<?php if ( $shortcode_atts['show_content'] == 'true' ) : ?>
			
			<div class="caption reptro-course-item-inner">
				<a href="<?php echo esc_url( $button_link ); ?>">
					<h3 class="course-title"><?php the_title(); ?></h3>
				</a>
				<div class="reptro-ls-course-item-content">
					<?php if ( ! empty( $short_description ) ) : ?>
						<div class="entry-content"><?php echo do_shortcode( wp_specialchars_decode( $short_description ) ); ?></div>
					<?php endif; ?>
					<?php if ( isset( $shortcode_atts['progress_bar'] ) && $shortcode_atts['progress_bar'] == 'true' ) : ?>
						<?php echo do_shortcode( '[learndash_course_progress course_id="' . get_the_ID() . '" user_id="' . get_current_user_id() . '"]' ); ?>
					<?php endif; ?>
				</div>
			</div><!-- .entry-header -->
		<?php endif; ?>
	</article><!-- #post-## -->
</div>