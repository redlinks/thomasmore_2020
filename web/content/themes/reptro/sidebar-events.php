<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reptro
 */

$blog_layout = cs_get_option('blog_layout');

if ( ! is_active_sidebar( 'events_sidebar' ) ) {
	return;
}
?>

<?php if( $blog_layout != 'full_width' ):?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( apply_filters( 'reptro_widget_area_class', 'col-lg-4' ) ); ?>">
		<?php dynamic_sidebar( 'events_sidebar' ); ?>
	</aside><!-- #secondary -->
<?php endif;?>