<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package reptro
 */

$course_layout = cs_get_option('course_layout');

if ( ! is_active_sidebar( 'course' ) ) {
	return;
}
?>

<?php if( $course_layout != 'full_width' ):?>

	<aside id="secondary" class="widget-area <?php echo esc_attr( apply_filters( 'reptro_widget_area_class', 'col-lg-3' ) ); ?>">
		<?php dynamic_sidebar( 'course' ); ?>
	</aside><!-- #secondary -->
<?php endif;?>