<?php
/**
 * The template for displaying WooCommerce pages.
 *
 * @package reptro
 */

get_header(); ?>

	<div id="primary" class="content-area <?php echo esc_attr( apply_filters( 'reptro_content_area_class', 'col-lg-8' ) ); ?>">
		<main id="main" class="site-main reptro-woocommerce-page-content">

			<?php woocommerce_content(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
$xt_shop_layout 	= cs_get_option('xt_shop_layout');
$xt_product_layout 	= cs_get_option('xt_product_layout');

if( is_product() && $xt_product_layout != 'full_width' ){
	get_sidebar('woocommerce_product');
}elseif( !is_product() && $xt_shop_layout != 'full_width' ){
	get_sidebar('woocommerce_shop');
}

get_footer();