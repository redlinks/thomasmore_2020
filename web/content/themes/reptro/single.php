<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package reptro
 */

get_header(); ?>
	<?php 
		$blog_author_bio = cs_get_option( 'blog_author_bio' );
		$blog_post_nav = cs_get_option( 'blog_post_nav' );
	?>

	<div id="primary" class="content-area <?php echo esc_attr( apply_filters( 'reptro_content_area_class', 'col-lg-8' ) ); ?>">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();

			if( is_singular( 'sfwd-lessons' ) || is_singular( 'sfwd-quiz' ) ){
				get_template_part( 'template-parts/content', 'ld_content' );
			}else{
				get_template_part( 'template-parts/content', get_post_format() );
			}

			if( get_post_type() == 'post' && $blog_author_bio == true ){
				reptro_get_author_bio();
			}


			if( get_post_type() == 'post' && $blog_post_nav == true ){
				the_post_navigation(array(
				    'prev_text'=>esc_html__( 'Previous', 'reptro' ),
				    'next_text'=>esc_html__( 'Next', 'reptro' ),
				));
			}

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'reptro_single_get_sidebar' );
get_footer();
