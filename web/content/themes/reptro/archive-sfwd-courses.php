<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package reptro
 */

get_header(); ?>

	<div id="primary" class="content-area <?php echo esc_attr( apply_filters( 'reptro_content_area_class', 'col-lg-8' ) ); ?>">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */

			echo '<ul class="reptro-course-items reptro-ld-course-items reptro-course-grid row">';

			while ( have_posts() ) : the_post();

				get_template_part( 'learndash/content', 'courses' );

			endwhile;

			echo '</ul>';

			xt_wp_numeric_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar('course');
get_footer();
