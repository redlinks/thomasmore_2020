<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package reptro
 */

get_header(); ?>

	<div id="primary" class="content-area col-lg-10">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header text-center">
					<h1 class="reptro-section-title text-center margin-bottom-small margin-top-none section-title-font-size-xx_large"><span><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'reptro' ); ?></span></h1>
				</header><!-- .page-header -->

				<div class="page-content text-center">
					<div class="not-found-content">
						<p class="section-title-small"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'reptro' ); ?></p>
					</div>
					<div class="not-found-search search-inline">
						<?php get_search_form(); ?>
					</div>
					<div class="not-found-home back-top-home">
						<a class="btn btn-lg btn-fill" href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Back to Home', 'reptro' ); ?></a>
					</div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
