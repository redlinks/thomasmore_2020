<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A Font Icon select box.
 */

class XT_Elementor_Control_Icon extends Base_Data_Control {
	public function get_type() {
		return 'icon';
	}
	
	public static function get_icons() {
		$icons       = [];
		$icons_packs = apply_filters( 'xt_reptro_ei_icons_family', array( 'font-awesome', 'simple-line-icons', 'education-icons', 'high-school', 'scientific-study' ) );

		// Font Awesome
		if (in_array('font-awesome', $icons_packs)){
			$font_awesome_icons = require XT_REPTRO_ICONS_PLUGIN_DIR . '/icons/font-awesome/font-awesome.php';
			foreach ($font_awesome_icons as $icon) {
				$icons["fa fa-{$icon}"] = "fa-{$icon}";
			}
		}

		// Simple Line Icons
		if (in_array('simple-line-icons', $icons_packs)){

			$simple_line_icons = require XT_REPTRO_ICONS_PLUGIN_DIR . '/icons/simple-line-icons/simple-line-icons.php';

			foreach($simple_line_icons as $simple_line_icon){
			    $icons[$simple_line_icon] = $simple_line_icon;
			}
		}

		//Education Icons

		if (in_array('education-icons', $icons_packs)){

			$education_icons = require XT_REPTRO_ICONS_PLUGIN_DIR . '/icons/education-icons/education-icons.php';

			foreach($education_icons as $education_icon){
			    $icons[$education_icon] = $education_icon;
			}
		}

		//High School
		if (in_array('high-school', $icons_packs)){

			$highschool_icons = require XT_REPTRO_ICONS_PLUGIN_DIR . '/icons/high-school-set/highschool.php';

			foreach($highschool_icons as $highschool_icon){
			    $icons[$highschool_icon] = $highschool_icon;
			}
		}

		// Scientific
		if (in_array('scientific-study', $icons_packs)){

			$scientific_icons = require XT_REPTRO_ICONS_PLUGIN_DIR . '/icons/scientific-study/scientific.php';

			foreach($scientific_icons as $scientific_icon){
			    $icons[$scientific_icon] = $scientific_icon;
			}
		}

		return $icons;
	}

	protected function get_default_settings() {
		return [
			'icons' => self::get_icons(),
		];
	}
	public function content_template() {
		?>
		<div class="elementor-control-field">
			<label class="elementor-control-title">{{{ data.label }}}</label>
			<div class="elementor-control-input-wrapper">
				<select class="elementor-control-icon" data-setting="{{ data.name }}" data-placeholder="<?php _e( 'Select Icon', 'elementor-icons' ); ?>">
					<option value=""><?php _e( 'Select Icon', 'elementor-icons' ); ?></option>
					<# _.each( data.icons, function( option_title, option_value ) { #>
					<option value="{{ option_value }}">{{{ option_title }}}</option>
					<# } ); #>
				</select>
			</div>
		</div>
		<# if ( data.description ) { #>
		<div class="elementor-control-field-description">{{ data.description }}</div>
		<# } #>
		<?php
	}
}
add_action('elementor/controls/controls_registered', function($el) {
	$el->register_control('icon', new XT_Elementor_Control_Icon);
});