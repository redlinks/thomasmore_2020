<?php
/**
 * Plugin Name: Reptro Elementor Icons
 * Plugin URI:  https://wpbean.com
 * Description: Override Elementor's 'Icon' control to include custom icon packs. For Reptro themes.
 * Author: wpbean
 * Version: 1.0.1
 * Author URI:  https://wpbean.com
 * Text Domain: elementor-icons
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'XT_REPTRO_ICONS_PLUGIN_DIR', plugin_dir_path(__FILE__) );

/**
 * Localization
 */

function xt_reptro_icons_textdomain() {
	load_plugin_textdomain( 'elementor-icons', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'xt_reptro_icons_textdomain' );

/**
 * Requred files 
 */

if( defined('ELEMENTOR_VERSION') ){
	require_once dirname( __FILE__ ) . '/inc/get-icons.php';
}

/**
 * Enqueueing icons
 */

// ENQUEUE // Enqueueing Frontend stylesheet and scripts.
add_action( 'elementor/editor/after_enqueue_scripts', 'xt_reptro_enqueue_icons_css');
// FRONTEND // After Elementor registers all styles.
add_action( 'elementor/frontend/after_register_styles', 'xt_reptro_enqueue_icons_css' );
// EDITOR // Before the editor scripts enqueuing.
add_action( 'elementor/editor/before_enqueue_scripts', 'xt_reptro_enqueue_icons_css' );


function xt_reptro_enqueue_icons_css() {
	$icons_packs = apply_filters( 'xt_reptro_ei_icons_family', array( 'font-awesome', 'simple-line-icons', 'education-icons', 'high-school', 'scientific-study' ) );

	if (in_array('simple-line-icons', $icons_packs)){
    	wp_enqueue_style('simple-line-icons', plugins_url('/icons/simple-line-icons/css/simple-line-icons.css', __FILE__ ), array(), '1.0 ');
	}
	if (in_array('education-icons', $icons_packs)){
   		wp_enqueue_style('el-education-icons', plugins_url('/icons/education-icons/css/education-icons.css', __FILE__ ), array(), '1.0 ');
	}
	if (in_array('high-school', $icons_packs)){
    	wp_enqueue_style('high-school-set', plugins_url('/icons/high-school-set/css/highschool.css', __FILE__ ), array(), '1.0 ');
	}
	if (in_array('scientific-study', $icons_packs)){
	    wp_enqueue_style('scientific-study', plugins_url('/icons/scientific-study/css/scientific-study.css', __FILE__ ), array(), '1.0 ');
	}
}