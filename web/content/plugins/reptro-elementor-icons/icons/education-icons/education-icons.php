<?php

return [
	'edi-abacus' => 'edi-abacus',
	'edi-bell' => 'edi-bell',
	'edi-blackboard' => 'edi-blackboard',
	'edi-blackboard-1' => 'edi-blackboard-1',
	'edi-book' => 'edi-book',
	'edi-books' => 'edi-books',
	'edi-books-1' => 'edi-books-1',
	'edi-diploma' => 'edi-diploma',
	'edi-earth-globe' => 'edi-earth-globe',
	'edi-exam' => 'edi-exam',
	'edi-exam-1' => 'edi-exam-1',
	'edi-ink' => 'edi-ink',
	'edi-library' => 'edi-library',
	'edi-mortarboard' => 'edi-mortarboard',
	'edi-open-book' => 'edi-open-book',
	'edi-professor' => 'edi-professor',
	'edi-school-material' => 'edi-school-material',
	'edi-student' => 'edi-student',
	'edi-student-1' => 'edi-student-1',
	'edi-university' => 'edi-university',
];