<?php


/**
 * Get Sidebars for CS framework
 */

if( !function_exists('reptro_sidebars_list_on_option') ){
	function reptro_sidebars_list_on_option(){
		$result = array();

        foreach( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
            $result[ esc_attr( $sidebar['id'] ) ] = esc_html( $sidebar['name'] );      
        }
        
        unset($result['footer-widgets']);

        return $result;
	}
}