<?php

/**
 * PHP implode with key and value ( Owl carousel data attr )
 */

if( !function_exists('reptro_carousel_data_attr_implode') ){
	function reptro_carousel_data_attr_implode( $array ){
		
		foreach ($array as $key => $value) {

			if( isset($value) && $value != '' ){
				$output[] = $key . '=' . '"' . esc_attr( $value ) . '"' ;
			}
		}

        return implode( ' ', $output );
	}
}


/**
 * DropDown Category Walker
 */

if( !class_exists('Reptro_Dropdown_Category_Walker') ){
	class Reptro_Dropdown_Category_Walker extends Walker_Category {

	    /**
	     * Starts the list before the elements are added.
	     *
	     * Added for Flatsome theme issue fix
	     */
	    public function start_lvl( &$output, $depth = 0, $args = array() ) {
	        if ( 'list' != $args['style'] )
	            return;
	 
	        $indent = str_repeat("\t", $depth);
	        $output .= "$indent<ul class='reptro-course-category-dropdown dropdown-menu'>\n";
	    }


		public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
			/** This filter is documented in wp-includes/category-template.php */
			$cat_name = apply_filters(
				'list_cats',
				esc_attr( $category->name ),
				$category
			);

			$termchildren = get_term_children( $category->term_id, $category->taxonomy );

			// Don't generate an element if the category name is empty.
			if ( ! $cat_name ) {
				return;
			}

			$termname = 'reptro_cat_'.$category->term_id;

			if( count($termchildren)>0 ){
				$link = '<span>'. $cat_name . '<i class="fa fa-plus"></i></span>';
			}else{
				$link = '<span>'. $cat_name . '</span>';
			}


			if ( 'list' == $args['style'] ) {

				$output .= "\t<li";
				$css_classes = array(
					'reptro-category-item',
					'reptro-category-item-' . $category->term_id,
				);

	            if( count($termchildren)>0 ){
	                $css_classes[] =  'cat-item-have-child';
	            }

				if ( ! empty( $args['current_category'] ) ) {
					$_current_category = get_term( $args['current_category'], $category->taxonomy );
					if ( $category->term_id == $args['current_category'] ) {
						$css_classes[] = 'current-cat';
					} elseif ( $category->term_id == $_current_category->parent ) {
						$css_classes[] = 'reptro-current-cat-parent';
					}
				}

				$css_classes = implode( ' ', apply_filters( 'category_css_class', $css_classes, $category, $depth, $args ) );

				$output .=  ' class="' . $css_classes . '"';
				$output .= " data-filter=.". esc_attr( $termname ) .">$link\n";
			} else {
				$output .= "\t$link<br />\n";
			}
		}
	}
}


/**
 * Category filter for isotope
 */

if( !function_exists('reptro_category_filter_isotope') ){
	function reptro_category_filter_isotope( $taxonomy, $cat_include = array(), $cat_exclude = array() ){

		$terms_args = apply_filters( 'reptro_filter_terms_args', array(
			'taxonomy'	=> $taxonomy,
			'title_li'	=> '',
			'echo'		=> 0,
			'walker'    => new Reptro_Dropdown_Category_Walker,
		));

		$output 	= '';

		if( !empty($cat_exclude) && is_array($cat_exclude) ){
			$terms_args = array( 'exclude' => $cat_exclude );
		}

		if( !empty($cat_include) && is_array($cat_include) ){
			$terms_args = array( 'include' => $cat_include );
		}

		$terms = get_terms( $taxonomy, apply_filters( 'reptro_filter_terms_args', $terms_args ) );
		
        $output .= '<div class="reptro-category-items-filter">';
        $output .= '<ul class="reptro-category-items">';
        
        $output .= '<li class="reptro-category-item active" data-filter="*"><span>'. apply_filters( 'reptro_category_filter_all_text', esc_html__( 'All', 'xt-reptro-cpt-shortcode' ) ) .'</span></li>';

		// foreach ( $terms as $term ) {

  //           $termid = 'reptro_cat_' . esc_attr( $term->term_id );

		// 	$output .= '<li class="reptro-category-item" data-filter="' . '.' . esc_attr( $termid ) . '"><span>' . esc_html( $term->name ) . '</span></li>';

		// }

		$output .= wp_list_categories( $terms_args );
		$output .= '</ul>';
		$output .= '</div>';
		
		echo $output;

	}
}

/**
 * Post Category name as div class
 */

if( !function_exists('reptro_isotope_categories_as_class') ){
	function reptro_isotope_categories_as_class( $taxonomy ){
	    global $post;
	    $category_ids 	= array();
	    $terms 			= get_the_terms( $post->ID, $taxonomy );
	                                                   
	    if ( $terms && ! is_wp_error( $terms ) ) {
	     
	        foreach ( $terms as $term ) {
	            $category_ids[] = 'reptro_cat_' . esc_attr( $term->term_id );
	        }
	           
	    }

	    return $category_ids;      
	}
}

/**
 * get only first post category
 */

if(!function_exists('reptro_get_post_first_category')){
	function reptro_get_post_first_category( $post_id ) {

		$output = '';
		
		if($post_id == ''){
			$post_id = get_the_id();
		}

        $categories = get_the_category( $post_id );

        if( $categories && !empty($categories) ){
        	$output = '<a href="'. esc_url( get_category_link($categories[0]->term_id ) ) .'">'. esc_html( $categories[0]->cat_name ) .'</a>';
        }

        return $output;
	}
}


/**
 * The Events Calendar Get the Venue
 */

if( !function_exists('reptro_tec_get_venue') ){
	function reptro_tec_get_venue( $post_id = null ){
		if( function_exists('tribe_get_venue_id') ){
			$venue_id = tribe_get_venue_id( $post_id );
		}else{
			$venue_id = '';
		}
		$venue    = '';

		if( $venue_id ){
			$venue = sprintf( '<span class="reptro-event-venue"><i class="sli-location-pin"></i>%s</span>', esc_html( get_the_title($venue_id) ) );
		}

		return $venue;
	}
}

/**
 * Event start date
 */

if(!function_exists('reptro_get_event_start_date')){
	function reptro_get_event_start_date( $event_id ) {
		
		if($event_id == ''){
			$event_id = get_the_id();
		}

        $start_date = strtotime(get_post_meta( $event_id, '_EventStartDate', true ));

        if( $start_date ):
        	?>
        	<div class="reptro-event-date">
				<span class="reptro-event-day"><?php echo esc_html( date_i18n(apply_filters( 'reptro_event_date_format', 'd' ), $start_date) ); ?></span>
				<span class="reptro-event-month"><?php echo esc_html( date_i18n(apply_filters( 'reptro_event_month_format', 'M' ), $start_date) ); ?></span>
				<span class="reptro-event-year"><?php echo esc_html( date_i18n(apply_filters( 'reptro_event_year_format', 'Y' ), $start_date) ); ?></span>
			</div>
        	<?php
        endif;
	}
}

/**
 * Event start time
 */

if(!function_exists('reptro_get_event_start_time')){
	function reptro_get_event_start_time( $event_id ) {

		$event_time = '';
		
		if($event_id == ''){
			$event_id = get_the_id();
		}

		$all_day 	= get_post_meta( $event_id, '_EventAllDay', true );
        $start_date = strtotime( get_post_meta( $event_id, '_EventStartDate', true ) );
        $end_date 	= strtotime( get_post_meta( $event_id, '_EventEndDate', true ) );
        $start_time = date_i18n( get_option('time_format'), $start_date);
        $end_time 	= date_i18n( get_option('time_format'), $end_date);

        if( $all_day != 'yes' ){
			$event_time = sprintf( ', %s %s %s', esc_html( $start_time ), esc_html__('to', 'xt-reptro-cpt-shortcode'), esc_html( $end_time ) );
		}

        if( $start_date ):
        	?>
			<span class="reptro-event-time">
				<i class="sli-clock"></i><?php echo esc_html( date_i18n(apply_filters( 'reptro_event_day_format', 'l' ), $start_date) ); ?><?php echo $event_time; ?>
			</span>
        	<?php
        endif;
	}
}

/**
 * Show Course Category Icon
 * 
 * $term_id : Category ID
 */

if(!function_exists('reptro_get_course_category_icon')){
	function reptro_get_course_category_icon( $term_id, $size = null ) {

		$output 	= '';
		$term_data 	= get_term_meta( $term_id, '_xt_course_category_options', true );

		if( is_array($term_data) && !empty($term_data) ){
			$output = wp_get_attachment_image( $term_data['_xt_course_cat_icon'], $size );
		}

		return $output;
	}
}

/**
 * Show Course Category Image
 * 
 * $term_id : Category ID
 */

if(!function_exists('reptro_get_course_category_image')){
	function reptro_get_course_category_image( $term_id, $size = null ) {

		$output 	= '';
		$term_data 	= get_term_meta( $term_id, '_xt_course_category_options', true );

		if( is_array($term_data) && !empty($term_data) && array_key_exists('_xt_course_cat_img', $term_data)){
			$output = wp_get_attachment_image( $term_data['_xt_course_cat_img'], $size );
		}

		return $output;
	}
}


/**
 * LP Course Grid / slider shortcode
 */
if ( defined( 'LP_PLUGIN_FILE' ) ) {
	add_shortcode( 'schooling_course', 'reptro_course_shortcode_function' );
}

if( !function_exists('reptro_course_shortcode_function') ){
	function reptro_course_shortcode_function( $atts ){
		extract(shortcode_atts(array(
			'post'  				=> 6,
			'order' 				=> 'ASC',
			'orderby' 				=> 'menu_order',
			'autoplay'				=> 'yes',
			'loop'					=> '',
			'items'					=> 3,
			'desktopsmall'			=> 3,
			'tablet'				=> 2,
			'mobile'				=> 1,
			'navigation'			=> 'yes',
			'pagination'			=> '',
			'column'				=> 3,
			'course_type'			=> '', // free / paid
			'featured'				=> '', // on
			'course_categories'		=> '', // comma separated categories id
			'course_tags'			=> '', // comma separated tags id
			'course_ids'			=> '', // comma separated course ids
			'not_in'				=> '', // comma separated course ids to exclude
			'type'					=> 'grid', // grid, slider, list
			'category_filtering'	=> '', //yes, only of gird
			'shortcode_title'		=> '',
			'widget_use'			=> 'yes',		
			'widget_current_cat'	=> '',	// in widget show current course categories course posts only	
			'all_courses_btn'		=> 'yes',	

	    ), $atts));

		$course_ids = ( $course_ids ? explode( ',', $course_ids ) : null );
		$not_in 	= ( $not_in ? explode( ',', $not_in ) : null );

		$args = array( 
			'post_type' 				=> 'lp_course', 
			'orderby' 					=> $orderby,
			'order' 					=> $order,
			'posts_per_page' 			=> $post,
			'post__in' 					=> $course_ids,
			'post__not_in' 				=> $not_in,
		);

		// featured course

		if( $featured == 'yes' ){

			$args['meta_query'][] = array(
				'key' 		=> '_lp_featured',
				'value'		=> 'yes',
			);
		}

		// In widget show current category courses

		if( $widget_use == 'yes' && $widget_current_cat == 'yes' && is_tax('course_category') ){

			$current_term = get_queried_object();

			if($current_term)
				$current_term_id = $current_term->term_id;

			if( $current_term_id ){
				$args['tax_query'][] = array(
					'taxonomy' 	=> 'course_category',
			        'field'    	=> 'id',
					'terms'    	=> $current_term_id,
			        'operator' 	=> 'IN' 
				);
			}

		}

		// course course_type 
		if( $course_type ){

			if( $course_type == 'free' ){

				$args['meta_query'] = array(
					'relation' 		=> 'OR',
					array(
						'key' 		=> '_lp_price',
						'compare'	=> 'NOT EXISTS'
					),
					array(
						'key' 		=> '_lp_sale_price',
						'value' 	=> '0',
					)
				);

			}elseif( $course_type == 'paid' ){

				$args['meta_query'] = array(
					array(
						'key' 		=> '_lp_price',
						'compare'	=> 'EXISTS'
					)
				);
			}
		}

		// only form selected course categories
		if( $course_categories && $course_categories != '' ){
			$course_categories = explode(',', $course_categories);
			$args['tax_query'][] = array(
				'taxonomy' 	=> 'course_category',
		        'field'    	=> 'id',
				'terms'    	=> $course_categories,
		        'operator' 	=> 'IN' 
			);
		}

		// only form selected course tags
		if( $course_tags && $course_tags != '' ){
			$course_tags = explode(',', $course_tags);
			$args['tax_query'][] = array(
				'taxonomy' 	=> 'course_tag',
		        'field'    	=> 'id',
				'terms'    	=> $course_tags,
		        'operator' 	=> 'IN' 
			);
		}

		$wp_query 	= new WP_Query( $args );

		$column 	= ( $type == 'grid' ? apply_filters( 'reptro_course_grid_column', $column ) : '' );
		if($column){
			$column = 12/$column;
			$column = array( 'col-lg-'. $column , 'col-md-6' );
		}

		//  shortcode container classes
		$container_classes = array( 'reptro-course-items', 'reptro-course-' . $type );

		if( $type == 'grid' ){
			$container_classes[] = 'row';

			if($category_filtering == 'yes'){
				$container_classes[] = 'reptro-course-isotope';
			}
		}


		/**
		 * Slider Config
		 */
		
		if( $type == 'slider' ){
			$container_classes[] = 'owl-carousel';
			$container_classes[] = 'owl-theme';
		}

		$container_tag = 'ul';
		if( $type == 'slider' ){
			$container_tag = 'div';
		}

		$slider_attr = array(
        	'data-autoplay' 	=> ( $autoplay == 'yes' ? 'true' : 'false' ),
        	'data-loop' 	    => ( $loop == 'yes' ? 'true' : 'false' ),
        	'data-items' 		=> $items,
        	'data-desktopsmall' => $desktopsmall,
        	'data-tablet' 		=> $tablet,
        	'data-mobile' 		=> $mobile,
        	'data-navigation' 	=> ( $navigation == 'yes' ? 'true' : 'false' ),
        	'data-pagination' 	=> ( $pagination == 'yes' ? 'true' : 'false' ),
        	'data-direction' 	=> ( is_rtl() ? 'true' : 'false' ),
        );

        if( $type == 'grid' && $category_filtering == 'yes' ){
        	wp_enqueue_script('xt-reptro-imagesloaded');
        	wp_enqueue_script('xt-reptro-isotope');
        }

		ob_start();

		if ( $wp_query->have_posts() && !is_search() ): ?>
			<div class="reptro-course-items-area reptro-course-items-area-<?php echo esc_html( $type )?>">

				<?php if( $shortcode_title ): ?>
					<h3 class="reptro-shortcode-title"><?php echo esc_html( $shortcode_title ); ?></h3>
				<?php endif; ?>

				<?php 
					if( $type == 'grid' && $category_filtering == 'yes' ){
						reptro_category_filter_isotope( 'course_category', ( isset($course_categories) && !empty($course_categories) ? $course_categories : array() ) );
					}
				?>

				<<?php echo esc_html( $container_tag ); ?> class="<?php echo esc_attr( implode(' ', $container_classes) ); ?>" <?php echo (  $type == 'slider' ? reptro_carousel_data_attr_implode( $slider_attr ) : '' ); ?>>
					<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

						<?php
							learn_press_get_template( 'content-course.php', array( 'column' => $column, 'type' => $type, 'category_filtering' => $category_filtering ) );
						?>

					<?php endwhile; ?>
				</<?php echo esc_html( $container_tag ); ?>>

				<?php if( $all_courses_btn == 'yes' && learn_press_get_page_id( 'courses' ) ): ?>
					<div class="text-center reptro-all-courses-btn">
						<a class="btn btn-lg btn-fill" href="<?php echo esc_url( learn_press_get_page_link( 'courses' ) ); ?>"><?php echo esc_html( get_the_title( learn_press_get_page_id( 'courses' ) ) ) ?></a>
					</div>
				<?php endif; ?>

			</div>
		<?php
		endif;
		wp_reset_postdata();
		return ob_get_clean();
	}
}

/**
 * Course Category
 */

if ( defined( 'LP_PLUGIN_FILE' ) ) {
	add_shortcode( 'schooling_course_category', 'reptro_course_category_shortcode_function' );
}

if( !function_exists('reptro_course_category_shortcode_function') ){
	function reptro_course_category_shortcode_function( $atts ){
		extract(shortcode_atts(array(
			'img_type'  		=> 'icon', // image
			'content_type'  	=> 'grid', // slider
			'show_icon'  		=> 'yes',
			'hide_empty'  		=> false,
			'childless'  		=> false,
			'include'			=> '', // array or comma/space-separated string of term ids to include
			'exclude'			=> '', // Array or comma/space-separated string of term ids to exclude
			'child_of'			=> '',
			'number'			=> 0,
			'autoplay'			=> 'yes',
			'loop'				=> '',
			'items'				=> 6,
			'desktopsmall'		=> 5,
			'tablet'			=> 3,
			'mobile'			=> 2,
			'navigation'		=> 'yes',
			'pagination'		=> '',
			'column'			=> 6,
	    ), $atts));

		$args = array(
			'hide_empty' => ( $hide_empty == 'yes' ? true : false ),
			'childless'  => ( $childless == 'yes' ? true : false ),
			'child_of'   => $child_of,
			'include'    => $include,
			'exclude'    => $exclude,
			'number'	 => $number
		);
		$terms = get_terms( 'course_category', $args );


		$column 	= ( $content_type == 'grid' ? apply_filters( 'reptro_course_category_grid_column', $column ) : array() );
		if($column){
			$column = 12/$column;
			$column = array( 'col-lg-'. $column , 'col-md-6' );
		}

		//  shortcode container classes
		$container_classes = array( 'reptro-course-category-wrapper', 'reptro-course-category-img-type-' . $img_type, 'reptro-course-category-' . $content_type, 'reptro-course-category-icon-' . $show_icon  );

		if( $content_type == 'grid' ){
			$container_classes[] = 'row';
		}


		/**
		 * Slider Config
		 */
		
		if( $content_type == 'slider' ){
			$container_classes[] = 'owl-carousel';
			$container_classes[] = 'owl-theme';
		}

		$slider_attr = array(
        	'data-autoplay' 	=> ( $autoplay == 'yes' ? 'true' : 'false' ),
        	'data-loop' 	    => ( $loop == 'yes' ? 'true' : 'false' ),
        	'data-items' 		=> $items,
        	'data-desktopsmall' => $desktopsmall,
        	'data-tablet' 		=> $tablet,
        	'data-mobile' 		=> $mobile,
        	'data-navigation' 	=> ( $navigation == 'yes' ? 'true' : 'false' ),
        	'data-pagination' 	=> ( $pagination == 'yes' ? 'true' : 'false' ),
        	'data-direction' 	=> ( is_rtl() ? 'true' : 'false' ),
        );

        $item_classes = array('reptro-course-category-item');

        if( $content_type == 'grid' ){
        	$item_classes = array_merge($item_classes, $column);
        }

        $item_classes = apply_filters( 'reptro_course_category_item_class', $item_classes );

	    ob_start();
	    ?>
			<div class="<?php echo esc_attr( implode(' ', $container_classes) ); ?>" <?php echo ( $content_type == 'slider' ? reptro_carousel_data_attr_implode( $slider_attr ) : '' ); ?>>
				<?php if( $terms ): ?>
					<?php foreach ( $terms as $term): ?>
						<div class="<?php echo esc_attr( implode(' ', $item_classes) ); ?>">
							<div class="reptro-course-item-inner">

								<?php if( $img_type == 'icon' ): ?>
									<a href="<?php echo esc_url( get_term_link( $term->term_id ) ); ?>">
										<?php if( $show_icon == 'yes' ): ?>
											<?php echo reptro_get_course_category_icon( $term->term_id, 'full' ); ?>
										<?php endif; ?>
										<h4><?php echo esc_html( $term->name ); ?></h4>
									</a>

								<?php elseif( $img_type == 'image'): ?>
									<a href="<?php echo esc_url( get_term_link( $term->term_id ) ); ?>">
										<?php 
											if( reptro_get_course_category_image( $term->term_id, 'reptro-blog-small' ) ){
												printf( '<div class="reptro-cat-image">%s</div>', reptro_get_course_category_image( $term->term_id, 'reptro-blog-small' ));
											}else{
												printf( '<div class="reptro-cat-image">%s</div>', reptro_get_placeholder_image());
											}
										?>
										<div class="reptro-cat-description">
											<h4><?php echo esc_html( $term->name ); ?></h4>
											<?php 
												$cat_count = $term->count;
												if($cat_count){
													printf('<span>%s %s</span>', esc_html( $cat_count ), ( $cat_count <= 1 ? esc_html__( 'Course', 'xt-reptro-cpt-shortcode' ) : esc_html__( 'Courses', 'xt-reptro-cpt-shortcode' ) ) );
												}
											?>
										</div>
									</a>
								<?php endif; ?>

							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
	    <?php
	    return ob_get_clean();
	}
}


/**
 * Events Shortcode
 */

add_shortcode( 'schooling_events', 'reptro_events_shortcode_function' );

if( !function_exists('reptro_events_shortcode_function') ){
	function reptro_events_shortcode_function( $atts ){
		extract(shortcode_atts(array(
			'type'				=> 'grid', // grid, slider
			'column'			=> 3,
			'event_categories'	=> '', // comma separated categories id
			'event_ids'			=> '', // comma separated events ids
			'not_in'			=> '', // comma separated events ids to exclude
			'post'  			=> 6,
			'order' 			=> 'ASC',
			'orderby' 			=> 'menu_order',
			'autoplay'			=> 'yes',
			'loop'				=> 'no',
			'navigation'		=> 'yes',
			'pagination'		=> 'no',
			'items'				=> 3,
			'desktopsmall'		=> 3,
			'tablet'			=> 2,
			'mobile'			=> 1,
			'show_cost'			=> 'yes',
			'show_excerpt'		=> 'no',
			'show_venue'		=> 'yes',
	    ), $atts));

		$event_ids 	= ( $event_ids ? explode( ',', $event_ids ) : null );
		$not_in 	= ( $not_in ? explode( ',', $not_in ) : null );

		$args = array( 
			'post_type' 				=> 'tribe_events', 
			'orderby' 					=> $orderby,
			'order' 					=> $order,
			'posts_per_page' 			=> $post,
			'post__in' 					=> $event_ids,
			'post__not_in' 				=> $not_in,
		);

		// only form selected event categories
		if( $event_categories && $event_categories != '' ){
			$event_categories = explode(',', $event_categories);
			$args['tax_query'][] = array(
				'taxonomy' 	=> 'tribe_events_cat',
		        'field'    	=> 'id',
				'terms'    	=> $event_categories,
		        'operator' 	=> 'IN' 
			);
		}

		$image_size = apply_filters( 'reptro_event_image_size', 'thumbnail' );

		$wp_query = new WP_Query( $args );

		ob_start();
			if ( $wp_query->have_posts() ){
				?>
					<div class="reptro-events-items">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post();?>
							<div <?php post_class( apply_filters( 'reptro_event_loop_classes', array( 'reptro-event-item', 'xt-smooth-shadow' ) ) ) ?>>
								<a href="<?php esc_url( the_permalink() ); ?>" class="reptro-event-item-link">
									<?php $start_date = strtotime(get_post_meta( get_the_id(), '_EventStartDate', true )); ?>
									<?php reptro_get_event_start_date( get_the_id() ); ?>

									<div class="reptro-event-details">
										<?php the_post_thumbnail( $image_size ); ?>
										<div class="reptro-event-details-side">
											<?php reptro_get_event_start_time( get_the_id() ); ?>
											<h3 class="reptro-event-title"><?php the_title(); ?></h3>
											<?php if( $show_venue == 'yes' ): ?>
												<span class="reptro-event-venue"><?php echo reptro_tec_get_venue( get_the_id() ); ?></span>
											<?php endif; ?>
										</div>
									</div>
								</a>
							</div>
						<?php endwhile; ?>	
					</div>
				<?php
			}
		wp_reset_postdata();
	    return ob_get_clean();
	}
}



/**
 * Hero area 
 */

if( !function_exists('reptro_hero_area_shortcode') ){
	function reptro_hero_area_shortcode( $atts ) {

		extract( shortcode_atts(
			array(
				'hero_title' 			=> '',
				'hero_description' 		=> '',
				'contact_form' 			=> '',
				'need_button' 			=> 'yes',
				'need_lightbox' 		=> 'yes',
				'button_url' 			=> '',
				'hero_button_icon' 		=> 'fa fa-play',
				'right_bg_img' 			=> ''
			), $atts )
		);

		if($right_bg_img){
			$right_bg_img = wp_get_attachment_image_src( $right_bg_img, 'full' );
		}

		wp_enqueue_style('fancybox');
    	wp_enqueue_script('fancybox');

		ob_start();
		?>
	        <div class="reptro-hero-area">
        		<div class="reptro-hero-area-left reptro-hero-area-column">
        			<?php printf('<h2>%s</h2>', esc_html( $hero_title )); ?>
        			<?php echo wpautop( wp_kses_post( $hero_description ) ); ?>
					<?php
						if( $contact_form ){
							echo do_shortcode( '[contact-form-7 id="'. $contact_form .'"]' );
						}
					?>

					<?php 
						if( $need_button == 'yes' ){
							printf( '<a class="reptro-hero-area-play-btn" %s href="%s"><i class="%s"></i></a>', ( $need_lightbox == 'yes' ? 'data-fancybox' : '' ), esc_url( $button_url ), esc_attr( $hero_button_icon ));
						} 
					?>	
        		</div>
    			<div class="reptro-hero-area-right reptro-hero-area-column" style="background-image: url(<?php echo esc_url( is_array($right_bg_img) && !empty($right_bg_img) ? $right_bg_img[0] : '' ); ?>)"></div>
	        </div>
		<?php
		return ob_get_clean();
	}
}
add_shortcode( 'schooling_hero_area', 'reptro_hero_area_shortcode' );


/**
 * Client Logo ShortCode
 */
if( !function_exists('reptro_client_logo_shortcode') ){
	function reptro_client_logo_shortcode($atts){
		extract(shortcode_atts(array(
			'column'		=> 6,
			'logo_images'	=> ''
	    ), $atts));

	    if($column){
			$column = 12/$column;
			$column = 'col-lg-'. $column;
		}

		ob_start();
		?>

		<?php if( is_array($logo_images) && !empty($logo_images) ) :
			echo '<div class="xt-client-logo">';
		        echo '<div class="xt-logos">';
		        	echo '<div class="row">';
	                	foreach ($logo_images as $logo_image):
	                		$logo_image_url = $logo_image['logo_img']['url'];
	                		$logo_url  = esc_url($logo_image['logo_url']);
							echo '<div class="'. esc_attr( $column ) .' col-md-6 logo-item">';
								echo '<a href="'.$logo_url.'">';
									echo '<img src="'.esc_url( $logo_image_url).'" class="img-responsive" alt="">';
								echo '</a>';
							echo '</div>';
						endforeach; 
		        	echo '</div>';
		        echo '</div>';
			echo '</div>';
		endif;
	    return ob_get_clean();
	}
}
add_shortcode( 'business_x_client_logo', 'reptro_client_logo_shortcode' );


/**
 * Gallery ShortCode
 */

if( !function_exists('reptro_portfolio_shortcode') ){
	function reptro_portfolio_shortcode( $atts ) {

		extract( shortcode_atts(
			array(
				'post'  				=> 9,
				'order' 				=> 'ASC',
				'orderby' 				=> 'menu_order',
				'columns' 				=> 3,
				'image_size' 			=> 'large',
                'post_in_ids'     		=> '',
                'post_not_in_ids'  		=> ''				
			), $atts )
		);

        $post_in_ids     = ( $post_in_ids ? explode( ',', $post_in_ids ) : null );
        $post_not_in_ids = ( $post_not_in_ids ? explode( ',', $post_not_in_ids ) : null );

		$args = array(
			'post_type' 				=> 'business_x_gallery', 
			'orderby' 					=> $orderby,
			'order' 					=> $order,
			'posts_per_page' 			=> $post,
			'meta_key' 					=> '_thumbnail_id',
			'post__in'          		=> $post_in_ids,
            'post__not_in'      		=> $post_not_in_ids			
	    );

    	wp_enqueue_style('fancybox');
    	wp_enqueue_script('fancybox');
    	wp_enqueue_script('isotope');

    	if( $columns ){
    		$columns = 12/$columns;
    		$columns = array( 'col-lg-'.$columns, 'col-md-6' );
    	}

		$wp_query = new WP_Query( $args );

		ob_start();
		
		if ( $wp_query->have_posts() ): ?>

        <div class="xt-portfolio">
    		<?php $gallery_cats = get_terms( 'business_x_gallery_cat' ); ?>
            <?php if( ! empty( $gallery_cats ) && ! is_wp_error( $gallery_cats ) ) : ?> 
                <div class="portfolio-nav">
                    <ul>
                        <li data-filter="*" class="current"> <span><?php echo esc_html__( 'All Category', 'xt-reptro-cpt-shortcode' ); ?> </span></li>
                        <?php foreach ( $gallery_cats as $gallery_cat ) : ?>
                        	<li data-filter=".<?php echo esc_attr( $gallery_cat->slug ); ?>"><span><?php echo esc_html( $gallery_cat->name ); ?></span></li>
	                    <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <div class="xt-project-gallery">
                <div class="portfolio-container row">
                    <?php
						while ($wp_query->have_posts()) : $wp_query->the_post(); 
						global $post;
						$gallery_large = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
						$gallery_sub_title = reptro_get_post_meta( 'xt_gallery_cat_title', '_xt_gallery_sub_title' );
	                    $terms = get_the_terms( $post->ID, 'business_x_gallery_cat' );
	                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) : 

	                        $gallery_cat_slug = array();

	                        foreach ( $terms as $term ) {
	                            $gallery_cat_slug[]   = $term->slug;
	                        }

	                        if( is_array($columns) ) {
	                        	$gallery_class_array = array_merge( $columns, $gallery_cat_slug );
	                        }

	                        $gallery_class_array = join( " ", $gallery_class_array );

	                    endif;
					?>
						<?php if( has_post_thumbnail() ) : ?>
                            <div class="grid-item <?php echo esc_attr( $gallery_class_array ); ?>">
                                <figure>
                                    <?php the_post_thumbnail( $image_size ) ?>
                                    <figcaption class="fig-caption">
                                        <i class="fa fa-search"></i>
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <span class="sub-title"><?php echo esc_html( $gallery_sub_title ); ?></span>
                                        <a class="xt-project-lightbox" href="<?php echo esc_url( $gallery_large[0] ); ?>" data-fancybox="gallery"></a>
                                    </figcaption>
                                </figure>
                            </div>
                    	<?php endif; ?>
					<?php endwhile; ?>
                </div> 
            </div>
        </div>

		<?php
		endif;
		wp_reset_postdata();
		return ob_get_clean();
	}
}
add_shortcode( 'business_x_portfolio_scode', 'reptro_portfolio_shortcode' );


/**
 * Team ShortCode
 */
if( !function_exists('reptro_team_shortcode') ){
    function reptro_team_shortcode( $atts ) {

        extract( shortcode_atts(
            array(
                'post'                  => 3,
				'column'  				=> 3,
                'order'                 => 'ASC',
                'number_of_words'  		=> 10,
                'orderby'               => 'menu_order',
                'post_in_ids'     		=> '',
                'post_not_in_ids'  		=> '',
                'link_to_details'  		=> 'no', // yes
                'margin_bottom'  		=> 'no', // yes
                'row_equal_height'  	=> 'yes', // no
                'show_desc'  			=> 'yes', // yes, no
            ), $atts )
        );

        $post_in_ids     = ( $post_in_ids ? explode( ',', $post_in_ids ) : null );
        $post_not_in_ids = ( $post_not_in_ids ? explode( ',', $post_not_in_ids ) : null );

        $args = array(
            'post_type'                 => 'business_x_team', 
			'orderby' 					=> $orderby,
			'order' 					=> $order,
			'posts_per_page' 			=> $post,
			'post__in'          		=> $post_in_ids,
            'post__not_in'      		=> $post_not_in_ids,
        );

        $wp_query = new WP_Query( $args );

        $column = 12/$column;
		$column = 'col-lg-'.esc_attr( $column ). ' col-md-6';

		if( $margin_bottom == 'yes' )
			$column .= ' team-item-margin-botttom';

		if( $row_equal_height == 'yes' )
			$column .= ' xt-grid-equal-height';

        ob_start();
        
        if ( $wp_query->have_posts() ): ?>
        	<div class="xt-team-members row">
	        	<?php while ($wp_query->have_posts()) : $wp_query->the_post(); 
		            global $post;                    
		            $team_designation 	= reptro_get_post_meta( 'business_x_team_options', 'member_designation' );
					$team_desc 			= get_the_content();
		        ?>
					<div class="<?php echo esc_attr($column); ?>">
						<?php echo ($link_to_details == 'yes' ? '<a href="'.get_the_permalink().'">' : ''); ?>
						    <div class="single-team text-center shadow">
						    	<div class="member-thumb">
									<?php 
							            if( has_post_thumbnail() ){
							                the_post_thumbnail( 'reptro-team-thumb');
							            } else{
							            	printf('<img src="%s">', get_template_directory_uri() . '/assets/images/placeholder-user.png');
							            }
									 ?>
						    	</div>
						    	<div class="member-desc">	
					    		<?php the_title('<h3>', '</h3>'); ?>
						    		<?php $team_designation ? printf('<span>%s</span>', $team_designation) : '' ?>
									<?php 
										if( $show_desc == 'yes' ){
											echo wpautop( esc_html( wp_trim_words( $team_desc , $number_of_words ) ) );
										} 
									?>
						    	</div>
						    </div>
					    <?php echo ($link_to_details == 'yes' ? '</a>' : ''); ?>
					</div>

		        <?php endwhile; ?>
        	</div>
        <?php

        endif;
        wp_reset_postdata();
        return ob_get_clean();
    }
}
add_shortcode( 'business_x_team_scode', 'reptro_team_shortcode' );



/**
 * Feature ShortCode
 */
if( !function_exists( 'reptro_feature_shortcode' ) ){
    function reptro_feature_shortcode( $atts, $content = null ){
        extract( shortcode_atts( array(
			'icon'         => 'sli-energy',
			'title'        => '',
			'feature_text' => '',
			'service_box_url'      => ''
        ), $atts) );

        ob_start();
    ?>	

        <div class="xt-service">
        	<div class="each-inner">
	            <div class="xt-service-inner">
	            	<?php
	            		if( $icon ) {
	            			if( $service_box_url ){
	                    		printf( '<a href="%s"><i class="%s"></i></a>', esc_url($service_box_url), esc_attr( $icon ) );
	                    	}else{
	                    		printf( '<i class="%s"></i>', esc_attr( $icon ) );
	                    	}
	            		} 

	                    if ( $title ) {
	                    	if( $service_box_url ){
	                    		printf( '<h3><a href="%s">%s</a></h3><hr class="title-hr">', esc_url($service_box_url), esc_html( $title ) ); 
	                    	}else{
	                    		printf( '<h3>%s</h3><hr class="title-hr">', esc_html( $title ) ); 
	                    	}
	                    }
	                    if ( $feature_text ) {
	                    	printf( '%s', wpautop( wp_kses_post( $feature_text ) ) );
	                    }
	                ?>
	            </div>                    
	        </div>                    
        </div>

    <?php
        return ob_get_clean();
    }
}
add_shortcode( 'business_x_feature_scode', 'reptro_feature_shortcode' ); 


/**
 * Feature ShortCode Two
 */

if( !function_exists( 'reptro_feature_two_shortcode' ) ){
    function reptro_feature_two_shortcode( $atts, $content = null ){
        extract( shortcode_atts( array(
            'column'	          => 4,
            'type'	          	  => 'default', // color_bg
			'feature_items'		  => '',
			'btn_title'		  	  => '',
			'btn_url'		  	  => ''
        ), $atts) );

        $column = 12/$column;
		$column = apply_filters( 'xt_feature_two_columns', 'col-lg-'.esc_attr( $column ). ' col-md-6' );
        ob_start();
    ?>	

		<?php if( $feature_items ): ?>
			<div class="xt-service-two-area">
				<div class="row">
					<?php foreach ($feature_items as $key => $item ): ?>
				        <div class="xt-service-two-item <?php echo esc_attr( $column ); ?>">
				            <div class="xt-service-two-inner">
				            	<div class="xt-service-two-content">
					            	<?php
					            		if( $item['icon'] ) {
				                    		printf( '<span class="xt-service-two-icon"><i class="%s"></i></span>', esc_attr( $item['icon'] ) );
					            		} 

					                    if ( $item['title'] ) {
				                    		printf( '<h3>%s</h3>', esc_html( $item['title'] ) ); 
					                    }

					                    if ( $item['service_content']  ) {
					                    	printf( '%s', wpautop( esc_html( $item['service_content'] ) ) );
					                    }

					                    if ( $item['btn_title']  ) {
					                    	printf( '<a class="xt-service-two-btn" href="%2$s" target="_self" title="%1$s">%1$s</a>', esc_html( $item['btn_title'] ), esc_url( $item['btn_url'] ) );
					                    }
					                ?>
				                </div>
				            </div>                                      
				        </div>
			    	<?php endforeach; ?>
		        </div>
	        </div>
    	<?php endif; ?>

    <?php
        return ob_get_clean();
    }
}
add_shortcode( 'business_x_feature_two', 'reptro_feature_two_shortcode' ); 


/**
 * Testimonial ShortCode
 */
if( !function_exists('testimonial_shortcode') ){
	function testimonial_shortcode( $atts ) {

		extract( shortcode_atts(
			array(
				'post'  				=> 4,
				'order' 				=> 'ASC',
				'orderby' 				=> 'menu_order',
                'post_in_ids'     		=> '',
                'post_not_in_ids'  		=> ''				
			), $atts )
		);

        $post_in_ids     = ( $post_in_ids ? explode( ',', $post_in_ids ) : null );
        $post_not_in_ids = ( $post_not_in_ids ? explode( ',', $post_not_in_ids ) : null );

		$args = array(
			'post_type' 				=> 'business_x_testi', 
			'orderby' 					=> $orderby,
			'order' 					=> $order,
			'posts_per_page' 			=> $post,
			'posts_per_page' 			=> $post,
			'post__in'          		=> $post_in_ids,
            'post__not_in'      	=> $post_not_in_ids,	
	    );

		$wp_query = new WP_Query( $args );

		ob_start();
		
		if ( $wp_query->have_posts() ): ?>

            <div class="xt-featured-member">
                <div class="slider-content">
                    <div class="carousel xt-carousel-fade slide" data-ride="carousel" id="quote-carousel">
                    	<!-- Bottom Carousel Indicators -->
                        <ol class="carousel-indicators">
	                        <?php 

	                        	$y = 0;
	                        	while ( $wp_query->have_posts() ) : $wp_query->the_post();
	                        	$testimonial_featured_img  = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'reptro-client-img' ); 

								if( has_post_thumbnail() ){
									$testimonial_img 	  = $testimonial_featured_img[0];
								}else{
									$testimonial_img 	  = 'http://placehold.it/600x600';
								}
	                        ?>
	                            <li data-target="#quote-carousel" data-slide-to="<?php echo esc_attr( $y ); ?>" class="<?php if( $y == 0 ){ echo esc_attr( 'active' ); } ?>">
	                            	<img class="img-responsive" src="<?php echo esc_url( $testimonial_img ); ?>" alt=""><span></span>
	                            </li>
	                        <?php 
	                        	$y++;
	                        	endwhile;
	                        	wp_reset_postdata();
							?> 
                        </ol>
                        <div class="carousel-inner text-center">

						<?php 
							$x = 0;
							while ( $wp_query->have_posts() ) : $wp_query->the_post();
							global $post;
							$testi_designation = reptro_get_post_meta( '_xt_testimonial_options', 'testimonial_designation' );
							$testimonial_social_icons  = reptro_get_post_meta( '_xt_testimonial_options', 'clients_all_social_icons' );
							$x++;
						?>
                            <div class="item <?php if( $x == 1 ){ echo esc_attr( 'active' ); } ?>">
                            	<div class="row">
	                                <div class="col-lg-7">
	                                    <div class="member-info">
	                                        <?php the_title( '<h4>', '</h4>' ); ?>
	                                        <?php if( $testi_designation ) :
	                                        	printf( '<span>%s</span><hr class="title-hr">', esc_html( $testi_designation ) ); 
	                                        endif; ?>
	                                        <div class="clearfix"></div>
	                                        <div class="testi-code">
	                                        	<?php echo wp_kses_post( wpautop( get_the_content() ) ) ?>
	                                        </div>

	                                        <?php if( is_array( $testimonial_social_icons ) ) : ?>                                                	
	                                        	<ul>
		                                        <?php foreach ( $testimonial_social_icons  as $key => $testimonial_social_icon ) :      

													printf( '<li><a href="%s"><i class="%s"></i></a></li>', esc_url( $testimonial_social_icon['client_social_icons_url'] ), esc_attr( $testimonial_social_icon['client_social_icons'] ) ); 

		                                        endforeach; ?>
		                                        </ul>
		                                    <?php endif; ?>

	                                    </div>
	                                </div>
	                                <?php if( has_post_thumbnail() ) : ?>
	                                    <div class="col-lg-5">
	                                        <div class="member-img">
	                                        	<?php the_post_thumbnail( 'reptro-client-img' ); ?>
	                                        </div>
	                                    </div>
	                                <?php endif; ?>
                                </div>
                            </div> 

						<?php 
							endwhile;
							wp_reset_postdata();
						?>   

                        </div>
                    </div>
                </div>
            </div>

		<?php

		endif;
		wp_reset_postdata();

		return ob_get_clean();
	}
}
add_shortcode( 'business_x_testimonial_scode', 'testimonial_shortcode' );



/**
 * Blog ShortCode
 */
if( !function_exists( 'reptro_blog_shortcode' ) ){
	function reptro_blog_shortcode( $atts ) {

		extract( shortcode_atts(
			array(
				'type'     		   => 'grid', // slider
				'post'  	       => 3,
				'order' 		   => 'ASC',
				'orderby' 		   => 'menu_order',
				'number_of_words'  => 15,
				'column' 		   => 3,
                'post_in_ids'      => '',
                'post_not_in_ids'  => '',
                'ignore_sticky'    => true,
                'blog_sticky'      => false,
                'autoplay'		   => 'yes',
				'loop'			   => 'no',
				'items'			   => 3,
				'desktopsmall'	   => 3,
				'tablet'		   => 2,
				'mobile'		   => 1,
				'navigation'	   => 'yes',
				'pagination'	   => 'no'
			), $atts )
		);

        $post_in_ids 		= ( $post_in_ids ? explode( ',', $post_in_ids ) : null );
        $post_not_in_ids 	= ( $post_not_in_ids ? explode( ',', $post_not_in_ids ) : null );


		$sticky = get_option('sticky_posts');
		if (!empty($sticky) && $blog_sticky == true ) {
			$post_in_ids = $sticky;
		}

		$args = array(
			'post_type' 			=> 'post', 
			'orderby' 				=> $orderby,
			'order' 		    	=> $order,
			'posts_per_page'    	=> $post,
			'post__in'          	=> $post_in_ids,
            'post__not_in'      	=> $post_not_in_ids,
            'ignore_sticky_posts'	=> $ignore_sticky,
            'meta_key' 			=> '_thumbnail_id',
	    );


		$container_classes = array( 'xt-blog-post-' . $type );

		if( $type == 'grid' ){
			$container_classes[] = 'row';
		}

	    /**
		 * Slider Config
		 */
		
		if( $type == 'slider' ){
			$container_classes[] = 'owl-carousel';
			$container_classes[] = 'owl-theme';
		}

		$slider_attr = array(
        	'data-autoplay' 	=> ( $autoplay == 'yes' ? 'true' : 'false' ),
        	'data-loop' 	    => ( $loop == 'yes' ? 'true' : 'false' ),
        	'data-items' 		=> $items,
        	'data-desktopsmall' => $desktopsmall,
        	'data-tablet' 		=> $tablet,
        	'data-mobile' 		=> $mobile,
        	'data-navigation' 	=> ( $navigation == 'yes' ? 'true' : 'false' ),
        	'data-pagination' 	=> ( $pagination == 'yes' ? 'true' : 'false' ),
        	'data-direction' 	=> ( is_rtl() ? 'true' : 'false' ),
        );


		$wp_query = new WP_Query( $args );
		$column = 12/$column;
		$column = 'col-lg-'.esc_attr( $column ). ' col-md-6';

		if( $type == 'slider' ){
			$column = '';
		}

		ob_start();
		
		if ( $wp_query->have_posts() ) : ?>

			<div class="<?php echo esc_attr( implode(' ', $container_classes) ); ?>" <?php echo (  $type == 'slider' ? reptro_carousel_data_attr_implode( $slider_attr ) : '' ); ?>>

	            <?php while ( $wp_query->have_posts() ) : $wp_query->the_post();?>

					<article <?php post_class( $column ) ?>>
						<div class="xt-post-item">
							<?php if ( has_post_thumbnail() ) : ?>
								<div class="xt-post-item-thumb">
									<figure>
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'reptro-blog-small' ); ?></a>
									</figure> 
									<div class="xt-post-item-categories">
										<?php 
											if(function_exists('reptro_get_post_first_category')){
												echo reptro_get_post_first_category( get_the_id() );
											}
										?>
									</div>
								</div>
							<?php endif; ?>
							<div class="xt-post-item-content">
								<div class="xt-post-item-date"><?php echo get_the_date( get_option( 'date_format' ) ); ?></div>
								<h4 class="xt-post-item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							</div>
						</div>
					</article>

	            <?php endwhile; ?>

	        </div>

		<?php
		endif;
		wp_reset_postdata();

		return ob_get_clean();
	}
}
add_shortcode( 'business_x_blog_scode', 'reptro_blog_shortcode' );
 


/**
 * Video ShortCode
 */
if( !function_exists( 'reptro_video_shortcode' ) ){
	function reptro_video_shortcode( $atts ){
		extract(shortcode_atts(array(
			'video_link'	    => '',
			'bg_img'            => '',
			'type'              => 'with_image', // button_only
			'btn_text'          => esc_html__( 'Video Showcase', 'xt-reptro-cpt-shortcode' ),
	    ), $atts));

		ob_start();

		wp_enqueue_style('fancybox');
    	wp_enqueue_script('fancybox');
	?>

        <div class="xt-video xt-video-popup-type-<?php echo esc_attr( $type ) ?>">
            <?php 
        		if($type == 'button_only'){
	                if( $video_link ){
	                	printf( '<div class="xt-video-popup-btn-wrapper"><a class="xt-video-popup-btn" data-fancybox href="%s"><span class="video-popup-play-icon"></span></a><span>%s</span></div>', esc_url( $video_link ), esc_html( $btn_text ) );
	                }
        		}else{
        			echo '<div class="xt-video-img">';
	        			if( $bg_img ){
		            		echo $bg_img;
		            	}
		                if( $video_link ){
		                	printf( '<div class="xt-video-overlay"><a class="xt-video-popup-btn" data-fancybox href="%s"><span class="video-popup-play-icon"></span></a></div>', esc_url( $video_link ) );
		                }
	                echo '</div>';
        		}
            ?>
        </div>

		<?php
	    return ob_get_clean();
	}
}
add_shortcode( 'business_x_video_scode', 'reptro_video_shortcode' );


/**
 * Section Title ShortCode
 */
if( !function_exists('reptro_section_title_shortcode') ){
	function reptro_section_title_shortcode($atts){
		extract(shortcode_atts(array(
			'title_text'		=> '',
			'tag'				=> 'h2',
			'x_class'			=> '',
			'margin_bottom'		=> 'small', // xsmall, medium, large, xlarge, none
			'margin_top'		=> 'small', // xsmall, medium, large, xlarge, none
			'type'				=> 'default', // border
			'align'				=> 'default', // left, right, center
			'font_size'			=> 'default', // xx_large
			'color'				=> 'default', // white
	    ), $atts) );


		$class = 'reptro-section-title'. ' text-'.esc_attr( $align ). ' margin-bottom-'.esc_attr( $margin_bottom ). ' section-title-type-'.esc_attr( $type ) . ' margin-top-'.esc_attr( $margin_top ) . ' section-title-font-size-'.esc_attr( $font_size ) . ' section-title-color-'.esc_attr( $color );

		if($x_class){
			$class .= ' '.$x_class;
		}

		ob_start();

	    if( $title_text ){
	    	printf( '<%1$s class="%2$s"><span>%3$s</span></%1$s>', $tag, esc_attr( $class ), esc_html( $title_text ) );
	    }

	    return ob_get_clean();
	}
}
add_shortcode( 'business_x_section_title_scode', 'reptro_section_title_shortcode' );


/**
 * Section Title Small ShortCode
 */

add_shortcode( 'busienss_x_section_title_small', 'busienss_x_section_title_small_shortcode' );

if( !function_exists('busienss_x_section_title_small_shortcode') ){
	function busienss_x_section_title_small_shortcode($atts){
		extract(shortcode_atts(array(
			'title_text'	=> '',
			'x_class'		=> '',
			'margin_bottom'	=> 'small', // xsmall, medium, large, none
			'margin_top'	=> 'none', // xsmall, small, medium, large
			'align'			=> 'default', // left, right
			'font_size'		=> 'default', // sub_title, default
			'border_bottom'	=> '', // yes
			'color'			=> 'default', // white
	    ), $atts));

		$class = 'section-title-small text-'. esc_attr( $align ). ' margin-bottom-'.esc_attr( $margin_bottom ) . ' margin-top-'.esc_attr( $margin_top ) . ' section-title-small-font-'.esc_attr( $font_size ) . ' section-title-small-border-'.esc_attr( $border_bottom ) . ' section-title-small-color-'.esc_attr( $color ) ;

		if($x_class){
			$class .= ' '.$x_class;
		}

		ob_start();

	    if( $title_text ){
	    	printf( '<%1$s class="%2$s"><span>%3$s</span></%1$s>', 'p', esc_attr( $class ), esc_html( $title_text ) );
	    }

	    return ob_get_clean();
	}
}


/**
 * Slider ShortCode
 */
if( !function_exists('reptro_slider_shortcode') ){
    function reptro_slider_shortcode( $atts ) {

        extract( shortcode_atts(
            array(
                'post'                  => 3,
                'order'                 => 'ASC',
                'orderby'               => 'menu_order',
                'post_in_ids'     		=> '',
                'post_not_in_ids'  		=> '',
                'autoplay'				=> 'yes',
				'loop'					=> '',
				'navigation'			=> 'yes',
				'pagination'			=> '',				
            ), $atts )
        );

        $post_in_ids     = ( $post_in_ids ? explode( ',', $post_in_ids ) : null );
        $post_not_in_ids = ( $post_not_in_ids ? explode( ',', $post_not_in_ids ) : null );

        $args = array(
            'post_type'                 => 'business_x_slider', 
            'orderby'                   => $orderby,
            'order'                     => $order,
            'posts_per_page'            => $post,
			'post__in'          		=> $post_in_ids,
            'post__not_in'      		=> $post_not_in_ids            
        );

        $slider_attr = array(
			'data-autoplay' 	=> ( $autoplay == 'yes' ? 'true' : '' ),
			'data-loop' 	    => ( $loop == 'yes' ? 'true' : '' ),
			'data-navigation' 	=> ( $navigation == 'yes' ? 'true' : '' ),
			'data-pagination' 	=> ( $pagination == 'yes' ? 'true' : '' ),
			'data-direction' 	=> ( is_rtl() ? 'true' : '' ),
		);

        $wp_query = new WP_Query( $args );

        ob_start();
        
        if ( $wp_query->have_posts() ): ?>

        <div class="header-slider header-slider-preloader">
            <div class="theme-main-slider animation-slides owl-carousel owl-theme" <?php echo reptro_carousel_data_attr_implode( $slider_attr ); ?>>                
                <?php
                    while ($wp_query->have_posts()) : $wp_query->the_post(); 
                    global $post;                
                    $slider_btn_options     = reptro_get_post_meta( '_xt_sider_options', 'slider_btn_options' );     
                    $slider_featured_img  	= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), array('1920','710') ); 
                    
                    if( has_post_thumbnail() ){
                        $slider_img       = $slider_featured_img[0];
                    }else{
                        $slider_img       = 'http://placehold.it/1920x710';
                    }
                ?>

                <div style="background-image:url(<?php echo esc_url( $slider_img ); ?>)" class="xt-main-slider-item">
                    <div class="slide-table">
                        <div class="slide-tablecell">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-10 col-md-10">
                                        <div class="slide-text">
                                           	<?php the_title( '<h2>', '</h2>' ); ?>
                                           	<?php echo wpautop( get_the_content() ); ?>
											<?php if ( is_array( $slider_btn_options ) ) : ?>
						                    	<div class="theme-main-slide-buttons">
								                	<?php 
								                		foreach( $slider_btn_options as $key => $slider_btn_option ) : 
										            		if( $slider_btn_option['slider_button_type'] == 'slider_button_border' ) { 						            			
																printf( '<a class="btn btn-lg btn-border" href="%s">%s</a>', esc_url( $slider_btn_option['slider_button_url'] ), esc_html( $slider_btn_option['slider_button_title'] ) ); 						            			
												            } 
												            if( $slider_btn_option['slider_button_type'] == 'slider_button_fill' ) {
												            	printf( '<a class="btn btn-lg btn-fill" href="%s">%s</a>', esc_url( $slider_btn_option['slider_button_url'] ), esc_html( $slider_btn_option['slider_button_title'] ) );					
												            }
										            	endforeach; 
										            ?>
										        </div>
								            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="slider_preloader">
                <div class="slider_preloader_status">&nbsp;</div>
            </div>
        </div>

        <?php
        endif;
        wp_reset_postdata();
        return ob_get_clean();
    }
}
add_shortcode( 'business_x_slider_scode', 'reptro_slider_shortcode' );


/**
 * Fancy List ShortCode
 */
if( !function_exists( 'reptro_fancy_list_shortcode' ) ){
	function reptro_fancy_list_shortcode( $atts ){
		extract(shortcode_atts(array(
			'reptro_fancy_lists'	  => ''
	    ), $atts) );

		ob_start();

	?>	
	<?php if( is_array( $reptro_fancy_lists ) ): ?>	
		<ul class="reptro_fancy_lists">

            <?php foreach ( $reptro_fancy_lists as $reptro_fancy_list ) { 
            	$title  = $reptro_fancy_list['title'];									

				printf( '<li><i class="fa fa-check-circle-o"></i>%s</li>', esc_html( $title ) );			
			}
			?>
    	</ul>

    <?php endif; ?>

	<?php
	    return ob_get_clean();
	}
}
add_shortcode( 'fancy_list_scode', 'reptro_fancy_list_shortcode' );


/**
 * Progress Bar ShortCode
 */
if( !function_exists( 'reptro_progress_bar_shortcode' ) ){
	function reptro_progress_bar_shortcode( $atts ){
		extract(shortcode_atts(array(
			'progress_bars'	 => '',
	    ), $atts) );

	    wp_enqueue_script('bars');

		ob_start();

	?>		
	<?php if( is_array( $progress_bars ) ): ?>	
        <div class="xt-skills">
    		<div class="technical-skills">
                <?php foreach ( $progress_bars as $progress_bar ) { 		

                	$title  	= $progress_bar['title'];						
                	$data_value = $progress_bar['data_value'];

					printf( '<div class="bar_group" data-max="100"><div class=" bar_group__bar thick elastic" data-value="%s" data-tooltip="true" data-label="%s" data-show-values="true" data-unit="%s"></div></div>', esc_attr( $data_value), esc_attr( $title), '%' );
				}
				?>
    		</div>
        </div>
    <?php endif; ?>

		<?php
	    return ob_get_clean();
	}
}
add_shortcode( 'business_x_progress_bar_scode', 'reptro_progress_bar_shortcode' );



/**
 *  Buttons Group nested
 */
if( !function_exists('reptro_button_group_shortcode') ){
	function reptro_button_group_shortcode( $params, $content = null ) {

		extract(shortcode_atts(array(
			'x_class' => ''
		), $params));
		
		return '<div class="btn-group ' . $x_class . '">' . do_shortcode( $content ) . '</div>';

	}
}
add_shortcode('xt_button_group_nested','reptro_button_group_shortcode');


/**
 *  Theme Primary Content area nested
 */

if( !function_exists('reptro_theme_primary_content_area_shortcode') ){
	function reptro_theme_primary_content_area_shortcode( $params, $content = null ) {

		extract(shortcode_atts(array(
			'x_class' => ''
		), $params));
		
		return '<div class="xt-theme-primary-content-area ' . $x_class . '">' . do_shortcode( $content ) . '</div>';

	}
}
add_shortcode('xt_theme_primary_content_area_nested','reptro_theme_primary_content_area_shortcode');





/**
 * Blog Widget ShortCode
 */
if( !function_exists( 'reptro_blog_widget_shortcode' ) ){
	function reptro_blog_widget_shortcode( $atts ) {

		extract( shortcode_atts(
			array(
				'number'   		=> 3,
				'order'    		=> 'ASC',
				'orderby'  		=> 'menu_order',
                'include'  		=> '',
                'exclude'   	=> ''
			), $atts )
		);

		$include = $include ? explode(',', $include) : array();
		$exclude = $exclude ? explode(',', $exclude) : array();

		$args = array(
			'post_type' 		=> 'post', 
			'orderby' 			=> $orderby,
			'order' 		    => $order,
			'posts_per_page'    => $number,
			'post__in'      	=> $include,
			'post__not_in'      => $exclude,
			'meta_key' 			=> '_thumbnail_id'
	    );

		$wp_query = new WP_Query( $args );

		ob_start();
		
		if ( $wp_query->have_posts() ) : 
        	while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
        	?>

	            <div class="widget-blog-inner <?php echo esc_attr( get_the_post_thumbnail_url() ? 'widget-blog-inner-has-thumbnail' : 'widget-blog-inner-no-thumbnail' ); ?>">
	            	<?php if( has_post_thumbnail() && get_the_post_thumbnail_url() ): ?>
	                    <div class="widget-blog-img">	                            
			            	<a href="<?php the_permalink(); ?>">
			                    <?php the_post_thumbnail( 'thumbnail' ); ; ?>
			                </a>
	                    </div>
               		<?php endif; ?>
                	<div class="widget-blog-details">
                        <a href="<?php the_permalink(); ?>"><?php the_title( '<h4>', '</h4>' ); ?></a>
                        <span class="blog-details-date"><?php echo esc_html( get_the_date( 'd M Y' ) ); ?></span>
                    </div>
            	</div>

            <?php 
        	endwhile;
		endif;

		wp_reset_postdata();

		return ob_get_clean();
	}
}
add_shortcode( 'business_x_blog_widget_scode', 'reptro_blog_widget_shortcode' );


/**
 * Contact Info Widget ShortCode
 */

add_shortcode( 'business_x_contact_widget', 'busienss_x_contact_info_widget' );

if( !function_exists('busienss_x_contact_info_widget') ){
	function busienss_x_contact_info_widget($atts){
		extract(shortcode_atts(array(
			'title_text'		=> '',
			'location_text'		=> '',
			'location_value'	=> '',
			'phone_text'		=> '',
			'phone_value'		=> '',
			'email_text'		=> '',
			'email_value'		=> '',
			'facebook'			=> '',
			'twitter'			=> '',
			'gplus'				=> '',
			'youtube'			=> '',
			'linkedin'			=> '',
			'dribbble'			=> '',
			'github'			=> '',
			'behance'			=> '',
	    ), $atts));

	    wp_enqueue_style('kc-icons');

		ob_start();
		?>
			<div class="widget-contact-info">
				<?php if( $location_text || $location_value || $phone_text || $phone_value || $email_text || $email_value ) : ?>
					<ul>
						<?php 
							$location_text || $location_value ? printf('<li><span class="contact-info-icon"><i class="sli-location-pin"></i></span><span class="contact-info-content"><span class="contact-info-heading">%1$s</span><span class="contact-info-details">%2$s</span></span>', esc_html( $location_text ), esc_html( $location_value) ) : ''; 
							$phone_text || $phone_value ? printf('<li><span class="contact-info-icon"><i class="sli-phone"></i></span><span class="contact-info-content"><span class="contact-info-heading">%1$s</span><span class="contact-info-details">%2$s</span></span>', esc_html($phone_text), esc_html($phone_value) ) : ''; 
							$email_text || $email_value ? printf('<li><span class="contact-info-icon"><i class="sli-paper-plane"></i></span><span class="contact-info-content"><span class="contact-info-heading">%1$s</span><span class="contact-info-details">%2$s</span></span>', esc_html($email_text), esc_html($email_value) ) : ''; 
						?>
					</ul>
				<?php endif; ?>

				<?php if( $facebook || $twitter || $gplus || $youtube || $linkedin || $dribbble || $github || $behance  ): ?>
					<div class="social-icons">
						<ul>
							<?php
								$facebook ? printf('<li><a href="%s" target="_blank" class="fa fa-facebook"></a></li>', esc_url($facebook) ) : '';
								$twitter ? printf('<li><a href="%s" target="_blank" class="fa fa-twitter"></a></li>', esc_url($twitter) ) : '';
								$gplus ? printf('<li><a href="%s" target="_blank" class="fa fa-google-plus"></a></li>', esc_url($gplus) ) : '';
								$youtube ? printf('<li><a href="%s" target="_blank" class="fa fa-youtube-play"></a></li>', esc_url($youtube) ) : '';
								$linkedin ? printf('<li><a href="%s" target="_blank" class="fa fa-linkedin"></a></li>', esc_url($linkedin) ) : '';
								$dribbble ? printf('<li><a href="%s" target="_blank" class="fa fa-dribbble"></a></li>', esc_url($dribbble) ) : '';
								$github ? printf('<li><a href="%s" target="_blank" class="fa fa-github"></a></li>', esc_url($github) ) : '';
								$behance ? printf('<li><a href="%s" target="_blank" class="fa fa-behance"></a></li>', esc_url($behance) ) : '';
							?>
						</ul>
					</div>
				<?php endif; ?>
			</div>

		<?php
	    return ob_get_clean();
	}
}


/**
 * Call / Mail Us ( contact_call_to_action )
 */

add_shortcode( 'xt_contact_call_to_action', 'busienss_x_contact_call_to_action' );

if( !function_exists('busienss_x_contact_call_to_action') ){
	function busienss_x_contact_call_to_action($atts){
		extract(shortcode_atts(array(
			'email_contact_title'		=> '',
			'email_contact_address'		=> '',
			'phone_contact_title'		=> '',
			'phone_contact_number'		=> ''
	    ), $atts));

		ob_start();
		?>
			<div class="xt-contact-call-to-action">
			    <div class="row">
			        <div class="col-lg-6 xt-contact-phone">
			        	<div class="xt-contact-call-to-action-inner">
				            <i class="fa fa-envelope"></i>
				            <div class="xt-contact-title">
				                <?php echo wpautop(wp_kses_post($phone_contact_title)); ?>
				            </div>
				            <div class="xt-contact-detail">
				                <?php echo esc_html( $phone_contact_number ); ?>            
				            </div>
			            </div>
			        </div>

			        <div class="col-lg-6 xt-contact-email">
			        	<div class="xt-contact-call-to-action-inner">
				            <i class="fa fa-mobile"></i>
				            <div class="xt-contact-title">
				                <?php echo wpautop(wp_kses_post($email_contact_title)); ?>
				            </div>
				            <div class="xt-contact-detail">
				            	<?php echo esc_html( $email_contact_address ); ?>           
				            </div>
			       		</div>
			        </div>
			    </div>
			</div>

		<?php
	    return ob_get_clean();
	}
}



/**
 * Contact Form 7
 */

if( !function_exists('reptro_contact_form_7_shortcode') ){
	function reptro_contact_form_7_shortcode( $atts ) {

		extract( shortcode_atts(
			array(
				'contact_form' 			=> ''
			), $atts )
		);

		ob_start();
		?>

		<?php
			if( $contact_form ){
				echo do_shortcode( '[contact-form-7 id="'. $contact_form .'"]' );
			}
		?>

		<?php
		return ob_get_clean();
	}
}
add_shortcode( 'reptro_contact_form_7_scode', 'reptro_contact_form_7_shortcode' );