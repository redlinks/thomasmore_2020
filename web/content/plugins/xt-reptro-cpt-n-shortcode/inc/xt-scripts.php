<?php

/**
 * Plugin scripts
 */


function xt_reptro_adding_plugin_scripts() {
	wp_register_script('xt-reptro-imagesloaded', plugins_url('../assets/js/imagesloaded.pkgd.min.js', __FILE__), array('jquery'), '4.1.4', true);
	wp_register_script('xt-reptro-isotope', plugins_url('../assets/js/isotope.pkgd.min.js', __FILE__), array('jquery'), '3.0.5', true);
}
add_action( 'wp_enqueue_scripts', 'xt_reptro_adding_plugin_scripts', 20 ); 