<?php

/**
 * Portfolio post type
 */

if ( ! function_exists('reptro_portfolio_post') ) {

	function reptro_portfolio_post() {

		$labels = array(
			'name'                  => _x( 'Portfolio', 'Post Type General Name', 'xt-reptro-cpt-shortcode' ),
			'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'xt-reptro-cpt-shortcode' ),
			'menu_name'             => __( 'Portfolio', 'xt-reptro-cpt-shortcode' ),
			'name_admin_bar'        => __( 'Portfolio', 'xt-reptro-cpt-shortcode' ),
			'archives'              => __( 'Portfolio Archives', 'xt-reptro-cpt-shortcode' ),
			'attributes'            => __( 'Portfolio Attributes', 'xt-reptro-cpt-shortcode' ),
			'parent_item_colon'     => __( 'Portfolio Item:', 'xt-reptro-cpt-shortcode' ),
			'all_items'             => __( 'All Portfolio', 'xt-reptro-cpt-shortcode' ),
			'add_new_item'          => __( 'Add New Portfolio', 'xt-reptro-cpt-shortcode' ),
			'add_new'               => __( 'Add New Portfolio', 'xt-reptro-cpt-shortcode' ),
			'new_item'              => __( 'Add New Portfolio', 'xt-reptro-cpt-shortcode' ),
			'edit_item'             => __( 'Edit Portfolio', 'xt-reptro-cpt-shortcode' ),
			'update_item'           => __( 'Update Portfolio', 'xt-reptro-cpt-shortcode' ),
			'view_item'             => __( 'View Portfolio', 'xt-reptro-cpt-shortcode' ),
			'view_items'            => __( 'View Portfolio', 'xt-reptro-cpt-shortcode' ),
			'search_items'          => __( 'Search Portfolio', 'xt-reptro-cpt-shortcode' ),
			'not_found'             => __( 'No Portfolio Found', 'xt-reptro-cpt-shortcode' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'xt-reptro-cpt-shortcode' ),
			'featured_image'        => __( 'Portfolio Image', 'xt-reptro-cpt-shortcode' ),
			'set_featured_image'    => __( 'Set Portfolio image', 'xt-reptro-cpt-shortcode' ),
			'remove_featured_image' => __( 'Remove Portfolio image', 'xt-reptro-cpt-shortcode' ),
			'use_featured_image'    => __( 'Use as Portfolio featured image', 'xt-reptro-cpt-shortcode' ),
			'insert_into_item'      => __( 'Insert into item', 'xt-reptro-cpt-shortcode' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'xt-reptro-cpt-shortcode' ),
			'items_list'            => __( 'Items list', 'xt-reptro-cpt-shortcode' ),
			'items_list_navigation' => __( 'Items list navigation', 'xt-reptro-cpt-shortcode' ),
			'filter_items_list'     => __( 'Filter items list', 'xt-reptro-cpt-shortcode' ),
		);
		$rewrite = array(
			'slug'                  => apply_filters( 'business_x_project_slug', 'project-details' ),
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Portfolio', 'xt-reptro-cpt-shortcode' ),
			'description'           => __( 'reptro Portfolio', 'xt-reptro-cpt-shortcode' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'thumbnail', 'editor', 'comments' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 80,
			'menu_icon'             => 'dashicons-format-gallery',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,		
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
		);
		register_post_type( 'business_x_gallery', $args );

	}
	add_action( 'init', 'reptro_portfolio_post', 0 );

}

/**
 * Portfolio post type Taxanomy
 */
if ( ! function_exists('reptro_portfolio_taxonomy') ) {
	function reptro_portfolio_taxonomy() {
	    // Custom taxanomy for Portfolio Item 
	    register_taxonomy(
	        'business_x_gallery_cat',  
	        'business_x_gallery',                  
	        array(
	            'hierarchical'          => true,
	            'label'                 => 'Portfolio Category',  
	            'query_var'             => true,
	            'show_admin_column'     => true,
	            'rewrite'               => array(
	                'slug'              => 'portfolio-category', 
	                'with_front'    => true 
	            )
	        )
	    );
	}
	add_action( 'init', 'reptro_portfolio_taxonomy');
}



/**
 * Testimonial post type
 */
if ( ! function_exists('reptro_testimonial_post_type') ) {

	// Register Custom Post Type
	function reptro_testimonial_post_type() {

		$labels = array(
			'name'                  => _x( 'Testimonials', 'Post Type General Name', 'xt-reptro-cpt-shortcode' ),
			'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'xt-reptro-cpt-shortcode' ),
			'menu_name'             => __( 'Testimonial', 'xt-reptro-cpt-shortcode' ),
			'name_admin_bar'        => __( 'Testimonial', 'xt-reptro-cpt-shortcode' ),
			'archives'              => __( 'Testimonial Archives', 'xt-reptro-cpt-shortcode' ),
			'attributes'            => __( 'Testimonial Attributes', 'xt-reptro-cpt-shortcode' ),
			'parent_item_colon'     => __( 'Testimonial item:', 'xt-reptro-cpt-shortcode' ),
			'all_items'             => __( 'All Testimonials', 'xt-reptro-cpt-shortcode' ),
			'add_new_item'          => __( 'Add New Testimonial', 'xt-reptro-cpt-shortcode' ),
			'add_new'               => __( 'Add Testimonial', 'xt-reptro-cpt-shortcode' ),
			'new_item'              => __( 'New Testimonial', 'xt-reptro-cpt-shortcode' ),
			'edit_item'             => __( 'Edit Testimonial', 'xt-reptro-cpt-shortcode' ),
			'update_item'           => __( 'Update Testimonial', 'xt-reptro-cpt-shortcode' ),
			'view_item'             => __( 'View Testimonial', 'xt-reptro-cpt-shortcode' ),
			'view_items'            => __( 'View Testimonials', 'xt-reptro-cpt-shortcode' ),
			'search_items'          => __( 'Search Testimonial', 'xt-reptro-cpt-shortcode' ),
			'not_found'             => __( 'Not found', 'xt-reptro-cpt-shortcode' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'xt-reptro-cpt-shortcode' ),
			'featured_image'        => __( 'Featured Image', 'xt-reptro-cpt-shortcode' ),
			'set_featured_image'    => __( 'Set testimonial featured image', 'xt-reptro-cpt-shortcode' ),
			'remove_featured_image' => __( 'Remove testimonial featured image', 'xt-reptro-cpt-shortcode' ),
			'use_featured_image'    => __( 'Use as Testimonial featured image', 'xt-reptro-cpt-shortcode' ),
			'insert_into_item'      => __( 'Insert into item', 'xt-reptro-cpt-shortcode' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'xt-reptro-cpt-shortcode' ),
			'items_list'            => __( 'Testimonial list', 'xt-reptro-cpt-shortcode' ),
			'items_list_navigation' => __( 'Testimonials list navigation', 'xt-reptro-cpt-shortcode' ),
			'filter_items_list'     => __( 'Filter Testimonials list', 'xt-reptro-cpt-shortcode' ),
		);
		$args = array(
			'label'                 => __( 'Testimonial', 'xt-reptro-cpt-shortcode' ),
			'description'           => __( 'reptro Testimonial', 'xt-reptro-cpt-shortcode' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 80,
			'menu_icon'             => 'dashicons-id-alt',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,		
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'business_x_testi', $args );

	}
	add_action( 'init', 'reptro_testimonial_post_type', 0 );

}




/**
 * Slider post type
 */

if ( ! function_exists('reptro_slider_post_type') ) {

	function reptro_slider_post_type() {

		$labels = array(
			'name'                  => _x( 'Sliders', 'Post Type General Name', 'xt-reptro-cpt-shortcode' ),
			'singular_name'         => _x( 'Slider', 'Post Type Singular Name', 'xt-reptro-cpt-shortcode' ),
			'menu_name'             => __( 'Sliders', 'xt-reptro-cpt-shortcode' ),
			'name_admin_bar'        => __( 'Slider', 'xt-reptro-cpt-shortcode' ),
			'archives'              => __( 'Slider Archives', 'xt-reptro-cpt-shortcode' ),
			'attributes'            => __( 'Slider Attributes', 'xt-reptro-cpt-shortcode' ),
			'parent_item_colon'     => __( 'Slider Item:', 'xt-reptro-cpt-shortcode' ),
			'all_items'             => __( 'All Sliders', 'xt-reptro-cpt-shortcode' ),
			'add_new_item'          => __( 'Add New Slider', 'xt-reptro-cpt-shortcode' ),
			'add_new'               => __( 'Add New Slider', 'xt-reptro-cpt-shortcode' ),
			'new_item'              => __( 'Add New Slider', 'xt-reptro-cpt-shortcode' ),
			'edit_item'             => __( 'Edit Slider', 'xt-reptro-cpt-shortcode' ),
			'update_item'           => __( 'Update Slider', 'xt-reptro-cpt-shortcode' ),
			'view_item'             => __( 'View Slider', 'xt-reptro-cpt-shortcode' ),
			'view_items'            => __( 'View Sliders', 'xt-reptro-cpt-shortcode' ),
			'search_items'          => __( 'Search Slider', 'xt-reptro-cpt-shortcode' ),
			'not_found'             => __( 'No Slider Found', 'xt-reptro-cpt-shortcode' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'xt-reptro-cpt-shortcode' ),
			'featured_image'        => __( 'Slider Image', 'xt-reptro-cpt-shortcode' ),
			'set_featured_image'    => __( 'Set Slider image', 'xt-reptro-cpt-shortcode' ),
			'remove_featured_image' => __( 'Remove Slider image', 'xt-reptro-cpt-shortcode' ),
			'use_featured_image'    => __( 'Use as Slider featured image', 'xt-reptro-cpt-shortcode' ),
			'insert_into_item'      => __( 'Insert into item', 'xt-reptro-cpt-shortcode' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'xt-reptro-cpt-shortcode' ),
			'items_list'            => __( 'Items list', 'xt-reptro-cpt-shortcode' ),
			'items_list_navigation' => __( 'Items list navigation', 'xt-reptro-cpt-shortcode' ),
			'filter_items_list'     => __( 'Filter items list', 'xt-reptro-cpt-shortcode' ),
		);
		$args = array(
			'label'                 => __( 'Slider', 'xt-reptro-cpt-shortcode' ),
			'description'           => __( 'reptro Slider', 'xt-reptro-cpt-shortcode' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 80,
			'menu_icon'             => 'dashicons-slides',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,		
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'business_x_slider', $args );

	}
	add_action( 'init', 'reptro_slider_post_type', 0 );

}


/**
 * Team post type
 */
if ( ! function_exists('reptro_team_post_type') ) {

	// Register Custom Post Type
	function reptro_team_post_type() {

		$labels = array(
			'name'                  => _x( 'Instructor', 'Post Type General Name', 'xt-reptro-cpt-shortcode' ),
			'singular_name'         => _x( 'Instructor', 'Post Type Singular Name', 'xt-reptro-cpt-shortcode' ),
			'menu_name'             => __( 'Instructor', 'xt-reptro-cpt-shortcode' ),
			'name_admin_bar'        => __( 'Instructor', 'xt-reptro-cpt-shortcode' ),
			'archives'              => __( 'Instructor Archives', 'xt-reptro-cpt-shortcode' ),
			'attributes'            => __( 'Instructor Attributes', 'xt-reptro-cpt-shortcode' ),
			'parent_item_colon'     => __( 'Instructor item:', 'xt-reptro-cpt-shortcode' ),
			'all_items'             => __( 'All Instructors', 'xt-reptro-cpt-shortcode' ),
			'add_new_item'          => __( 'Add New Instructor', 'xt-reptro-cpt-shortcode' ),
			'add_new'               => __( 'Add Instructor', 'xt-reptro-cpt-shortcode' ),
			'new_item'              => __( 'New Instructor', 'xt-reptro-cpt-shortcode' ),
			'edit_item'             => __( 'Edit Instructor', 'xt-reptro-cpt-shortcode' ),
			'update_item'           => __( 'Update Instructor', 'xt-reptro-cpt-shortcode' ),
			'view_item'             => __( 'View Instructor', 'xt-reptro-cpt-shortcode' ),
			'view_items'            => __( 'View Instructors', 'xt-reptro-cpt-shortcode' ),
			'search_items'          => __( 'Search Instructor', 'xt-reptro-cpt-shortcode' ),
			'not_found'             => __( 'Not found', 'xt-reptro-cpt-shortcode' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'xt-reptro-cpt-shortcode' ),
			'featured_image'        => __( 'Featured Image', 'xt-reptro-cpt-shortcode' ),
			'set_featured_image'    => __( 'Set Instructor featured image', 'xt-reptro-cpt-shortcode' ),
			'remove_featured_image' => __( 'Remove Instructor featured image', 'xt-reptro-cpt-shortcode' ),
			'use_featured_image'    => __( 'Use as Instructor featured image', 'xt-reptro-cpt-shortcode' ),
			'insert_into_item'      => __( 'Insert into item', 'xt-reptro-cpt-shortcode' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Instructor', 'xt-reptro-cpt-shortcode' ),
			'items_list'            => __( 'Instructor list', 'xt-reptro-cpt-shortcode' ),
			'items_list_navigation' => __( 'Instructors list navigation', 'xt-reptro-cpt-shortcode' ),
			'filter_items_list'     => __( 'Filter Instructors list', 'xt-reptro-cpt-shortcode' ),
		);
		$rewrite = array(
			'slug'                  => apply_filters( 'business_x_team_slug', 'instructor' ),
			'with_front'            => true,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Instructor', 'xt-reptro-cpt-shortcode' ),
			'description'           => __( 'Instructor Details', 'xt-reptro-cpt-shortcode' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 80,
			'menu_icon'             => 'dashicons-id-alt',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,		
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
		);
		register_post_type( 'business_x_team', $args );

	}
	add_action( 'init', 'reptro_team_post_type', 0 );
}








