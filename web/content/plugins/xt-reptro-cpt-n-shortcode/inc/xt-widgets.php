<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Register widgets
 */

if(!function_exists('reptro_register_course_widgets')){
	function reptro_register_course_widgets() {
	    register_widget( 'reptro_blog_post_list_widget' );
	    register_widget( 'reptro_contact_info_widget' );
	    register_widget( 'reptro_cta_widget' );
	}
}

add_action( 'widgets_init', 'reptro_register_course_widgets' );


/**
 * Blog post list Widget
 */
if(!class_exists('reptro_blog_post_list_widget')) {
	class reptro_blog_post_list_widget extends WP_Widget {

		/* Widget name and description */		
		function __construct() {
			parent::__construct(
				'reptro_blog_post_list_widget',
				esc_html__( 'Reptro Blog Post',  'xt-reptro-cpt-shortcode' ),
				array( 'description' => esc_html__( 'Reptro Blog Post List',  'xt-reptro-cpt-shortcode' ), )
			);
		}


		/* Front-end display of widget */
		public function widget( $args, $instance ) {

			extract( $args );
			extract( $instance );

			echo  $before_widget;

			if ( ! empty( $title ) ) {
				echo  $before_title . apply_filters( 'widget_title', $title ). $after_title;
			}
 
			echo do_shortcode( '[business_x_blog_widget_scode type="list" exclude="'.$exclude.'" include="'.$include.'" number="'.$number.'" orderby="'.$orderby.'" order="'.$order.'"]' ); 
 
			echo  $after_widget;
		}


		/* Back-end widget form */
		public function form( $instance ) {
			extract( $instance );
			$number 	= isset($number) ? $number : 3 ;
			$orderby 	= isset($orderby) ? $orderby : 'menu_order' ;
			$order 		= isset($order) ? $order : 'ASC' ;
			if( !isset( $exclude ) ){
				$exclude = '';
			}
			if( !isset( $include ) ){
				$include = '';
			}


		?>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php if( isset( $title ) ) echo esc_attr( $title ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>"><?php esc_html_e( 'Post Orderby:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<select class="widefat" id="<?php echo  $this->get_field_id( 'orderby' ); ?>" name="<?php echo  $this->get_field_name( 'orderby' ); ?>">
					<option value="none" <?php selected( $orderby, 'none' ) ?>><?php esc_html_e( 'None', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="ID" <?php selected( $orderby, 'ID' ) ?>><?php esc_html_e( 'ID', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="author" <?php selected( $orderby, 'author' ) ?>><?php esc_html_e( 'Author', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="title" <?php selected( $orderby, 'title' ) ?>><?php esc_html_e( 'Title', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="name" <?php selected( $orderby, 'name' ) ?>><?php esc_html_e( 'Name', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="date" <?php selected( $orderby, 'date' ) ?>><?php esc_html_e( 'Date', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="rand" <?php selected( $orderby, 'rand' ) ?>><?php esc_html_e( 'Rand', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="menu_order" <?php selected( $orderby, 'menu_order' ) ?>><?php esc_html_e( 'Menu order', 'xt-reptro-cpt-shortcode' ); ?></option>
				</select>
			</p>
			
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php esc_html_e( 'Post order:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<select class="widefat" id="<?php echo  $this->get_field_id( 'order' ); ?>" name="<?php echo  $this->get_field_name( 'order' ); ?>">
					<option value="ASC" <?php selected( $order, 'ASC' ) ?>><?php esc_html_e( 'Ascending', 'xt-reptro-cpt-shortcode' ); ?></option>
					<option value="DESC" <?php selected( $order, 'DESC' ) ?>><?php esc_html_e( 'Descending', 'xt-reptro-cpt-shortcode' ); ?></option>
				</select>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of Posts:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php if( isset( $number ) ) echo esc_attr( $number ); ?>">
				<span class="reptro-widget-description"><?php esc_html_e( 'Number of posts to show. Default: 3', 'xt-reptro-cpt-shortcode' ); ?></span>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>"><?php esc_html_e( 'Include Post:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'include' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'include' ) ); ?>" type="text" value="<?php if( isset( $include ) ) echo esc_attr( $include ); ?>">
				<span class="reptro-widget-description"><?php esc_html_e( 'Comma separated string of user IDs to include', 'xt-reptro-cpt-shortcode' ); ?></span>
			</p>			

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'exclude' ) ); ?>"><?php esc_html_e( 'Exclude Post:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'exclude' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'exclude' ) ); ?>" type="text" value="<?php if( isset( $exclude ) ) echo esc_attr( $exclude ); ?>">
				<span class="reptro-widget-description"><?php esc_html_e( 'Comma separated string of user IDs to exclude', 'xt-reptro-cpt-shortcode' ); ?></span>
			</p>

			<?php 
		}

		/* Sanitize widget form values as they are saved */		
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] 		= ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['number'] 	= ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : '';
			$instance['orderby'] 	= ( ! empty( $new_instance['orderby'] ) ) ? strip_tags( $new_instance['orderby'] ) : '';
			$instance['order'] 		= ( ! empty( $new_instance['order'] ) ) ? strip_tags( $new_instance['order'] ) : '';
			$instance['exclude']    = ( ! empty( $new_instance['exclude'] ) ) ? strip_tags( $new_instance['exclude'] ) : '';
			$instance['include']    = ( ! empty( $new_instance['include'] ) ) ? strip_tags( $new_instance['include'] ) : '';

			return $instance;
		}

	}
}


/**
 * Contact Info Widget
 */
if(!class_exists('reptro_contact_info_widget')) {
	class reptro_contact_info_widget extends WP_Widget {

		/* Widget name and description */		
		function __construct() {
			parent::__construct(
				'reptro_contact_info_widget',
				esc_html__( 'Reptro Contact Info',  'xt-reptro-cpt-shortcode' ),
				array( 'description' => esc_html__( 'Contact info details.',  'xt-reptro-cpt-shortcode' ), )
			);
		}

		/* Front-end display of widget */
		public function widget( $args, $instance ) {

			extract( $args );
			extract( $instance );
			echo  $before_widget;

			if ( ! empty( $title ) ) {
				echo  $before_title . apply_filters( 'widget_title', $title ). $after_title;
			}

			echo do_shortcode( '[business_x_contact_widget location_text="'.$location_text.'" location_value="'.$location_value.'" phone_text="'.$phone_text.'" phone_value="'.$phone_value.'" email_text="'.$email_text.'" email_value="'.$email_value.'" facebook="'.$facebook.'" twitter="'.$twitter.'" gplus="'.$gplus.'" youtube="'.$youtube.'" linkedin="'.$linkedin.'" dribbble="'.$dribbble.'" github="'.$github.'" behance="'.$behance.'"]' ); 
 
			echo  $after_widget;
		}


		/* Back-end widget form */
		public function form( $instance ) {
			extract( $instance );
			$location_text 	= isset($location_text) ? $location_text : esc_html__( 'Location:', 'xt-reptro-cpt-shortcode' ) ;
			$location_value = isset($location_value) ? $location_value : '' ;
			$phone_text 	= isset($phone_text) ? $phone_text : esc_html__( 'Phone:', 'xt-reptro-cpt-shortcode' ) ;
			$phone_value 	= isset($phone_value) ? $phone_value : '' ;
			$email_text 	= isset($email_text) ? $email_text : esc_html__( 'Email:', 'xt-reptro-cpt-shortcode' ) ;
			$email_value 	= isset($email_value) ? $email_value : '' ;
			$facebook 		= isset($facebook) ? $facebook : '' ;
			$twitter 		= isset($twitter) ? $twitter : '' ;
			$gplus 			= isset($gplus) ? $gplus : '' ;
			$youtube 		= isset($youtube) ? $youtube : '' ;
			$linkedin 		= isset($linkedin) ? $linkedin : '' ;
			$dribbble 		= isset($dribbble) ? $dribbble : '' ;
			$github 		= isset($github) ? $github : '' ;
			$behance 		= isset($behance) ? $behance : '' ;

			?>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php if( isset( $title ) ) echo esc_attr( $title ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'location_text' ) ); ?>"><?php esc_html_e( 'Location:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'location_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'location_text' ) ); ?>" type="text" value="<?php if( isset( $location_text ) ) echo esc_attr( $location_text ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'location_value' ) ); ?>"><?php esc_html_e( 'Location details:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'location_value' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'location_value' ) ); ?>" type="text" value="<?php if( isset( $location_value ) ) echo esc_attr( $location_value ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'phone_text' ) ); ?>"><?php esc_html_e( 'Phone heading:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone_text' ) ); ?>" type="text" value="<?php if( isset( $phone_text ) ) echo esc_attr( $phone_text ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'phone_value' ) ); ?>"><?php esc_html_e( 'Phone details:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'phone_value' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'phone_value' ) ); ?>" type="text" value="<?php if( isset( $phone_value ) ) echo esc_attr( $phone_value ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'email_text' ) ); ?>"><?php esc_html_e( 'Email heading:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'email_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'email_text' ) ); ?>" type="text" value="<?php if( isset( $email_text ) ) echo esc_attr( $email_text ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'email_value' ) ); ?>"><?php esc_html_e( 'Email details:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'email_value' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'email_value' ) ); ?>" type="text" value="<?php if( isset( $email_value ) ) echo esc_attr( $email_value ); ?>">
			</p>


			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php esc_html_e( 'Facebook:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" type="text" value="<?php if( isset( $facebook ) ) echo esc_attr( $facebook ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php esc_html_e( 'Twitter:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" type="text" value="<?php if( isset( $twitter ) ) echo esc_attr( $twitter ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'gplus' ) ); ?>"><?php esc_html_e( 'Google Plus:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'gplus' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'gplus' ) ); ?>" type="text" value="<?php if( isset( $gplus ) ) echo esc_attr( $gplus ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>"><?php esc_html_e( 'YouTube:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ); ?>" type="text" value="<?php if( isset( $youtube ) ) echo esc_attr( $youtube ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>"><?php esc_html_e( 'Linkedin:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'linkedin' ) ); ?>" type="text" value="<?php if( isset( $linkedin ) ) echo esc_attr( $linkedin ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'dribbble' ) ); ?>"><?php esc_html_e( 'Dribbble:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'dribbble' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'dribbble' ) ); ?>" type="text" value="<?php if( isset( $dribbble ) ) echo esc_attr( $dribbble ); ?>">
			</p>


			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'github' ) ); ?>"><?php esc_html_e( 'Github:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'github' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'github' ) ); ?>" type="text" value="<?php if( isset( $github ) ) echo esc_attr( $github ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'behance' ) ); ?>"><?php esc_html_e( 'Behance:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'behance' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'behance' ) ); ?>" type="text" value="<?php if( isset( $behance ) ) echo esc_attr( $behance ); ?>">
			</p>

			<?php 
		}

		/* Sanitize widget form values as they are saved */		
		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] 			= ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['number'] 		= ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : '';
			$instance['location_text'] 	= ( ! empty( $new_instance['location_text'] ) ) ? strip_tags( $new_instance['location_text'] ) : '';
			$instance['location_value'] = ( ! empty( $new_instance['location_value'] ) ) ? strip_tags( $new_instance['location_value'] ) : '';
			$instance['phone_text'] 	= ( ! empty( $new_instance['phone_text'] ) ) ? strip_tags( $new_instance['phone_text'] ) : '';
			$instance['phone_value'] 	= ( ! empty( $new_instance['phone_value'] ) ) ? strip_tags( $new_instance['phone_value'] ) : '';
			$instance['email_text'] 	= ( ! empty( $new_instance['email_text'] ) ) ? strip_tags( $new_instance['email_text'] ) : '';
			$instance['email_value'] 	= ( ! empty( $new_instance['email_value'] ) ) ? strip_tags( $new_instance['email_value'] ) : '';
			$instance['facebook'] 		= ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
			$instance['twitter'] 		= ( ! empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
			$instance['gplus'] 			= ( ! empty( $new_instance['gplus'] ) ) ? strip_tags( $new_instance['gplus'] ) : '';
			$instance['youtube'] 		= ( ! empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : '';
			$instance['linkedin'] 		= ( ! empty( $new_instance['linkedin'] ) ) ? strip_tags( $new_instance['linkedin'] ) : '';
			$instance['dribbble'] 		= ( ! empty( $new_instance['dribbble'] ) ) ? strip_tags( $new_instance['dribbble'] ) : '';
			$instance['github'] 		= ( ! empty( $new_instance['github'] ) ) ? strip_tags( $new_instance['github'] ) : '';
			$instance['behance'] 		= ( ! empty( $new_instance['behance'] ) ) ? strip_tags( $new_instance['behance'] ) : '';
			
			return $instance;
		}

	}
}


/**
 * Call to action Widget
 */

if(!class_exists('reptro_cta_widget')) {
	class reptro_cta_widget extends WP_Widget {

		/* Widget name and description */
		
		function __construct() {
			parent::__construct(
				'reptro_cta_widget',
				esc_html__( 'Reptro Call to Action',  'xt-reptro-cpt-shortcode' ),
				array( 'description' => esc_html__( 'Reptro theme call to action widget.',  'xt-reptro-cpt-shortcode' ), )
			);
		}


		/* Front-end display of widget */

		public function widget( $args, $instance ) {

			extract( $args );
			extract( $instance );

			echo $before_widget;

			?>
				<div class="reptro-cta-widget shadow">
					<div class="reptro-volunteer-inner">
						<div class="reptro-volunteer-content-box">

							<?php if ( ! empty( $title ) ): ?>
								<h4 class="widget-title"><?php echo esc_html( apply_filters( 'widget_title', $title ) ); ?></h4>
							<?php endif; ?>

							<?php if ( ! empty( $action_content ) ): ?>
								<div class="call-to-action-text"><?php echo esc_html( $action_content ); ?></div>
							<?php endif; ?>

							<?php if ( ! empty( $btn_text ) ): ?>
								<a class="reptro-call-to-action-btn" <?php echo ( $new_window == 'on' ? 'target="_blank"' : '' ); ?> href="<?php echo esc_url( $btn_url ); ?>" title="<?php echo esc_html( $btn_text ); ?>"><?php echo esc_html( $btn_text ); ?></a>
							<?php endif; ?>		
						</div>
					</div>
				</div>
			<?php

			echo $after_widget;
		}


		/* Back-end widget form */

		public function form( $instance ) {
			extract( $instance );
			if( !isset( $action_content ) ){
				$action_content = '';
			}
			if( !isset( $btn_text ) ){
				$btn_text = '';
			}
			if( !isset( $btn_url ) ){
				$btn_url = '';
			}
			?>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php if( isset( $title ) ) echo esc_attr( $title ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'action_content' ) ); ?>"><?php esc_html_e( 'Content:', 'xt-reptro-cpt-shortcode' ); ?></label>
				<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'action_content' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'action_content' ) ); ?>"><?php if( isset( $action_content ) ) echo esc_attr( $action_content ); ?></textarea>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'btn_text' ) ); ?>"><?php esc_html_e( 'Button Text:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'btn_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'btn_text' ) ); ?>" type="text" value="<?php if( isset( $btn_text ) ) echo esc_attr( $btn_text ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'btn_url' ) ); ?>"><?php esc_html_e( 'Button URL:', 'xt-reptro-cpt-shortcode' ); ?></label> 
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'btn_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'btn_url' ) ); ?>" type="text" value="<?php if( isset( $btn_url ) ) echo esc_attr( $btn_url ); ?>">
			</p>

			<p>
				<span style="display: block"><?php esc_html_e( 'Open link on new window?', 'xt-reptro-cpt-shortcode' ); ?></span>
				<input class="checkbox" type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'new_window' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'new_window' ) ); ?>" <?php checked( $new_window, 'on' );?>>
				<label for="<?php echo esc_attr( $this->get_field_id( 'new_window' ) ); ?>"><?php esc_html_e( 'Yes', 'xt-reptro-cpt-shortcode' ); ?></label>
			</p>


			<?php 
		}

		/* Sanitize widget form values as they are saved */

		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] 			= ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['action_content'] = ( ! empty( $new_instance['action_content'] ) ) ? strip_tags( $new_instance['action_content'] ) : '';
			$instance['btn_text'] 		= ( ! empty( $new_instance['btn_text'] ) ) ? strip_tags( $new_instance['btn_text'] ) : '';
			$instance['btn_url'] 		= ( ! empty( $new_instance['btn_url'] ) ) ? strip_tags( $new_instance['btn_url'] ) : '';
			$instance['new_window']     = ( ! empty( $new_instance['new_window'] ) ) ? strip_tags( $new_instance['new_window'] ) : '';

			return $instance;
		}

	}
}
