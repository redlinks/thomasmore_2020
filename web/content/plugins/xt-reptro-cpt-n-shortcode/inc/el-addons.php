<?php



/**
 * Add a New Elementor Widegt Category
 */

add_action( 'elementor/init', 'reptro_add_elementor_category' );

if( !function_exists('reptro_add_elementor_category') ){
    function reptro_add_elementor_category(){
        \Elementor\Plugin::instance()->elements_manager->add_category(
            'reptro_widgets',
            array(
                'title' => esc_html__( 'Reptro', 'xt-reptro-cpt-shortcode' ),
            ),
            1
        );
    }
}


/**
 * Add Reptro Elementor Widegts
 */

add_action( 'elementor/widgets/widgets_registered', 'reptro_add_elementor_widgets' );

if( !function_exists('reptro_add_elementor_widgets') ){
	function reptro_add_elementor_widgets(){

        include Reptro_Plugin_Template_Path . 'hero-area.php';
        include Reptro_Plugin_Template_Path . 'section-title.php';
        include Reptro_Plugin_Template_Path . 'section-sub-title.php';
        include Reptro_Plugin_Template_Path . 'service.php';
        include Reptro_Plugin_Template_Path . 'main-slider.php';
        include Reptro_Plugin_Template_Path . 'instructor.php';
        include Reptro_Plugin_Template_Path . 'portfolio.php';
        include Reptro_Plugin_Template_Path . 'feature.php';
        include Reptro_Plugin_Template_Path . 'feature-two.php';
        include Reptro_Plugin_Template_Path . 'video-popup.php';
        include Reptro_Plugin_Template_Path . 'testimonial.php';
        include Reptro_Plugin_Template_Path . 'posts.php';
        include Reptro_Plugin_Template_Path . 'fancy-list.php';
        include Reptro_Plugin_Template_Path . 'progressbar.php';
        include Reptro_Plugin_Template_Path . 'contact-call-to-action.php';
        include Reptro_Plugin_Template_Path . 'events.php';
        include Reptro_Plugin_Template_Path . 'course.php';
        include Reptro_Plugin_Template_Path . 'course-categories.php';
        include Reptro_Plugin_Template_Path . 'client-logo.php';
        include Reptro_Plugin_Template_Path . 'contact-form-seven.php';
        include Reptro_Plugin_Template_Path . 'call-to-action.php';
	}
}


/**
 * Section Padding 
 */

add_action('elementor/element/before_section_end', function( $section, $section_id, $args ) {
    if( $section->get_name() == 'section' && $section_id == 'section_layout' ){

        $section->add_control(
            'reptro_section_padding',
            [
                'label'         => _x( 'Section Padding (top & bottom)', 'Section Control', 'xt-reptro-cpt-shortcode' ),
                'type'          => Elementor\Controls_Manager::SELECT,
                'options'       => [
                    'default'   => _x( 'Default', 'Section Control', 'xt-reptro-cpt-shortcode' ),
                    'no'        => _x( 'No', 'Section Control', 'xt-reptro-cpt-shortcode' ),
                    'medium'    => _x( 'Medium', 'Section Control', 'xt-reptro-cpt-shortcode' ),
                    'large'     => _x( 'Large', 'Section Control', 'xt-reptro-cpt-shortcode' ),
                ],
                'default'       => 'large',
                'render_type'   => 'ui',
                'prefix_class'  => 'reptro-section-padding-',
                'label_block'   => true,
                'hide_in_inner' => true,
            ]
        );
    }
}, 10, 3 );


/**
 * theme primary background color & text color white for section
 */

add_action('elementor/element/before_section_end', function( $section, $section_id, $args ) {
    if( $section->get_name() == 'section' && $section_id == 'section_layout' ){

        $section->add_control(
            'reptro_section_bg_and_color_control',
            [
                'label'         => esc_html__( 'Primary Background & Color White', 'xt-reptro-cpt-shortcode' ),
                'description'   => esc_html__( 'Check this to set default theme color background and white color text.', 'xt-reptro-cpt-shortcode' ),
                'type'          => Elementor\Controls_Manager::SWITCHER,
                'default'       => '',
                'prefix_class'  => 'xt_row_primary_bg_color_white xt_row_primary_bg_color_white_',
                'hide_in_inner' => true,
            ]
        );

        $section->add_control(
            'reptro_section_top_bottom_padding',
            [
                'label'         => esc_html__( 'Section inner padding', 'xt-reptro-cpt-shortcode' ),
                'description'   => esc_html__( 'Disable section inner top & bottom padding.', 'xt-reptro-cpt-shortcode' ),
                'type'          => Elementor\Controls_Manager::SWITCHER,
                'default'       => 'no',
                'condition' => [
                    'gap' => [ 'extended' ],
                ],
                'prefix_class'  => 'reptro_section_top_bottom_padding_',
                'hide_in_inner' => true,
            ]
        );
    }
}, 10, 3 );


/**
 * button theme primary
 */

add_action( 'elementor/element/before_section_end', function( $element, $section_id, $args ) {
    if ( 'button' === $element->get_name() && 'section_button' === $section_id ) {

        $element->add_control(
            'reptro_need_button_theme_primary',
            [
                'label'         => esc_html__( 'Button Theme Primary Style?', 'xt-reptro-cpt-shortcode' ),
                'type'          => Elementor\Controls_Manager::SWITCHER,
                'return_value'  => 'yes',
            ]
        );
    }
}, 10, 3 );


/**
 * button option theme primary
 */

add_action( 'elementor/element/before_section_end', function( $element, $section_id, $args ) {
    if ( 'button' === $element->get_name() && 'section_button' === $section_id ) {

        $element->add_control(
            'button_theme_primary_style_option',
            [
                'label'         => esc_html__( 'Button Type', 'xt-reptro-cpt-shortcode' ),
                'type'          => Elementor\Controls_Manager::SELECT,
                'label_block'   => true,
                'default'       => 'border',
                'options'       => [
                    'fill'      => esc_html__( 'Theme Button Fill', 'xt-reptro-cpt-shortcode' ),
                    'border'    => esc_html__( 'Theme Button Border', 'xt-reptro-cpt-shortcode' )
                ],
                'prefix_class' => 'repto-button-',
                'condition'     => [
                    'reptro_need_button_theme_primary' => "yes"
                ]                
            ]
        );
    }
}, 10, 3 );


/**
 * theme primary background color for column
 */
add_action('elementor/element/before_section_end', function( $section, $section_id, $args ) {
    if( $section->get_name() == 'column' && $section_id == 'layout' ){

        $section->add_control(
            'reptro_column_bg_color',
            [
                'label'        => esc_html__( 'Theme Primary Background Color & Text Color White:', 'xt-reptro-cpt-shortcode' ),
                'type'         => Elementor\Controls_Manager::SWITCHER,
                'default'      => '',
                'prefix_class' => 'xt_column_primary_bg_color xt_column_primary_bg_color_'
            ]
        );
    }
}, 10, 3 );



/**
 * theme primary accordin style
 */

add_action('elementor/element/before_section_end', function( $section, $section_id, $args ) {
    if( $section->get_name() == 'accordion' && $section_id == 'section_title' ){

        $section->add_control(
            'reptro_accordian_style',
            [
                'label'        => esc_html__( 'Theme Primary Accordian Style:', 'xt-reptro-cpt-shortcode' ),
                'type'         => Elementor\Controls_Manager::SWITCHER,
                'default'      => '',
                'prefix_class' => 'xt_accordian_style_'
            ]
        );
    }
}, 10, 3 );


/**
 * theme primary counter style
 */

add_action('elementor/element/before_section_end', function( $section, $section_id, $args ) {
    if( $section->get_name() == 'counter' && $section_id == 'section_counter' ){

        $section->add_control(
            'reptro_counter_style',
            [
                'label'        => esc_html__( 'Theme Primary Counter Style:', 'xt-reptro-cpt-shortcode' ),
                'type'         => Elementor\Controls_Manager::SWITCHER,
                'default'      => '',
                'prefix_class' => 'xt_counter_style_'
            ]
        );
    }
}, 10, 3 );