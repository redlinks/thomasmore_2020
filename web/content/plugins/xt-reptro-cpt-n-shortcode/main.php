<?php
/**
 * Plugin Name:       Reptro CPT and Shortcode
 * Plugin URI:        https://xoothemes.com
 * Description:       Custom post types and shortcodes for Reptro theme.
 * Version:           1.1.9
 * Author:            XooThemes
 * Author URI:        https://xoothemes.com
 * Text Domain:       xt-reptro-cpt-shortcode
 * Domain Path:       /languages
 */


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 


/* Set constant path to the plugin directory. */

define( 'Reptro_Plugin_Path', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'Reptro_Plugin_Template_Path', Reptro_Plugin_Path . '/templates/' );

/**
 * Localization
 */

function xt_reptro_textdomain() {
	load_plugin_textdomain( 'xt-reptro-cpt-shortcode', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'xt_reptro_textdomain' );



/**
 * Requred files 
 */

$theme = wp_get_theme();

if ( 'Reptro' == $theme->name || 'Reptro' == $theme->parent_theme ) {
	require_once dirname( __FILE__ ) . '/inc/ds_breadcrumbs.php';
	require_once dirname( __FILE__ ) . '/inc/xt-functions.php';
	require_once dirname( __FILE__ ) . '/inc/xt-cpt.php';
	require_once dirname( __FILE__ ) . '/admin/codestar-framework/cs-framework.php';
	require_once dirname( __FILE__ ) . '/inc/xt-shortcode.php';
	require_once dirname( __FILE__ ) . '/inc/xt-widgets.php';
	require_once dirname( __FILE__ ) . '/inc/xt-scripts.php';

	if( defined('ELEMENTOR_VERSION') ) {
		require_once dirname( __FILE__ ) . '/inc/el-addons.php';
	}
}