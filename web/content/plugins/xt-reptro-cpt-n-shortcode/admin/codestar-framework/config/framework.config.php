<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => esc_html__('Theme Options', 'xt-reptro-cpt-shortcode'),
  'menu_type'       => 'menu',
  'menu_slug'       => 'reptro-options',
  'ajax_save'       => false,
  'show_reset_all'  => false,
  'framework_title' => sprintf( '%s <small>%s</small>', esc_html__('Reptro', 'xt-reptro-cpt-shortcode'), 'by XooThemes' ),
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();


// ------------------------------
// Header Setting                  -
// ------------------------------

$options[]      = array(
  'name'        => 'header_settings',
  'title'       => esc_html__('Header Settings', 'xt-reptro-cpt-shortcode'),
  'icon'        => 'cs-icon fa fa-arrow-circle-up',

  // begin: fields
  'fields'      => array(
    array(
      'type'    => 'notice',
      'class'   => 'info',
      'content' => sprintf( '<h3>%s</h3>', esc_html__('Pre Header', 'xt-reptro-cpt-shortcode') ),
    ),
    array(
      'id'      => 'xt_show_pre_header',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Pre Header', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Default enable.', 'xt-reptro-cpt-shortcode'),
      'default' => true,
    ),
    array(
      'id'          => 'xt_pre_header_column',
      'type'        => 'image_select',
      'title'       => esc_html__('Pre Header Column', 'xt-reptro-cpt-shortcode'),
      'default'     => '66-33',
      'wrap_class'  => 'xt-pre-header-column',
      'options'     => array(
        '50-50' => esc_url( get_template_directory_uri().'/assets/images/admin/columns/50-50.png' ),
        '33-66' => esc_url( get_template_directory_uri().'/assets/images/admin/columns/33-66.png' ),
        '66-33' => esc_url( get_template_directory_uri().'/assets/images/admin/columns/66-33.png' ),
      ),
    ),
    array(
      'id'      => 'xt_pre_header_phone',
      'type'    => 'text',
      'title'   => esc_html__('Phone number', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Will be shown in the header top bar.', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'      => 'xt_pre_header_email',
      'type'    => 'text',
      'title'   => esc_html__('Email address', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Will be shown in the header top bar.', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'          => 'xt_business_hour',
      'type'        => 'text',
      'title'       => esc_html__('Business Hour', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'             => 'pre_header_right_content',
      'type'           => 'select',
      'title'          => esc_html__('Pre Header Right Content', 'xt-reptro-cpt-shortcode'),
      'options'        => array(
        'login_register'  => esc_html__('Login register links', 'xt-reptro-cpt-shortcode'),
        'social_icons'    => esc_html__('Social icons', 'xt-reptro-cpt-shortcode'),
        'nav_menu'        => esc_html__('Navigation Menu', 'xt-reptro-cpt-shortcode'),
      ),
      'default'        => 'social_icons',
      'desc'           => esc_html__('Default: Social icons', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'             => 'xt_pre_header_menu',
      'type'           => 'select',
      'title'          => esc_html__('Pre Header Menu', 'xt-reptro-cpt-shortcode'),
      'desc'           => esc_html__('Select a menu for pre header.', 'xt-reptro-cpt-shortcode'),
      'options'        => 'menus',
      'default_option' => esc_html__('Select a menu', 'xt-reptro-cpt-shortcode'),
      'dependency'     => array( 'pre_header_right_content', '==', 'nav_menu' ),
    ),
    array(
      'id'              => 'xt_social_icons',
      'type'            => 'group',
      'title'           => esc_html__('Social Icons', 'xt-reptro-cpt-shortcode'),
      'button_title'    => esc_html__('Add New', 'xt-reptro-cpt-shortcode'),
      'accordion_title' => esc_html__('Add New Social Network', 'xt-reptro-cpt-shortcode'),
      'dependency'      => array( 'pre_header_right_content', '==', 'social_icons' ),
      'fields'          => array(

        array(
          'id'      => 'icon',
          'type'    => 'icon',
          'title'   => esc_html__('Select an Icon', 'xt-reptro-cpt-shortcode'),
        ),

        array(
          'id'          => 'url',
          'type'        => 'text',
          'title'       => esc_html__('Social Network URL', 'xt-reptro-cpt-shortcode')
        ),

      ),
    ),
    array(
      'type'    => 'notice',
      'class'   => 'info',
      'content' => sprintf( '<h3>%s</h3>', esc_html__('Main Header', 'xt-reptro-cpt-shortcode') ),
    ),
    array(
      'id'      => 'xt_show_mobile_menu_login',
      'type'    => 'switcher',
      'title'   => esc_html__('Show login/logout in mobile menu', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Default enable.', 'xt-reptro-cpt-shortcode'),
      'default' => true,
    ),
    array(
      'id'      => 'xt_show_header_btn',
      'type'    => 'switcher',
      'title'   => esc_html__('Header button', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Show header button. Default enable.', 'xt-reptro-cpt-shortcode'),
      'default' => true,
    ),
    array(
      'id'      => 'xt_show_header_btn_phone',
      'type'    => 'switcher',
      'title'   => esc_html__('Header button on Phone', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Show header button on Phone. Default disable.', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'             => 'xt_header_btn_type',
      'type'           => 'select',
      'title'          => esc_html__('Header Right Content', 'xt-reptro-cpt-shortcode'),
      'options'        => array(
        'login_or_profile'  => esc_html__('LearnPress Dashboard', 'xt-reptro-cpt-shortcode'),
        'static_btn'        => esc_html__('Static Button', 'xt-reptro-cpt-shortcode'),
      ),
      'default'        => 'login_or_profile',
      'dependency'     => array( 'xt_show_header_btn', '==', 'true' ),
      'desc'           => esc_html__('Default: LearnPress Dashboard', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'          => 'xt_show_header_btn_text',
      'type'        => 'text',
      'title'       => esc_html__('Header button text', 'xt-reptro-cpt-shortcode'),
      'default'     => esc_html__('Enroll Now', 'xt-reptro-cpt-shortcode'),
      'dependency'  => array( 'xt_header_btn_type', '==', 'static_btn' ),
    ),
    array(
      'id'          => 'xt_show_header_btn_url',
      'type'        => 'text',
      'title'       => esc_html__('Header button URL', 'xt-reptro-cpt-shortcode'),
      'default'     => esc_html__('#', 'xt-reptro-cpt-shortcode'),
      'dependency'  => array( 'xt_header_btn_type', '==', 'static_btn' ),
    ),
    array(
      'id'          => 'xt_header_btn_window',
      'type'        => 'switcher',
      'title'       => esc_html__('New Window', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('Open button on new window. Default enable.', 'xt-reptro-cpt-shortcode'),
      'default'     => true,
      'dependency'  => array( 'xt_header_btn_type', '==', 'static_btn' ),
    ),
  )
);


// ------------------------------
// Page Header Setting                  -
// ------------------------------

$options[]      = array(
  'name'        => 'page_settings',
  'title'       => esc_html__('Page Settings', 'xt-reptro-cpt-shortcode'),
  'icon'        => 'dashicons dashicons-admin-page',

  // begin: fields
  'fields'      => array(
    array(
      'id'        => 'xt_page_header_bg',
      'type'      => 'image',
      'title'     => esc_html__('Page Header Background', 'xt-reptro-cpt-shortcode'),
      'desc'      => esc_html__('Page header background image.', 'xt-reptro-cpt-shortcode'),
      'add_title' => esc_html__('Add Image', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'      => 'xt_page_header_bg_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Page header background color', 'xt-reptro-cpt-shortcode'),
      'default' => 'rgba(38, 40, 44, 0.8)',
      'rgba'    => true,
    ),
    array(
      'id'        => 'xt_page_header_p_top',
      'type'      => 'slider',
      'default'   => 45,
      'title'     => esc_html__( 'Padding Top', 'xt-reptro-cpt-shortcode' ),
      'desc'      => esc_html__('Page header padding top. Default 45px.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'step'    => 1,
        'min'     => 5,
        'max'     => 200,
      )
    ),
    array(
      'id'        => 'xt_page_header_p_bottom',
      'type'      => 'slider',
      'default'   => 45,
      'title'     => esc_html__( 'Padding Bottom', 'xt-reptro-cpt-shortcode' ),
      'desc'      => esc_html__('Page header padding bottom. Default 45px.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'step'    => 1,
        'min'     => 5,
        'max'     => 200,
      )
    ),
    array(
      'id'      => 'xt_show_breadcrumb',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Breadcrumb', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Show breadcrumb in page header. Default enable.', 'xt-reptro-cpt-shortcode'),
      'default' => true,
    ),
  )
);


// ------------------------------
// General Setting                  -
// ------------------------------

$options[]      = array(
  'name'        => 'general_settings',
  'title'       => esc_html__('General Settings', 'xt-reptro-cpt-shortcode'),
  'icon'        => 'fa fa-cogs',

  // begin: fields
  'fields'      => array(
    array(
      'id'        => 'xt_site_layout',
      'type'      => 'image_select',
      'title'     => esc_html__('Site Layout', 'xt-reptro-cpt-shortcode' ),
      'desc'      => esc_html__('You can change entire site layout to full-width or boxed layout.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'full_width'    => esc_url( get_template_directory_uri().'/assets/images/admin/full-width-page.png' ),
        'boxed'         => esc_url( get_template_directory_uri().'/assets/images/admin/box-layout-page.png' ),
      ),
      'default'    => 'boxed',
    ),
    array(
      'id'      => 'xt_show_back_to_top',
      'type'    => 'switcher',
      'title'   => esc_html__('Back to Top Icon', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Show back to top icon. Default enable.', 'xt-reptro-cpt-shortcode'),
      'default' => true,
    ),
    array(
      'id'      => 'xt_show_site_pre_loader',
      'type'    => 'switcher',
      'title'   => esc_html__('Site Preloader', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Show site preloader. Default enable.', 'xt-reptro-cpt-shortcode'),
      'default' => true,
    ),
    array(
      'id'      => 'xt_off_canvas_sidebar',
      'type'    => 'switcher',
      'title'   => esc_html__('Off Canvas Sidebar', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Show Off Canvas Sidebar. Default enable.', 'xt-reptro-cpt-shortcode'),
      'default' => true,
    ),
  )
);    


// ------------------------------
// Style Setting                  -
// ------------------------------

$options[]      = array(
  'name'        => 'style_settings',
  'title'       => esc_html__('Style Settings', 'xt-reptro-cpt-shortcode'),
  'icon'        => 'dashicons dashicons-admin-settings',

  // begin: fields
  'fields'      => array(
    array(
      'id'        => 'body_font_size',
      'type'      => 'slider',
      'default'   => 16,
      'title'     => esc_html__( 'Font size', 'xt-reptro-cpt-shortcode' ),
      'desc'      => esc_html__('Body font size. Default 16px.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'step'    => 1,
        'min'     => 5,
        'max'     => 100,
      )
    ),
    array(
      'id'        => 'body_line_height',
      'type'      => 'slider',
      'default'   => 26,
      'title'     => esc_html__( 'Line Height', 'xt-reptro-cpt-shortcode' ),
      'desc'      => esc_html__('Body line height. Default 26px.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'step'    => 1,
        'min'     => 5,
        'max'     => 100,
      )
    ),
    array(
      'id'      => 'xt_body_bg',
      'type'    => 'color_picker',
      'title'   => esc_html__('Body Background', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Site body background color.', 'xt-reptro-cpt-shortcode'),
      'default' => '#ffffff',
    ),
    array(
      'id'      => 'xt_body_bg_boxed',
      'type'    => 'color_picker',
      'title'   => esc_html__('Body Background ( Boxed Site )', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Select background color for site layout boxed.', 'xt-reptro-cpt-shortcode'),
      'default' => '#efefef',
    ),
    array(
      'id'      => 'xt_body_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Body color', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Site body color.', 'xt-reptro-cpt-shortcode'),
      'default' => '#555',
    ),
    array(
      'id'      => 'xt_title_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Title color', 'xt-reptro-cpt-shortcode'),
      'desc'    => esc_html__('Site title color.', 'xt-reptro-cpt-shortcode'),
      'default' => '#303d48',
    ),
    array(
      'type'    => 'notice',
      'class'   => 'info',
      'content' => sprintf( '<h3>%s</h3>', esc_html__('Color Customization', 'xt-reptro-cpt-shortcode') ),
    ),
    array(
      'id'      => 'need_color_customizer',
      'type'    => 'switcher',
      'title'   => esc_html__('Need color customizer?', 'xt-reptro-cpt-shortcode'),
      'default' => false
    ),
    array(
      'id'         => 'xt_primary_color',
      'type'       => 'color_picker',
      'title'      => esc_html__('Theme Primary Color', 'xt-reptro-cpt-shortcode'),
      'default'    => '#E91E63',
      'dependency' => array( 'need_color_customizer', '==', 'true' )
    ),
    array(
      'id'         => 'xt_primary_color_dark',
      'type'       => 'color_picker',
      'title'      => esc_html__('Theme Primary Color Dark', 'xt-reptro-cpt-shortcode'),
      'default'    => '#C2185B',
      'dependency' => array( 'need_color_customizer', '==', 'true' )
    ),
    array(
      'id'         => 'xt_primary_color_light',
      'type'       => 'color_picker',
      'title'      => esc_html__('Theme Primary Color Light', 'xt-reptro-cpt-shortcode'),
      'default'    => 'rgba(233, 30, 99, 0.9)',
      'dependency' => array( 'need_color_customizer', '==', 'true' )
    ),
  ),
);


// ------------------------------
// Blog Setting                  -
// ------------------------------

$options[]      = array(
  'name'        => 'blog_settings',
  'title'       => esc_html__('Blog Settings', 'xt-reptro-cpt-shortcode'),
  'icon'        => 'fa fa-rss',

  // begin: fields
  'fields'      => array(
    array(
      'id'          => 'feature_image_width',
      'type'        => 'number',
      'title'       => esc_html__('Blog Feature Image Width', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('If you changed the image size, you have to regenerate thumbnails. You can use any regenerate thumbnails plugin for that.', 'xt-reptro-cpt-shortcode'),
      'default'     => 1200,
      'after'       => ' <i class="cs-text-muted">(px)</i>',
    ),
    array(
      'id'      => 'feature_image_height',
      'type'    => 'number',
      'title'   => esc_html__('Blog Feature Image Height', 'xt-reptro-cpt-shortcode'),
      'default' => 560,
      'after'   => ' <i class="cs-text-muted">(px)</i>',
    ),
    array(
      'id'        => 'blog_layout',
      'type'      => 'image_select',
      'title'     => esc_html__('Blog Layout', 'xt-reptro-cpt-shortcode'),
      'desc'      => esc_html__('Choose a layout for your blog, It will also work on single, archive, search pages.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'full_width'  => esc_url( get_template_directory_uri().'/assets/images/admin/full-width.png' ),
        'left'        => esc_url( get_template_directory_uri().'/assets/images/admin/left.png' ),
        'right'       => esc_url( get_template_directory_uri().'/assets/images/admin/right.png' ),
      ),
      'radio'     => true,
      'default'   => 'right'
    ),
    array(
      'id'      => 'blog_author_bio',
      'type'    => 'switcher',
      'title'   => esc_html__('Author Bio', 'xt-reptro-cpt-shortcode'),
      'default' => true,
      'desc'    => esc_html__('Show author bio on single blog post. Default yes.', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'      => 'blog_post_nav',
      'type'    => 'switcher',
      'title'   => esc_html__('Post Nav', 'xt-reptro-cpt-shortcode'),
      'default' => true,
      'desc'    => esc_html__('Show next / prev nav on single blog post. Default yes.', 'xt-reptro-cpt-shortcode'),
    ),
  ),
);


// ------------------------------
// Footer Setting                  -
// ------------------------------

$options[]      = array(
  'name'        => 'footer_settings',
  'title'       => esc_html__('Footer Settings', 'xt-reptro-cpt-shortcode'),
  'icon'        => 'fa fa-chevron-circle-down',

  // begin: fields
  'fields'      => array(
    array(
      'type'    => 'notice',
      'class'   => 'info',
      'content' => sprintf( '<h3>%s</h3>', esc_html__('Footer', 'xt-reptro-cpt-shortcode') ),
    ),
    array(
      'id'        => 'footer_top_space',
      'type'      => 'slider',
      'default'   => 75,
      'title'     => esc_html__( 'Footer top padding', 'xt-reptro-cpt-shortcode' ),
      'options'   => array(
        'step'    => 1,
        'min'     => 10,
        'max'     => 300,
      )
    ),
    array(
      'id'        => 'footer_bottom_space',
      'type'      => 'slider',
      'default'   => 45,
      'title'     => esc_html__( 'Footer bottom padding', 'xt-reptro-cpt-shortcode' ),
      'options'   => array(
        'step'    => 1,
        'min'     => 10,
        'max'     => 300,
      )
    ),
    array(
      'id'             => 'footer_widget_column',
      'type'           => 'select',
      'title'          => esc_html__('Footer Widgets Columns', 'xt-reptro-cpt-shortcode'),
      'options'        => array(
        '6'    => '2 Columns',
        '4'    => '3 Columns',
        '3'    => '4 Columns',
      ),
      'default'        => '3',
      'default_option' => esc_html__('Select an option', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'      => 'footer_background_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Footer Background Color', 'xt-reptro-cpt-shortcode'),
      'default' => '#26282c',
    ),
    array(
      'id'      => 'footer_title_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Footer Widget Title Color', 'xt-reptro-cpt-shortcode'),
      'default' => '#ffffff',
    ),
    array(
      'id'      => 'footer_content_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Footer widget Content Color', 'xt-reptro-cpt-shortcode'),
      'default' => '#999999',
    ),
    array(
      'id'      => 'footer_link_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Footer widget Link Color', 'xt-reptro-cpt-shortcode'),
      'default' => '#ffffff',
    ),
    array(
      'type'    => 'notice',
      'class'   => 'info',
      'content' => sprintf( '<h3>%s</h3>', esc_html__('Bottom Bar', 'xt-reptro-cpt-shortcode') ),
    ),
    array(
      'id'      => 'need_footer_bottom_bar',
      'type'    => 'switcher',
      'title'   => esc_html__('Need Footer Bottom Bar', 'xt-reptro-cpt-shortcode'),
      'default' => true
    ),
    array(
      'id'             => 'bottom_bar_content',
      'type'           => 'select',
      'title'          => esc_html__('Footer Bottom Bar Content', 'xt-reptro-cpt-shortcode'),
      'options'        => array(
        'menu_social'       => esc_html__('Menu & Social icons', 'xt-reptro-cpt-shortcode'),
        'copyright_menu'    => esc_html__('Copyright Text & Menu', 'xt-reptro-cpt-shortcode'),
        'copyright_social'  => esc_html__('Copyright Text & Social icons', 'xt-reptro-cpt-shortcode'),
        'copyright'         => esc_html__('Copyright Text', 'xt-reptro-cpt-shortcode'),
      ),
      'default'        => 'copyright_social',
      'default_option' => esc_html__('Select an option', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'          => 'copyright_text',
      'type'        => 'textarea',
      'title'       => esc_html__('Copyright Text', 'xt-reptro-cpt-shortcode'),
      'default'     => esc_html__('&copy; 2018, All Rights Reserved.', 'xt-reptro-cpt-shortcode'),
    ),
    array(
      'id'      => 'bottom_bar_background_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Bottom Bar Background Color', 'xt-reptro-cpt-shortcode'),
      'default' => '#202026',
    ),
    array(
      'id'      => 'bottom_bar_text_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Bottom Bar Text Color', 'xt-reptro-cpt-shortcode'),
      'default' => '#999',
    ),
    array(
      'id'      => 'bottom_bar_link_color',
      'type'    => 'color_picker',
      'title'   => esc_html__('Bottom Bar Link Color', 'xt-reptro-cpt-shortcode'),
      'default' => '#ffffff',
    ),
    array(
      'id'              => 'footer_bottom_bar_social_icons',
      'type'            => 'group',
      'title'           => esc_html__('Footer Bar Right Social Icons', 'xt-reptro-cpt-shortcode'),
      'button_title'    => esc_html__('Add New', 'xt-reptro-cpt-shortcode'),
      'accordion_title' => esc_html__('Add New Social Network', 'xt-reptro-cpt-shortcode'),
      'fields'          => array(

        array(
          'id'      => 'icon',
          'type'    => 'icon',
          'title'   => esc_html__('Select an Icon', 'xt-reptro-cpt-shortcode'),
        ),
        array(
          'id'          => 'url',
          'type'        => 'text',
          'title'       => esc_html__('Social Network URL', 'xt-reptro-cpt-shortcode')
        ),
        array(
          'id'          => 'label',
          'type'        => 'text',
          'title'       => esc_html__('Social Network Label', 'xt-reptro-cpt-shortcode')
        ),

      ),
    ),
  ),
);


// ------------------------------
// Course Setting                  -
// ------------------------------

$options[]      = array(
  'name'        => 'course_settings',
  'title'       => esc_html__('Course Settings', 'xt-reptro-cpt-shortcode'),
  'icon'        => 'fa fa-book',

  // begin: fields
  'fields'      => array(
    array(
      'id'        => 'course_layout',
      'type'      => 'image_select',
      'title'     => esc_html__('Course Archive Page Layout', 'xt-reptro-cpt-shortcode'),
      'desc'      => esc_html__('Choose a layout for your course archive page.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'full_width'  => esc_url( get_template_directory_uri().'/assets/images/admin/full-width.png' ),
        'left'        => esc_url( get_template_directory_uri().'/assets/images/admin/left.png' ),
        'right'       => esc_url( get_template_directory_uri().'/assets/images/admin/right.png' ),
      ),
      'radio'     => true,
      'default'   => 'full_width'
    ),
    array(
      'id'             => 'course_columns',
      'type'           => 'select',
      'title'          => esc_html__('Course Column', 'xt-reptro-cpt-shortcode'),
      'desc'           => esc_html__('Select number of columns for the course in course archive page.', 'xt-reptro-cpt-shortcode'),
      'options'        => array(
        '2'    => esc_html__('2 columns', 'xt-reptro-cpt-shortcode'),
        '3'    => esc_html__('3 columns', 'xt-reptro-cpt-shortcode'),
        '4'    => esc_html__('4 columns', 'xt-reptro-cpt-shortcode'),
        '6'    => esc_html__('6 columns', 'xt-reptro-cpt-shortcode'),
      ),
      'default' => '3',
    ),
    array(
      'id'        => 'single_course_layout',
      'type'      => 'image_select',
      'title'     => esc_html__('Single Course Page Layout', 'xt-reptro-cpt-shortcode'),
      'desc'      => esc_html__('Choose a layout for your single course page.', 'xt-reptro-cpt-shortcode'),
      'options'   => array(
        'full_width'  => esc_url( get_template_directory_uri().'/assets/images/admin/full-width.png' ),
        'left'        => esc_url( get_template_directory_uri().'/assets/images/admin/left.png' ),
        'right'       => esc_url( get_template_directory_uri().'/assets/images/admin/right.png' ),
      ),
      'radio'     => true,
      'default'   => 'right'
    ),
    array(
      'id'          => 'show_course_info',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Course Informations in Overview Tab', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('In single coures page, there is a course information column on right side of the course description. Default Yes.', 'xt-reptro-cpt-shortcode'),
      'default'     => true,
    ),
    array(
      'id'             => 'course_archive_image_size',
      'type'           => 'select',
      'title'          => esc_html__('Course Image Size', 'xt-reptro-cpt-shortcode'),
      'desc'           => esc_html__('Course loop image size. For LearnPress Course Thumbnail, width and height can be changed from LearnPress settings.', 'xt-reptro-cpt-shortcode'),
      'options'        => array(
        'course_thumbnail'      => esc_html__('LearnPress Course Thumbnail', 'xt-reptro-cpt-shortcode'),
        'reptro-course-thumb'   => esc_html__('Reptro Course Thumb', 'xt-reptro-cpt-shortcode'),
      ),
      'default' => 'reptro-course-thumb',
    ),

    array(
      'type'    => 'notice',
      'class'   => 'info',
      'content' => sprintf( '<h3>%s</h3>', esc_html__('Course Loop Settings', 'xt-reptro-cpt-shortcode') ),
    ),
    array(
      'id'          => 'show_course_price',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Course Price', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('Show course price in loop? Default Yes.', 'xt-reptro-cpt-shortcode'),
      'default'     => true,
    ),
    array(
      'id'          => 'show_course_rating',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Course Rating', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('Show course rating in loop? Default Yes.', 'xt-reptro-cpt-shortcode'),
      'default'     => true,
    ),
    array(
      'id'          => 'show_course_details_button',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Course Button', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('Show course details button in loop? Default Yes.', 'xt-reptro-cpt-shortcode'),
      'default'     => true,
    ),
    array(
      'id'          => 'show_course_instructor',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Course Instructor', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('Show course instructor in loop? Default Yes.', 'xt-reptro-cpt-shortcode'),
      'default'     => true,
    ),
    array(
      'id'          => 'show_course_students',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Course Students', 'xt-reptro-cpt-shortcode'),
      'desc'        => esc_html__('Show course students in loop? Default Yes.', 'xt-reptro-cpt-shortcode'),
      'default'     => true,
    ),
  ),
);



// ------------------------------
// WooCommerce Settings
// ------------------------------


if ( class_exists( 'woocommerce' ) ):

  $options[]   = array(
    'name'     => 'xt_woocommerce_settigs',
    'title'    => esc_html__('WooCommerce Settings', 'xt-reptro-cpt-shortcode'),
    'icon'     => 'fa fa-shopping-cart',
    'fields'   => array(

      array(
        'id'        => 'xt_shop_layout',
        'type'      => 'image_select',
        'title'     => esc_html__('Shop Page Layout', 'xt-reptro-cpt-shortcode'),
        'desc'      => esc_html__('Choose a layout for your shop page, It will also work on few others WooCommerce pages.', 'xt-reptro-cpt-shortcode'),
        'options'   => array(
          'full_width'  => esc_url( get_template_directory_uri().'/assets/images/admin/full-width.png' ),
          'left'        => esc_url( get_template_directory_uri().'/assets/images/admin/left.png' ),
          'right'       => esc_url( get_template_directory_uri().'/assets/images/admin/right.png' ),
        ),
        'radio'     => true,
        'default'   => 'right'
      ),
      array(
        'id'        => 'xt_product_layout',
        'type'      => 'image_select',
        'title'     => esc_html__('Product Page Layout', 'xt-reptro-cpt-shortcode'),
        'desc'      => esc_html__('Choose a layout for your product page.', 'xt-reptro-cpt-shortcode'),
        'options'   => array(
          'full_width'  => esc_url( get_template_directory_uri().'/assets/images/admin/full-width.png' ),
          'left'        => esc_url( get_template_directory_uri().'/assets/images/admin/left.png' ),
          'right'       => esc_url( get_template_directory_uri().'/assets/images/admin/right.png' ),
        ),
        'radio'     => true,
        'default'   => 'right'
      ),
      array(
        'id'          => 'xt_shop_number_of_products',
        'type'        => 'number',
        'title'       => esc_html__('Shop number of products', 'xt-reptro-cpt-shortcode'),
        'desc'        => esc_html__('Number of products to show on the shop page, default 9 products.', 'xt-reptro-cpt-shortcode'),
        'default'     => 9,
        'after'       => ' Products',
      ),
      array(
        'id'       => 'xt_shop_loop_column',
        'type'     => 'select',
        'title'    => esc_html__('Product column', 'xt-reptro-cpt-shortcode'),
        'desc'     => esc_html__('Change number of products per row', 'xt-reptro-cpt-shortcode'),
        'options'  => array(
          '1'  => esc_html__('1 column', 'xt-reptro-cpt-shortcode'),
          '2'  => esc_html__('2 columns', 'xt-reptro-cpt-shortcode'),
          '3'  => esc_html__('3 columns', 'xt-reptro-cpt-shortcode'),
          '4'  => esc_html__('4 columns', 'xt-reptro-cpt-shortcode'),
          '5'  => esc_html__('5 columns', 'xt-reptro-cpt-shortcode'),
          '6'  => esc_html__('6 columns', 'xt-reptro-cpt-shortcode'),
          '7'  => esc_html__('7 columns', 'xt-reptro-cpt-shortcode'),
        ),
        'default'  => '3',
      ),
      array(
        'id'       => 'xt_related_loop_column',
        'type'     => 'select',
        'title'    => esc_html__('Related & Upsell Products column', 'xt-reptro-cpt-shortcode'),
        'desc'     => esc_html__('Change number of products per row for related & UpSell products', 'xt-reptro-cpt-shortcode'),
        'options'  => array(
          '1'  => esc_html__('1 column', 'xt-reptro-cpt-shortcode'),
          '2'  => esc_html__('2 columns', 'xt-reptro-cpt-shortcode'),
          '3'  => esc_html__('3 columns', 'xt-reptro-cpt-shortcode'),
          '4'  => esc_html__('4 columns', 'xt-reptro-cpt-shortcode'),
          '5'  => esc_html__('5 columns', 'xt-reptro-cpt-shortcode'),
          '6'  => esc_html__('6 columns', 'xt-reptro-cpt-shortcode'),
          '7'  => esc_html__('7 columns', 'xt-reptro-cpt-shortcode'),
        ),
        'default'  => '3',
      ),
      array(
        'id'       => 'xt_related_per_page',
        'type'     => 'select',
        'title'    => esc_html__('Number of Related Products', 'xt-reptro-cpt-shortcode'),
        'desc'     => esc_html__('Change number of products related products to show', 'xt-reptro-cpt-shortcode'),
        'options'  => array(
          '1'  => esc_html__('1 Product', 'xt-reptro-cpt-shortcode'),
          '2'  => esc_html__('2 Products', 'xt-reptro-cpt-shortcode'),
          '3'  => esc_html__('3 Products', 'xt-reptro-cpt-shortcode'),
          '4'  => esc_html__('4 Products', 'xt-reptro-cpt-shortcode'),
          '5'  => esc_html__('5 Products', 'xt-reptro-cpt-shortcode'),
          '6'  => esc_html__('6 Products', 'xt-reptro-cpt-shortcode'),
          '7'  => esc_html__('7 Products', 'xt-reptro-cpt-shortcode'),
        ),
        'default'  => '3',
      ),
      array(
        'id'      => 'xt_need_woo_zoom',
        'type'    => 'switcher',
        'title'   => esc_html__('Need Image Zoom?', 'xt-reptro-cpt-shortcode'),
        'desc'    => esc_html__('Enable or disable image zooming. Default enable.', 'xt-reptro-cpt-shortcode'),
        'default' => true
      ),
      array(
        'id'      => 'xt_need_woo_lightbox',
        'type'    => 'switcher',
        'title'   => esc_html__('Need Image Lightbox?', 'xt-reptro-cpt-shortcode'),
        'desc'    => esc_html__('Enable or disable image lightbox. Default enable.', 'xt-reptro-cpt-shortcode'),
        'default' => true
      ),
      array(
        'id'      => 'xt_need_woo_lightbox_slider',
        'type'    => 'switcher',
        'title'   => esc_html__('Need Image Gallery LightBox Slider?', 'xt-reptro-cpt-shortcode'),
        'desc'    => esc_html__('Enable or disable image gallery lightbox slider. Default enable.', 'xt-reptro-cpt-shortcode'),
        'default' => true
      ),
    )
  );

endif;


CSFramework::instance( $settings, $options );
