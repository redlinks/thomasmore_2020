<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// CUSTOMIZE SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options              = array();


/**
 * Shortcode Style Settings
 */

$options[]            = array(
  'name'              => 'xt_theme_style_settings',
  'title'             => esc_html__('Style Settings', 'xt-reptro-cpt-shortcode' ),
  'description'       => esc_html__('Theme style settings.', 'xt-reptro-cpt-shortcode' ),
  'sections'          => array(

    // begin: section
    array(
      'name'          => 'header_style',
      'title'         => esc_html__('Header Style', 'xt-reptro-cpt-shortcode' ),
      'settings'      => array(
        array(
          'name'      => 'header_bg',
          'default'   => '',
          'control'   => array(
            'type'    => 'cs_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => esc_html__('Header background color', 'xt-reptro-cpt-shortcode' ),
            ),
          ),
        ),
        array(
          'name'      => 'header_color',
          'default'   => '',
          'control'   => array(
            'type'    => 'cs_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => esc_html__('Header color', 'xt-reptro-cpt-shortcode' ),
            ),
          ),
        ),
        array(
          'name'      => 'nav_pre_header_bg',
          'default'   => '',
          'control'   => array(
            'type'    => 'cs_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => esc_html__('Navbar or header top bar background', 'xt-reptro-cpt-shortcode' ),
            ),
          ),
        ),
        array(
          'name'          => 'logo_max_width',
          'default'       => 120,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 120 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Logo max width', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
        array(
          'name'          => 'logo_max_height',
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'title'     => esc_html__('Logo max height', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
      ),

    ),
    // end: section

    // begin: section
    array(
      'name'          => 'menu_style',
      'title'         => esc_html__('Menu Style', 'xt-reptro-cpt-shortcode' ),
      'settings'      => array(
        array(
          'name'          => 'menu_left_right_space',
          'default'       => 25,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 25 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Menu items left spacing', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
        array(
          'name'          => 'menu_top_space',
          'default'       => 30,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 30 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Menu items top spacing', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
        array(
          'name'          => 'menu_bottom_space',
          'default'       => 30,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 30 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Menu items bottom spacing', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
        array(
          'name'          => 'dropdown_menu_width',
          'default'       => 290,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 290 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Dropdown menu minimum width', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
      ),

    ),
    // end: section

    // begin: section
    array(
      'name'          => 'theme_main_slider',
      'title'         => esc_html__('Slider Style', 'xt-reptro-cpt-shortcode' ),
      'settings'      => array(
        array(
          'name'      => 'main_slider_overlay',
          'default'   => 'rgba(0,0,0,0.4)',
          'control'   => array(
            'type'    => 'cs_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => esc_html__('Slider background overlay color', 'xt-reptro-cpt-shortcode' ),
            ),
          ),
        ),
         array(
          'name'      => 'main_slider_color',
          'default'   => '#fff',
          'control'   => array(
            'type'    => 'cs_field',
            'options' => array(
              'type'  => 'color_picker',
              'title' => esc_html__('Slider text color', 'xt-reptro-cpt-shortcode' ),
            ),
          ),
        ),
        array(
          'name'          => 'main_slider_min_height',
          'default'       => 600,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 600 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Slider height', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
        array(
          'name'          => 'main_slider_padding_top',
          'default'       => 80,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 80 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Slider padding top', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
        array(
          'name'          => 'main_slider_padding_bottom',
          'default'       => 300,
          'control'       => array(
            'type'        => 'cs_field',
            'options'     => array(
              'type'      => 'number',
              'desc'      => esc_html__('Default 300 PX.', 'xt-reptro-cpt-shortcode'),
              'title'     => esc_html__('Slider padding bottom', 'xt-reptro-cpt-shortcode'),
            ),
          ),
        ),
      ),
    ),
    // end: section
  ),
  // end: sections
);


CSFramework_Customize::instance( $options );
