<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options      = array();


// -----------------------------------------
// Page Side Metabox Options               -
// -----------------------------------------

$options[]    = array(
  'id'        => 'business_x_page_side_options',
  'title'     => esc_html__('Page Settings', 'xt-reptro-cpt-shortcode'),
  'post_type' => 'page',
  'context'   => 'side',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => '_business_x_page_side_options_fields',
      'fields' => array(

        array(
          'id'        => 'page_layout',
          'type'      => 'image_select',
          'title'     => esc_html__('Page Layout', 'xt-reptro-cpt-shortcode' ),
          'help'      => esc_html__('Default box layout, You can also select full screen layout.', 'xt-reptro-cpt-shortcode'),
          'options'   => array(
            'full_screen' => esc_url( get_template_directory_uri().'/assets/images/admin/full-width-page.png' ),
            'grid'        => esc_url( get_template_directory_uri().'/assets/images/admin/box-layout-page.png' ),
          ),
          'default'    => 'full_screen',
        ),
        array(
          'id'      => 'need_page_title',
          'type'    => 'switcher',
          'title'   => esc_html__('Need Page Title ?', 'xt-reptro-cpt-shortcode'),
          'help'    => esc_html__('Page title settings avaiable on theme option.', 'xt-reptro-cpt-shortcode'),
          'default' => true,
        ),
        array(
          'id'          => 'page_sidebar_enable',
          'title'       => esc_html__('Need Sidebar?', 'xt-reptro-cpt-shortcode' ),
          'type'        => 'switcher',
          'help'        => esc_html__('Set off, if you don\'t need sidebar on this page.', 'xt-reptro-cpt-shortcode'),
          'default'     => false,
        ),
        array(
          'id'          => 'page_sidebar_position',
          'type'        => 'image_select',
          'title'       => esc_html__('Page Sidebar Position', 'xt-reptro-cpt-shortcode' ),
          'desc'        => esc_html__('Options: Left, Right or No Sidebar.', 'xt-reptro-cpt-shortcode' ),
          'options'     => array(
            'left'        => esc_url( get_template_directory_uri().'/assets/images/admin/left.png' ),
            'right'       => esc_url( get_template_directory_uri().'/assets/images/admin/right.png' ),
            'no_sidebar'  => esc_url( get_template_directory_uri().'/assets/images/admin/full-width.png' ),
          ),
          'default'     => 'no_sidebar',
          'dependency'  => array( 'page_sidebar_enable', '!=', '' ),
        ),
        array(
          'id'          => 'page_choose_sidebar',
          'type'        => 'select',
          'title'       => esc_html__('Select a sidebar for this plage.', 'xt-reptro-cpt-shortcode' ),
          'help'        => esc_html__('Go to theme option for generating new sidebars.', 'xt-reptro-cpt-shortcode' ),
          'options'     => reptro_sidebars_list_on_option(),
          'default'     => 'sidebar-1',
          'dependency'  => array( 'page_sidebar_position_no_sidebar', '!=', 'true' ),
        ),

        array(
          'id'      => 'xt_disable_header_footer',
          'type'    => 'switcher',
          'title'   => esc_html__('Disable Header & Footer', 'xt-reptro-cpt-shortcode'),
          'help'    => esc_html__('You can disable header & footer of this page using this. It\'s may help if you want to create a landing page.', 'xt-reptro-cpt-shortcode'),
        ),
      ),
    ),

  ),
);

// -----------------------------------------
// Gallery Post Type Metabox Options               -
// -----------------------------------------

$options[]    = array(
  'id'        => 'xt_gallery_cat_title',
  'title'     => esc_html__('Add Subtitle', 'xt-reptro-cpt-shortcode'),
  'post_type' => 'business_x_gallery',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'xt_gallery_side_title',
      'fields' => array(

        array(
          'id'          => '_xt_gallery_sub_title',
          'type'        => 'text',
          'title'       => esc_html__('Galary Sub Title', 'xt-reptro-cpt-shortcode' ),
        ),
      ),
    ),

  ),
);


// -----------------------------------------
// Testimonial Metabox Options               -
// -----------------------------------------


$options[]    = array(
  'id'        => '_xt_testimonial_options',
  'title'     => esc_html__('Testimonial Options', 'xt-reptro-cpt-shortcode'),
  'post_type' => 'business_x_testi',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'xt_testimonial_options_fields',
      'fields' => array(

        array(
          'id'      => 'testimonial_designation', 
          'type'    => 'text',
          'title'   => esc_html__('Client\'s Designation', 'xt-reptro-cpt-shortcode'),
          'desc'    => esc_html__('Add your client\'s designation', 'xt-reptro-cpt-shortcode'),
        ),
        array(
          'id'              => 'clients_all_social_icons',
          'type'            => 'group',
          'title'           => esc_html__('Client\'s Social Icons', 'xt-reptro-cpt-shortcode'),
          'button_title'    => esc_html__('Add New Social Icon', 'xt-reptro-cpt-shortcode'),
          'accordion_title' => esc_html__('Add New Social Network', 'xt-reptro-cpt-shortcode'),
          'fields'          => array(
            array(
              'id'      => 'client_social_icons',
              'type'    => 'icon',
              'title'   => esc_html__('Select an Icon', 'xt-reptro-cpt-shortcode'),
            ),
            array(
              'id'          => 'client_social_icons_url',
              'type'        => 'text',
              'title'       => esc_html__('Social Network URL', 'xt-reptro-cpt-shortcode')
            ),
          ),
          'default' => array(
            array(
              'client_social_icons'       => 'fa fa-twitter',
              'client_social_icons_url'   => '#',
            ),
            array(
              'client_social_icons'       => 'fa fa-facebook',
              'client_social_icons_url'   => '#',
            ),
            array(
              'client_social_icons'       => 'fa fa-linkedin',
              'client_social_icons_url'   => '#',
            ),           
          )
        ),
      ),
    ),
  ),
);


// -----------------------------------------
// Sider Metabox Options               -
// -----------------------------------------

$options[]    = array(
  'id'        => '_xt_sider_options',
  'title'     => esc_html__('Slider Options', 'xt-reptro-cpt-shortcode'),
  'post_type' => 'business_x_slider',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'xt_slider_options_fields',
      'fields' => array(

        array(
          'id'              => 'slider_btn_options',
          'type'            => 'group',
          'title'           => esc_html__('Slider Buttons', 'xt-reptro-cpt-shortcode'),
          'button_title'    => esc_html__('Add New Button', 'xt-reptro-cpt-shortcode'),
          'accordion_title' => esc_html__('Add New Slider Button', 'xt-reptro-cpt-shortcode'),
          'fields'          => array(
            array(
              'id'      => 'slider_button_title',
              'type'    => 'text',
              'title'   => esc_html__('Slider Button Title', 'xt-reptro-cpt-shortcode'),
            ),
            array(
              'id'          => 'slider_button_url',
              'type'        => 'text',
              'title'       => esc_html__('Slider Button URL', 'xt-reptro-cpt-shortcode')
            ),
            array(
              'id'             => 'slider_button_type',
              'type'           => 'select',
              'title'          => 'Select Button Type',
              'options'        => array(
                'slider_button_border'   => 'Button Border',
                'slider_button_fill'     => 'Button Fill',
              ),
            ),
          ),
          'default' => array(
            array(
              'slider_button_title'       => 'Hire Us',
              'slider_button_url'         => '#',
              'slider_button_type'        => 'Button Fill',
            ),           
          )
        ),

      ),
    ),

  ),
);

// -----------------------------------------
// Course Metabox Options                    -
// -----------------------------------------
$options[]    = array(
  'id'        => '_course_side_options',
  'title'     => esc_html__( 'Course Options', 'xt-reptro-cpt-shortcode' ),
  'post_type' => 'lp_course',
  'context'   => 'normal',
  'priority'  => 'high',
  'sections'  => array(

    array(
      'name'   => '_course_options',
      'fields' => array(

        array(
          'id'       => 'skill-level',
          'type'     => 'select',
          'title'    => esc_html__( 'Course skill level', 'xt-reptro-cpt-shortcode' ),
          'desc'     => esc_html__( 'What kind of students can take this course.', 'xt-reptro-cpt-shortcode' ),
          'options'  => array(
            'all'           => esc_html__( 'All level', 'xt-reptro-cpt-shortcode' ),
            'newbie'        => esc_html__( 'Newbie', 'xt-reptro-cpt-shortcode' ),
            'beginner'      => esc_html__( 'Beginner', 'xt-reptro-cpt-shortcode' ),
            'intermediate'  => esc_html__( 'Intermediate', 'xt-reptro-cpt-shortcode' ),
            'advanced'      => esc_html__( 'Advanced', 'xt-reptro-cpt-shortcode' ),
          ),
          'default'  => 'all',
        ),
        array(
          'id'      => 'language',
          'type'    => 'text',
          'title'   => esc_html__( 'Course Language', 'xt-reptro-cpt-shortcode' ),
          'desc'    => esc_html__( 'Which language skill is required to take this course', 'xt-reptro-cpt-shortcode' ),
          'default' => esc_html__( 'English', 'xt-reptro-cpt-shortcode' ),
        ),

      ),
    ),

  ),
);


// -----------------------------------------
// Team Metabox Options               -
// -----------------------------------------

$options[]    = array(
  'id'        => 'business_x_team_options',
  'title'     => esc_html__('Team Options', 'xt-reptro-cpt-shortcode'),
  'post_type' => 'business_x_team',
  'context'   => 'normal',
  'priority'  => 'default',
  'sections'  => array(

    array(
      'name'   => 'business_x_team_options_fields',
      'fields' => array(

        array(
          'id'      => 'member_designation', 
          'type'    => 'text',
          'title'   => esc_html__('Member\'s Designation', 'xt-reptro-cpt-shortcode'),
          'desc'    => esc_html__('Add your team member\'s designation', 'xt-reptro-cpt-shortcode'),
        ),
      ),
    ),
  ),
);



CSFramework_Metabox::instance( $options );
