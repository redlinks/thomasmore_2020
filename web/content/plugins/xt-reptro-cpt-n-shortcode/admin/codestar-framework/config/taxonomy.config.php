<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// TAXONOMY OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options     = array();


/**
 * LearnPress course category meta
 */

$options[]   = array(
  'id'       => '_xt_course_category_options',
  'taxonomy' => 'course_category',
  'fields'   => array(

    array(
	  'id'        => '_xt_course_cat_icon',
	  'type'      => 'image',
	  'title'     => esc_html__('Category Icon', 'xt-reptro-cpt-shortcode'),
	  'add_title' => esc_html__('Add Icon', 'xt-reptro-cpt-shortcode'),
	  'desc' 	  => esc_html__('Upoad PNG icon, Recommended size 512x512.', 'xt-reptro-cpt-shortcode'),
	),
	array(
	  'id'        => '_xt_course_cat_img',
	  'type'      => 'image',
	  'title'     => esc_html__('Category Image', 'xt-reptro-cpt-shortcode'),
	  'add_title' => esc_html__('Add Category Image', 'xt-reptro-cpt-shortcode'),
	  'desc' 	  => esc_html__('Upoad any image.', 'xt-reptro-cpt-shortcode'),
	),

  ),
);


CSFramework_Taxonomy::instance( $options );
