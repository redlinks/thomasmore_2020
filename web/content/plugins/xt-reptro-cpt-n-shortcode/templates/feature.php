<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Feature_Item extends Widget_Base {

	public function get_name() {
		return 'reptro_feature_item';
	}

	public function get_title() {
		return esc_html__( 'Feature Box', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-icon-box';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_feature_content',
  			[
  				'label' => esc_html__( 'Feature Box', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // feature icon
        $this->add_control(
            'reptro_feature_icon',
            [
                'type'          => \Elementor\Controls_Manager::ICON,
                'label'         => __( 'Icon', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'sli-energy',
            ]
        );
        
        // service box title
        $this->add_control(
            'reptro_feature_box_title',
            [   
                'label'         => esc_html__( 'Heading', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'placeholder'   => __( 'Place your title text here.', 'xt-reptro-cpt-shortcode' ),
                'default'       => esc_html__( 'Lorem Ipsum is dummy text', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // service box title
        $this->add_control(
            'feature_text',
            [   
                'label'             => __( 'Description', 'xt-reptro-cpt-shortcode' ),
                'type'              => Controls_Manager::TEXTAREA,
                'placeholder'       => __( 'Place your content text here.', 'xt-reptro-cpt-shortcode' ),       
                'default'           => __( 'Lorem Ipsum is placeholder text commonly used in the graphic, print, and publishing industries.', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // service box url
        $this->add_control(
            'service_box_url',
            [   
                'label'         => esc_html__( 'URL', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'placeholder'   => __( 'http://yoursite.com', 'xt-reptro-cpt-shortcode' ),
            ]
        );

        $this->end_controls_section();

	}

	protected function render() {
        $settings           = $this->get_settings();
        $icon               = ( array_key_exists('reptro_feature_icon', $settings) ? $settings['reptro_feature_icon'] : 'sli-energy' );
        $title              =  $settings['reptro_feature_box_title'];
        $feature_text       =  $settings['feature_text'];
        $service_box_url    =  $settings['service_box_url'];

        echo do_shortcode( '[business_x_feature_scode icon="'.$icon.'" title="'.$title.'" feature_text="'.$feature_text.'" service_box_url="'.$service_box_url.'"]' );
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Feature_Item() );