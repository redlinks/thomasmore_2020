<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Contact_form_Seven extends Widget_Base {

	public function get_name() {
		return 'reptro_contact_form_seven';
	}

	public function get_title() {
		return esc_html__( 'Contact Form', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-form-horizontal';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

        $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

        $contact_forms = array();
        if ( $cf7 ) {
            foreach ( $cf7 as $cform ) {
                $contact_forms[ $cform->ID ] = $cform->post_title;
            }
        } else {
            $contact_forms[ esc_html__( 'No contact forms found', 'xt-reptro-cpt-shortcode' ) ] = 0;
        }

  		$this->start_controls_section(
  			'reptro_hero_area_content',
  			[
  				'label' => esc_html__( 'Contact Form', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        $this->add_control(
            'contact_form',
            [
                'label'         => esc_html__( 'Select a Contact Form', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => '',
                'options'       => $contact_forms,
            ]
        );

		$this->end_controls_section();		
	}

	protected function render() {
		$settings  = $this->get_settings();
        $contact_form   =  $settings['contact_form'];
       
        echo do_shortcode( '[reptro_contact_form_7_scode contact_form="'.$contact_form.'"]' );


	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Contact_form_Seven() );