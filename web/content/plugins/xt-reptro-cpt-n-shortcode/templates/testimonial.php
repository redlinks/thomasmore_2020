<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Testimonial extends Widget_Base {

	public function get_name() {
		return 'reptro_testimonial';
	}

	public function get_title() {
		return esc_html__( 'Testimonial', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-blockquote';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_testimonial_content',
  			[
  				'label' => esc_html__( 'Testimonial Settings', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // number of testimonial items
        $this->add_control(
            'posts_per_page',
            [
                'label'         => __('Number Of Testimonials To Show', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 4,
                'description'   => __('Default 4.', 'xt-reptro-cpt-shortcode')
            ]
        );  

        // order
        $this->add_control(
            'reptro_testimonial_order',
            [
                'type'          => Controls_Manager::SELECT,
                'label'         => __( 'Order', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'DESC',
                'description'   => __('Testimonial order', 'xt-reptro-cpt-shortcode'),
                'options'       => [
                    'ASC'       => __( 'Ascending', 'xt-reptro-cpt-shortcode' ),
                    'DESC'      => __( 'Descending', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );        

        // order by
        $this->add_control(
            'reptro_testimonial_order_by',
            [
                'type'    => Controls_Manager::SELECT,
                'label'   => __( 'Order by', 'xt-reptro-cpt-shortcode' ),
                'default' => 'date',
                'description'   => __('Testimonial orderby', 'xt-reptro-cpt-shortcode'),
                'options' => [
                    'none'          => __('No order', 'xt-reptro-cpt-shortcode' ),
                    'ID'            => __('Post ID', 'xt-reptro-cpt-shortcode' ),
                    'author'        => __('Author', 'xt-reptro-cpt-shortcode' ),
                    'title'         => __('Title', 'xt-reptro-cpt-shortcode' ),
                    'date'          => __('Published date', 'xt-reptro-cpt-shortcode' ),
                    'modified'      => __('Modified date', 'xt-reptro-cpt-shortcode' ),
                    'parent'        => __('By parent', 'xt-reptro-cpt-shortcode' ),
                    'rand'          => __('Random order', 'xt-reptro-cpt-shortcode' ),
                    'comment_count' => __('Comment count', 'xt-reptro-cpt-shortcode' ),
                    'menu_order'    => __('Menu order', 'xt-reptro-cpt-shortcode' ),
                    'post__in'      => __('By include order', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        // specific testimonial include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Specific Testimonial', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __(' You can put comma separated testimonial id for showing those specific member only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific testimonial exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Specific Testimonial Exclude', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('You can put comma separated testimonial member id for excluding those specific member.', 'xt-reptro-cpt-shortcode')

            ]
        );            

		$this->end_controls_section();		
	}

	protected function render() {
        $settings        = $this->get_settings();
        $posts_per_page  =  $settings['posts_per_page'];
        $orderby         = $settings['reptro_testimonial_order_by'];
        $order           = $settings['reptro_testimonial_order'];
        $post_in_ids     = $settings['post_in_ids'];
        $post_not_in_ids = $settings['post_not_in_ids'];
        
        echo do_shortcode( '[business_x_testimonial_scode post="'.$posts_per_page.'" orderby="'.$orderby.'" order="'.$order.'" post_in_ids="'.$post_in_ids.'" post_not_in_ids="'.$post_not_in_ids.'"]' );
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Testimonial() );