<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Hero_Area extends Widget_Base {

	public function get_name() {
		return 'reptro_hero_area';
	}

	public function get_title() {
		return esc_html__( 'Hero Area', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-inner-section';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

        $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

        $contact_forms = array();
        if ( $cf7 ) {
            foreach ( $cf7 as $cform ) {
                $contact_forms[ $cform->ID ] = $cform->post_title;
            }
        } else {
            $contact_forms[ esc_html__( 'No contact forms found', 'xt-reptro-cpt-shortcode' ) ] = 0;
        }

  		$this->start_controls_section(
  			'reptro_hero_area_content',
  			[
  				'label' => esc_html__( 'Hero Area', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        $this->add_control(
            'hero_area_image_heading',
            [
                'label'         => esc_html__( 'Background image', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
            ]
        );

        $this->add_control(
            'right_bg_img',
            [
                'label'     => esc_html__( 'Choose Image', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::MEDIA,
                'default'   => [
                    'url'   => Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $this->add_control(
            'hero_title',
            [   
                'label'         => esc_html__( 'Title', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'short_description',
            [   
                'label'         => esc_html__( 'Short Description', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'contact_form',
            [
                'label'         => esc_html__( 'Select a Contact Form', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => '',
                'options'       => $contact_forms,
            ]
        );

        $this->add_control(
            'need_button',
            [
                'label'         => esc_html__( 'Need Action Button?', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );

        $this->add_control(
            'need_lightbox',
            [
                'label'         => esc_html__( 'Action Button LightBox?', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'return_value'  => 'yes',
                'condition' => [
                    'need_button' => "yes",
                ]
            ]
        );

        $this->add_control(
            'hero_button_icon',
            [
                'label'     => esc_html__( 'Action Button Icon', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::ICON,
                'default'   => 'fa fa-play',
                'condition' => [
                    'need_button' => "yes",
                ]
            ]
        );

        $this->add_control(
            'button_url',
            [
                'label'         => 'Action Button Link',
                'type'          => Controls_Manager::URL,
                'condition'     => [
                    'need_button' => "yes",
                ],
            ]
        );

		$this->end_controls_section();		
	}

	protected function render() {
        $settings          = $this->get_settings();        
        $right_bg_img      =  $settings['right_bg_img']['id'];
        $hero_title        =  $settings['hero_title'];
        $short_description =  $settings['short_description'];
        $contact_form      =  $settings['contact_form'];
        $need_button       =  $settings['need_button'];
        $need_lightbox     =  $settings['need_lightbox'];
        $hero_button_icon  =  ( array_key_exists('hero_button_icon', $settings) ? $settings['hero_button_icon'] : 'fa fa-play' );
        $button_url        =  $settings['button_url']['url'];
        
        echo do_shortcode( '[schooling_hero_area hero_title="'.$hero_title.'" hero_description="'.$short_description.'" contact_form="'.$contact_form.'" need_lightbox="'.$need_lightbox.'" hero_button_icon="'.$hero_button_icon.'" need_button="'.$need_button.'" button_url="'.$button_url.'" right_bg_img="'.$right_bg_img.'"]' );

	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Hero_Area() );