<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Feature_Two_Item extends Widget_Base {

	public function get_name() {
		return 'reptro_feature_two_item';
	}

	public function get_title() {
		return esc_html__( 'Feature Box 2', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-info-box';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_feature_content',
  			[
  				'label' => esc_html__( 'Feature Box 2', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // feature item grid column
        $this->add_control(
            'column_settings',
            [
                'label'     => esc_html__( 'Columns', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 4,
                'options'   => [
                    6           => esc_html__( '6 Columns', 'xt-reptro-cpt-shortcode' ),
                    4           => esc_html__( '4 Columns', 'xt-reptro-cpt-shortcode' ),
                    3           => esc_html__( '3 Columns', 'xt-reptro-cpt-shortcode' ),
                    2           => esc_html__( '2 Columns', 'xt-reptro-cpt-shortcode' ),
                    1           => esc_html__( '1 Columns', 'xt-reptro-cpt-shortcode' )
                ]              
            ]
        );         

        // feature item field options
        $this->add_control(
            'feature_items',
            [
                'label'         => __('Feature Items', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::REPEATER,
                'default'       => [
                    [
                        'icon'            => 'hs-learning',
                        'title'           => __( 'Online Courses', 'xt-reptro-cpt-shortcode' ),
                        'service_content' => __( 'You get speed, flexibility and better control to produce vehicles faster with greater precision.', 'xt-reptro-cpt-shortcode' )
                    ],
                    [
                        'icon'            => 'hs-file',
                        'title'           => __( 'Lesson', 'xt-reptro-cpt-shortcode' ),
                        'service_content' => __( 'You get speed, flexibility and better control to produce vehicles faster with greater precision.', 'xt-reptro-cpt-shortcode' )
                    ],
                    [
                        'icon'            => 'hs-clock',
                        'title'           => __( 'Quiz', 'xt-reptro-cpt-shortcode' ),
                        'service_content' => __( 'You get speed, flexibility and better control to produce vehicles faster with greater precision.', 'xt-reptro-cpt-shortcode' )
                    ],
                    [
                        'icon'            => 'edi-diploma',
                        'title'           => __( 'Certification', 'xt-reptro-cpt-shortcode' ),
                        'service_content' => __( 'You get speed, flexibility and better control to produce vehicles faster with greater precision.', 'xt-reptro-cpt-shortcode' )
                    ]
                ],
                'fields' => [
                    [
                        'name'    => 'icon',
                        'label'   => __( 'Icon', 'xt-reptro-cpt-shortcode' ),
                        'type'    => Controls_Manager::ICON,
                        'default' => 'fa fa-heart'
                    ],
                    [
                        'name'        => 'title',
                        'label'       => __( 'Title', 'xt-reptro-cpt-shortcode' ),
                        'type'        => Controls_Manager::TEXT,
                        'default'     => __( 'Lorem Ipsum', 'xt-reptro-cpt-shortcode' )           
                    ],                    
                    [
                        'name'        => 'service_content',
                        'label'       => __( 'Content', 'xt-reptro-cpt-shortcode' ),
                        'type'        => Controls_Manager::TEXTAREA,
                        'default'     => __( 'Nullam a ultrices ex, quis finibus neque. Etiam facilisis consectetur ante ac bibendum. Sed pretium lacinia sollicitudin.', 'xt-reptro-cpt-shortcode' ),             
                    ],
                    [
                        'name'        => 'btn_title',
                        'label'       => __( 'Button Text', 'xt-reptro-cpt-shortcode' ),
                        'type'        => Controls_Manager::TEXT,
                    ],   
                    [
                        'name'        => 'btn_url',
                        'label'       => __( 'Button URL', 'xt-reptro-cpt-shortcode' ),
                        'type'        => Controls_Manager::TEXT,
                        'placeholder' => 'http://yoursite.com'    
                    ]                                        
                ],
                'title_field' => '{{{ title }}}'              
            ]
        );

        $this->end_controls_section();

	}

	protected function render() {
        $settings        = $this->get_settings();
        $feature_items   =  $settings['feature_items'];
        $column_settings =  $settings['column_settings'];

        echo reptro_feature_two_shortcode(array(
            'feature_items' => $feature_items,
            'column'        => $column_settings
        ));
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Feature_Two_Item() );