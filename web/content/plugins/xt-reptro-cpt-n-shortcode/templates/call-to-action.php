<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Call_To_Action extends Widget_Base {

	public function get_name() {
		return 'reptro_call_to_action';
	}

	public function get_title() {
		return esc_html__( 'Call To Action', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'fa fa-bullhorn';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_call_to_action',
  			[
  				'label' => esc_html__( 'Call To Action', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // type
        $this->add_control(
            'reptro_call_to_action_type',
            [   
                'label'         => esc_html__( 'Type', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'default',
                'options' => [
                    'default'      => esc_html__( 'Default', 'xt-reptro-cpt-shortcode' ),
                    'image'        => esc_html__( 'Image', 'xt-reptro-cpt-shortcode' ),
                ],
            ]
        );

        // video background image
        $this->add_control(
            'bg_img',
            [
                'label'     => __( 'Background Image', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::MEDIA,
                'default'   => [
                    'url'   => Utils::get_placeholder_image_src(),
                ],
                'condition'     => [
                    '.reptro_call_to_action_type' => 'image'
                ], 
            ]
        );

        // image size
        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name'          => 'image_size',
                'label'         => __( 'Image Size', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'full',
                'condition'     => [
                    '.reptro_call_to_action_type' => 'image'
                ],
            ]
        );
        
        // title
        $this->add_control(
            'reptro_call_to_action_title',
            [   
                'label'         => esc_html__( 'Title', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'default' 		=> esc_html__( 'Contact With Us For More Details' , 'xt-reptro-cpt-shortcode' ),
            ]
        );

        // description
        $this->add_control(
            'reptro_call_to_action_description',
            [   
                'label'         => esc_html__( 'Title', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXTAREA,
                'default' 		=> esc_html__( 'We\'re here to help. Send us an email or call us at 1(800)555-5555.' , 'xt-reptro-cpt-shortcode' ),
            ]
        );

        // icon
        $this->add_control(
            'reptro_call_to_action_icon',
            [
                'label'     => esc_html__( 'Action Icon', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::ICON,
                'default'   => 'sli-envelope-letter',
            ]
        );

        // button text
        $this->add_control(
            'reptro_call_to_action_btn_text',
            [   
                'label'         => esc_html__( 'Button Text', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'default' 		=> esc_html__( 'Get In Touch' , 'xt-reptro-cpt-shortcode' ),
            ]
        );

        // url
		$this->add_control(
		'reptro_call_to_action_url',
		[
			'label' 		=> esc_html__( 'Button URL', 'xt-reptro-cpt-shortcode' ),
			'type' 			=> Controls_Manager::URL,
			'default' 		=> [
				'url' 			=> '',
				'is_external' 	=> '',
			],
			'show_external' => true,
			]
		);

        $this->end_controls_section();

	}


    private function render_image( $settings ) {
        $image_id   = $settings['bg_img']['id'];
        $image_size = $settings['image_size_size'];
        if ( 'custom' === $image_size ) {
            $image_src = Group_Control_Image_Size::get_attachment_image_src( $image_id, 'image_size', $settings );
        } else {
            $image_src = wp_get_attachment_image_src( $image_id, $image_size );
            $image_src = $image_src[0];
        }

        return sprintf( '<img src="%s" alt="%s" />', esc_url($image_src), esc_html( get_post_meta( $image_id, '_wp_attachment_image_alt', true) ) );
    }

	protected function render() {
        $settings   	    = $this->get_settings();
        $type               = $settings['reptro_call_to_action_type'];
        $title   			= $settings['reptro_call_to_action_title'];
        $description   		= $settings['reptro_call_to_action_description'];
        $btn_text   		= $settings['reptro_call_to_action_btn_text'];
		$action_url 		= $settings['reptro_call_to_action_url'];
		$url 				= $action_url['url'];
		$target 			= $action_url['is_external'] ? 'target="_blank"' : '';
		$action_icon		= ( array_key_exists('reptro_call_to_action_icon', $settings) ? $settings['reptro_call_to_action_icon'] : 'sli-envelope-letter' );
        ?>
        	<div class="reptro-ctl-area reptro-ctl-<?php echo esc_attr( $type ) ?>">
                <?php
                    if( $type == 'image' ){
                        echo $this->render_image( $settings );
                    }
                ?>
                <div class="reptro-ctl-content-wrapper">
        			<?php if( $action_icon ): ?>
        				<div class="reptro-ctl-icon">
        					<i class="<?php echo esc_attr( $action_icon ); ?>"></i>
        				</div>
        			<?php endif; ?>

        			<?php if( $title || $description ): ?>
        				<div class="reptro-ctl-content">
        					<?php if( $title ): ?>
        						<h2 class="reptro-section-title text-default margin-bottom-small section-title-type-default margin-top-none section-title-font-size-default section-title-color-default section-title-highlight-inline"><?php echo esc_html( $title ) ?></h2>
        					<?php endif; ?>
        					<?php if( $description ): ?>
        						<p class="section-title-small text-default margin-bottom-small margin-top-none section-title-small-font-default section-title-small-border-no section-title-small-color-default"><span><?php echo esc_html( $description ) ?></span></p>
        					<?php endif; ?>
        				</div>
        			<?php endif; ?>

        			<?php if( $url || $btn_text): ?>
            			<div class="reptro-ctl-button">
            				<a class="btn btn-lg btn-border" href="<?php echo esc_url( $url ) ?>"><?php echo esc_html( $btn_text ) ?></a>
            			</div>
        			<?php endif; ?>
                </div>
        	</div>
        <?php
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Call_To_Action() );