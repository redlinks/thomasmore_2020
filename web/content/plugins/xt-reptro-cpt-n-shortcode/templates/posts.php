<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Post_Item extends Widget_Base {

	public function get_name() {
		return 'reptro_post_item';
	}

	public function get_title() {
		return esc_html__( 'Post Grid/Slider', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-posts-grid';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_post_item_content',
  			[
  				'label' => esc_html__( 'General Settings', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // conent type
        $this->add_control(
            'post_content_type',
            [
                'label'             => esc_html__( 'Content Type', 'xt-reptro-cpt-shortcode' ),
                'type'              => Controls_Manager::SELECT,
                'default'           => 'grid',
                'options'           => [
                    'slider'        => esc_html__( 'Slider',   'xt-reptro-cpt-shortcode' ),
                    'grid'          => esc_html__( 'Grid', 'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // number of posts
        $this->add_control(
            'posts_per_page',
            [
                'label'         => esc_html__('Number Of Posts To Show', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 3,
                'description'   => esc_html__('Default 3.', 'xt-reptro-cpt-shortcode')
            ]
        );  

        // post item grid column
        $this->add_control(
            'column_settings',
            [
                'label'     => esc_html__( 'Columns', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 3,
                'condition'     => [
                    '.post_content_type' => 'grid'
                ],                
                'options'   => [
                    6           => esc_html__( '6 Columns', 'xt-reptro-cpt-shortcode' ),
                    4           => esc_html__( '4 Columns', 'xt-reptro-cpt-shortcode' ),
                    3           => esc_html__( '3 Columns', 'xt-reptro-cpt-shortcode' ),
                    2           => esc_html__( '2 Columns', 'xt-reptro-cpt-shortcode' ),
                    1           => esc_html__( '1 Columns', 'xt-reptro-cpt-shortcode' )
                ]              
            ]
        );  

        // number of words to show
        $this->add_control(
            'number_of_words',
            [
                'label'         => esc_html__('Number Of Words To Show', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 15,
                'description'   => esc_html__('Default 15.', 'xt-reptro-cpt-shortcode')
            ]
        );

        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name'          => 'image_size',
                'default'       => 'reptro-blog-small',
            ]
        );

        $this->end_controls_section();

        // post query settings
        $this->start_controls_section(
            'reptro_post_query_settings',
            [
                'label' => esc_html__( 'Query Settings', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // order
        $this->add_control(
            'reptro_post_item_order',
            [
                'type'          => Controls_Manager::SELECT,
                'label'         => esc_html__( 'Order', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'DESC',
                'description'   => esc_html__('Post\'s order', 'xt-reptro-cpt-shortcode'),
                'options'       => [
                    'ASC'       => esc_html__( 'Ascending', 'xt-reptro-cpt-shortcode' ),
                    'DESC'      => esc_html__( 'Descending', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );        

        // order by
        $this->add_control(
            'reptro_post_item_order_by',
            [
                'type'    => Controls_Manager::SELECT,
                'label'   => esc_html__( 'Order by', 'xt-reptro-cpt-shortcode' ),
                'default' => 'date',
                'description'   => esc_html__('Post\'s orderby', 'xt-reptro-cpt-shortcode'),
                'options' => [
                    'none'          => esc_html__('No order', 'xt-reptro-cpt-shortcode' ),
                    'ID'            => esc_html__('Post ID', 'xt-reptro-cpt-shortcode' ),
                    'author'        => esc_html__('Author', 'xt-reptro-cpt-shortcode' ),
                    'title'         => esc_html__('Title', 'xt-reptro-cpt-shortcode' ),
                    'date'          => esc_html__('Published date', 'xt-reptro-cpt-shortcode' ),
                    'modified'      => esc_html__('Modified date', 'xt-reptro-cpt-shortcode' ),
                    'parent'        => esc_html__('By parent', 'xt-reptro-cpt-shortcode' ),
                    'rand'          => esc_html__('Random order', 'xt-reptro-cpt-shortcode' ),
                    'comment_count' => esc_html__('Comment count', 'xt-reptro-cpt-shortcode' ),
                    'menu_order'    => esc_html__('Menu order', 'xt-reptro-cpt-shortcode' ),
                    'post__in'      => esc_html__('By include order', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        // specific post include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Include', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__('You can put comma separated post id for showing those specific properties only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific post exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Exclude', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__('You can put comma separated post id for excluding those specific properties.', 'xt-reptro-cpt-shortcode')

            ]
        ); 

        $this->add_control(
            'ignore_sticky',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Ignore Sticky Posts?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );          

		$this->end_controls_section();		

        /**
         * -------------------------------------------
         * posts settings tab starts here
         * -------------------------------------------
         */   
        
        /**
         * -------------------------------------------
         * post item's carousel section
         * -------------------------------------------
         */    
        $this->start_controls_section(
            'reptro_post_carousel_settings',
            [
                'label'         => esc_html__('Carousel Settings', 'xt-reptro-cpt-shortcode'),
                'tab'           => Controls_Manager::TAB_SETTINGS,
                'condition'     => [
                    '.post_content_type' => 'slider'
                ]
            ]
        );

        // slider autoplay?
        $this->add_control(
            'autoplay',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Slider Autoplay?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
                'description'   => esc_html__('Show the carousel autoplay as in a slideshow.', 'xt-reptro-cpt-shortcode'),
            ]
        );

        // slider loop?
        $this->add_control(
            'loop',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Slider Loop?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes',
                'description'   => esc_html__('Show the carousel loop as in a slideshow?', 'xt-reptro-cpt-shortcode'),
            ]
        );      

        // show navigation?
        $this->add_control(
            'arrows',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Slider Navigation?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );

        // show pagination?
        $this->add_control(
            'dots',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Slider Pagination?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes',
            ]
        );

        $this->end_controls_section();  

        /**
         * -------------------------------------------
         * post item's responsive section
         * -------------------------------------------
         */  
        $this->start_controls_section(
            'section_responsive',
            [
                'label'         => esc_html__('Responsive Options', 'xt-reptro-cpt-shortcode'),
                'tab'           => Controls_Manager::TAB_SETTINGS,
                'condition'     => [
                    '.post_content_type' => 'slider'
                ]                
            ]
        );

        // heading for desktop
        $this->add_control(
            'heading_desktop',
            [
                'label'         => esc_html__( 'Desktop', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,                
            ]
        );

        // number of items in desktop
        $this->add_control(
            'desktop_columns',
            [
                'label'         => esc_html__('Columns per row', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 8,
                'step'          => 1,
                'default'       => 3
            ]
        );       

        // heading for small desktop
        $this->add_control(
            'heading_small_desktop',
            [
                'label'         => esc_html__( 'Desktop Small', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );        

        // number of items in small desktop
        $this->add_control(
            'small_desktop_columns',
            [
                'label'         => esc_html__('Columns per row', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 7,
                'step'          => 1,
                'default'       => 3              
            ]
        );

        // heading for tablet
        $this->add_control(
            'heading_tablet',
            [
                'label'         => esc_html__( 'Tablet', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );

        // number of items in tablet
        $this->add_control(
            'tablet_columns',
            [
                'label'         => esc_html__('Columns per row', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 5,
                'step'          => 1,
                'default'       => 2        
            ]
        );

        // heading for mobile
        $this->add_control(
            'heading_mobile',
            [
                'label'         => esc_html__( 'Mobile Phone', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );

        // number of items in mobile
        $this->add_control(
            'mobile_columns',
            [
                'label'         => esc_html__('Columns per row', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'min'           => 1,
                'max'           => 3,
                'step'          => 1,
                'default'       => 1
            ]
        );

	}

    private function render_image( $image_id, $settings ) {
        $image_size = $settings['image_size_size'];
        if ( 'custom' === $image_size ) {
            $image_src = Group_Control_Image_Size::get_attachment_image_src( $image_id, 'image_size', $settings );
        } else {
            $image_src = wp_get_attachment_image_src( $image_id, $image_size );
            $image_src = $image_src[0];
        }

        return sprintf( '<img src="%s" alt="%s" />', esc_url($image_src), esc_html( get_post_meta( $image_id, '_wp_attachment_image_alt', true) ) );
    }

	protected function render() {
        $settings              = $this->get_settings();
        $post_content_type     = $settings['post_content_type'];
        $posts_per_page        = $settings['posts_per_page'];
        $column_settings       = $settings['column_settings'];
        $number_of_words       = $settings['number_of_words'];
        $orderby               = $settings['reptro_post_item_order_by'];
        $order                 = $settings['reptro_post_item_order'];
        $post_in_ids           = $settings['post_in_ids'];
        $post_not_in_ids       = $settings['post_not_in_ids'];
        $ignore_sticky         = $settings['ignore_sticky'];
        $autoplay              = $settings['autoplay'];
        $loop                  = $settings['loop'];
        $arrows                = $settings['arrows'];
        $dots                  = $settings['dots'];
        $desktop_columns       = $settings['desktop_columns'];
        $small_desktop_columns = $settings['small_desktop_columns'];
        $tablet_columns        = $settings['tablet_columns'];
        $mobile_columns        = $settings['mobile_columns'];
        
        $post_in_ids        = ( $post_in_ids ? explode( ',', $post_in_ids ) : null );
        $post_not_in_ids    = ( $post_not_in_ids ? explode( ',', $post_not_in_ids ) : null );


        $args = array(
            'post_type'             => 'post', 
            'orderby'               => $orderby,
            'order'                 => $order,
            'posts_per_page'        => $posts_per_page,
            'post__in'              => $post_in_ids,
            'post__not_in'          => $post_not_in_ids,
            'ignore_sticky_posts'   => $ignore_sticky,
            'meta_key'              => '_thumbnail_id',
        );


        $container_classes = array( 'xt-blog-post-' . $post_content_type );

        if( $post_content_type == 'grid' ){
            $container_classes[] = 'row';
        }

        /**
         * Slider Config
         */
        
        if( $post_content_type == 'slider' ){
            $container_classes[] = 'owl-carousel';
            $container_classes[] = 'owl-theme';
        }

        $slider_attr = array(
            'data-autoplay'     => ( $autoplay == 'yes' ? 'true' : 'false' ),
            'data-loop'         => ( $loop == 'yes' ? 'true' : 'false' ),
            'data-items'        => $desktop_columns,
            'data-desktopsmall' => $small_desktop_columns,
            'data-tablet'       => $tablet_columns,
            'data-mobile'       => $mobile_columns,
            'data-navigation'   => ( $arrows == 'yes' ? 'true' : 'false' ),
            'data-pagination'   => ( $dots == 'yes' ? 'true' : 'false' ),
            'data-direction'    => ( is_rtl() ? 'true' : 'false' ),
        );


        $wp_query = new \WP_Query( $args );
        $column = 12/$column_settings;
        $column = 'col-lg-'.esc_attr( $column ). ' col-md-6';

        if( $post_content_type == 'slider' ){
            $column = '';
        }

        if ( $wp_query->have_posts() ) :
            ?>
                <div class="<?php echo esc_attr( implode(' ', $container_classes) ); ?>" <?php echo (  $post_content_type == 'slider' ? reptro_carousel_data_attr_implode( $slider_attr ) : '' ); ?>>

                    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                        <article <?php post_class( $column ) ?>>
                            <div class="xt-post-item">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <div class="xt-post-item-thumb">
                                        <figure>
                                            <a href="<?php the_permalink(); ?>"><?php echo $this->render_image( get_post_thumbnail_id( get_the_id() ), $settings ); ?></a>
                                        </figure> 
                                        <div class="xt-post-item-categories">
                                            <?php 
                                                if(function_exists('reptro_get_post_first_category')){
                                                    echo reptro_get_post_first_category( get_the_id() );
                                                }
                                            ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="xt-post-item-content">
                                    <div class="xt-post-item-date"><?php echo get_the_date( get_option( 'date_format' ) ); ?></div>
                                    <h4 class="xt-post-item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                </div>
                            </div>
                        </article>

                    <?php endwhile; ?>

                </div>


                <?php if( is_admin() && $post_content_type == 'slider' ): ?>
                    <script>
                        (function($) {

                            "use strict";

                            $(".xt-blog-post-slider").each(function() {
                                var t         = $(this),
                                auto          = t.data("autoplay") ? !0 : !1,
                                loop          = t.data("loop") ? !0 : !1,
                                rtl           = t.data("direction") ? !0 : !1,
                                items         = t.data("items") ? parseInt(t.data("items")) : '',
                                desktopsmall  = t.data("desktopsmall") ? parseInt(t.data("desktopsmall")) : '',
                                tablet        = t.data("tablet") ? parseInt(t.data("tablet")) : '',
                                mobile        = t.data("mobile") ? parseInt(t.data("mobile")) : '',
                                nav           = t.data("navigation") ? !0 : !1,
                                pag           = t.data("pagination") ? !0 : !1,
                                navTextLeft   = t.data("direction") ? 'right' : 'left',
                                navTextRight  = t.data("direction") ? 'left' : 'right';

                                $(this).owlCarousel({
                                    autoplay: auto,
                                    rtl: rtl,
                                    items: items,
                                    responsiveClass: true,
                                    responsive:{
                                        0:{
                                            items: mobile,
                                        },
                                        480:{
                                            items: mobile,
                                        },
                                        768:{
                                            items: tablet,
                                        },
                                        1170:{
                                            items: desktopsmall,
                                        },
                                        1200:{
                                            items: items,
                                        }
                                    },
                                    nav: nav,
                                    navText : ['<i class="fa fa-arrow-'+navTextLeft+'" aria-hidden="true"></i>','<i class="fa fa-arrow-'+navTextRight+'" aria-hidden="true"></i>'],
                                    dots: pag,
                                    loop: loop,
                                    margin: 30,
                                });
                            });
                        }(jQuery));
                    </script>
                <?php endif; ?>
            <?php
        endif;
        wp_reset_postdata();
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Post_Item() );