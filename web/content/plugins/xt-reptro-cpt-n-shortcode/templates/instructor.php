<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Instructor extends Widget_Base {

	public function get_name() {
		return 'reptro_instructor';
	}

	public function get_title() {
		return esc_html__( 'Instructor', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-person';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_instructor_content',
  			[
  				'label' => esc_html__( 'General Settings', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // number of instructor
        $this->add_control(
            'posts_per_page',
            [
                'label'         => __('Number Of Instructor Members To Show', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 3,
                'description'   => __('Default 3.', 'xt-reptro-cpt-shortcode')
            ]
        );  

        // instructor grid column
        $this->add_control(
            'column_settings',
            [
                'label'     => esc_html__( 'Columns', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 3,
                'options'   => [
                    6           => esc_html__( '6 Columns', 'xt-reptro-cpt-shortcode' ),
                    4           => esc_html__( '4 Columns', 'xt-reptro-cpt-shortcode' ),
                    3           => esc_html__( '3 Columns', 'xt-reptro-cpt-shortcode' ),
                    2           => esc_html__( '2 Columns', 'xt-reptro-cpt-shortcode' ),
                    1           => esc_html__( '1 Columns', 'xt-reptro-cpt-shortcode' )
                ]              
            ]
        );  

        // number of words to show
        $this->add_control(
            'number_of_words',
            [
                'label'         => __('Number Of Words To Show', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 10,
                'description'   => __('Default 10.', 'xt-reptro-cpt-shortcode')
            ]
        );  

        // need instructor details page?
        $this->add_control(
            'instructor_details_page',
            [
                'label'         => __( 'Instructor Details Page Linking', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'no',
                'return_value'  => 'yes',
                'description'   => __('You want instructor to link there details page.', 'xt-reptro-cpt-shortcode')
            ]
        );    

        // need margin bottom?
        $this->add_control(
            'item_margin_bottom',
            [
                'label'         => __( 'Item Margin Bottom', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'no',
                'return_value'  => 'yes'
            ]
        );

        $this->add_control(
            'row_equal_height',
            [
                'label'         => __( 'Row Equal Height', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );   

        $this->end_controls_section();

        // instructor query settings
        $this->start_controls_section(
            'reptro_instructor_query_settings',
            [
                'label' => esc_html__( 'Query Settings', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // order
        $this->add_control(
            'reptro_instructor_order',
            [
                'type'          => Controls_Manager::SELECT,
                'label'         => __( 'Order', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'DESC',
                'description'   => __('Instructor order', 'xt-reptro-cpt-shortcode'),
                'options'       => [
                    'ASC'       => __( 'Ascending', 'xt-reptro-cpt-shortcode' ),
                    'DESC'      => __( 'Descending', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );        

        // order by
        $this->add_control(
            'reptro_instructor_order_by',
            [
                'type'    => Controls_Manager::SELECT,
                'label'   => __( 'Order by', 'xt-reptro-cpt-shortcode' ),
                'default' => 'date',
                'description'   => __('Instructor orderby', 'xt-reptro-cpt-shortcode'),
                'options' => [
                    'none'          => __('No order', 'xt-reptro-cpt-shortcode' ),
                    'ID'            => __('Post ID', 'xt-reptro-cpt-shortcode' ),
                    'author'        => __('Author', 'xt-reptro-cpt-shortcode' ),
                    'title'         => __('Title', 'xt-reptro-cpt-shortcode' ),
                    'date'          => __('Published date', 'xt-reptro-cpt-shortcode' ),
                    'modified'      => __('Modified date', 'xt-reptro-cpt-shortcode' ),
                    'parent'        => __('By parent', 'xt-reptro-cpt-shortcode' ),
                    'rand'          => __('Random order', 'xt-reptro-cpt-shortcode' ),
                    'comment_count' => __('Comment count', 'xt-reptro-cpt-shortcode' ),
                    'menu_order'    => __('Menu order', 'xt-reptro-cpt-shortcode' ),
                    'post__in'      => __('By include order', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        // specific instructor include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Specific Instructor', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __(' You can put comma separated instructor id for showing those specific member only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific instructor exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Specific Instructor Exclude', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('You can put comma separated instructor member id for excluding those specific member.', 'xt-reptro-cpt-shortcode')

            ]
        );        

   

		$this->end_controls_section();		
	}

	protected function render() {
        $settings                = $this->get_settings();
        $posts_per_page          =  $settings['posts_per_page'];
        $column_settings         =  $settings['column_settings'];
        $number_of_words         =  $settings['number_of_words'];
        $orderby                 = $settings['reptro_instructor_order_by'];
        $order                   = $settings['reptro_instructor_order'];
        $post_in_ids             = $settings['post_in_ids'];
        $post_not_in_ids         = $settings['post_not_in_ids'];
        $instructor_details_page = $settings['instructor_details_page'];
        $item_margin_bottom      = $settings['item_margin_bottom'];
        $row_equal_height       = $settings['row_equal_height'];
        
        echo do_shortcode( '[business_x_team_scode post="'.$posts_per_page.'" column="'.$column_settings.'" number_of_words="'.$number_of_words.'" orderby="'.$orderby.'" order="'.$order.'" post_in_ids="'.$post_in_ids.'" post_not_in_ids="'.$post_not_in_ids.'" margin_bottom="'.$item_margin_bottom.'" row_equal_height="'.$row_equal_height.'" link_to_details="'.$instructor_details_page.'"]' );
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Instructor() );