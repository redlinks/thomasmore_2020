<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Events extends Widget_Base {

	public function get_name() {
		return 'reptro_events';
	}

	public function get_title() {
		return esc_html__( 'Events', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'fa fa-calendar';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_events_content',
  			[
  				'label' => esc_html__( 'General Settings', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // number of events
        $this->add_control(
            'posts_per_page',
            [
                'label'         => __('Number Of Events To Show', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 10,
                'description'   => __('Default 10.', 'xt-reptro-cpt-shortcode')
            ]
        );     

        // show venue category?
        $this->add_control(
            'show_venue',
            [
                'label'         => __( 'Show Venue', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );   

        $this->end_controls_section();

        // event query settings
        $this->start_controls_section(
            'reptro_event_query_settings',
            [
                'label' => esc_html__( 'Query Settings', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // order
        $this->add_control(
            'reptro_events_order',
            [
                'type'          => Controls_Manager::SELECT,
                'label'         => __( 'Order', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'DESC',
                'description'   => __('Events order', 'xt-reptro-cpt-shortcode'),
                'options'       => [
                    'ASC'       => __( 'Ascending', 'xt-reptro-cpt-shortcode' ),
                    'DESC'      => __( 'Descending', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );        

        // order by
        $this->add_control(
            'reptro_events_order_by',
            [
                'type'    => Controls_Manager::SELECT,
                'label'   => __( 'Order by', 'xt-reptro-cpt-shortcode' ),
                'default' => 'date',
                'description'   => __('Events orderby', 'xt-reptro-cpt-shortcode'),
                'options' => [
                    'none'          => __('No order', 'xt-reptro-cpt-shortcode' ),
                    'ID'            => __('Post ID', 'xt-reptro-cpt-shortcode' ),
                    'author'        => __('Author', 'xt-reptro-cpt-shortcode' ),
                    'title'         => __('Title', 'xt-reptro-cpt-shortcode' ),
                    'date'          => __('Published date', 'xt-reptro-cpt-shortcode' ),
                    'modified'      => __('Modified date', 'xt-reptro-cpt-shortcode' ),
                    'parent'        => __('By parent', 'xt-reptro-cpt-shortcode' ),
                    'rand'          => __('Random order', 'xt-reptro-cpt-shortcode' ),
                    'comment_count' => __('Comment count', 'xt-reptro-cpt-shortcode' ),
                    'menu_order'    => __('Menu order', 'xt-reptro-cpt-shortcode' ),
                    'post__in'      => __('By include order', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        // events categories
        $this->add_control(
            'event_categories',
            [   
                'label'         => esc_html__( 'Events Categories', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Comma separated event\'s category id. It will show this selected event\'s category only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific event include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Include', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('You can put comma separated event id for showing those specific events only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific event exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Exclude', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('You can put comma separated event id for excluding those specific events.', 'xt-reptro-cpt-shortcode')

            ]
        );        

		$this->end_controls_section();

	}

	protected function render() {
        $settings              = $this->get_settings();
        $event_categories      =  $settings['event_categories'];
        $post_in_ids           = $settings['post_in_ids'];
        $post_not_in_ids       = $settings['post_not_in_ids'];
        $posts_per_page        =  $settings['posts_per_page'];
        $orderby               = $settings['reptro_events_order_by'];
        $order                 = $settings['reptro_events_order'];
        $show_venue            = $settings['show_venue'];
        
        echo do_shortcode( '[schooling_events event_categories="'.$event_categories.'" event_ids="'.$post_in_ids.'" not_in="'.$post_not_in_ids.'" post="'.$posts_per_page.'" orderby="'.$orderby.'" order="'.$order.'" show_venue="'.$show_venue.'"]' );

	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Events() );