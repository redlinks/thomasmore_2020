<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Portfolio_Item extends Widget_Base {

	public function get_name() {
		return 'reptro_portfolio_item';
	}

	public function get_title() {
		return esc_html__( 'Portfolio Items', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-gallery-justified';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_service_box_content',
  			[
  				'label' => esc_html__( 'Portfolio Items', 'xt-reptro-cpt-shortcode' )
  			]
  		);
        // number of portfolio items
        $this->add_control(
            'posts_per_page',
            [
                'label'         => __('Number Of Portfolio To Show', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 9,
                'description'   => __('Default 9.', 'xt-reptro-cpt-shortcode')
            ]
        );  

        // portfolio grid column
        $this->add_control(
            'column_settings',
            [
                'label'     => esc_html__( 'Columns', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::SELECT,
                'default'   => 3,
                'options'   => [
                    6           => esc_html__( '6 Columns', 'xt-reptro-cpt-shortcode' ),
                    4           => esc_html__( '4 Columns', 'xt-reptro-cpt-shortcode' ),
                    3           => esc_html__( '3 Columns', 'xt-reptro-cpt-shortcode' ),
                    2           => esc_html__( '2 Columns', 'xt-reptro-cpt-shortcode' ),
                    1           => esc_html__( '1 Columns', 'xt-reptro-cpt-shortcode' )
                ]              
            ]
        );  

        // order
        $this->add_control(
            'reptro_portfolio_order',
            [
                'type'          => Controls_Manager::SELECT,
                'label'         => __( 'Order', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'DESC',
                'description'   => __('portfolio order', 'xt-reptro-cpt-shortcode'),
                'options'       => [
                    'ASC'       => __( 'Ascending', 'xt-reptro-cpt-shortcode' ),
                    'DESC'      => __( 'Descending', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );        

        // order by
        $this->add_control(
            'reptro_portfolio_order_by',
            [
                'type'    => Controls_Manager::SELECT,
                'label'   => __( 'Order by', 'xt-reptro-cpt-shortcode' ),
                'default' => 'date',
                'description'   => __('portfolio orderby', 'xt-reptro-cpt-shortcode'),
                'options' => [
                    'none'          => __('No order', 'xt-reptro-cpt-shortcode' ),
                    'ID'            => __('Post ID', 'xt-reptro-cpt-shortcode' ),
                    'author'        => __('Author', 'xt-reptro-cpt-shortcode' ),
                    'title'         => __('Title', 'xt-reptro-cpt-shortcode' ),
                    'date'          => __('Published date', 'xt-reptro-cpt-shortcode' ),
                    'modified'      => __('Modified date', 'xt-reptro-cpt-shortcode' ),
                    'parent'        => __('By parent', 'xt-reptro-cpt-shortcode' ),
                    'rand'          => __('Random order', 'xt-reptro-cpt-shortcode' ),
                    'comment_count' => __('Comment count', 'xt-reptro-cpt-shortcode' ),
                    'menu_order'    => __('Menu order', 'xt-reptro-cpt-shortcode' ),
                    'post__in'      => __('By include order', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        // specific instructor include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Include', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('You can put comma separated portfolio id for showing those specific properties only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific instructor exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Exclude', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('You can put comma separated portfolio id for excluding those specific properties.', 'xt-reptro-cpt-shortcode')

            ]
        );        

        $this->end_controls_section();

	}

	protected function render() {
        $settings        = $this->get_settings();
        $posts_per_page  =  $settings['posts_per_page'];
        $column_settings =  $settings['column_settings'];
        $orderby         = $settings['reptro_portfolio_order_by'];
        $order           = $settings['reptro_portfolio_order'];
        $post_in_ids     = $settings['post_in_ids'];
        $post_not_in_ids = $settings['post_not_in_ids'];

        wp_enqueue_style('fancybox');
                    wp_enqueue_script('fancybox');
                    wp_enqueue_script('isotope');

        echo do_shortcode( '[business_x_portfolio_scode post="'.$posts_per_page.'" columns="'.$column_settings.'" orderby="'.$orderby.'" order="'.$order.'" post_in_ids="'.$post_in_ids.'" post_not_in_ids="'.$post_not_in_ids.'"]' );

        ?>
            <?php if( is_admin() ): ?>
                <script>
                    (function($) {

                        "use strict";

                            var $container = $('.elementor-element-<?php echo esc_attr( $this->get_id() ); ?> .portfolio-container');

                            if ( $.isFunction($.fn.isotope) ) {
                                $container.isotope({
                                    filter: '*',
                                    animationOptions: {
                                        queue: true
                                    }
                                });
                            }

                            $('.portfolio-nav li').click(function(){
                                $('.portfolio-nav .current').removeClass('current');
                                $(this).addClass('current');

                                if ( $.isFunction($.fn.isotope) ) {
                                    var selector = $(this).attr('data-filter');
                                    $container.isotope({
                                        filter: selector,
                                        animationOptions: {
                                        queue: true
                                    }
                                    });
                                    return false;
                                }
                            });

                            /**
                             * FancyBox
                            */


                            if ( $.isFunction($.fn.fancybox) ) {
                                $("[data-fancybox]").fancybox();
                            }

                            // Resize isotope on editor
                            $('.elementor-editor-active .elementor-section').on('hover', function() {
                                $container.isotope( 'layout' );
                            });

                            $(document).ready(function() {
                              setTimeout(function() {
                                $container.isotope( 'layout' );
                              }, 1000);
                            });

                    }(jQuery));
                </script>
            <?php endif; ?>
        <?php
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Portfolio_Item() );