<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Progress_Bar_Item extends Widget_Base {

	public function get_name() {
		return 'reptro_progress_bar_item';
	}

	public function get_title() {
		return esc_html__( 'Progressbar', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-skill-bar';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_progress_bar_content',
  			[
  				'label' => esc_html__( 'Progressbar', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // fancy list item options
        $this->add_control(
            'progress_bars',
            [
                'label'         => __('Fancy List Items', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::REPEATER,
                'default'       => [
                    [ 
                        'title'      => __( 'Progress Bar title', 'xt-reptro-cpt-shortcode' ),
                        'data_value' => __( 70, 'xt-reptro-cpt-shortcode' ),
                    ],
                    [ 
                        'title'      => __( 'Python', 'xt-reptro-cpt-shortcode' ),
                        'data_value' => __( 75, 'xt-reptro-cpt-shortcode' ),
                    ],
                    [ 
                        'title'      => __( 'PHP', 'xt-reptro-cpt-shortcode' ),
                        'data_value' => __( 80, 'xt-reptro-cpt-shortcode' ),
                    ],
                    [ 
                        'title'      => __( 'JavaScript', 'xt-reptro-cpt-shortcode' ),
                        'data_value' => __( 85, 'xt-reptro-cpt-shortcode' ),
                    ],
                    [ 
                        'title'      => __( 'Shopify', 'xt-reptro-cpt-shortcode' ),
                        'data_value' => __( 90, 'xt-reptro-cpt-shortcode' ),
                    ],
                ],
                'fields' => [
                    [
                        'name'        => 'title',
                        'label'       => __( 'Title', 'xt-reptro-cpt-shortcode' ),
                        'type'        => Controls_Manager::TEXT,
                    ],
                    [
                        'name'        => 'data_value',
                        'label'       => __( 'Progress Bar Data Value', 'xt-reptro-cpt-shortcode' ),
                        'type'          => Controls_Manager::NUMBER,
                        'default'       => 80,
                    ]
                ],
                'title_field' => '{{{ title }}}'              
            ]
        );

        $this->end_controls_section();

	}

	protected function render() {
        $settings           = $this->get_settings();
        $progress_bars      =  $settings['progress_bars'];


        echo reptro_progress_bar_shortcode(array(
            'progress_bars' => $progress_bars
        ));

        if( is_admin() ):
            ?>
                <script>
                    (function($) {

                        "use strict";

                        !function(a){"use strict";function t(){d=1,a(".bar_group").each(function(){a(this).addClass("group_ident-"+d),a(this).data("gid",d),d++})}function i(){a(".bar_group").each(function(){var t=[];a(this).children().each(function(){t.push(a(this).attr("data-value"))}),p["group_ident-"+a(this).data("gid")]=t,void 0!==a(this).attr("data-max")?a(this).data("bg_max",a(this).attr("data-max")):a(this).data("bg_max",Math.max.apply(null,t))})}function s(){a(".bar_group__bar").each(function(){void 0!==a(this).attr("data-label")&&a('<p class="b_label">'+a(this).attr("data-label")+"</p>").insertBefore(a(this))})}function n(){a(".bar_group__bar").each(function(){"true"==a(this).attr("data-show-values")&&(a(this).css("margin-bottom","40px"),void 0!==a(this).attr("data-unit")?(a(this).append('<p class="bar_label_min">0 '+a(this).attr("data-unit")+"</p>"),a(this).append('<p class="bar_label_max">'+a(this).parent().data("bg_max")+" "+a(this).attr("data-unit")+"</p>")):(a(this).append('<p class="bar_label_min">0</p>'),a(this).append('<p class="bar_label_max">'+a(this).parent().data("bg_max")+"</p>")))})}function r(){a(".bar_group__bar").each(function(){"true"==a(this).attr("data-tooltip")&&(a(this).css("margin-bottom","40px"),a(this).append('<div class="b_tooltip"><span>'+a(this).attr("data-value")+'</span><div class="b_tooltip--tri"></div></div>'))})}function o(t){var i=a(t),s=a(window),n=s.scrollTop(),r=n+s.height(),o=i.offset().top,h=o+i.height();r>h-45&&i.css("width",i.attr("data-value")/i.parent().data("bg_max")*100+"%")}function h(){t(),i(),s(),r(),n()}var p="",d="";p={},a(".bar_group__bar").each(function(){o(a(this))}),a(window).scroll(function(){a(".bar_group__bar").each(function(){o(a(this))})}),h()}(jQuery);

                    }(jQuery));   
                </script>     
            <?php
        endif;

	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Progress_Bar_Item() );