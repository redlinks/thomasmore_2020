<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Section_Sub_Title extends Widget_Base {

	public function get_name() {
		return 'reptro_section_sub_title';
	}

	public function get_title() {
		return esc_html__( 'Section Sub Title', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-post-title';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {


  		$this->start_controls_section(
  			'reptro_section_sub_title_content',
  			[
  				'label' => esc_html__( 'Section Sub Title', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // text area for title
        $this->add_control(
            'title_text',
            [   
                'label'         => esc_html__( 'Title text', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXTAREA,
                'default'       => esc_html__( 'Title text here', 'xt-reptro-cpt-shortcode' ),
            ]
        );

        // title alignment
        $this->add_control(
            'align',
            [
                'label'         => esc_html__( 'Title Align', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'default',
                'options'       => [
                    'default'   => esc_html__( 'Default', 'xt-reptro-cpt-shortcode' ),
                    'center'    => esc_html__( 'Center', 'xt-reptro-cpt-shortcode' ),
                    'left'      => esc_html__( 'Left', 'xt-reptro-cpt-shortcode' ),
                    'right'     => esc_html__( 'Right', 'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // title margin top
        $this->add_control(
            'margin_top',
            [
                'label'         => esc_html__( 'Margin top', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'none',
                'options'       => [
                    'none'      => esc_html__( 'No Margin', 'xt-reptro-cpt-shortcode' ),
                    'xsmall'    => esc_html__( 'Extra Small', 'xt-reptro-cpt-shortcode' ),
                    'small'     => esc_html__( 'Small', 'xt-reptro-cpt-shortcode' ),
                    'medium'    => esc_html__( 'Medium', 'xt-reptro-cpt-shortcode' ),
                    'large'     => esc_html__( 'Large', 'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // title margin bottom
        $this->add_control(
            'margin_bottom',
            [
                'label'         => esc_html__( 'Margin bottom', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'small',
                'options'       => [
                    'none'      => esc_html__( 'No Margin', 'xt-reptro-cpt-shortcode' ),
                    'xsmall'    => esc_html__( 'Extra Small', 'xt-reptro-cpt-shortcode' ),
                    'small'     => esc_html__( 'Small', 'xt-reptro-cpt-shortcode' ),
                    'medium'    => esc_html__( 'Medium', 'xt-reptro-cpt-shortcode' ),
                    'large'     => esc_html__( 'Large', 'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // title color
        $this->add_control(
            'color',
            [
                'label'         => esc_html__( 'Color', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'default',
                'options'       => [
                    'default'   => esc_html__( 'Default', 'xt-reptro-cpt-shortcode' ),
                    'white'     => esc_html__( 'White', 'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // show border bottom?
        $this->add_control(
            'border_bottom',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Border Bottom?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes',
                'description'   => __('Title border bottom.', 'xt-reptro-cpt-shortcode'),
            ]
        );

        // extra clss
        $this->add_control(
            'x_class',
            [   
                'label'         => esc_html__( 'Extra class', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__( 'Extra CSS class.', 'xt-reptro-cpt-shortcode' ),
            ]
        );

		$this->end_controls_section();		
	}

	protected function render() {
        $settings      = $this->get_settings();        
        $title_text    =  $settings['title_text'];
        $align         =  $settings['align'];
        $margin_top    =  $settings['margin_top'];
        $margin_bottom =  $settings['margin_bottom'];
        $border_bottom =  $settings['border_bottom'];
        $color         =  $settings['color'];
        $x_class       =  $settings['x_class'];

        
        echo do_shortcode( '[busienss_x_section_title_small title_text="'.$title_text.'" align="'.$align.'" margin_top="'.$margin_top.'" margin_bottom="'.$margin_bottom.'" font_size="default" border_bottom="'.$border_bottom.'" color="'.$color.'" x_class="'.$x_class.'"]' );

	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Section_Sub_Title() );