<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Course extends Widget_Base {

	public function get_name() {
		return 'reptro_course';
	}

	public function get_title() {
		return esc_html__( 'Courses', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'fa fa-graduation-cap';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_course_content',
  			[
  				'label' => esc_html__( 'General Settings', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // conent type
        $this->add_control(
            'course_content_type',
            [
                'label'             => esc_html__( 'Content Type', 'xt-reptro-cpt-shortcode' ),
                'type'              => Controls_Manager::SELECT,
                'default'           => 'grid',
                'options'           => [
                    'grid'          => esc_html__( 'Grid', 'xt-reptro-cpt-shortcode' ),
                    'slider'        => esc_html__( 'Slider',   'xt-reptro-cpt-shortcode' ),
                    'list'          => esc_html__( 'List',   'xt-reptro-cpt-shortcode' ),
                ]
            ]
        );

        // conent type
        $this->add_control(
            'test_dropdown_cats',
            [
                'label'             => esc_html__( 'Cats', 'xt-reptro-cpt-shortcode' ),
                'type'              => Controls_Manager::SELECT,
                'options'           => [
                    'grid'          => esc_html__( 'Grid', 'xt-reptro-cpt-shortcode' ),
                    'slider'        => esc_html__( 'Slider',   'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // category filtering?
        $this->add_control(
            'category_filtering',
            [
                'label'         => __( 'Category Filtering', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'condition'     => [
                    '.course_content_type' => 'grid'
                ],                
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );  

        // course grid column
        $this->add_control(
            'column_settings',
            [
                'label'         => esc_html__( 'Course Grid Columns', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 3,
                'condition'     => [
                    '.course_content_type' => 'grid'
                ],                
                'options'       => [
                    6           => esc_html__( '6 Columns', 'xt-reptro-cpt-shortcode' ),
                    4           => esc_html__( '4 Columns', 'xt-reptro-cpt-shortcode' ),
                    3           => esc_html__( '3 Columns', 'xt-reptro-cpt-shortcode' ),
                    2           => esc_html__( '2 Columns', 'xt-reptro-cpt-shortcode' ),
                    1           => esc_html__( '1 Columns', 'xt-reptro-cpt-shortcode' )
                ]              
            ]
        );  

        // show all courses button?
        $this->add_control(
            'show_all_course_btn',
            [
                'label'         => __( 'Show All Courses Button?', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );     

        $this->end_controls_section();

        // course query settings
        $this->start_controls_section(
            'reptro_course_query_settings',
            [
                'label' => esc_html__( 'Query Settings', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // course method type
        $this->add_control(
            'course_type',
            [
                'label'         => esc_html__( 'Course Type', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'condition'     => [
                    '.course_content_type' => 'grid'
                ],                
                'options'       => [
                    ''          => esc_html__( 'Select a course type', 'xt-reptro-cpt-shortcode' ),
                    'free'      => esc_html__( 'Free', 'xt-reptro-cpt-shortcode' ),
                    'paid'      => esc_html__( 'Paid', 'xt-reptro-cpt-shortcode' )
                ],
                'description'   => esc_html__('Course type. Free / Premium.', 'xt-reptro-cpt-shortcode')            
            ]
        );  

        // show only featured courses?
        $this->add_control(
            'featured',
            [
                'label'         => __( 'Show Only Featured Courses?', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SWITCHER,                
                'default'       => 'no',
                'return_value'  => 'yes'
            ]
        );  


        // number of courses
        $this->add_control(
            'posts_per_page',
            [
                'label'         => __('Number Of Course', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 10,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 50,
                    ],
                ],
            ]
        ); 

        // order
        $this->add_control(
            'reptro_course_order',
            [
                'type'          => Controls_Manager::SELECT,
                'label'         => __( 'Order', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'DESC',
                'description'   => __('Order', 'xt-reptro-cpt-shortcode'),
                'options'       => [
                    'ASC'       => __( 'Ascending', 'xt-reptro-cpt-shortcode' ),
                    'DESC'      => __( 'Descending', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );        

        // order by
        $this->add_control(
            'reptro_course_order_by',
            [
                'type'              => Controls_Manager::SELECT,
                'label'             => __( 'Order by', 'xt-reptro-cpt-shortcode' ),
                'default'           => 'date',
                'description'       => __('Orderby', 'xt-reptro-cpt-shortcode'),
                'options'           => [
                    'none'          => __('No order', 'xt-reptro-cpt-shortcode' ),
                    'ID'            => __('Post ID', 'xt-reptro-cpt-shortcode' ),
                    'author'        => __('Author', 'xt-reptro-cpt-shortcode' ),
                    'title'         => __('Title', 'xt-reptro-cpt-shortcode' ),
                    'date'          => __('Published date', 'xt-reptro-cpt-shortcode' ),
                    'modified'      => __('Modified date', 'xt-reptro-cpt-shortcode' ),
                    'parent'        => __('By parent', 'xt-reptro-cpt-shortcode' ),
                    'rand'          => __('Random order', 'xt-reptro-cpt-shortcode' ),
                    'comment_count' => __('Comment count', 'xt-reptro-cpt-shortcode' ),
                    'menu_order'    => __('Menu order', 'xt-reptro-cpt-shortcode' ),
                    'post__in'      => __('By include order', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        // course categories
        $this->add_control(
            'course_categories',
            [   
                'label'         => esc_html__( 'Course Categories', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Comma separated course categories id. It will show courses form this categories only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // course tags
        $this->add_control(
            'course_tags',
            [   
                'label'         => esc_html__( 'Course Tags', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Comma separated course tags id. It will show courses form this tags only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific course include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Specific Course Ids', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Comma separated course id. It will show this selected courses only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific course exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Exclude Specific Course Ids', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Comma separated course id. It will hide the selected courses.', 'xt-reptro-cpt-shortcode')

            ]
        );        

		$this->end_controls_section();		

        /**
         * -------------------------------------------
         * courses settings tab starts here
         * -------------------------------------------
         */   
        
        /**
         * -------------------------------------------
         * course item's carousel section
         * -------------------------------------------
         */    
        $this->start_controls_section(
            'reptro_course_carousel_settings',
            [
                'label'         => __('Carousel Settings', 'xt-reptro-cpt-shortcode'),
                'tab'           => Controls_Manager::TAB_SETTINGS,
                'condition'     => [
                    '.course_content_type' => 'slider'
                ]
            ]
        );

        // slider autoplay?
        $this->add_control(
            'autoplay',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Autoplay?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
                'description'   => __('Show the carousel autoplay as in a slideshow.', 'xt-reptro-cpt-shortcode'),
            ]
        );

        // slider loop?
        $this->add_control(
            'loop',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Loop?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes',
                'description'   => __('Show the carousel loop as in a slideshow?', 'xt-reptro-cpt-shortcode'),
            ]
        );      

        // show navigation?
        $this->add_control(
            'arrows',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Navigation?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );

        // show pagination?
        $this->add_control(
            'dots',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Pagination?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
            ]
        );

        $this->end_controls_section();  

        /**
         * -------------------------------------------
         * course item's responsive section
         * -------------------------------------------
         */  
        $this->start_controls_section(
            'section_responsive',
            [
                'label'         => __('Responsive Options', 'xt-reptro-cpt-shortcode'),
                'tab'           => Controls_Manager::TAB_SETTINGS,
                'condition'     => [
                    '.course_content_type' => 'slider'
                ]                
            ]
        );

        // heading for desktop
        $this->add_control(
            'heading_desktop',
            [
                'label'         => __( 'Desktop', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,                
            ]
        );

        $this->add_control(
            'desktop_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 3,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 10,
                    ],
                ],
            ]
        );      

        // heading for small desktop
        $this->add_control(
            'heading_small_desktop',
            [
                'label'         => __( 'Desktop Small', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );        

        // number of items in small desktop
        $this->add_control(
            'small_desktop_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 3,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 10,
                    ],
                ],
            ]
        );

        // heading for tablet
        $this->add_control(
            'heading_tablet',
            [
                'label'         => __( 'Tablet', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );

        // number of items in tablet
        $this->add_control(
            'tablet_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 2,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 10,
                    ],
                ],
            ]
        );

        // heading for mobile
        $this->add_control(
            'heading_mobile',
            [
                'label'         => __( 'Mobile Phone', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );

        // number of items in mobile
        $this->add_control(
            'mobile_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 1,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 10,
                    ],
                ],
            ]
        );

        $this->end_controls_section(); 

	}

	protected function render() {
        
        $settings              = $this->get_settings();
        $type                  = $settings['course_content_type'];
        $cat_filter            = $settings['category_filtering'];
        $course_type           = $settings['course_type'];
        $featured              = $settings['featured'];
        $column_settings       = $settings['column_settings'];
        $course_categories     = $settings['course_categories'];
        $course_tags           = $settings['course_tags'];
        $post_in_ids           = $settings['post_in_ids'];
        $post_not_in_ids       = $settings['post_not_in_ids'];
        $show_all_course_btn   = $settings['show_all_course_btn'];
        $posts_per_page        = $settings['posts_per_page']['size'];
        $orderby               = $settings['reptro_course_order_by'];
        $order                 = $settings['reptro_course_order'];
        $autoplay              = $settings['autoplay'];
        $loop                  = $settings['loop'];
        $arrows                = $settings['arrows'];
        $dots                  = $settings['dots'];
        $desktop_columns       = $settings['desktop_columns']['size'];
        $small_desktop_columns = $settings['small_desktop_columns']['size'];
        $tablet_columns        = $settings['tablet_columns']['size'];
        $mobile_columns        = $settings['mobile_columns']['size'];
        
        echo do_shortcode( '[schooling_course type="'.$type.'" category_filtering="'.$cat_filter.'" course_type="'.$course_type.'" featured="'.$featured.'" column="'.$column_settings.'" post="'.$posts_per_page.'" order="'.$order.'" orderby="'.$orderby.'" course_categories="'.$course_categories.'" course_tags="'.$course_tags.'" course_ids="'.$post_in_ids.'" not_in="'.$post_not_in_ids.'" all_courses_btn="'.$show_all_course_btn.'" autoplay="'.$autoplay.'" loop="'.$loop.'" navigation="'.$arrows.'" pagination="'.$dots.'" items="'.$desktop_columns.'" desktopsmall="'.$small_desktop_columns.'" tablet="'.$tablet_columns.'" mobile="'.$mobile_columns.'"]' );

        ?>
            <?php if( is_admin() ): ?>
                <script>
                    (function($) {

                        "use strict";

                        <?php if( $type == 'slider' ): ?>
                            $(".reptro-course-slider").each(function() {
                                var t         = $(this),
                                auto          = t.data("autoplay") ? !0 : !1,
                                loop          = t.data("loop") ? !0 : !1,
                                rtl           = t.data("direction") ? !0 : !1,
                                items         = t.data("items") ? parseInt(t.data("items")) : '',
                                desktopsmall  = t.data("desktopsmall") ? parseInt(t.data("desktopsmall")) : '',
                                tablet        = t.data("tablet") ? parseInt(t.data("tablet")) : '',
                                mobile        = t.data("mobile") ? parseInt(t.data("mobile")) : '',
                                nav           = t.data("navigation") ? !0 : !1,
                                pag           = t.data("pagination") ? !0 : !1,
                                navTextLeft   = t.data("direction") ? 'right' : 'left',
                                navTextRight  = t.data("direction") ? 'left' : 'right';

                                $(this).owlCarousel({
                                    autoplay: auto,
                                    rtl: rtl,
                                    items: items,
                                    responsiveClass: true,
                                    responsive:{
                                        0:{
                                            items: mobile,
                                        },
                                        480:{
                                            items: mobile,
                                        },
                                        768:{
                                            items: tablet,
                                        },
                                        1170:{
                                            items: desktopsmall,
                                        },
                                        1200:{
                                            items: items,
                                        }
                                    },
                                    nav: nav,
                                    navText : ['<i class="fa fa-arrow-'+navTextLeft+'" aria-hidden="true"></i>','<i class="fa fa-arrow-'+navTextRight+'" aria-hidden="true"></i>'],
                                    dots: pag,
                                    loop: loop,
                                    margin: 30,
                                });
                            });
                        <?php endif; ?>

                        <?php if( $type == 'grid' && $cat_filter == 'yes' ): ?>
                            
                            var $courseContainer = $('.elementor-element-<?php echo esc_attr( $this->get_id() ); ?> .reptro-course-isotope');

                            if ( $.isFunction($.fn.isotope) ) {
                                $courseContainer.isotope({
                                    filter: '*',
                                    animationOptions: {
                                        queue: true
                                    }
                                });
                            }

                            $('.reptro-category-items li').click(function(){
                                $('.reptro-category-items .active').removeClass('active');
                                $(this).addClass('active');

                                if ( $.isFunction($.fn.isotope) ) {
                                    var CourseCatSelector = $(this).attr('data-filter');
                                    $courseContainer.isotope({
                                        filter: CourseCatSelector,
                                        animationOptions: {
                                            queue: true
                                        }
                                    });
                                    return false;
                                }
                            }); 

                            $('.elementor-editor-active .elementor-section').on('hover', function() {
                                $courseContainer.isotope( 'layout' );
                            });

                            $(document).ready(function() {
                                setTimeout(function() {
                                    $courseContainer.isotope( 'layout' );
                                }, 1000);
                            });

                        <?php endif; ?>

                    }(jQuery));
                </script>
            <?php endif; ?>
        <?php
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Course() );