<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Video_PopUp extends Widget_Base {

	public function get_name() {
		return 'reptro_video_popup';
	}

	public function get_title() {
		return esc_html__( 'Video PopUp', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-play';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_video_popup_content',
  			[
  				'label' => esc_html__( 'Video PopUp', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        $this->add_control(
            'reptro_video_popup_type',
            [
                'label'         => esc_html__( 'PopUp Type', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'with_image',            
                'options'       => [
                    'with_image'   => esc_html__( 'Image Background', 'xt-reptro-cpt-shortcode' ),
                    'button_only'  => esc_html__( 'Button only', 'xt-reptro-cpt-shortcode' ),
                ]              
            ]
        ); 

        $this->add_control(
            'reptro_video_btn_text',
            [   
                'label'         => esc_html__( 'Button Text', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'default'       => esc_html__( 'Video Showcase', 'xt-reptro-cpt-shortcode' ),
                'description'   => esc_html__('Video Button Text.', 'xt-reptro-cpt-shortcode'),
                'condition'     => [
                    '.reptro_video_popup_type' => 'button_only'
                ], 

            ]
        ); 

        // video background image
        $this->add_control(
            'bg_img',
            [
                'label'     => __( 'Video Background Image', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::MEDIA,
                'default'   => [
                    'url'   => Utils::get_placeholder_image_src(),
                ],
                'condition'     => [
                    '.reptro_video_popup_type' => 'with_image'
                ], 

            ]
        );

        // image size
        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name'          => 'image_size',
                'label'         => __( 'Image Size', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'full',
                'condition'     => [
                    '.reptro_video_popup_type' => 'with_image'
                ],
            ]
        );
        
        // video link/URL
        $this->add_control(
            'reptro_video_popup_link',
            [   
                'label'         => esc_html__( 'Video Link URL', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'placeholder'   => __( 'Place your video link/URL here.', 'xt-reptro-cpt-shortcode' ),
            ]
        );

        $this->end_controls_section();

	}

    // render image function
    private function render_image( $settings ) {
        $image_id   = $settings['bg_img']['id'];
        $image_size = $settings['image_size_size'];
        if ( 'custom' === $image_size ) {
            $image_src = Group_Control_Image_Size::get_attachment_image_src( $image_id, 'image_size', $settings );
        } else {
            $image_src = wp_get_attachment_image_src( $image_id, $image_size );
            $image_src = $image_src[0];
        }

        return sprintf( '<img src="%s" alt="%s" />', esc_url($image_src), esc_html( get_post_meta( $image_id, '_wp_attachment_image_alt', true) ) );
    } 

	protected function render() {
        $settings   =  $this->get_settings();
        $bg_img     =  $this->render_image( $settings );
        $link       =  $settings['reptro_video_popup_link'];
        $type       =  $settings['reptro_video_popup_type'];
        $btn_text   =  $settings['reptro_video_btn_text'];

        if( function_exists('reptro_video_shortcode') ){
            echo reptro_video_shortcode( array( 'video_link' => $link, 'bg_img' => $bg_img, 'type' => $type, 'btn_text' => $btn_text ) );
        }
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Video_PopUp() );