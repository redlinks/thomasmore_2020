<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Course_Categories extends Widget_Base {

    public function get_name() {
        return 'reptro_course_categories';
    }

    public function get_title() {
        return esc_html__( 'Course Categories', 'xt-reptro-cpt-shortcode' );
    }

    public function get_icon() {
        return 'eicon-folder';
    }

    public function get_categories() {
        return [ 'reptro_widgets' ];
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'reptro_course_categories_content',
            [
                'label' => esc_html__( 'General Settings', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // image type
        $this->add_control(
            'cat_img_type',
            [
                'label'             => esc_html__( 'Image Type', 'xt-reptro-cpt-shortcode' ),
                'type'              => Controls_Manager::SELECT,
                'default'           => 'icon',
                'options'           => [
                    'icon'          => esc_html__( 'PNG Icon', 'xt-reptro-cpt-shortcode' ),
                    'image'         => esc_html__( 'Image Background',   'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // conent type
        $this->add_control(
            'course_content_type',
            [
                'label'             => esc_html__( 'Content Type', 'xt-reptro-cpt-shortcode' ),
                'type'              => Controls_Manager::SELECT,
                'default'           => 'grid',
                'options'           => [
                    'grid'          => esc_html__( 'Grid', 'xt-reptro-cpt-shortcode' ),
                    'slider'        => esc_html__( 'Slider',   'xt-reptro-cpt-shortcode' )
                ]
            ]
        );

        // course category grid column
        $this->add_control(
            'column_settings',
            [
                'label'         => esc_html__( 'Columns', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 6,
                'condition'     => [
                    '.course_content_type' => 'grid'
                ],                
                'options'       => [
                    6           => esc_html__( '6 Columns', 'xt-reptro-cpt-shortcode' ),
                    4           => esc_html__( '4 Columns', 'xt-reptro-cpt-shortcode' ),
                    3           => esc_html__( '3 Columns', 'xt-reptro-cpt-shortcode' ),
                    2           => esc_html__( '2 Columns', 'xt-reptro-cpt-shortcode' ),
                    1           => esc_html__( '1 Columns', 'xt-reptro-cpt-shortcode' )
                ]              
            ]
        );  

        // number of cats
        $this->add_control(
            'posts_per_page',
            [
                'label'         => __( 'Number Of Categories', 'wpb-elementor-addons' ),
                'description'   => __('Maximum number of categories to return. Default 0, for showing all.', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 0,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 50,
                    ],
                ],
            ]
        );
        
        // show course icon?
        $this->add_control(
            'show_cat_icon',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Show Icon?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
                'description'   => __('Show category icon?', 'xt-reptro-cpt-shortcode'),
                'condition'     => [
                    '.cat_img_type' => 'icon'
                ], 
            ]
        );

        $this->end_controls_section();

        // course_category_query settings
        $this->start_controls_section(
            'reptro_course_category_query_settings',
            [
                'label' => esc_html__( 'Query Settings', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // hide empty cats?
        $this->add_control(
            'hide_empty_cats',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Hide Empty Categories?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes'
            ]
        );


        // category which have no children with limit?
        $this->add_control(
            'limit_result',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Limit Results To Category That Have No Children?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes'
            ]
        );

        // include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Include', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Comma/space-separated term ids to include.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Exclude', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Comma/space-separated term ids to exclude.', 'xt-reptro-cpt-shortcode')

            ]
        );    

        // child of
        $this->add_control(
            'child_of',
            [   
                'label'         => esc_html__( 'Child Of', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('Category ID to retrieve it\'s childrens.', 'xt-reptro-cpt-shortcode')

            ]
        );        


        $this->end_controls_section();      

        /**
         * -------------------------------------------
         * courses settings tab starts here
         * -------------------------------------------
         */   
        
        /**
         * -------------------------------------------
         * course item's carousel section
         * -------------------------------------------
         */    
        $this->start_controls_section(
            'reptro_course_cat_carousel_settings',
            [
                'label'         => __('Carousel Settings', 'xt-reptro-cpt-shortcode'),
                'tab'           => Controls_Manager::TAB_SETTINGS,
                'condition'     => [
                    '.course_content_type' => 'slider'
                ]
            ]
        );

        // slider autoplay?
        $this->add_control(
            'autoplay',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Autoplay?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
                'description'   => __('Show the carousel autoplay as in a slideshow.', 'xt-reptro-cpt-shortcode'),
            ]
        );

        // slider loop?
        $this->add_control(
            'loop',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Loop?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes',
                'description'   => __('Show the carousel loop as in a slideshow?', 'xt-reptro-cpt-shortcode'),
            ]
        );      

        // show navigation?
        $this->add_control(
            'arrows',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Navigation?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );

        // show pagination?
        $this->add_control(
            'dots',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Pagination?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes',
            ]
        );

        $this->end_controls_section();  

        /**
         * -------------------------------------------
         * course item's responsive section
         * -------------------------------------------
         */  
        $this->start_controls_section(
            'section_responsive',
            [
                'label'         => __('Responsive Options', 'xt-reptro-cpt-shortcode'),
                'tab'           => Controls_Manager::TAB_SETTINGS,
                'condition'     => [
                    '.course_content_type' => 'slider'
                ]                
            ]
        );

        // heading for desktop
        $this->add_control(
            'heading_desktop',
            [
                'label'         => __( 'Desktop', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,                
            ]
        );

        // number of items in desktop
        $this->add_control(
            'desktop_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 4,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 12,
                    ],
                ],
            ]
        );       

        // heading for small desktop
        $this->add_control(
            'heading_small_desktop',
            [
                'label'         => __( 'Desktop Small', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );        

        // number of items in small desktop

        $this->add_control(
            'small_desktop_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 3,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 12,
                    ],
                ],
            ]
        ); 

        // heading for tablet
        $this->add_control(
            'heading_tablet',
            [
                'label'         => __( 'Tablet', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );

        // number of items in tablet
        $this->add_control(
            'tablet_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 2,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 12,
                    ],
                ],
            ]
        ); 

        // heading for mobile
        $this->add_control(
            'heading_mobile',
            [
                'label'         => __( 'Mobile Phone', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::HEADING,
                'separator'     => 'before',
            ]
        );

        // number of items in mobile
        $this->add_control(
            'mobile_columns',
            [
                'label'         => __( 'Columns per row', 'wpb-elementor-addons' ),
                'type'          => Controls_Manager::SLIDER,
                'default'       => [
                    'size'      => 1,
                ],
                'range'         => [
                    'px'        => [
                        'min'   => 1,
                        'max'   => 12,
                    ],
                ],
            ]
        );

        $this->end_controls_section(); 

    }

    protected function render() {
        $settings              = $this->get_settings();
        $img_type              = $settings['cat_img_type'];
        $type                  = $settings['course_content_type'];
        $column_settings       = $settings['column_settings'];
        $show_icon             = $settings['show_cat_icon'];
        $hide_empty_cats       = $settings['hide_empty_cats'];
        $limit_result          = $settings['limit_result'];
        $post_in_ids           = $settings['post_in_ids'];
        $post_not_in_ids       = $settings['post_not_in_ids'];
        $child_of              = $settings['child_of'];
        $posts_per_page        = $settings['posts_per_page']['size'];
        $autoplay              = $settings['autoplay'];
        $loop                  = $settings['loop'];
        $arrows                = $settings['arrows'];
        $dots                  = $settings['dots'];
        $desktop_columns       = $settings['desktop_columns']['size'];
        $small_desktop_columns = $settings['small_desktop_columns']['size'];
        $tablet_columns        = $settings['tablet_columns']['size'];
        $mobile_columns        = $settings['mobile_columns']['size'];
        
        echo do_shortcode( '[schooling_course_category img_type="'.$img_type.'" content_type="'.$type.'" column="'.$column_settings.'" show_icon="'.$show_icon.'" hide_empty="'.$hide_empty_cats.'" childless="'.$limit_result.'" include="'.$post_in_ids.'" exclude="'.$post_not_in_ids.'" child_of="'.$child_of.'" number="'.$posts_per_page.'" autoplay="'.$autoplay.'" loop="'.$loop.'" navigation="'.$arrows.'" pagination="'.$dots.'" items="'.$desktop_columns.'" desktopsmall="'.$small_desktop_columns.'" tablet="'.$tablet_columns.'" mobile="'.$mobile_columns.'"]' );

        ?>
            <?php if( is_admin() && $type == 'slider' ): ?>
                <script>
                    (function($) {

                        "use strict";

                            $(".reptro-course-category-slider").each(function() {
                                var t         = $(this),
                                auto          = t.data("autoplay") ? !0 : !1,
                                loop          = t.data("loop") ? !0 : !1,
                                rtl           = t.data("direction") ? !0 : !1,
                                items         = t.data("items") ? parseInt(t.data("items")) : '',
                                desktopsmall  = t.data("desktopsmall") ? parseInt(t.data("desktopsmall")) : '',
                                tablet        = t.data("tablet") ? parseInt(t.data("tablet")) : '',
                                mobile        = t.data("mobile") ? parseInt(t.data("mobile")) : '',
                                nav           = t.data("navigation") ? !0 : !1,
                                pag           = t.data("pagination") ? !0 : !1,
                                navTextLeft   = t.data("direction") ? 'right' : 'left',
                                navTextRight  = t.data("direction") ? 'left' : 'right';

                                $(this).owlCarousel({
                                    autoplay: auto,
                                    rtl: rtl,
                                    items: items,
                                    responsiveClass: true,
                                    responsive:{
                                        0:{
                                            items: mobile,
                                        },
                                        480:{
                                            items: mobile,
                                        },
                                        768:{
                                            items: tablet,
                                        },
                                        1170:{
                                            items: desktopsmall,
                                        },
                                        1200:{
                                            items: items,
                                        }
                                    },
                                    nav: nav,
                                    navText : ['<i class="fa fa-arrow-'+navTextLeft+'" aria-hidden="true"></i>','<i class="fa fa-arrow-'+navTextRight+'" aria-hidden="true"></i>'],
                                    dots: pag,
                                    loop: loop,
                                    margin: 30,
                                });
                            });

                    }(jQuery));
                </script>
            <?php endif; ?>
        <?php
    }

    protected function content_template() {

    }

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Course_Categories() );