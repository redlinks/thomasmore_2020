<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Main_Slider extends Widget_Base {

	public function get_name() {
		return 'reptro_main_slider';
	}

	public function get_title() {
		return esc_html__( 'Main Slider', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-slider-device';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_main_slider_content',
  			[
  				'label' => esc_html__( 'Slider', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // number of sliders
        $this->add_control(
            'posts_per_page',
            [
                'label'         => __('Number Of Sliders', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::NUMBER,
                'default'       => 3,
                'description'   => __('Default 3.', 'xt-reptro-cpt-shortcode')
            ]
        );  

        // order
        $this->add_control(
            'wpb_ea_order',
            [
                'type'          => Controls_Manager::SELECT,
                'label'         => __( 'Order', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'DESC',
                'options'       => [
                    'ASC'       => __( 'Ascending', 'xt-reptro-cpt-shortcode' ),
                    'DESC'      => __( 'Descending', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );        

        // order by
        $this->add_control(
            'wpb_ea_order_by',
            [
                'type'    => Controls_Manager::SELECT,
                'label'   => __( 'Order by', 'xt-reptro-cpt-shortcode' ),
                'default' => 'date',
                'options' => [
                    'none'          => __('No order', 'xt-reptro-cpt-shortcode' ),
                    'ID'            => __('Post ID', 'xt-reptro-cpt-shortcode' ),
                    'author'        => __('Author', 'xt-reptro-cpt-shortcode' ),
                    'title'         => __('Title', 'xt-reptro-cpt-shortcode' ),
                    'date'          => __('Published date', 'xt-reptro-cpt-shortcode' ),
                    'modified'      => __('Modified date', 'xt-reptro-cpt-shortcode' ),
                    'parent'        => __('By parent', 'xt-reptro-cpt-shortcode' ),
                    'rand'          => __('Random order', 'xt-reptro-cpt-shortcode' ),
                    'comment_count' => __('Comment count', 'xt-reptro-cpt-shortcode' ),
                    'menu_order'    => __('Menu order', 'xt-reptro-cpt-shortcode' ),
                    'post__in'      => __('By include order', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        // specific slider include
        $this->add_control(
            'post_in_ids',
            [   
                'label'         => esc_html__( 'Specific Slide', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => __('You can put comma separated slider id for showing those specific sliders only.', 'xt-reptro-cpt-shortcode')

            ]
        );   

        // specific slider exclude
        $this->add_control(
            'post_not_in_ids',
            [   
                'label'         => esc_html__( 'Specific Slide Exclude', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__('You can put comma separated slider id for excluding those specific sliders.', 'xt-reptro-cpt-shortcode')

            ]
        );        

		$this->end_controls_section();

        /**
         *  Slider Settings
         */
        
        $this->start_controls_section(
            'reptro_post_carousel_settings',
            [
                'label'         => esc_html__('Slider Settings', 'xt-reptro-cpt-shortcode'),
                'tab'           => Controls_Manager::TAB_SETTINGS,
            ]
        );

        // slider autoplay?
        $this->add_control(
            'autoplay',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Slider Autoplay?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
                'description'   => esc_html__('Show the carousel autoplay as in a slideshow.', 'xt-reptro-cpt-shortcode'),
            ]
        );

        // slider loop?
        $this->add_control(
            'loop',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Slider Loop?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'no',
                'return_value'  => 'yes',
                'description'   => esc_html__('Enable slider loop?', 'xt-reptro-cpt-shortcode'),
            ]
        );      

        // show navigation?
        $this->add_control(
            'arrows',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Slider Navigation?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes'
            ]
        );

        // show pagination?
        $this->add_control(
            'dots',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => __( 'Slider Pagination?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
            ]
        );

        $this->end_controls_section(); 	
	}

	protected function render() {
        $settings        = $this->get_settings();
        $posts_per_page  =  $settings['posts_per_page'];
        $orderby         = $settings['wpb_ea_order_by'];
        $order           = $settings['wpb_ea_order'];
        $post_in_ids     = $settings['post_in_ids'];
        $post_not_in_ids = $settings['post_not_in_ids'];
        
        $autoplay              = $settings['autoplay'];
        $loop                  = $settings['loop'];
        $arrows                = $settings['arrows'];
        $dots                  = $settings['dots'];
        
        echo do_shortcode( '[business_x_slider_scode post="'.$posts_per_page.'" orderby="'.$orderby.'" order="'.$order.'" post_in_ids="'.$post_in_ids.'" post_not_in_ids="'.$post_not_in_ids.'" autoplay="'.$autoplay.'" loop="'.$loop.'" navigation="'.$arrows.'" pagination="'.$dots.'"]' );

        ?>
            <?php if( is_admin() ): ?>
                <script>
                    (function($) {

                        "use strict";
       
                        $(".theme-main-slider").each(function() {
                            var t = $(this),
                            auto          = t.data("autoplay") ? !0 : !1,
                            loop          = t.data("loop") ? !0 : !1,
                            rtl           = t.data("direction") ? !0 : !1,
                            nav           = t.data("navigation") ? !0 : !1,
                            pag           = t.data("pagination") ? !0 : !1;

                            $(this).owlCarousel({
                                autoHeight: true,
                                items: 1,
                                loop: loop,
                                autoplay: auto,
                                dots: pag,
                                nav: nav,
                                autoplayTimeout: 7000,
                                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                                animateIn: 'zoomIn',
                                animateOut: 'fadeOut',
                                autoplayHoverPause: false,
                                touchDrag: true,
                                mouseDrag: true,
                                rtl: rtl
                            });

                            $(this).on("translate.owl.carousel", function () {
                                $(this).find(".owl-item .slide-text > *").removeClass("fadeInUp animated").css("opacity","0");
                                $(this).find(".owl-item .slide-img").removeClass("fadeInRight animated").css("opacity","0");
                            });          
                            $(this).on("translated.owl.carousel", function () {
                                $(this).find(".owl-item.active .slide-text > *").addClass("fadeInUp animated").css("opacity","1");
                                $(this).find(".owl-item.active .slide-img").addClass("fadeInRight animated").css("opacity","1");
                            });
                        });

                        $('.slider_preloader_status').fadeOut();
                        $('.slider_preloader').delay(350).fadeOut('slow');
                        $('.header-slider').removeClass("header-slider-preloader");

                    }(jQuery));
                </script>
            <?php endif; ?>
        <?php
	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Main_Slider() );