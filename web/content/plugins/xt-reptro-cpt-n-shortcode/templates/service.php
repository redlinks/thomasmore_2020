<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Service_Item extends Widget_Base {

	public function get_name() {
		return 'reptro_service_item';
	}

	public function get_title() {
		return esc_html__( 'Service Box', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-image-box';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_service_box_content',
  			[
  				'label' => esc_html__( 'Service Box', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        $this->add_control(
            'image',
            [
                'label'     => esc_html__( 'Choose Image', 'xt-reptro-cpt-shortcode' ),
                'type'      => Controls_Manager::MEDIA,
                'default'   => [
                    'url'   => Utils::get_placeholder_image_src(),
                ],
            ]
        );

        // image size
        $this->add_group_control(
            Group_Control_Image_Size::get_type(),
            [
                'name'          => 'image_size',
                'label'         => esc_html__( 'Image Size', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'reptro-blog-small',
            ]
        );
        
        // service box title
        $this->add_control(
            'reptro_service_box_title',
            [   
                'label'         => esc_html__( 'Service title', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'placeholder'   => esc_html__( 'Place your title text here.', 'xt-reptro-cpt-shortcode' ),
                'default'       => esc_html__( 'Lorem Ipsum is dummy text', 'xt-reptro-cpt-shortcode' )
            ]
        );

        // service box title
        $this->add_control(
            'reptro_service_box_text',
            [   
                'label'             => esc_html__( 'Service content', 'xt-reptro-cpt-shortcode' ),
                'type'              => Controls_Manager::TEXTAREA,
                'placeholder'       => esc_html__( 'Place your content text here.', 'xt-reptro-cpt-shortcode' ),       
                'default'           => esc_html__( 'Lorem Ipsum is placeholder text commonly used in the graphic, print, and publishing industries.', 'xt-reptro-cpt-shortcode' )
            ]
        );
        
        $this->end_controls_section();

	}

    private function render_image( $settings ) {
        $image_id   = $settings['image']['id'];
        $image_size = $settings['image_size_size'];
        if ( 'custom' === $image_size ) {
            $image_src = Group_Control_Image_Size::get_attachment_image_src( $image_id, 'image_size', $settings );
        } else {
            $image_src = wp_get_attachment_image_src( $image_id, $image_size );
            $image_src = $image_src[0];
        }
        return sprintf( '<img src="%s" alt="%s" />', esc_url($image_src), esc_html( get_post_meta( $image_id, '_wp_attachment_image_alt', true) ) );
    }

	protected function render() {
        $settings       = $this->get_settings();
        $title          =  $settings['reptro_service_box_title'];
        $service_desc   =  $settings['reptro_service_box_text'];

        ?>
            <div class="reptro-service-item xt-smooth-shadow">
                <div class="reptro-service-item-inner">
                    <?php echo $this->render_image( $settings ); ?>
                    <div class="reptro-service-item-content">
                        <?php
                            if ( $title ) {
                                printf( '<h3>%s</h3><hr class="title-hr">', esc_html( $title ) ); 
                            }
                            if ( $service_desc ) {
                                printf( '%s', wpautop( esc_html( $service_desc ) ) );
                            }
                        ?>
                    </div>
                </div>
            </div>
        <?php
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Service_Item() );