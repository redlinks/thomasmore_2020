<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Section_Title extends Widget_Base {

	public function get_name() {
		return 'reptro_section_title';
	}

	public function get_title() {
		return esc_html__( 'Section Title', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-archive-title';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {


  		$this->start_controls_section(
  			'reptro_section_title_content',
  			[
  				'label' => esc_html__( 'Section Title', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        $this->add_control(
            'title_text',
            [   
                'label'         => esc_html__( 'Title', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXTAREA,
                'default'       => esc_html__( 'Title Text', 'xt-reptro-cpt-shortcode' ),
            ]
        );

        $this->add_control(
            'tag',
            [
                'label'         => esc_html__( 'Title tag', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'h2',
                'options'       => [
                    'h1'        => esc_html__( 'h1', 'xt-reptro-cpt-shortcode' ),
                    'h2'        => esc_html__( 'h2', 'xt-reptro-cpt-shortcode' ),
                    'h3'        => esc_html__( 'h3', 'xt-reptro-cpt-shortcode' ),
                    'h4'        => esc_html__( 'h4', 'xt-reptro-cpt-shortcode' ),
                    'h5'        => esc_html__( 'h5', 'xt-reptro-cpt-shortcode' ),
                    'h6'        => esc_html__( 'h6', 'xt-reptro-cpt-shortcode' ),
                ],
            ]
        );

        $this->add_control(
            'font_size',
            [
                'label'         => esc_html__( 'Font Size', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'default',
                'options'       => [
                    'default'       => esc_html__( 'Tag Default', 'xt-reptro-cpt-shortcode' ),
                    'xx_large'      => esc_html__( 'Extra Large', 'xt-reptro-cpt-shortcode' ),
                ],
            ]
        );

        $this->add_control(
            'align',
            [
                'label'         => esc_html__( 'Title Align', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'default',
                'options'       => [
                    'default'   => esc_html__( 'Default', 'xt-reptro-cpt-shortcode' ),
                    'center'    => esc_html__( 'Center', 'xt-reptro-cpt-shortcode' ),
                    'left'      => esc_html__( 'Left', 'xt-reptro-cpt-shortcode' ),
                    'right'     => esc_html__( 'Right', 'xt-reptro-cpt-shortcode' )
                ],
            ]
        );

        $this->add_control(
            'margin_top',
            [
                'label'         => esc_html__( 'Margin top', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'small',
                'options'       => [
                    'none'      => esc_html__( 'No Margin', 'xt-reptro-cpt-shortcode' ),
                    'xsmall'    => esc_html__( 'Extra Small', 'xt-reptro-cpt-shortcode' ),
                    'small'     => esc_html__( 'Small', 'xt-reptro-cpt-shortcode' ),
                    'medium'    => esc_html__( 'Medium', 'xt-reptro-cpt-shortcode' ),
                    'large'     => esc_html__( 'Large', 'xt-reptro-cpt-shortcode' ),
                    'xlarge'    => esc_html__( 'Extra Large', 'xt-reptro-cpt-shortcode' ),
                ],
            ]
        );

        $this->add_control(
            'margin_bottom',
            [
                'label'         => esc_html__( 'Margin bottom', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'small',
                'options'       => [
                    'none'      => esc_html__( 'No Margin', 'xt-reptro-cpt-shortcode' ),
                    'xsmall'    => esc_html__( 'Extra Small', 'xt-reptro-cpt-shortcode' ),
                    'small'     => esc_html__( 'Small', 'xt-reptro-cpt-shortcode' ),
                    'medium'    => esc_html__( 'Medium', 'xt-reptro-cpt-shortcode' ),
                    'large'     => esc_html__( 'Large', 'xt-reptro-cpt-shortcode' ),
                    'xlarge'    => esc_html__( 'Extra Large', 'xt-reptro-cpt-shortcode' ),
                ],
            ]
        );

        $this->add_control(
            'color',
            [
                'label'         => esc_html__( 'Title color', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 'default',
                'options'       => [
                    'default'       => esc_html__( 'Default', 'xt-reptro-cpt-shortcode' ),
                    'white'         => esc_html__( 'White', 'xt-reptro-cpt-shortcode' ),
                ],
            ]
        );

        // show border bottom?
        $this->add_control(
            'title_border',
            [
                'type'          => Controls_Manager::SWITCHER,
                'label'         => esc_html__( 'Title Border Bottom?', 'xt-reptro-cpt-shortcode' ),
                'default'       => 'yes',
                'return_value'  => 'yes',
                'description'   => esc_html__('Title border bottom.', 'xt-reptro-cpt-shortcode'),
            ]
        );

        // extra clss
        $this->add_control(
            'x_class',
            [   
                'label'         => esc_html__( 'Extra class', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT,
                'description'   => esc_html__( 'Extra CSS class.', 'xt-reptro-cpt-shortcode' ),
            ]
        );        

		$this->end_controls_section();		
	}

	protected function render() {
        $settings        = $this->get_settings();        
        $title_text      =  $settings['title_text'];
        $tag             =  $settings['tag'];
        $margin_bottom   =  $settings['margin_bottom'];
        $margin_top      =  $settings['margin_top'];
        $type            =  ( $settings['title_border'] == 'yes' ? 'border' : 'default' );
        $align           =  $settings['align'];
        $font_size       =  $settings['font_size'];
        $color           =  $settings['color'];
        $x_class         =  $settings['x_class'];
        
        echo do_shortcode( '[business_x_section_title_scode title_text="'.$title_text.'" tag="'.$tag.'" margin_bottom="'.$margin_bottom.'" margin_top="'.$margin_top.'" type="'.$type.'" align="'.$align.'" font_size="'.$font_size.'" color="'.$color.'" x_class="'.$x_class.'"]' );

	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Section_Title() );