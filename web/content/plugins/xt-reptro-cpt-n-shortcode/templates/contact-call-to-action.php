<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Contact_Call_To_Action extends Widget_Base {

	public function get_name() {
		return 'reptro_contact_call_to_action';
	}

	public function get_title() {
		return esc_html__( 'Contact Call To Action', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-call-to-action';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_contact_call_to_action',
  			[
  				'label' => esc_html__( 'Contact Call To Action', 'xt-reptro-cpt-shortcode' )
  			]
  		);
        
        // email title
        $this->add_control(
            'reptro_contact_call_to_action_email_title',
            [   
                'label'         => esc_html__( 'Email Contact Title', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXTAREA
            ]
        );

        // email address
        $this->add_control(
            'reptro_contact_call_to_action_email_address',
            [   
                'label'         => esc_html__( 'Email Address', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT
            ]
        );

        // phone title
        $this->add_control(
            'reptro_contact_call_to_action_phone_title',
            [   
                'label'         => esc_html__( 'Phone Contact Title', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXTAREA
            ]
        );

        // phone number
        $this->add_control(
            'reptro_contact_call_to_action_phone_number',
            [   
                'label'         => esc_html__( 'Phone Number', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::TEXT
            ]
        );

        $this->end_controls_section();

	}

	protected function render() {
        $settings      = $this->get_settings();
        $email_title   =  $settings['reptro_contact_call_to_action_email_title'];
        $email_address =  $settings['reptro_contact_call_to_action_email_address'];
        $phone_title   =  $settings['reptro_contact_call_to_action_phone_title'];
        $phone_number  =  $settings['reptro_contact_call_to_action_phone_number'];

        echo do_shortcode( '[xt_contact_call_to_action email_contact_title="'.$email_title.'" email_contact_address="'.$email_address.'" phone_contact_title="'.$phone_title.'" phone_contact_number="'.$phone_number.'"]' );
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Contact_Call_To_Action() );