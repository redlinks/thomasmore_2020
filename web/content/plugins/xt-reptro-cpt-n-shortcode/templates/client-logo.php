<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Client_Logo extends Widget_Base {

	public function get_name() {
		return 'reptro_client_logo';
	}

	public function get_title() {
		return esc_html__( 'Client Logo', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-logo';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_service_box_content',
  			[
  				'label' => esc_html__( 'Client Logo', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        $this->add_control(
            'logo_images',
            [
                'label'                 => __('Client\'s Logo', 'xt-reptro-cpt-shortcode'),
                'type'                  => Controls_Manager::REPEATER,
                'fields'                => [
                    [
                        'name'          => 'logo_img',
                        'label'         => __( 'Logo Image', 'xt-reptro-cpt-shortcode' ),
                        'type'          => Controls_Manager::MEDIA
                    ],
                    [
                        'name'          => 'logo_url',
                        'label'         => esc_html__( 'Logo URL', 'xt-reptro-cpt-shortcode' ),
                        'type'          => Controls_Manager::TEXT,
                        'placeholder'   => __( 'http://yoursite.com', 'xt-reptro-cpt-shortcode' )    
                    ]
                ]          
            ]
        );

        // course category grid column
        $this->add_control(
            'logo_column',
            [
                'label'         => esc_html__( 'Columns', 'xt-reptro-cpt-shortcode' ),
                'type'          => Controls_Manager::SELECT,
                'default'       => 6,          
                'options'       => [
                    6           => esc_html__( '6 Columns', 'xt-reptro-cpt-shortcode' ),
                    4           => esc_html__( '4 Columns', 'xt-reptro-cpt-shortcode' ),
                    3           => esc_html__( '3 Columns', 'xt-reptro-cpt-shortcode' ),
                    2           => esc_html__( '2 Columns', 'xt-reptro-cpt-shortcode' ),
                    1           => esc_html__( '1 Columns', 'xt-reptro-cpt-shortcode' )
                ]              
            ]
        );

        $this->end_controls_section();

	}

    protected function render() {
        $settings    = $this->get_settings();
        $logo_images =  $settings['logo_images'];
        $column      =  $settings['logo_column'];

        echo reptro_client_logo_shortcode(array(
            'logo_images' => $logo_images,
            'column'      => $column,
        ));
    }

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Client_Logo() );