<?php

namespace Elementor;


if ( ! defined( 'ABSPATH' ) ) exit; // If this file is called directly, abort.

class Reptro_Widget_Fancy_List_Item extends Widget_Base {

	public function get_name() {
		return 'reptro_fancy_list_item';
	}

	public function get_title() {
		return esc_html__( 'Fancy List', 'xt-reptro-cpt-shortcode' );
	}

	public function get_icon() {
		return 'eicon-checkbox';
	}

	public function get_categories() {
		return [ 'reptro_widgets' ];
	}

	protected function _register_controls() {

  		$this->start_controls_section(
  			'reptro_fancy_list_content',
  			[
  				'label' => esc_html__( 'Fancy List', 'xt-reptro-cpt-shortcode' )
  			]
  		);

        // fancy list item options
        $this->add_control(
            'reptro_fancy_lists',
            [
                'label'         => __('Fancy List Items', 'xt-reptro-cpt-shortcode'),
                'type'          => Controls_Manager::REPEATER,
                'default'       => [
                    [ 'title'           => __( 'Travel and Aviation Consulting.', 'xt-reptro-cpt-shortcode' ) ],
                    [ 'title'           => __( 'Business Services Consulting.', 'xt-reptro-cpt-shortcode' ) ],
                    [ 'title'           => __( 'Consumer Products Consulting.', 'xt-reptro-cpt-shortcode' ) ],
                    [ 'title'           => __( 'Financial Services Consulting.', 'xt-reptro-cpt-shortcode' ) ],
                    [ 'title'           => __( 'Energy and Environment Consulting.', 'xt-reptro-cpt-shortcode' ) ]
                ],
                'fields' => [
                    [
                        'name'        => 'title',
                        'label'       => __( 'Title', 'xt-reptro-cpt-shortcode' ),
                        'type'        => Controls_Manager::TEXT,
                    ]
                ],
                'title_field' => '{{{ title }}}'              
            ]
        );

        $this->end_controls_section();

	}

	protected function render() {
        $settings           = $this->get_settings();
        $reptro_fancy_lists =  $settings['reptro_fancy_lists'];


        echo reptro_fancy_list_shortcode(array(
            'reptro_fancy_lists' => $reptro_fancy_lists,
        ));
	}

	protected function content_template() {

	}

}

Plugin::instance()->widgets_manager->register_widget_type( new Reptro_Widget_Fancy_List_Item() );