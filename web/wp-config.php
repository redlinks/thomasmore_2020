<?php
define( 'WP_CACHE',true); 

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Location of your WordPress configuration. */
 require_once(ABSPATH . '../../config.php');


define('WP_DEBUG', false);

define('WPLANG', 'es_PE');

if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/wordpress');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['HTTP_HOST'] . '');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/content');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content'); 
} 

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD','direct');